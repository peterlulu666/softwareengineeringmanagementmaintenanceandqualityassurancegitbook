<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture5_files/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:141.06px;top:8.56px" class="cls_002"><span class="cls_002">People Management,</span></div>
<div style="position:absolute;left:147.19px;top:61.60px" class="cls_002"><span class="cls_002">People Organization</span></div>
<div style="position:absolute;left:63.83px;top:366.04px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:63.83px;top:408.04px" class="cls_004"><span class="cls_004">§</span><span class="cls_003">People</span><span class="cls_005"> are key in software development</span></div>
<div style="position:absolute;left:63.83px;top:450.04px" class="cls_004"><span class="cls_004">§</span><span class="cls_005">Different ways to </span><span class="cls_003">organize</span><span class="cls_005"> SD projects</span></div>
<div style="position:absolute;left:7.20px;top:504.24px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:521.28px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:192.95px;top:39.20px" class="cls_008"><span class="cls_008">Different ways to organize people</span></div>
<div style="position:absolute;left:5.67px;top:504.48px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">2</span></div>
<div style="position:absolute;left:5.67px;top:519.36px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">People management</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> People have different goals</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> People and productivity</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Group processes</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">Coordination of work</span></div>
<div style="position:absolute;left:61.20px;top:421.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Importance of informal communication</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Mintzberg’s coordination mechanisms</span></div>
<div style="position:absolute;left:58.20px;top:142.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Simple: direct supervision</span></div>
<div style="position:absolute;left:58.20px;top:177.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Machine bureaucracy: standardization of work</span></div>
<div style="position:absolute;left:85.20px;top:206.16px" class="cls_011"><span class="cls_011">processes</span></div>
<div style="position:absolute;left:58.20px;top:240.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Divisionalized form: standardization of work</span></div>
<div style="position:absolute;left:85.20px;top:269.28px" class="cls_011"><span class="cls_011">products</span></div>
<div style="position:absolute;left:58.20px;top:304.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Professional bureaucracy: standardization of</span></div>
<div style="position:absolute;left:85.20px;top:332.16px" class="cls_011"><span class="cls_011">worker skills</span></div>
<div style="position:absolute;left:58.20px;top:367.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Adhocracy: mutual adjustment</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">External and Internal forces</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Example context: a complex software</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_011"><span class="cls_011">development project in a new, not yet explored</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_011"><span class="cls_011">area, within a government agency</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> External force: the bureaucratic context is likely to</span></div>
<div style="position:absolute;left:88.20px;top:300.24px" class="cls_011"><span class="cls_011">want to push a bureaucratic type of organization,</span></div>
<div style="position:absolute;left:88.20px;top:329.28px" class="cls_011"><span class="cls_011">with bosses, and hierarchical decision procedures</span></div>
<div style="position:absolute;left:61.20px;top:363.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Internal force: the project really requires a more</span></div>
<div style="position:absolute;left:88.20px;top:392.16px" class="cls_011"><span class="cls_011">democratic, consensus-based type of</span></div>
<div style="position:absolute;left:88.20px;top:421.20px" class="cls_011"><span class="cls_011">organization</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Reddin’s management styles</span></div>
<div style="position:absolute;left:389.00px;top:159.60px" class="cls_012"><span class="cls_012">task directedness</span></div>
<div style="position:absolute;left:342.13px;top:189.60px" class="cls_012"><span class="cls_012">low</span></div>
<div style="position:absolute;left:538.88px;top:189.60px" class="cls_012"><span class="cls_012">high</span></div>
<div style="position:absolute;left:320.29px;top:257.68px" class="cls_013"><span class="cls_013">separation</span></div>
<div style="position:absolute;left:481.92px;top:257.68px" class="cls_013"><span class="cls_013">commitment</span></div>
<div style="position:absolute;left:208.50px;top:276.48px" class="cls_012"><span class="cls_012">low</span></div>
<div style="position:absolute;left:342.29px;top:286.48px" class="cls_013"><span class="cls_013">style</span></div>
<div style="position:absolute;left:514.92px;top:286.72px" class="cls_013"><span class="cls_013">style</span></div>
<div style="position:absolute;left:107.38px;top:303.60px" class="cls_012"><span class="cls_012">relation</span></div>
<div style="position:absolute;left:85.88px;top:324.48px" class="cls_012"><span class="cls_012">directedness</span></div>
<div style="position:absolute;left:490.42px;top:342.64px" class="cls_013"><span class="cls_013">integration</span></div>
<div style="position:absolute;left:340.17px;top:345.52px" class="cls_013"><span class="cls_013">relation</span></div>
<div style="position:absolute;left:210.31px;top:368.40px" class="cls_012"><span class="cls_012">high</span></div>
<div style="position:absolute;left:512.42px;top:371.68px" class="cls_013"><span class="cls_013">style</span></div>
<div style="position:absolute;left:351.17px;top:374.56px" class="cls_013"><span class="cls_013">style</span></div>
<div style="position:absolute;left:5.67px;top:506.64px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">6</span></div>
<div style="position:absolute;left:5.67px;top:521.52px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Focus</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> In both these schemes, we look from the manager</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_011"><span class="cls_011">to the team.</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> We may also take the opposite position, and</span></div>
<div style="position:absolute;left:88.20px;top:306.24px" class="cls_011"><span class="cls_011">consider the relation and task </span><span class="cls_014">maturity</span><span class="cls_011"> of</span></div>
<div style="position:absolute;left:88.20px;top:334.32px" class="cls_011"><span class="cls_011">individual team members.</span></div>
<div style="position:absolute;left:61.20px;top:404.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> The manager should align his dealings with team</span></div>
<div style="position:absolute;left:88.20px;top:432.24px" class="cls_011"><span class="cls_011">members with their maturity.</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Team Organization</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Hierarchical organization</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Matrix organization</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Chief programmer team</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> SWAT team</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Agile team/Extreme Programming (XP)</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Open Source Develoment</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Hierarchical team</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Matrix organization</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Chief programmer team</span></div>
<div style="position:absolute;left:5.67px;top:501.60px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:516.48px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Skilled worker with advanced tools (SWAT)</span></div>
<div style="position:absolute;left:5.67px;top:498.72px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:513.60px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Agile team</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Open Source Software Development</span></div>
<div style="position:absolute;left:513.17px;top:178.72px" class="cls_013"><span class="cls_013">core team</span></div>
<div style="position:absolute;left:45.42px;top:238.24px" class="cls_013"><span class="cls_013">active users</span></div>
<div style="position:absolute;left:496.17px;top:402.64px" class="cls_013"><span class="cls_013">co-developers</span></div>
<div style="position:absolute;left:73.67px;top:408.40px" class="cls_013"><span class="cls_013">passive users</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Some general rules</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Use fewer, and better, people</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Fit tasks to people</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Help people to get the most out of themselves</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Look for a well-balanced team</span></div>
<div style="position:absolute;left:61.20px;top:421.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> If someone doesn’t fit the team: remove him</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture5_files/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Summary</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Software is written by humans</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">Coordination issues/management styles</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">Common team organizations in software</span></div>
<div style="position:absolute;left:75.85px;top:311.28px" class="cls_011"><span class="cls_011">development:</span></div>
<div style="position:absolute;left:90.80px;top:345.28px" class="cls_015"><span class="cls_015">§</span><span class="cls_013"> Hierarchical team</span></div>
<div style="position:absolute;left:90.80px;top:374.32px" class="cls_015"><span class="cls_015">§</span><span class="cls_013"> Matrix organization</span></div>
<div style="position:absolute;left:90.80px;top:403.12px" class="cls_015"><span class="cls_015">§</span><span class="cls_013"> Agile team</span></div>
<div style="position:absolute;left:90.80px;top:431.20px" class="cls_015"><span class="cls_015">§</span><span class="cls_013"> Open source development</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">16</span></div>
</div>

</body>
</html>
