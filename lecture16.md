<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:"Calibri Italic",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_004{font-family:"Calibri Italic",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:"Calibri",serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:"Calibri",serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:"Calibri Italic",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_018{font-family:"Calibri Italic",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture16/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:168.75px;top:32.80px" class="cls_002"><span class="cls_002">User Interface Design</span></div>
<div style="position:absolute;left:103.20px;top:304.96px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:103.20px;top:346.96px" class="cls_003"><span class="cls_003">• What </span><span class="cls_004">is</span><span class="cls_003"> the user interface</span></div>
<div style="position:absolute;left:103.20px;top:389.92px" class="cls_003"><span class="cls_003">• How to design a user interface</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:71.79px;top:41.20px" class="cls_002"><span class="cls_002">Where is the user interface?</span></div>
<div style="position:absolute;left:50.95px;top:303.84px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Seeheim model: separate presentation and</span></div>
<div style="position:absolute;left:77.95px;top:339.84px" class="cls_007"><span class="cls_007">dialog from application</span></div>
<div style="position:absolute;left:50.95px;top:425.76px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> More recently: MVC - Model-View-Controller</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_005"><span class="cls_005">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:167.13px;top:27.52px" class="cls_002"><span class="cls_002">Seeheim model</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_005"><span class="cls_005">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:93.76px;top:37.12px" class="cls_002"><span class="cls_002">Model-View-Controller (MVC)</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_005"><span class="cls_005">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:119.33px;top:37.12px" class="cls_002"><span class="cls_002">What is the user interface?</span></div>
<div style="position:absolute;left:43.20px;top:123.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> User interface: </span><span class="cls_004">all</span><span class="cls_003"> aspects of a system that are</span></div>
<div style="position:absolute;left:70.20px;top:158.56px" class="cls_003"><span class="cls_003">relevant to the user</span></div>
<div style="position:absolute;left:43.20px;top:200.56px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Also called: </span><span class="cls_004">User Virtual Machine</span><span class="cls_003"> (UVM)</span></div>
<div style="position:absolute;left:43.20px;top:284.56px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> A system can have more than one UVM, one</span></div>
<div style="position:absolute;left:70.20px;top:319.60px" class="cls_003"><span class="cls_003">for each set of tasks or roles</span></div>
<div style="position:absolute;left:43.20px;top:361.60px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> An individual may also have more than one</span></div>
<div style="position:absolute;left:70.20px;top:396.64px" class="cls_003"><span class="cls_003">user interface to the same application, e.g. on</span></div>
<div style="position:absolute;left:70.20px;top:430.48px" class="cls_003"><span class="cls_003">a mobile phone and a laptop</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_005"><span class="cls_005">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:85.37px;top:14.24px" class="cls_009"><span class="cls_009">Two ways to look at a user</span></div>
<div style="position:absolute;left:226.67px;top:62.24px" class="cls_009"><span class="cls_009">interface</span></div>
<div style="position:absolute;left:65.33px;top:231.28px" class="cls_008"><span class="cls_008">•</span><span class="cls_004"> Design aspect</span><span class="cls_003">: how to design everything</span></div>
<div style="position:absolute;left:92.33px;top:270.40px" class="cls_003"><span class="cls_003">relevant to the user?</span></div>
<div style="position:absolute;left:65.33px;top:362.32px" class="cls_008"><span class="cls_008">•</span><span class="cls_004"> Human aspect</span><span class="cls_003">: what does the user need to</span></div>
<div style="position:absolute;left:92.33px;top:400.24px" class="cls_003"><span class="cls_003">understand?</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_005"><span class="cls_005">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:229.21px;top:37.12px" class="cls_002"><span class="cls_002">Human factors</span></div>
<div style="position:absolute;left:43.20px;top:124.68px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> Humanities</span></div>
<div style="position:absolute;left:79.20px;top:160.56px" class="cls_012"><span class="cls_012">-</span><span class="cls_013"> Psychology: how does one perceive, learn, remember, …</span></div>
<div style="position:absolute;left:79.20px;top:191.52px" class="cls_012"><span class="cls_012">-</span><span class="cls_013"> Organization and culture: how do people work together, …</span></div>
<div style="position:absolute;left:43.20px;top:223.56px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> Artistic design</span></div>
<div style="position:absolute;left:79.20px;top:259.68px" class="cls_012"><span class="cls_012">-</span><span class="cls_013"> Graphical arts: how doe shapes, color, etc affect the viewer</span></div>
<div style="position:absolute;left:79.20px;top:290.64px" class="cls_012"><span class="cls_012">-</span><span class="cls_013"> Cinematography: which movements induce certain</span></div>
<div style="position:absolute;left:101.70px;top:316.56px" class="cls_013"><span class="cls_013">reactions</span></div>
<div style="position:absolute;left:79.20px;top:348.48px" class="cls_012"><span class="cls_012">-</span><span class="cls_013"> Getting attractive solutions</span></div>
<div style="position:absolute;left:43.20px;top:380.52px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> Ergonomics</span></div>
<div style="position:absolute;left:79.20px;top:415.68px" class="cls_012"><span class="cls_012">-</span><span class="cls_013"> Relation between human characteristics and artifacts</span></div>
<div style="position:absolute;left:79.20px;top:447.60px" class="cls_012"><span class="cls_012">-</span><span class="cls_013"> Especially cognitive ergonomics</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_005"><span class="cls_005">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:235.75px;top:37.12px" class="cls_002"><span class="cls_002">Models in HCI</span></div>
<div style="position:absolute;left:56.70px;top:209.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Internal models (‘models for execution’)</span></div>
<div style="position:absolute;left:92.70px;top:248.56px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> Mental model (model of a system held by a user)</span></div>
<div style="position:absolute;left:92.70px;top:282.64px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> User model (model of  user held by a system)</span></div>
<div style="position:absolute;left:56.70px;top:357.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> External models (‘for communication’)</span></div>
<div style="position:absolute;left:92.70px;top:396.64px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> Model of human information processing</span></div>
<div style="position:absolute;left:92.70px;top:430.48px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> Conceptual models (such as Task Action Grammar)</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_005"><span class="cls_005">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:124.16px;top:15.68px" class="cls_009"><span class="cls_009">Model of human information</span></div>
<div style="position:absolute;left:273.76px;top:63.68px" class="cls_009"><span class="cls_009">processing</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_005"><span class="cls_005">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:165.03px;top:37.12px" class="cls_002"><span class="cls_002">Use of mental models</span></div>
<div style="position:absolute;left:43.20px;top:118.56px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Planning the use of technology</span></div>
<div style="position:absolute;left:79.20px;top:151.60px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> First search by author name</span></div>
<div style="position:absolute;left:43.20px;top:211.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Finetuning user actions while executing a task</span></div>
<div style="position:absolute;left:79.20px;top:244.48px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> Refine search in case of too many hits</span></div>
<div style="position:absolute;left:43.20px;top:304.56px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Evaluate results</span></div>
<div style="position:absolute;left:79.20px;top:337.60px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> Keep the titles on software engineering</span></div>
<div style="position:absolute;left:43.20px;top:397.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Cope with events while using the system</span></div>
<div style="position:absolute;left:79.20px;top:429.52px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> Accept slow response time in the morning</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_005"><span class="cls_005">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:95.22px;top:15.68px" class="cls_009"><span class="cls_009">Characteristics of mental models</span></div>
<div style="position:absolute;left:281.31px;top:63.68px" class="cls_009"><span class="cls_009">(Norman)</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> They are incomplete</span></div>
<div style="position:absolute;left:43.20px;top:219.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> They can only partly be ‘run’</span></div>
<div style="position:absolute;left:43.20px;top:265.60px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> They are unstable</span></div>
<div style="position:absolute;left:43.20px;top:311.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> They have vague boundaries</span></div>
<div style="position:absolute;left:43.20px;top:357.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> They are parsimonious</span></div>
<div style="position:absolute;left:43.20px;top:403.60px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> They have characteristics of superstition</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:664.34px;top:507.60px" class="cls_005"><span class="cls_005">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:197.04px;top:37.12px" class="cls_002"><span class="cls_002">Conceptual model</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> All that is modeled as far as it is relevant to</span></div>
<div style="position:absolute;left:70.20px;top:165.52px" class="cls_003"><span class="cls_003">the user</span></div>
<div style="position:absolute;left:43.20px;top:258.64px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Formal models</span></div>
<div style="position:absolute;left:79.20px;top:303.68px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> Some model the user’s knowledge (competence</span></div>
<div style="position:absolute;left:101.70px;top:336.56px" class="cls_017"><span class="cls_017">model)</span></div>
<div style="position:absolute;left:79.20px;top:377.60px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> Others focus on the interaction process</span></div>
<div style="position:absolute;left:79.20px;top:417.68px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> Others do both</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_005"><span class="cls_005">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:62.97px;top:37.12px" class="cls_002"><span class="cls_002">Viewpoints of conceptual models</span></div>
<div style="position:absolute;left:43.20px;top:127.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Psychological view: definition of all the user</span></div>
<div style="position:absolute;left:70.20px;top:163.68px" class="cls_007"><span class="cls_007">should know and understand about the system</span></div>
<div style="position:absolute;left:43.20px;top:249.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Linguistic view: definition of the dialog between</span></div>
<div style="position:absolute;left:70.20px;top:285.60px" class="cls_007"><span class="cls_007">the user and the system</span></div>
<div style="position:absolute;left:43.20px;top:372.48px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Design view: all that needs to be decided upon</span></div>
<div style="position:absolute;left:70.20px;top:408.48px" class="cls_007"><span class="cls_007">from the point of view of user interface design</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_005"><span class="cls_005">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:113.43px;top:37.12px" class="cls_018"><span class="cls_018">Design</span><span class="cls_002"> of the user interface</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_005"><span class="cls_005">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:88.66px;top:37.12px" class="cls_002"><span class="cls_002">Dimensions of task knowledge</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_005"><span class="cls_005">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:64.68px;top:37.12px" class="cls_002"><span class="cls_002">Gathering task knowledge (cnt’d)</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Cell A (individual, explicit): interviews,</span></div>
<div style="position:absolute;left:70.20px;top:211.60px" class="cls_003"><span class="cls_003">questionnaires, etc</span></div>
<div style="position:absolute;left:43.20px;top:258.64px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Cell B (individual, implicit): observations,</span></div>
<div style="position:absolute;left:70.20px;top:296.56px" class="cls_003"><span class="cls_003">interpretation of mental representations</span></div>
<div style="position:absolute;left:43.20px;top:342.64px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Cell C (group, explicit): study artifacts:</span></div>
<div style="position:absolute;left:70.20px;top:380.56px" class="cls_003"><span class="cls_003">documents, archives, etc</span></div>
<div style="position:absolute;left:43.20px;top:426.64px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Cell D (group, implicit): ethnography</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_005"><span class="cls_005">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.02px;top:37.12px" class="cls_002"><span class="cls_002">Guidelines for user interface design</span></div>
<div style="position:absolute;left:43.20px;top:123.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Use a simple and natural dialog</span></div>
<div style="position:absolute;left:43.20px;top:165.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Speak the user’s language</span></div>
<div style="position:absolute;left:43.20px;top:208.48px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Minimize memory load</span></div>
<div style="position:absolute;left:43.20px;top:250.48px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Be consistent</span></div>
<div style="position:absolute;left:43.20px;top:292.48px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Provide feedback</span></div>
<div style="position:absolute;left:43.20px;top:334.48px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Provide clearly marked exits</span></div>
<div style="position:absolute;left:43.20px;top:377.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Provide shortcut</span></div>
<div style="position:absolute;left:43.20px;top:419.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Give good error messages</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_005"><span class="cls_005">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture16/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:274.89px;top:37.12px" class="cls_002"><span class="cls_002">Summary</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Central issue: tune user’s mental model</span></div>
<div style="position:absolute;left:70.20px;top:165.52px" class="cls_003"><span class="cls_003">(model in memory) with the conceptual</span></div>
<div style="position:absolute;left:70.20px;top:204.64px" class="cls_003"><span class="cls_003">model (model created by designers)</span></div>
<div style="position:absolute;left:43.20px;top:296.56px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> User interface design requires input from</span></div>
<div style="position:absolute;left:70.20px;top:334.48px" class="cls_003"><span class="cls_003">different disciplines: cognitive psychology,</span></div>
<div style="position:absolute;left:70.20px;top:373.60px" class="cls_003"><span class="cls_003">ethnography, arts, …</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_005"><span class="cls_005">18</span></div>
</div>

</body>
</html>
