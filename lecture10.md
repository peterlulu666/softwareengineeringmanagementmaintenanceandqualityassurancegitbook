<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:"Calibri",serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:"Calibri",serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:"Calibri",serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:"Calibri",serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:"Calibri Italic",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_020{font-family:"Calibri Italic",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_021{font-family:"Calibri Italic",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_021{font-family:"Calibri Italic",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_022{font-family:"Calibri Italic",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_022{font-family:"Calibri Italic",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:"Calibri",serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:"Calibri",serif;font-size:25.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:"Calibri",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:"Calibri",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:"Calibri Italic",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_027{font-family:"Calibri Italic",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:"Calibri",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:"Calibri",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:"Calibri Italic",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_031{font-family:"Calibri Italic",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture10/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:275.00px;top:32.80px" class="cls_002"><span class="cls_002">Modeling</span></div>
<div style="position:absolute;left:103.20px;top:304.96px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:103.20px;top:346.96px" class="cls_003"><span class="cls_003">• What do we want to build</span></div>
<div style="position:absolute;left:103.20px;top:389.92px" class="cls_003"><span class="cls_003">• How do we write this down</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:100.60px;top:15.52px" class="cls_002"><span class="cls_002">System Modeling Techniques</span></div>
<div style="position:absolute;left:109.32px;top:192.24px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Classic modeling techniques:</span></div>
<div style="position:absolute;left:145.32px;top:237.28px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Entity-relationship modeling</span></div>
<div style="position:absolute;left:145.32px;top:278.32px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Finite state machines</span></div>
<div style="position:absolute;left:145.32px;top:319.12px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Data flow diagrams</span></div>
<div style="position:absolute;left:145.32px;top:359.20px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> CRC cards</span></div>
<div style="position:absolute;left:109.32px;top:401.28px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Object-oriented modeling: variety of UML</span></div>
<div style="position:absolute;left:136.32px;top:440.16px" class="cls_006"><span class="cls_006">diagrams</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_009"><span class="cls_009">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:101.53px;top:37.12px" class="cls_002"><span class="cls_002">Entity-Relationship Modeling</span></div>
<div style="position:absolute;left:43.20px;top:124.68px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> entity: distinguishable object of some type</span></div>
<div style="position:absolute;left:43.20px;top:195.48px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> entity type: type of a set of entities</span></div>
<div style="position:absolute;left:43.20px;top:267.48px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> attribute value: piece of information (partially)</span></div>
<div style="position:absolute;left:70.20px;top:296.52px" class="cls_011"><span class="cls_011">describing an entity</span></div>
<div style="position:absolute;left:43.20px;top:367.56px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> attribute: type of a set of attribute values</span></div>
<div style="position:absolute;left:43.20px;top:438.60px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> relationship: association between two or more entities</span></div>
<div style="position:absolute;left:7.20px;top:496.56px" class="cls_012"><span class="cls_012">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_009"><span class="cls_009">3</span></div>
<div style="position:absolute;left:7.20px;top:514.56px" class="cls_012"><span class="cls_012"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:177.10px;top:37.12px" class="cls_002"><span class="cls_002">Example ER-diagram</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_009"><span class="cls_009">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:171.01px;top:37.12px" class="cls_002"><span class="cls_002">Finite state machines</span></div>
<div style="position:absolute;left:43.20px;top:121.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Models a system in terms of (a finite number of)</span></div>
<div style="position:absolute;left:70.20px;top:149.52px" class="cls_006"><span class="cls_006">states, and transitions between those states</span></div>
<div style="position:absolute;left:43.20px;top:221.52px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Often depicted as state transition diagrams:</span></div>
<div style="position:absolute;left:79.20px;top:258.64px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Each state is a bubble</span></div>
<div style="position:absolute;left:79.20px;top:289.60px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Each transition is a labeled arc from one state to</span></div>
<div style="position:absolute;left:101.70px;top:314.56px" class="cls_008"><span class="cls_008">another</span></div>
<div style="position:absolute;left:43.20px;top:381.60px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Large system  </span><span class="cls_013">Þ</span><span class="cls_006"> large diagram hierarchical</span></div>
<div style="position:absolute;left:70.20px;top:410.64px" class="cls_006"><span class="cls_006">diagrams: statecharts</span><span class="cls_013">Þ</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_009"><span class="cls_009">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:66.57px;top:37.12px" class="cls_002"><span class="cls_002">Example state transition diagram</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_009"><span class="cls_009">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:187.82px;top:37.12px" class="cls_002"><span class="cls_002">Data flow diagrams</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> external entities</span></div>
<div style="position:absolute;left:43.20px;top:219.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> processes</span></div>
<div style="position:absolute;left:43.20px;top:311.68px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> data flows</span></div>
<div style="position:absolute;left:43.20px;top:403.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> data stores</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_009"><span class="cls_009">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:117.90px;top:37.12px" class="cls_002"><span class="cls_002">Example data flow diagram</span></div>
<div style="position:absolute;left:162.00px;top:143.28px" class="cls_015"><span class="cls_015">client</span></div>
<div style="position:absolute;left:478.94px;top:143.28px" class="cls_015"><span class="cls_015">management</span></div>
<div style="position:absolute;left:195.50px;top:173.12px" class="cls_016"><span class="cls_016">request</span></div>
<div style="position:absolute;left:450.75px;top:185.12px" class="cls_016"><span class="cls_016">report</span></div>
<div style="position:absolute;left:560.75px;top:185.12px" class="cls_016"><span class="cls_016">direction</span></div>
<div style="position:absolute;left:304.50px;top:215.12px" class="cls_016"><span class="cls_016">log data</span></div>
<div style="position:absolute;left:158.21px;top:223.44px" class="cls_015"><span class="cls_015">prelim.</span></div>
<div style="position:absolute;left:504.35px;top:223.44px" class="cls_015"><span class="cls_015">prelim.</span></div>
<div style="position:absolute;left:170.71px;top:244.32px" class="cls_015"><span class="cls_015">doc</span></div>
<div style="position:absolute;left:516.85px;top:244.32px" class="cls_015"><span class="cls_015">doc</span></div>
<div style="position:absolute;left:245.88px;top:269.12px" class="cls_016"><span class="cls_016">return</span></div>
<div style="position:absolute;left:124.38px;top:279.68px" class="cls_016"><span class="cls_016">borrow</span></div>
<div style="position:absolute;left:258.13px;top:288.32px" class="cls_016"><span class="cls_016">request</span></div>
<div style="position:absolute;left:122.13px;top:298.64px" class="cls_016"><span class="cls_016">request</span></div>
<div style="position:absolute;left:533.25px;top:299.12px" class="cls_016"><span class="cls_016">log data</span></div>
<div style="position:absolute;left:500.00px;top:335.28px" class="cls_015"><span class="cls_015">log file</span></div>
<div style="position:absolute;left:157.76px;top:343.44px" class="cls_015"><span class="cls_015">borrow</span></div>
<div style="position:absolute;left:293.96px;top:343.44px" class="cls_015"><span class="cls_015">prelim.</span></div>
<div style="position:absolute;left:171.26px;top:364.32px" class="cls_015"><span class="cls_015">title</span></div>
<div style="position:absolute;left:306.46px;top:364.32px" class="cls_015"><span class="cls_015">doc</span></div>
<div style="position:absolute;left:246.50px;top:395.12px" class="cls_016"><span class="cls_016">title</span></div>
<div style="position:absolute;left:157.88px;top:401.12px" class="cls_016"><span class="cls_016">title</span></div>
<div style="position:absolute;left:171.50px;top:431.28px" class="cls_015"><span class="cls_015">catalog adm.</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_009"><span class="cls_009">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:152.39px;top:15.68px" class="cls_017"><span class="cls_017">CRC: Class, Responsibility,</span></div>
<div style="position:absolute;left:251.51px;top:63.68px" class="cls_017"><span class="cls_017">Collaborators</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_009"><span class="cls_009">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:87.53px;top:37.12px" class="cls_002"><span class="cls_002">Intermezzo: what is an object?</span></div>
<div style="position:absolute;left:43.20px;top:118.56px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Modeling viewpoint: model of part of the world</span></div>
<div style="position:absolute;left:79.20px;top:151.60px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Identity+state+behavior</span></div>
<div style="position:absolute;left:43.20px;top:211.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Philosophical viewpoint: existential absractions</span></div>
<div style="position:absolute;left:79.20px;top:244.48px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Everything is an object</span></div>
<div style="position:absolute;left:43.20px;top:304.56px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Software engineering viewpoint: data abstraction</span></div>
<div style="position:absolute;left:43.20px;top:368.64px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Implementation viewpoint: structure in memory</span></div>
<div style="position:absolute;left:43.20px;top:433.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Formal viewpoint: state machine</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:161.32px;top:37.12px" class="cls_002"><span class="cls_002">Objects and attributes</span></div>
<div style="position:absolute;left:43.20px;top:124.68px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> Object is characterized by a set of attributes</span></div>
<div style="position:absolute;left:79.20px;top:160.56px" class="cls_018"><span class="cls_018">-</span><span class="cls_019"> A table </span><span class="cls_020">has</span><span class="cls_019"> a top, legs, …</span></div>
<div style="position:absolute;left:43.20px;top:191.64px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> In ERM, attributes denote </span><span class="cls_021">intrinsic</span><span class="cls_011"> properties; they do</span></div>
<div style="position:absolute;left:70.20px;top:221.64px" class="cls_011"><span class="cls_011">not depend on each other, they are descriptive</span></div>
<div style="position:absolute;left:43.20px;top:256.68px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> In ERM, relationships denote </span><span class="cls_021">mutual</span><span class="cls_011"> properties, such</span></div>
<div style="position:absolute;left:70.20px;top:285.48px" class="cls_011"><span class="cls_011">as the membership of a person of some organization</span></div>
<div style="position:absolute;left:43.20px;top:321.48px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> In UML, these relationships are called </span><span class="cls_021">associations</span></div>
<div style="position:absolute;left:43.20px;top:357.48px" class="cls_010"><span class="cls_010">•</span><span class="cls_011"> Formally, UML does not distinguish attributes and</span></div>
<div style="position:absolute;left:70.20px;top:386.52px" class="cls_011"><span class="cls_011">relationships; both are properties of a class</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:664.34px;top:507.60px" class="cls_009"><span class="cls_009">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:110.20px;top:37.12px" class="cls_002"><span class="cls_002">Objects, state, and behavior</span></div>
<div style="position:absolute;left:43.20px;top:121.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_022"> State</span><span class="cls_006"> = set of attributes of an object</span></div>
<div style="position:absolute;left:43.20px;top:193.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_022"> Class</span><span class="cls_006"> = set of objects with the same attributes</span></div>
<div style="position:absolute;left:43.20px;top:265.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Individual object: </span><span class="cls_022">instance</span></div>
<div style="position:absolute;left:43.20px;top:337.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Behavior is described by </span><span class="cls_022">services</span><span class="cls_006">, a set of</span></div>
<div style="position:absolute;left:70.20px;top:365.52px" class="cls_022"><span class="cls_022">responsibilities</span></div>
<div style="position:absolute;left:43.20px;top:437.52px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Service is invoked by </span><span class="cls_022">sending a message</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:124.40px;top:37.12px" class="cls_002"><span class="cls_002">Relations between objects</span></div>
<div style="position:absolute;left:43.20px;top:122.60px" class="cls_023"><span class="cls_023">•</span><span class="cls_024"> Specialization-generalization, is-a</span></div>
<div style="position:absolute;left:79.20px;top:152.48px" class="cls_025"><span class="cls_025">-</span><span class="cls_026"> A dog </span><span class="cls_027">is an</span><span class="cls_026"> animal</span></div>
<div style="position:absolute;left:79.20px;top:179.60px" class="cls_025"><span class="cls_025">-</span><span class="cls_026"> Expressed in hierarchy</span></div>
<div style="position:absolute;left:43.20px;top:205.64px" class="cls_023"><span class="cls_023">•</span><span class="cls_024"> Whole-part, has</span></div>
<div style="position:absolute;left:79.20px;top:235.52px" class="cls_025"><span class="cls_025">-</span><span class="cls_026"> A dog </span><span class="cls_027">has</span><span class="cls_026"> legs</span></div>
<div style="position:absolute;left:79.20px;top:261.68px" class="cls_025"><span class="cls_025">-</span><span class="cls_026"> Aggregation of parts into a whole</span></div>
<div style="position:absolute;left:79.20px;top:288.56px" class="cls_025"><span class="cls_025">-</span><span class="cls_026"> Distinction between ‘real-world’ part-of and ‘representational’</span></div>
<div style="position:absolute;left:101.70px;top:309.68px" class="cls_026"><span class="cls_026">part of (e.g. ‘Publisher’ as part of ‘Publication’)</span></div>
<div style="position:absolute;left:43.20px;top:335.48px" class="cls_023"><span class="cls_023">•</span><span class="cls_024"> Member-of, has</span></div>
<div style="position:absolute;left:79.20px;top:365.60px" class="cls_025"><span class="cls_025">-</span><span class="cls_026"> A soccer team </span><span class="cls_027">has</span><span class="cls_026"> players</span></div>
<div style="position:absolute;left:79.20px;top:392.48px" class="cls_025"><span class="cls_025">-</span><span class="cls_026"> Relation between a set and its members (usually not transitive)</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:53.93px;top:39.68px" class="cls_017"><span class="cls_017">Specialization-generalization relations</span></div>
<div style="position:absolute;left:43.20px;top:121.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Usually expressed in hierarchical structure</span></div>
<div style="position:absolute;left:79.20px;top:157.60px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> If a tree: single inheritance</span></div>
<div style="position:absolute;left:79.20px;top:188.56px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> F a DAG: multiple inheritance</span></div>
<div style="position:absolute;left:43.20px;top:255.60px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Common attributes are defined at a higher level</span></div>
<div style="position:absolute;left:70.20px;top:284.64px" class="cls_006"><span class="cls_006">in the object hierarchy, and </span><span class="cls_022">inherited</span><span class="cls_006"> by child</span></div>
<div style="position:absolute;left:70.20px;top:313.68px" class="cls_006"><span class="cls_006">nodes</span></div>
<div style="position:absolute;left:43.20px;top:385.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Alternative view: object hierarchy is a </span><span class="cls_022">type</span></div>
<div style="position:absolute;left:70.20px;top:413.52px" class="cls_022"><span class="cls_022">hierarchy</span><span class="cls_006">, with </span><span class="cls_022">types</span><span class="cls_006"> and </span><span class="cls_022">subtypes</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:54.43px;top:37.12px" class="cls_002"><span class="cls_002">Unified Modeling Language (UML)</span></div>
<div style="position:absolute;left:43.20px;top:128.56px" class="cls_028"><span class="cls_028">•</span><span class="cls_029">  Controlled by OMG consortium: Object Management Group</span></div>
<div style="position:absolute;left:43.20px;top:185.68px" class="cls_028"><span class="cls_028">•</span><span class="cls_029">  Latest version: UML 2</span></div>
<div style="position:absolute;left:43.20px;top:243.52px" class="cls_028"><span class="cls_028">•</span><span class="cls_029">  UML 2 has 13 diagram types</span></div>
<div style="position:absolute;left:79.20px;top:271.68px" class="cls_015"><span class="cls_015">-</span><span class="cls_030"> Static diagrams depict static structure</span></div>
<div style="position:absolute;left:79.20px;top:297.60px" class="cls_015"><span class="cls_015">-</span><span class="cls_030"> Dynamic diagrams show what happens during execution</span></div>
<div style="position:absolute;left:43.20px;top:352.48px" class="cls_028"><span class="cls_028">•</span><span class="cls_029">  Most often used diagrams:</span></div>
<div style="position:absolute;left:79.20px;top:381.60px" class="cls_015"><span class="cls_015">-</span><span class="cls_030"> class diagram: 75%</span></div>
<div style="position:absolute;left:79.20px;top:407.52px" class="cls_015"><span class="cls_015">-</span><span class="cls_030"> Use case diagram and communication diagram: 50%</span></div>
<div style="position:absolute;left:79.20px;top:433.68px" class="cls_015"><span class="cls_015">-</span><span class="cls_030"> Often loose semantics</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:186.45px;top:37.12px" class="cls_002"><span class="cls_002">UML diagram types</span></div>
<div style="position:absolute;left:73.20px;top:128.48px" class="cls_027"><span class="cls_027">Static diagrams:</span></div>
<div style="position:absolute;left:398.20px;top:128.48px" class="cls_027"><span class="cls_027">Dynamic diagrams:</span></div>
<div style="position:absolute;left:43.20px;top:191.60px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Class</span></div>
<div style="position:absolute;left:373.20px;top:191.60px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Activity</span></div>
<div style="position:absolute;left:43.20px;top:223.52px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Component</span></div>
<div style="position:absolute;left:373.20px;top:223.52px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Communication</span></div>
<div style="position:absolute;left:43.20px;top:254.48px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Deployment</span></div>
<div style="position:absolute;left:373.20px;top:254.48px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Composite structure</span></div>
<div style="position:absolute;left:43.20px;top:286.64px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Interaction overview</span></div>
<div style="position:absolute;left:373.20px;top:286.64px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Sequence</span></div>
<div style="position:absolute;left:43.20px;top:318.56px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Object</span></div>
<div style="position:absolute;left:373.20px;top:318.56px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  State machine</span></div>
<div style="position:absolute;left:43.20px;top:349.52px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Package</span></div>
<div style="position:absolute;left:373.20px;top:349.52px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Timing</span></div>
<div style="position:absolute;left:373.20px;top:381.68px" class="cls_025"><span class="cls_025">•</span><span class="cls_026">  Use case</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:192.70px;top:37.12px" class="cls_002"><span class="cls_002">UML class diagram</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> depicts the static structure of a system</span></div>
<div style="position:absolute;left:43.20px;top:219.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> most common example: subclass/superclass</span></div>
<div style="position:absolute;left:70.20px;top:258.64px" class="cls_003"><span class="cls_003">hierarchy</span></div>
<div style="position:absolute;left:43.20px;top:350.56px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> also mutual properties between two or more</span></div>
<div style="position:absolute;left:70.20px;top:388.48px" class="cls_003"><span class="cls_003">entities (ER relationships, often called</span></div>
<div style="position:absolute;left:70.20px;top:426.64px" class="cls_003"><span class="cls_003">associations in OO)</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:145.83px;top:15.68px" class="cls_017"><span class="cls_017">Example class diagram (1):</span></div>
<div style="position:absolute;left:245.84px;top:63.68px" class="cls_017"><span class="cls_017">generalization</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:56.27px;top:39.68px" class="cls_017"><span class="cls_017">Example class diagram (2) association</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:145.83px;top:15.68px" class="cls_017"><span class="cls_017">Example class diagram (3):</span></div>
<div style="position:absolute;left:259.41px;top:63.68px" class="cls_017"><span class="cls_017">composition</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:57.78px;top:39.68px" class="cls_017"><span class="cls_017">Interface: class with abstract features</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:154.85px;top:37.12px" class="cls_002"><span class="cls_002">State machine diagram</span></div>
<div style="position:absolute;left:43.20px;top:123.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Resembles finite state machines, but:</span></div>
<div style="position:absolute;left:43.20px;top:250.48px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Usually allows for local variables of states</span></div>
<div style="position:absolute;left:43.20px;top:334.48px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Has external inputs and outputs</span></div>
<div style="position:absolute;left:43.20px;top:419.68px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Allows for hierarchical states</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:76.05px;top:37.12px" class="cls_002"><span class="cls_002">Example state machine diagram</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:96.95px;top:15.68px" class="cls_017"><span class="cls_017">Example state machine diagram:</span></div>
<div style="position:absolute;left:149.15px;top:63.68px" class="cls_017"><span class="cls_017">global and expanded view</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:184.34px;top:37.12px" class="cls_002"><span class="cls_002">Interaction diagram</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Two types: sequence diagram and</span></div>
<div style="position:absolute;left:70.20px;top:165.52px" class="cls_003"><span class="cls_003">communication diagram</span></div>
<div style="position:absolute;left:43.20px;top:258.64px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Sequence diagram: emphasizes the ordering</span></div>
<div style="position:absolute;left:70.20px;top:296.56px" class="cls_003"><span class="cls_003">of events, using a </span><span class="cls_031">lifeline</span></div>
<div style="position:absolute;left:43.20px;top:388.48px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Communication diagram emphasizes objects</span></div>
<div style="position:absolute;left:70.20px;top:426.64px" class="cls_003"><span class="cls_003">and their relationships</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:116.10px;top:37.12px" class="cls_002"><span class="cls_002">Example sequence diagram</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:62.35px;top:37.12px" class="cls_002"><span class="cls_002">Example communication diagram</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:176.72px;top:37.12px" class="cls_002"><span class="cls_002">Component diagram</span></div>
<div style="position:absolute;left:43.20px;top:127.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Class diagram with stereotype &lt;&lt;component>></span></div>
<div style="position:absolute;left:43.20px;top:213.60px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Way to identify larger entities</span></div>
<div style="position:absolute;left:43.20px;top:300.48px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> One way to depict a module view (see Software</span></div>
<div style="position:absolute;left:70.20px;top:336.48px" class="cls_006"><span class="cls_006">Architecture chapter)</span></div>
<div style="position:absolute;left:43.20px;top:422.64px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Components are connected by interfaces</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:98.80px;top:37.12px" class="cls_002"><span class="cls_002">Example component diagram</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">29</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:204.13px;top:37.12px" class="cls_002"><span class="cls_002">Use case diagram</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">30</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture10/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:274.89px;top:37.12px" class="cls_002"><span class="cls_002">Summary</span></div>
<div style="position:absolute;left:109.32px;top:137.28px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Classic notations:</span></div>
<div style="position:absolute;left:145.32px;top:173.20px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Entity-relationship diagrams</span></div>
<div style="position:absolute;left:145.32px;top:204.16px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Finite state machines</span></div>
<div style="position:absolute;left:145.32px;top:235.12px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> Data flow diagrams</span></div>
<div style="position:absolute;left:145.32px;top:266.32px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> CRC cards</span></div>
<div style="position:absolute;left:109.32px;top:333.12px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Unified Modeling Language (UML)</span></div>
<div style="position:absolute;left:145.32px;top:370.24px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> evolved from earlier OO notations</span></div>
<div style="position:absolute;left:145.32px;top:401.20px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> 13 diagram types</span></div>
<div style="position:absolute;left:145.32px;top:432.16px" class="cls_007"><span class="cls_007">-</span><span class="cls_008"> widely used</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_009"><span class="cls_009">31</span></div>
</div>

</body>
</html>
