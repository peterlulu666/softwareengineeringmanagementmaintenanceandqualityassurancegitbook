<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:22.0px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:22.0px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:23.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:23.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_024{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:27.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:27.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_023{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_023{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture1/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:133.75px;top:11.68px" class="cls_002"><span class="cls_002">Software Engineering</span></div>
<div style="position:absolute;left:195.00px;top:64.80px" class="cls_003"><span class="cls_003">Principles and Practice</span></div>
<div style="position:absolute;left:292.00px;top:100.64px" class="cls_004"><span class="cls_004">Third Edition</span></div>
<div style="position:absolute;left:469.06px;top:240.32px" class="cls_005"><span class="cls_005">Hans van Vliet</span></div>
<div style="position:absolute;left:460.90px;top:272.24px" class="cls_005"><span class="cls_005">Vrije Universiteit</span></div>
<div style="position:absolute;left:420.00px;top:367.28px" class="cls_006"><span class="cls_006">ISBN: 978-0-470-3146-9</span></div>
<div style="position:absolute;left:20.67px;top:486.48px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:20.67px;top:501.36px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:253.88px;top:179.68px" class="cls_002"><span class="cls_002">Chapter 1</span></div>
<div style="position:absolute;left:227.06px;top:232.72px" class="cls_002"><span class="cls_002">Introduction</span></div>
<div style="position:absolute;left:20.67px;top:486.48px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:20.67px;top:501.36px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">ARIANE Flight 501</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Disintegration after 39 sec</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Caused by large correction for attitude deviation</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Caused by wrong data being sent to On Board</span></div>
<div style="position:absolute;left:88.20px;top:311.28px" class="cls_010"><span class="cls_010">Computer</span></div>
<div style="position:absolute;left:61.20px;top:381.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Caused by software exception in Inertial</span></div>
<div style="position:absolute;left:88.20px;top:409.20px" class="cls_010"><span class="cls_010">Reference System after 36 sec.</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">3</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">The details</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Overflow in conversion of variable BH from 64-bit</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">floating point to 16-bit signed integer</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Of 7 risky conversions, 4 were protected; BH was</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_010"><span class="cls_010">not</span></div>
<div style="position:absolute;left:61.20px;top:340.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Reasoning: physically limited, or large margin of</span></div>
<div style="position:absolute;left:88.20px;top:369.12px" class="cls_010"><span class="cls_010">safety</span></div>
<div style="position:absolute;left:61.20px;top:438.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> In case of exception: report failure on databus and</span></div>
<div style="position:absolute;left:88.20px;top:467.28px" class="cls_010"><span class="cls_010">shut down</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">4</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Possible explanations</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Inadequate testing</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Wrong type of reuse</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Wrong design philosophy</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">5</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Inadequate testing?</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Specification does not contain Ariane 5 trajectory</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">data as a functional requirement</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> In tests, the SRI’s (components that measure</span></div>
<div style="position:absolute;left:88.20px;top:306.24px" class="cls_010"><span class="cls_010">altitude and movements of the launcher) were</span></div>
<div style="position:absolute;left:88.20px;top:334.32px" class="cls_010"><span class="cls_010">simulated by software modules</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">6</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Improper reuse?</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> If a component works perfectly well in one</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">environment, it doesn’t necessarily do so in</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_010"><span class="cls_010">another</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Ariane 5 is much faster than Ariane 4, and</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_010"><span class="cls_010">horizontal velocity builds up more rapidly</span></div>
<div style="position:absolute;left:88.20px;top:300.18px" class="cls_012"><span class="cls_012">Þ</span><span class="cls_010"> excessive values for parameter in question</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_013"><span class="cls_013">§</span><span class="cls_010"> Wish for quick alignment after hold in shutdown</span></div>
<div style="position:absolute;left:88.20px;top:369.06px" class="cls_012"><span class="cls_012">Þ</span><span class="cls_010"> this software runs for a while after lift-off. It</span></div>
<div style="position:absolute;left:88.20px;top:398.16px" class="cls_010"><span class="cls_010">doesn’t have any purpose for the Ariane 5, but</span></div>
<div style="position:absolute;left:88.20px;top:427.20px" class="cls_010"><span class="cls_010">was still kept</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">7</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Wrong design philosophy?</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> If something breaks down, such is caused by a</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">random hardware failure</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Action: shut down that part</span></div>
<div style="position:absolute;left:61.20px;top:346.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> There is no provision for design errors</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">8</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Further information:</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> IEEE Computer, jan. 1997, p. 129-130</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> <A HREF="http://www.cs.vu.nl/~hans/ariane5report.html">http://www.cs.vu.nl/~hans/ariane5report.html</A> </span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">9</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:22.40px" class="cls_008"><span class="cls_008">Software engineering</span></div>
<div style="position:absolute;left:61.20px;top:56.48px" class="cls_008"><span class="cls_008">The beginning</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span></div>
<div style="position:absolute;left:88.20px;top:179.28px" class="cls_010"><span class="cls_010">1968/69 NATO conferences: introduction of the</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">term Software Engineering</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Idea: software development is not an art, or a bag</span></div>
<div style="position:absolute;left:88.20px;top:306.24px" class="cls_010"><span class="cls_010">of tricks</span></div>
<div style="position:absolute;left:61.20px;top:375.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Build software like we build bridges</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">10</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Current status</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> a lot has been achieved</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> but …</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> some of our bridges still collapse</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">11</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Tacoma Narrows bridge</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">12</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Same bridge, a while later</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">13</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Further references</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Henry Petroski, Design Paradigms: Case Histories</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">of Error and Judgement in Engineering</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> A. Spector & D. Gifford, A Computer Science</span></div>
<div style="position:absolute;left:88.20px;top:306.24px" class="cls_010"><span class="cls_010">Perspective of Bridge Design, Comm. Of the ACM</span></div>
<div style="position:absolute;left:88.20px;top:334.32px" class="cls_014"><span class="cls_014">29</span><span class="cls_010">, 4 (1986) p 267-283</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">14</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:62.58px;top:46.40px" class="cls_008"><span class="cls_008">Relative distribution of software/hardware</span></div>
<div style="position:absolute;left:62.58px;top:80.48px" class="cls_008"><span class="cls_008">costs</span></div>
<div style="position:absolute;left:98.70px;top:131.12px" class="cls_015"><span class="cls_015">100</span></div>
<div style="position:absolute;left:166.70px;top:191.76px" class="cls_010"><span class="cls_010">Hardware</span></div>
<div style="position:absolute;left:400.45px;top:212.64px" class="cls_010"><span class="cls_010">Development</span></div>
<div style="position:absolute;left:106.57px;top:255.68px" class="cls_015"><span class="cls_015">60</span></div>
<div style="position:absolute;left:345.08px;top:332.64px" class="cls_010"><span class="cls_010">Software</span></div>
<div style="position:absolute;left:106.57px;top:386.24px" class="cls_015"><span class="cls_015">20</span></div>
<div style="position:absolute;left:410.33px;top:395.76px" class="cls_010"><span class="cls_010">Maintenance</span></div>
<div style="position:absolute;left:116.82px;top:458.96px" class="cls_015"><span class="cls_015">1955</span></div>
<div style="position:absolute;left:332.58px;top:458.96px" class="cls_015"><span class="cls_015">1970</span></div>
<div style="position:absolute;left:535.95px;top:458.96px" class="cls_015"><span class="cls_015">1985</span></div>
<div style="position:absolute;left:334.08px;top:474.56px" class="cls_015"><span class="cls_015">Year</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">15</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Point to ponder #1</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Why does software maintenance cost so much?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">16</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Definition (IEEE)</span></div>
<div style="position:absolute;left:88.20px;top:190.24px" class="cls_016"><span class="cls_016">Software Engineering is the application</span></div>
<div style="position:absolute;left:88.20px;top:229.12px" class="cls_016"><span class="cls_016">of a </span><span class="cls_024">systematic</span><span class="cls_016">, </span><span class="cls_024">disciplined</span><span class="cls_016">, </span><span class="cls_024">quantifiable</span></div>
<div style="position:absolute;left:88.20px;top:267.28px" class="cls_016"><span class="cls_016">approach to the development, </span><span class="cls_024">operation</span><span class="cls_016">,</span></div>
<div style="position:absolute;left:88.20px;top:305.20px" class="cls_016"><span class="cls_016">and </span><span class="cls_024">maintenance</span><span class="cls_016"> of software; that is, the</span></div>
<div style="position:absolute;left:88.20px;top:344.32px" class="cls_016"><span class="cls_016">application of engineering to software</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">17</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Central themes</span></div>
<div style="position:absolute;left:61.20px;top:182.32px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> SE is concerned with</span></div>
<div style="position:absolute;left:375.70px;top:182.32px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> you’re doing it</span></div>
<div style="position:absolute;left:88.20px;top:213.28px" class="cls_018"><span class="cls_018">BIG programs</span></div>
<div style="position:absolute;left:402.70px;top:213.28px" class="cls_018"><span class="cls_018">together</span></div>
<div style="position:absolute;left:61.20px;top:250.24px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> complexity is an</span></div>
<div style="position:absolute;left:375.70px;top:250.24px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> software must</span></div>
<div style="position:absolute;left:88.20px;top:282.16px" class="cls_018"><span class="cls_018">issue</span></div>
<div style="position:absolute;left:402.70px;top:282.16px" class="cls_018"><span class="cls_018">effectively support</span></div>
<div style="position:absolute;left:402.70px;top:313.12px" class="cls_018"><span class="cls_018">users</span></div>
<div style="position:absolute;left:61.20px;top:319.12px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> software evolves</span></div>
<div style="position:absolute;left:375.70px;top:350.32px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> involves different</span></div>
<div style="position:absolute;left:61.20px;top:356.32px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> development must</span></div>
<div style="position:absolute;left:402.70px;top:381.28px" class="cls_018"><span class="cls_018">disciplines</span></div>
<div style="position:absolute;left:88.20px;top:388.24px" class="cls_018"><span class="cls_018">be efficient</span></div>
<div style="position:absolute;left:375.70px;top:419.20px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> SE is a balancing act</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">18</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Building software ~ Building bridges?</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> yes, and no</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> software is logical, rather than physical</span></div>
<div style="position:absolute;left:61.20px;top:317.22px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> progress is hard to see (speed </span><span class="cls_012">¹</span><span class="cls_010"> progress)</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> software is not continuous</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">19</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Simple life cycle model</span></div>
<div style="position:absolute;left:317.33px;top:177.68px" class="cls_015"><span class="cls_015">problem</span></div>
<div style="position:absolute;left:367.20px;top:207.68px" class="cls_015"><span class="cls_015">requirements engineering</span></div>
<div style="position:absolute;left:284.08px;top:237.68px" class="cls_015"><span class="cls_015">reqs specification</span></div>
<div style="position:absolute;left:372.70px;top:267.68px" class="cls_015"><span class="cls_015">design</span></div>
<div style="position:absolute;left:322.95px;top:297.68px" class="cls_015"><span class="cls_015">design</span></div>
<div style="position:absolute;left:367.20px;top:327.68px" class="cls_015"><span class="cls_015">implementation</span></div>
<div style="position:absolute;left:317.33px;top:357.68px" class="cls_015"><span class="cls_015">system</span></div>
<div style="position:absolute;left:367.20px;top:387.68px" class="cls_015"><span class="cls_015">testing</span></div>
<div style="position:absolute;left:289.70px;top:417.68px" class="cls_015"><span class="cls_015">working system</span></div>
<div style="position:absolute;left:367.20px;top:447.68px" class="cls_015"><span class="cls_015">maintenance</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">20</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Point to ponder #2</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_010"><span class="cls_010">Is this a good model</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_013"><span class="cls_013">§</span><span class="cls_010"> of how we go about?</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_013"><span class="cls_013">§</span><span class="cls_010"> of how we should go about?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">21</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Requirements Engineering</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> yields a description of the desired system:</span></div>
<div style="position:absolute;left:97.20px;top:178.24px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> which functions</span></div>
<div style="position:absolute;left:97.20px;top:207.28px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> possible extensions</span></div>
<div style="position:absolute;left:97.20px;top:236.32px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> required documentation</span></div>
<div style="position:absolute;left:97.20px;top:264.16px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> performance requirements</span></div>
<div style="position:absolute;left:61.20px;top:329.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> includes a feasibility study</span></div>
<div style="position:absolute;left:61.20px;top:398.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> resulting document: </span><span class="cls_014">requirements specification</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">22</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Design</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> earliest design decisions captured in </span><span class="cls_021">software</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_021"><span class="cls_021">architecture</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> decomposition into parts/components; what are</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_010"><span class="cls_010">the functions of, and interfaces between, those</span></div>
<div style="position:absolute;left:88.20px;top:300.24px" class="cls_010"><span class="cls_010">components?</span></div>
<div style="position:absolute;left:61.20px;top:369.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> emphasis on</span><span class="cls_021"> what</span><span class="cls_010"> rather than </span><span class="cls_021">how</span></div>
<div style="position:absolute;left:61.20px;top:438.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> resulting document: </span><span class="cls_021">specification</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">23</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Implementation</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> focus on individual components</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> goal: a working, flexible, robust, … piece of</span></div>
<div style="position:absolute;left:88.20px;top:242.16px" class="cls_010"><span class="cls_010">software</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_021"> not a bag of tricks</span></div>
<div style="position:absolute;left:61.20px;top:381.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> present-day languages have a module and/or</span></div>
<div style="position:absolute;left:88.20px;top:409.20px" class="cls_010"><span class="cls_010">class concept</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">24</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Testing</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> does the software do what it is supposed to do?</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> are we building the right system? (</span><span class="cls_021">validation</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> are we building the system right? (</span><span class="cls_021">verification</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> start testing activities in phase 1, on day 1</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">25</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Maintenance</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> correcting errors found after the software has</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">been delivered</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> adapting the software to changing requirements,</span></div>
<div style="position:absolute;left:88.20px;top:306.24px" class="cls_010"><span class="cls_010">changing environments, ...</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">26</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Global distribution of effort</span></div>
<div style="position:absolute;left:145.70px;top:225.68px" class="cls_015"><span class="cls_015">design 15%</span></div>
<div style="position:absolute;left:572.08px;top:231.68px" class="cls_015"><span class="cls_015">coding 20%</span></div>
<div style="position:absolute;left:123.45px;top:267.68px" class="cls_015"><span class="cls_015">requirements</span></div>
<div style="position:absolute;left:123.45px;top:286.64px" class="cls_015"><span class="cls_015">engineering 10%</span></div>
<div style="position:absolute;left:106.95px;top:333.68px" class="cls_015"><span class="cls_015">specification 10%</span></div>
<div style="position:absolute;left:577.70px;top:381.68px" class="cls_015"><span class="cls_015">testing 45%</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">27</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Global distribution of effort</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> rule of thumb: 40-20-40 distribution of effort</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> trend: enlarge requirements specification/design</span></div>
<div style="position:absolute;left:88.20px;top:277.20px" class="cls_010"><span class="cls_010">slots; reduce test slot</span></div>
<div style="position:absolute;left:61.20px;top:346.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> beware: maintenance alone consumes 50-75% of</span></div>
<div style="position:absolute;left:88.20px;top:375.12px" class="cls_010"><span class="cls_010">total effort</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">28</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Kinds of maintenance activities</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_021"> corrective maintenance</span><span class="cls_010">: correcting errors</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_021"> adaptive maintenance</span><span class="cls_010">: adapting to changes in the</span></div>
<div style="position:absolute;left:88.20px;top:277.20px" class="cls_010"><span class="cls_010">environment (both hardware and software)</span></div>
<div style="position:absolute;left:61.20px;top:346.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_021"> perfective maintenance</span><span class="cls_010">: adapting to changing</span></div>
<div style="position:absolute;left:88.20px;top:375.12px" class="cls_010"><span class="cls_010">user requirements</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">29</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Distribution of maintenance activities</span></div>
<div style="position:absolute;left:572.08px;top:213.68px" class="cls_015"><span class="cls_015">corrective 21%</span></div>
<div style="position:absolute;left:73.70px;top:291.68px" class="cls_015"><span class="cls_015">perfective 50%</span></div>
<div style="position:absolute;left:572.08px;top:339.68px" class="cls_015"><span class="cls_015">adaptive 25%</span></div>
<div style="position:absolute;left:572.08px;top:387.68px" class="cls_015"><span class="cls_015">preventive 4%</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">30</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Point to ponder #3</span></div>
<div style="position:absolute;left:88.20px;top:179.28px" class="cls_010"><span class="cls_010">You are a tester, and the product you are testing</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">does not yet meet the testing requirements agreed</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_010"><span class="cls_010">upon in writing. Your manager wants to ship the</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_010"><span class="cls_010">product now, continue testing so that the next</span></div>
<div style="position:absolute;left:88.20px;top:294.24px" class="cls_010"><span class="cls_010">release will meet the testing requirements. What</span></div>
<div style="position:absolute;left:88.20px;top:323.28px" class="cls_010"><span class="cls_010">do you do?</span></div>
<div style="position:absolute;left:61.20px;top:357.12px" class="cls_013"><span class="cls_013">§</span><span class="cls_010"> Discuss the issue with your manager?</span></div>
<div style="position:absolute;left:61.20px;top:392.16px" class="cls_013"><span class="cls_013">§</span><span class="cls_010"> Talk to the manager’s boss?</span></div>
<div style="position:absolute;left:61.20px;top:427.20px" class="cls_013"><span class="cls_013">§</span><span class="cls_010"> Talk to the customer?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">31</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Hammurabi’s Code</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">32</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Hammurabi’s Code (translation)</span></div>
<div style="position:absolute;left:88.20px;top:179.28px" class="cls_010"><span class="cls_010">64: If a builder build a house for a man and do not</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">make its construction firm, and the house which</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_010"><span class="cls_010">he has built collapse and cause the death of the</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_010"><span class="cls_010">owner of the house, that builder shall be put to</span></div>
<div style="position:absolute;left:88.20px;top:294.24px" class="cls_010"><span class="cls_010">death.</span></div>
<div style="position:absolute;left:88.20px;top:329.28px" class="cls_010"><span class="cls_010">73: If it cause the death of a son of the owner of</span></div>
<div style="position:absolute;left:88.20px;top:357.12px" class="cls_010"><span class="cls_010">the house, they shall put to death a son of that</span></div>
<div style="position:absolute;left:88.20px;top:386.16px" class="cls_010"><span class="cls_010">builder.</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">33</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Software Engineering Ethics - Principles</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Act consistently with</span></div>
<div style="position:absolute;left:375.70px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Managers shall</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">the public interest</span></div>
<div style="position:absolute;left:402.70px;top:173.28px" class="cls_010"><span class="cls_010">promote an ethical</span></div>
<div style="position:absolute;left:402.70px;top:202.32px" class="cls_010"><span class="cls_010">approach</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Act in a manner that is</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_010"><span class="cls_010">in the best interest of</span></div>
<div style="position:absolute;left:375.70px;top:237.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Advance the integrity</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_010"><span class="cls_010">the client and</span></div>
<div style="position:absolute;left:402.70px;top:265.20px" class="cls_010"><span class="cls_010">and reputation of the</span></div>
<div style="position:absolute;left:88.20px;top:294.24px" class="cls_010"><span class="cls_010">employer</span></div>
<div style="position:absolute;left:402.70px;top:294.24px" class="cls_010"><span class="cls_010">profession</span></div>
<div style="position:absolute;left:61.20px;top:329.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Ensure that products</span></div>
<div style="position:absolute;left:375.70px;top:329.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Be fair to and</span></div>
<div style="position:absolute;left:88.20px;top:357.12px" class="cls_010"><span class="cls_010">meet the highest</span></div>
<div style="position:absolute;left:402.70px;top:357.12px" class="cls_010"><span class="cls_010">supportive of</span></div>
<div style="position:absolute;left:88.20px;top:386.16px" class="cls_010"><span class="cls_010">professional</span></div>
<div style="position:absolute;left:402.70px;top:386.16px" class="cls_010"><span class="cls_010">colleagues</span></div>
<div style="position:absolute;left:88.20px;top:415.20px" class="cls_010"><span class="cls_010">standards possible</span></div>
<div style="position:absolute;left:375.70px;top:421.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Participate in lifelong</span></div>
<div style="position:absolute;left:61.20px;top:450.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Maintain integrity in</span></div>
<div style="position:absolute;left:402.70px;top:450.24px" class="cls_010"><span class="cls_010">learning and promote</span></div>
<div style="position:absolute;left:88.20px;top:478.32px" class="cls_010"><span class="cls_010">professional judgment</span></div>
<div style="position:absolute;left:402.70px;top:478.32px" class="cls_010"><span class="cls_010">an ethical approach</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">34</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Quo Vadis?</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> It takes at least 15-20 years for a technology to</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">become mature; this also holds for computer</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_010"><span class="cls_010">science: UNIX, relational databases, structured</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_010"><span class="cls_010">programming, …</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Software engineering has made tremendous</span></div>
<div style="position:absolute;left:88.20px;top:363.12px" class="cls_010"><span class="cls_010">progression</span></div>
<div style="position:absolute;left:61.20px;top:432.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> There is no silver bullet, though</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">35</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Recent developments</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010">Rise of agile methods</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Shift from producing software to using software</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Success of Open Source Software</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Software development becomes more</span></div>
<div style="position:absolute;left:75.85px;top:381.12px" class="cls_010"><span class="cls_010">heterogeneous</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">36</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">The Agile Manifesto</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Individuals and interactions over processes and</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">tools</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Working software over comprehensive</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_010"><span class="cls_010">documentation</span></div>
<div style="position:absolute;left:61.20px;top:306.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Customer collaboration over contract negotiation</span></div>
<div style="position:absolute;left:61.20px;top:340.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Responding to change over following a plan</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">37</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.14px" class="cls_008"><span class="cls_008">Producing  software  </span><span class="cls_022">Þ</span><span class="cls_008"> Using software</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010">Builders build pieces, integrators integrate them</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010">Component-Based Development (CBSD)</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Software Product Lines (SPL)</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010">Commercial Off-The-Shelves (COTS)</span></div>
<div style="position:absolute;left:61.20px;top:421.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Service Orientation (SOA)</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">38</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background39.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Open Source: crowdsourcing</span></div>
<div style="position:absolute;left:61.20px;top:141.12px" class="cls_023"><span class="cls_023">1.</span><span class="cls_010">  Go to LEGO site</span></div>
<div style="position:absolute;left:61.20px;top:173.28px" class="cls_023"><span class="cls_023">2.</span><span class="cls_010">  Use CAD tool to design your favorite castle</span></div>
<div style="position:absolute;left:61.20px;top:205.20px" class="cls_023"><span class="cls_023">3.</span><span class="cls_010">  Generate bill of materials</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_023"><span class="cls_023">4.</span><span class="cls_010">  Pieces are collected, packaged, and sent to you</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_023"><span class="cls_023">5.</span><span class="cls_010">  Leave your model in LEGO’s gallery</span></div>
<div style="position:absolute;left:61.20px;top:332.16px" class="cls_023"><span class="cls_023">6.</span><span class="cls_010">  Most downloaded designs are prepackaged</span></div>
<div style="position:absolute;left:61.20px;top:395.28px" class="cls_009"><span class="cls_009">q</span><span class="cls_010"> No requirements engineers needed!</span></div>
<div style="position:absolute;left:61.20px;top:427.20px" class="cls_009"><span class="cls_009">q</span><span class="cls_010"> Gives rise to new business model</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">39</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:21450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background40.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Heterogeneity</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Old days: software development department had</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_010"><span class="cls_010">everything under control</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010">Nowadays:</span></div>
<div style="position:absolute;left:90.80px;top:276.16px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Teams scattered around the globe</span></div>
<div style="position:absolute;left:90.80px;top:305.20px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Components acquired from others</span></div>
<div style="position:absolute;left:90.80px;top:334.24px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Includes open source parts</span></div>
<div style="position:absolute;left:90.80px;top:362.32px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Services found on the Web</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">40</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background41.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Point to ponder #4</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Which of the following do you consider as</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">Software Engineering Principles?</span></div>
<div style="position:absolute;left:97.20px;top:207.28px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Build with and for reuse</span></div>
<div style="position:absolute;left:97.20px;top:236.32px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Define software artifacts rigorously</span></div>
<div style="position:absolute;left:97.20px;top:264.16px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Establish a software process that provides flexibility</span></div>
<div style="position:absolute;left:97.20px;top:293.20px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Manage quality as formally as possible</span></div>
<div style="position:absolute;left:97.20px;top:322.24px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Minimize software components interaction</span></div>
<div style="position:absolute;left:97.20px;top:351.28px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Produce software in a stepwise fashion</span></div>
<div style="position:absolute;left:97.20px;top:380.32px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Change is inherent, so plan for it and manage it</span></div>
<div style="position:absolute;left:97.20px;top:408.16px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Tradeoffs are inherent, so make them explicit and document</span></div>
<div style="position:absolute;left:119.70px;top:432.16px" class="cls_020"><span class="cls_020">them</span></div>
<div style="position:absolute;left:97.20px;top:461.20px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Uncertainty is unavoidable, so identify and manage it</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">41</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background42.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">SUMMARY</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> software engineering is a balancing act, where</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">trade-offs are difficult</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> solutions are not right or wrong; at most they are</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_010"><span class="cls_010">better or worse</span></div>
<div style="position:absolute;left:61.20px;top:340.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> most of maintenance is (</span><span class="cls_021">inevitable</span><span class="cls_010">) evolution</span></div>
<div style="position:absolute;left:61.20px;top:409.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> there are many life cycle models, and they are all</span></div>
<div style="position:absolute;left:88.20px;top:438.24px" class="cls_021"><span class="cls_021">models</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">42</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>

</body>
</html>

