<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture4/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:75.19px;top:34.96px" class="cls_002"><span class="cls_002">Configuration Management</span></div>
<div style="position:absolute;left:44.08px;top:366.04px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:44.08px;top:408.04px" class="cls_004"><span class="cls_004">§</span><span class="cls_003"> manage items during software life cycle</span></div>
<div style="position:absolute;left:44.08px;top:450.04px" class="cls_004"><span class="cls_004">§</span><span class="cls_003"> usually supported by powerful tools</span></div>
<div style="position:absolute;left:7.20px;top:500.64px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:517.68px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Configuration management tasks</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> identification and definition of</span><span class="cls_010"> configuration items,</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_009"><span class="cls_009">such as source code modules, test cases,</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_009"><span class="cls_009">requirements specification</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> managing changes and making configuration items</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_009"><span class="cls_009">available during the software life cycle, usually</span></div>
<div style="position:absolute;left:88.20px;top:294.24px" class="cls_009"><span class="cls_009">through a</span><span class="cls_010"> Configuration Control Board </span><span class="cls_009">(CCB)</span></div>
<div style="position:absolute;left:61.20px;top:329.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> keeping track of the</span><span class="cls_010"> status </span><span class="cls_009">of all items (including the</span></div>
<div style="position:absolute;left:88.20px;top:357.12px" class="cls_009"><span class="cls_009">change requests)</span></div>
<div style="position:absolute;left:61.20px;top:427.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> crucial for large projects</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Configuration Control Board</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> ensures that every change to the baseline (change</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">request - CR) is properly authorized and executed</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> CCB needs certain information for every CR, such</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_010"><span class="cls_010">as who submits it, how much it will cost, urgency,</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_010"><span class="cls_010">etc</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> CCB assesses the CR. If it is approved, it results</span></div>
<div style="position:absolute;left:88.20px;top:329.28px" class="cls_010"><span class="cls_010">in a work package which has to be scheduled.</span></div>
<div style="position:absolute;left:61.20px;top:398.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> so, configuration management is not only about</span></div>
<div style="position:absolute;left:88.20px;top:427.20px" class="cls_010"><span class="cls_010">keeping track of changes, but also about workflow</span></div>
<div style="position:absolute;left:88.20px;top:455.28px" class="cls_010"><span class="cls_010">management</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Workflow of a change request</span></div>
<div style="position:absolute;left:221.81px;top:171.60px" class="cls_012"><span class="cls_012">change request (CR)</span></div>
<div style="position:absolute;left:175.13px;top:219.60px" class="cls_012"><span class="cls_012">investigate</span></div>
<div style="position:absolute;left:458.50px;top:219.60px" class="cls_012"><span class="cls_012">notify owner</span></div>
<div style="position:absolute;left:365.00px;top:225.60px" class="cls_012"><span class="cls_012">reject</span></div>
<div style="position:absolute;left:225.69px;top:261.60px" class="cls_012"><span class="cls_012">approve</span></div>
<div style="position:absolute;left:162.88px;top:309.60px" class="cls_012"><span class="cls_012">schedule work</span></div>
<div style="position:absolute;left:460.44px;top:309.60px" class="cls_012"><span class="cls_012">request info</span></div>
<div style="position:absolute;left:365.13px;top:327.60px" class="cls_012"><span class="cls_012">defer</span></div>
<div style="position:absolute;left:145.06px;top:399.60px" class="cls_012"><span class="cls_012">implement change</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Tool support for configuration management</span></div>
<div style="position:absolute;left:61.08px;top:125.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> if an item has to be changed, one person gets a</span></div>
<div style="position:absolute;left:88.08px;top:154.32px" class="cls_010"><span class="cls_010">copy thereof, and meanwhile it is locked to all</span></div>
<div style="position:absolute;left:88.08px;top:183.12px" class="cls_010"><span class="cls_010">others</span></div>
<div style="position:absolute;left:61.08px;top:218.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> new items can only be added to the baseline after</span></div>
<div style="position:absolute;left:88.08px;top:246.24px" class="cls_010"><span class="cls_010">thorough testing</span></div>
<div style="position:absolute;left:61.08px;top:281.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> changes in the status of an item (e.g. code</span></div>
<div style="position:absolute;left:88.08px;top:310.32px" class="cls_010"><span class="cls_010">finished) trigger further activities (e.g. start unit</span></div>
<div style="position:absolute;left:88.08px;top:338.16px" class="cls_010"><span class="cls_010">testing)</span></div>
<div style="position:absolute;left:61.08px;top:373.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> old versions of a component are kept as well,</span></div>
<div style="position:absolute;left:88.08px;top:402.24px" class="cls_010"><span class="cls_010">resulting in versions, like X.1, X.2, …</span></div>
<div style="position:absolute;left:61.08px;top:436.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> we may even create different branches of</span></div>
<div style="position:absolute;left:88.08px;top:465.12px" class="cls_010"><span class="cls_010">revisions: X.2.1, X.2.2, … and X.3.1, X.3.2, ...</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Functionalities of SCM tools</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_010">Components (storing, retrieving, accessing, …)</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> Structure (representation of system structure)</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_010">Construction (build an executable)</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_010">Auditing (follow trails, e.g. of changes)</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_010">Accounting (gather statistics)</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_010">Controlling (trace defects, impact analysis)</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> Process (assign tasks)</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> Team (support for collaboration)</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Models of configurations</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> version-oriented: </span><span class="cls_009">physical change results in a new</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_009"><span class="cls_009">version, so versions are characterized by their</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_009"><span class="cls_009">difference, i.e.</span><span class="cls_010"> delta</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> change-oriented: </span><span class="cls_009">basic unit in configuration</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_009"><span class="cls_009">management is a logical change</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> identification of configuration becomes different:</span></div>
<div style="position:absolute;left:88.20px;top:363.12px" class="cls_010"><span class="cls_010">“baseline X plus fix table bug” instead of “X3.2.1 +</span></div>
<div style="position:absolute;left:88.20px;top:392.16px" class="cls_010"><span class="cls_010">Y2.7 + Z3.5 + …”</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Evolution of SCM tools</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> Early tools: emphasis on product-oriented tasks</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_010">Nowadays: support for other functionalities too.</span></div>
<div style="position:absolute;left:75.85px;top:242.16px" class="cls_010"><span class="cls_010">They have become a (THE) major tool in large,</span></div>
<div style="position:absolute;left:75.85px;top:271.20px" class="cls_010"><span class="cls_010">multi-site projects</span></div>
<div style="position:absolute;left:61.20px;top:340.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_010">Agile projects: emphasis on running system: </span><span class="cls_013">daily</span></div>
<div style="position:absolute;left:75.85px;top:369.12px" class="cls_013"><span class="cls_013">builds</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Configuration Management Plan</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> Management section: organization,</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">responsibilities, standards to use, etc</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> Activities: identification of items, keeping status,</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_010"><span class="cls_010">handling CRs</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_011"><span class="cls_011">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture4/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Summary</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_010">CM is about managing al kinds of artifacts during</span></div>
<div style="position:absolute;left:75.85px;top:242.16px" class="cls_010"><span class="cls_010">software development</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_010">Crucial for large projects</span></div>
<div style="position:absolute;left:61.20px;top:381.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> Supported by powerful tools</span></div>
<div style="position:absolute;left:5.67px;top:500.88px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.76px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_011"><span class="cls_011">10</span></div>
</div>

</body>
</html>
