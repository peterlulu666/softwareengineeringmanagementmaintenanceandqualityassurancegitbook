## Quiz        

1. The information planning refer to this meta-project planning process

2. The software is **not** written from **scratch**. It must interface with existing software, extend existing software, use existing subroutine libraries, build upon an existing framework

3. Planning the project
    - first step
    - identify the project **characteristics** and their **impact** on the development process

4. Contents of project plan
    - Introduction
        - background
        - history
        - summary
    - Process model
        - life cycle
        - activities
        - milestones
        - ascertain milestones are reached
    - Organization of the project
        - various roles
            - One has to clearly delineate these roles and identify the responsibilities of each of them
        - If there are gaps in the knowledge required to fulfill any of these roles, the training and education needed to fill these gaps have to be identified
    - Standards, guidelines, procedures
        - follows the standards, guidelines and procedures agreed upon
    - Management activities
        - follow certain priorities in balancing requirements
        - when is documentation to be delivered, how is the quality of the documentation to be assessed, how does one ensure that the documentation is kept up-to-date
    - Risks
        - The more uncertain various aspects of the project are, the larger the risks
    - Staffing
        - personnel
        - skills
    - Methods and techniques
        - A large proportion of the technical **documentation** will be produced during these phases
    - Quality assurance
        - assure it meets the quality requirements
    - Work packages
        - broken down into activities
        - breakdown structure
    - Resources
    - Budget and schedule
    - Changes
        - Changes that are entered via the back door lead to badly structured code, insufficient documentation and cost and time overruns
    - Delivery

5. Project control
    - time
        - The time needed to build a system is obviously related to the size of the system, and thus to the total **manpower** required
        - The more people that are involved, the more time will be needed for coordination and communication. After a certain point, adding **more people** actually **lengthens** the development **time**
    - information
        - documentation
        - Agile projects less documentation less planning 
    - Organization
        - people and team
    - Quality
        - Quality is not an add-on feature, it has to be built in
    - Money
        - labor costs
        - personnel costs

6. Agile projects do less planning than document-driven projects
