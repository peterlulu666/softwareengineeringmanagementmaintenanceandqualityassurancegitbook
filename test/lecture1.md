<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft10{font-size:24px;font-family:Times;color:#fec14f;}
	.ft11{font-size:6px;font-family:Times;color:#595959;}
	.ft12{font-size:18px;font-family:Times;color:#0079c0;}
	.ft13{font-size:12px;font-family:Times;color:#212121;}
	.ft14{font-size:12px;font-family:Times;color:#e69138;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page1-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target001.png" alt="background image"/>
<p style="position:absolute;top:160px;left:97px;white-space:nowrap" class="ft10">305.4&#160;-&#160;Object-Relational&#160;Mapping,&#160;</p>
<p style="position:absolute;top:192px;left:154px;white-space:nowrap" class="ft10">Java&#160;Persistence&#160;API&#160;and</p>
<p style="position:absolute;top:224px;left:254px;white-space:nowrap" class="ft10">Hibernate</p>
<p style="position:absolute;top:813px;left:624px;white-space:nowrap" class="ft11">2</p>
<p style="position:absolute;top:520px;left:25px;white-space:nowrap" class="ft12">Learning&#160;Objectives:&#160;</p>
<p style="position:absolute;top:568px;left:30px;white-space:nowrap" class="ft13">In&#160;this&#160;presentation,&#160;we&#160;will&#160;get&#160;to&#160;know&#160;the&#160;Java&#160;persistence&#160;standard&#160;based&#160;on&#160;Hibernate.</p>
<p style="position:absolute;top:598px;left:30px;white-space:nowrap" class="ft13">By&#160;the&#160;end&#160;of&#160;this&#160;session,&#160;learners&#160;will&#160;be&#160;able&#160;to:</p>
<p style="position:absolute;top:629px;left:31px;white-space:nowrap" class="ft14">●</p>
<p style="position:absolute;top:629px;left:43px;white-space:nowrap" class="ft13">Explain&#160;Object-Relational&#160;Mapping&#160;(ORM).</p>
<p style="position:absolute;top:648px;left:31px;white-space:nowrap" class="ft14">●</p>
<p style="position:absolute;top:648px;left:43px;white-space:nowrap" class="ft13">Explain&#160;the&#160;Hibernate&#160;Application&#160;Architecture&#160;and&#160;Hibernate&#160;-&#160;Annotations.</p>
<p style="position:absolute;top:668px;left:31px;white-space:nowrap" class="ft14">●</p>
<p style="position:absolute;top:668px;left:43px;white-space:nowrap" class="ft13">Describe&#160;how&#160;to&#160;use&#160;JPA&#160;to&#160;store&#160;and&#160;manage&#160;Java&#160;objects&#160;in&#160;a&#160;relational&#160;database.</p>
<p style="position:absolute;top:688px;left:31px;white-space:nowrap" class="ft14">●</p>
<p style="position:absolute;top:688px;left:43px;white-space:nowrap" class="ft13">Demonstrate&#160;the&#160;JPA&#160;with&#160;Hibernate&#160;as&#160;Implementation.</p>
<p style="position:absolute;top:708px;left:31px;white-space:nowrap" class="ft14">●</p>
<p style="position:absolute;top:708px;left:43px;white-space:nowrap" class="ft13">Demonstrate&#160;setting&#160;up&#160;a&#160;Hibernate&#160;project.</p>
<p style="position:absolute;top:727px;left:31px;white-space:nowrap" class="ft14">●</p>
<p style="position:absolute;top:727px;left:43px;white-space:nowrap" class="ft13">Describe&#160;Hibernate&#160;Query&#160;Object&#160;(HQO)&#160;and&#160;Hibernate&#160;Query&#160;Language&#160;(HQL).</p>
<p style="position:absolute;top:747px;left:31px;white-space:nowrap" class="ft14">●</p>
<p style="position:absolute;top:747px;left:43px;white-space:nowrap" class="ft13">Demonstrate&#160;how&#160;to&#160;use&#160;Hibernate&#160;Query&#160;Object&#160;(HQO).</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft20{font-size:6px;font-family:Times;color:#595959;}
	.ft21{font-size:18px;font-family:Times;color:#0079c0;}
	.ft22{font-size:10px;font-family:Times;color:#ffab40;}
	.ft23{font-size:9px;font-family:Times;color:#212121;}
	.ft24{font-size:10px;font-family:Times;color:#000000;}
	.ft25{font-size:9px;font-family:Times;color:#ffab40;}
	.ft26{font-size:15px;font-family:Times;color:#0079c0;}
	.ft27{font-size:10px;font-family:Times;color:#212121;}
	.ft28{font-size:10px;font-family:Times;color:#e69138;}
	.ft29{font-size:3px;font-family:Times;color:#3f3f3f;}
	.ft210{font-size:9px;line-height:14px;font-family:Times;color:#212121;}
	.ft211{font-size:9px;line-height:13px;font-family:Times;color:#212121;}
	.ft212{font-size:9px;line-height:19px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page2-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target002.png" alt="background image"/>
<p style="position:absolute;top:397px;left:624px;white-space:nowrap" class="ft20">3</p>
<p style="position:absolute;top:108px;left:20px;white-space:nowrap" class="ft21">Table&#160;of&#160;Contents</p>
<p style="position:absolute;top:156px;left:23px;white-space:nowrap" class="ft22">❑</p>
<p style="position:absolute;top:157px;left:48px;white-space:nowrap" class="ft23">Object&#160;Relational&#160;Mapping&#160;(ORM)</p>
<p style="position:absolute;top:171px;left:23px;white-space:nowrap" class="ft22">❑</p>
<p style="position:absolute;top:171px;left:48px;white-space:nowrap" class="ft23">Java&#160;Persistence&#160;API&#160;(JPA)</p>
<p style="position:absolute;top:186px;left:23px;white-space:nowrap" class="ft22">❑</p>
<p style="position:absolute;top:185px;left:48px;white-space:nowrap" class="ft24">Overview&#160;of&#160;Java&#160;Hibernate</p>
<p style="position:absolute;top:201px;left:23px;white-space:nowrap" class="ft22">❑</p>
<p style="position:absolute;top:201px;left:48px;white-space:nowrap" class="ft23">Hibernate&#160;with&#160;JPA.</p>
<p style="position:absolute;top:215px;left:23px;white-space:nowrap" class="ft22">❑</p>
<p style="position:absolute;top:216px;left:48px;white-space:nowrap" class="ft23">Hibernate&#160;Application&#160;Architecture.</p>
<p style="position:absolute;top:230px;left:53px;white-space:nowrap" class="ft22">○</p>
<p style="position:absolute;top:230px;left:72px;white-space:nowrap" class="ft23">Persistent&#160;objects.</p>
<p style="position:absolute;top:244px;left:53px;white-space:nowrap" class="ft22">○</p>
<p style="position:absolute;top:245px;left:72px;white-space:nowrap" class="ft23">Configuration.</p>
<p style="position:absolute;top:259px;left:99px;white-space:nowrap" class="ft25">■</p>
<p style="position:absolute;top:259px;left:117px;white-space:nowrap" class="ft23">Hibernate&#160;Configuration&#160;Files</p>
<p style="position:absolute;top:274px;left:23px;white-space:nowrap" class="ft22">❑</p>
<p style="position:absolute;top:274px;left:48px;white-space:nowrap" class="ft23">Specify&#160;Exact&#160;Database&#160;Dialect</p>
<p style="position:absolute;top:288px;left:23px;white-space:nowrap" class="ft22">❑</p>
<p style="position:absolute;top:289px;left:48px;white-space:nowrap" class="ft23">Hibernate&#160;Configuration&#160;Property</p>
<p style="position:absolute;top:303px;left:23px;white-space:nowrap" class="ft22">❑</p>
<p style="position:absolute;top:303px;left:48px;white-space:nowrap" class="ft23">Session&#160;Factory&#160;in&#160;Hibernate</p>
<p style="position:absolute;top:318px;left:23px;white-space:nowrap" class="ft22">❑</p>
<p style="position:absolute;top:318px;left:48px;white-space:nowrap" class="ft23">Session&#160;in&#160;Hibernate</p>
<p style="position:absolute;top:332px;left:23px;white-space:nowrap" class="ft22">❑</p>
<p style="position:absolute;top:333px;left:48px;white-space:nowrap" class="ft23">Important&#160;“Session&#160;Methods”</p>
<p style="position:absolute;top:347px;left:25px;white-space:nowrap" class="ft23">❑</p>
<p style="position:absolute;top:347px;left:48px;white-space:nowrap" class="ft23">Query&#160;Object</p>
<p style="position:absolute;top:162px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:162px;left:366px;white-space:nowrap" class="ft23">Setting&#160;up&#160;a&#160;Hibernate.</p>
<p style="position:absolute;top:177px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:177px;left:366px;white-space:nowrap" class="ft23">Entity/POJO/Model&#160;Classes</p>
<p style="position:absolute;top:192px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:192px;left:366px;white-space:nowrap" class="ft23">Hibernate&#160;Application&#160;workflow.</p>
<p style="position:absolute;top:207px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:207px;left:366px;white-space:nowrap" class="ft23">Hibernate&#160;-&#160;Annotations.</p>
<p style="position:absolute;top:222px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:222px;left:366px;white-space:nowrap" class="ft210">Hibernate&#160;Query&#160;Object&#160;/&#160;Hibernate&#160;Query&#160;<br/>Language</p>
<p style="position:absolute;top:251px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:251px;left:366px;white-space:nowrap" class="ft23">HQL&#160;-&#160;Interfaces</p>
<p style="position:absolute;top:266px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:266px;left:366px;white-space:nowrap" class="ft23">Execution&#160;HQL&#160;Queries</p>
<p style="position:absolute;top:281px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:281px;left:366px;white-space:nowrap" class="ft23">HQL&#160;Methods</p>
<p style="position:absolute;top:296px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:296px;left:366px;white-space:nowrap" class="ft23">Problem&#160;with&#160;HQL&#160;and&#160;SQL</p>
<p style="position:absolute;top:311px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:311px;left:366px;white-space:nowrap" class="ft23">Overview&#160;of&#160;Hibernate&#160;Named&#160;Query</p>
<p style="position:absolute;top:326px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:326px;left:366px;white-space:nowrap" class="ft23">Advantages&#160;of&#160;Named&#160;Queries</p>
<p style="position:absolute;top:341px;left:337px;white-space:nowrap" class="ft22">❏</p>
<p style="position:absolute;top:341px;left:366px;white-space:nowrap" class="ft23">JDBC&#160;vs.&#160;Java&#160;Hibernate</p>
<p style="position:absolute;top:813px;left:624px;white-space:nowrap" class="ft20">4</p>
<p style="position:absolute;top:532px;left:25px;white-space:nowrap" class="ft26">Object&#160;Relational&#160;Mapping</p>
<p style="position:absolute;top:568px;left:30px;white-space:nowrap" class="ft211">Object&#160;Relational&#160;Mapping&#160;(ORM)&#160;is&#160;the&#160;concept/process&#160;of&#160;converting&#160;data&#160;from&#160;object-oriented&#160;language&#160;to&#160;a&#160;<br/>relational&#160;database,&#160;and&#160;vice&#160;versa.<br/>ORM&#160;translates&#160;the&#160;programming&#160;code&#160;attributes&#160;(variables)&#160;into&#160;the&#160;columns&#160;in&#160;the&#160;table.&#160;It&#160;is&#160;good&#160;for&#160;managing&#160;<br/>various&#160;database&#160;operations,&#160;such&#160;as&#160;insertion,&#160;update,&#160;and&#160;deletion&#160;effectively.&#160;</p>
<p style="position:absolute;top:706px;left:30px;white-space:nowrap" class="ft27">The&#160;mapping&#160;between&#160;the&#160;object&#160;model&#160;and&#160;the&#160;relational&#160;database&#160;should&#160;be&#160;as&#160;follows:</p>
<p style="position:absolute;top:740px;left:39px;white-space:nowrap" class="ft28">●</p>
<p style="position:absolute;top:740px;left:62px;white-space:nowrap" class="ft27">Class&#160;&lt;-&gt;&#160;Table.</p>
<p style="position:absolute;top:757px;left:39px;white-space:nowrap" class="ft28">●</p>
<p style="position:absolute;top:757px;left:62px;white-space:nowrap" class="ft27">Java&#160;Object&#160;&lt;-&gt;&#160;Row.</p>
<p style="position:absolute;top:774px;left:39px;white-space:nowrap" class="ft28">●</p>
<p style="position:absolute;top:774px;left:62px;white-space:nowrap" class="ft27">Class&#160;Attribute&#160;&lt;-&gt;&#160;Column.&#160;</p>
<p style="position:absolute;top:675px;left:441px;white-space:nowrap" class="ft29">&#160;image&#160;source:&#160;https://learnjava.co.in</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft30{font-size:6px;font-family:Times;color:#595959;}
	.ft31{font-size:15px;font-family:Times;color:#0079c0;}
	.ft32{font-size:14px;font-family:Times;color:#0079c0;}
	.ft33{font-size:16px;font-family:Times;color:#0079c0;}
	.ft34{font-size:12px;font-family:Times;color:#000000;}
	.ft35{font-size:4px;font-family:Times;color:#3f3f3f;}
	.ft36{font-size:19px;font-family:Times;color:#0079c0;}
	.ft37{font-size:12px;font-family:Times;color:#212121;}
	.ft38{font-size:12px;font-family:Times;color:#e69138;}
	.ft39{font-size:12px;font-family:Times;color:#212121;}
	.ft310{font-size:12px;line-height:17px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page3-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target003.png" alt="background image"/>
<p style="position:absolute;top:397px;left:624px;white-space:nowrap" class="ft30">5</p>
<p style="position:absolute;top:106px;left:25px;white-space:nowrap" class="ft31">Object&#160;Relational&#160;Mapping&#160;</p>
<p style="position:absolute;top:107px;left:254px;white-space:nowrap" class="ft32">(continued)</p>
<p style="position:absolute;top:105px;left:342px;white-space:nowrap" class="ft33">&#160;</p>
<p style="position:absolute;top:137px;left:244px;white-space:nowrap" class="ft34">ORM&#160;Architecture</p>
<p style="position:absolute;top:401px;left:269px;white-space:nowrap" class="ft35">&#160;image&#160;source:&#160;https://learnjava.co.in</p>
<p style="position:absolute;top:813px;left:624px;white-space:nowrap" class="ft30">6</p>
<p style="position:absolute;top:522px;left:25px;white-space:nowrap" class="ft33">Object&#160;Relational&#160;Mapping</p>
<p style="position:absolute;top:519px;left:260px;white-space:nowrap" class="ft36">&#160;</p>
<p style="position:absolute;top:524px;left:266px;white-space:nowrap" class="ft32">(continued)</p>
<p style="position:absolute;top:522px;left:354px;white-space:nowrap" class="ft33">&#160;</p>
<p style="position:absolute;top:562px;left:41px;white-space:nowrap" class="ft37">Java&#160;Persistence&#160;API&#160;</p>
<p style="position:absolute;top:584px;left:48px;white-space:nowrap" class="ft38">●</p>
<p style="position:absolute;top:584px;left:73px;white-space:nowrap" class="ft310">Java&#160;Persistence&#160;API&#160;(JPA)&#160;is&#160;the&#160;Object-Relational&#160;Mapping&#160;(ORM)&#160;standard&#160;for&#160;<br/>storing,&#160;accessing,&#160;and&#160;managing&#160;Java&#160;objects&#160;in&#160;a&#160;relational&#160;database.</p>
<p style="position:absolute;top:624px;left:48px;white-space:nowrap" class="ft38">●</p>
<p style="position:absolute;top:624px;left:73px;white-space:nowrap" class="ft310">JPA&#160;is&#160;only&#160;a&#160;specification;&#160;it&#160;is&#160;not&#160;an&#160;implementation,&#160;but&#160;several&#160;implementations&#160;<br/>are&#160;available.&#160;Popular&#160;implementations&#160;include&#160;Hibernate,&#160;EclipseLink,&#160;Apache&#160;<br/>OpenJPA,&#160;and&#160;many&#160;more.&#160;</p>
<p style="position:absolute;top:681px;left:48px;white-space:nowrap" class="ft38">●</p>
<p style="position:absolute;top:681px;left:73px;white-space:nowrap" class="ft310">JPA&#160;permits&#160;the&#160;developer&#160;to&#160;work&#160;directly&#160;with&#160;objects&#160;rather&#160;than&#160;with&#160;SQL&#160;<br/>statements.&#160;The&#160;JPA&#160;implementation&#160;is&#160;typically&#160;called&#160;a&#160;p<i>ersistence&#160;provider</i>.</p>
<p style="position:absolute;top:720px;left:48px;white-space:nowrap" class="ft38">●</p>
<p style="position:absolute;top:720px;left:73px;white-space:nowrap" class="ft37">JPA&#160;is&#160;the&#160;EE&#160;standard&#160;specification&#160;for&#160;ORM&#160;in&#160;Java&#160;EE.</p>
<p style="position:absolute;top:743px;left:48px;white-space:nowrap" class="ft38">●</p>
<p style="position:absolute;top:743px;left:73px;white-space:nowrap" class="ft37">JPA&#160;API&#160;is&#160;a&#160;set&#160;of&#160;rules&#160;and&#160;a&#160;framework&#160;to&#160;set&#160;interfaces&#160;for&#160;implementing&#160;ORM.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft40{font-size:6px;font-family:Times;color:#595959;}
	.ft41{font-size:15px;font-family:Times;color:#0079c0;}
	.ft42{font-size:12px;font-family:Times;color:#e69138;}
	.ft43{font-size:12px;font-family:Times;color:#212121;}
	.ft44{font-size:10px;font-family:Times;color:#0d6efd;}
	.ft45{font-size:10px;font-family:Times;color:#212121;}
	.ft46{font-size:18px;font-family:Times;color:#0079c0;}
	.ft47{font-size:9px;font-family:Times;color:#212121;}
	.ft48{font-size:10px;font-family:Times;color:#e69138;}
	.ft49{font-size:9px;font-family:Times;color:#212121;}
	.ft410{font-size:9px;font-family:Times;color:#e69138;}
	.ft411{font-size:4px;font-family:Times;color:#3f3f3f;}
	.ft412{font-size:10px;line-height:15px;font-family:Times;color:#212121;}
	.ft413{font-size:9px;line-height:13px;font-family:Times;color:#212121;}
	.ft414{font-size:9px;line-height:14px;font-family:Times;color:#212121;}
	.ft415{font-size:9px;line-height:14px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page4-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target004.png" alt="background image"/>
<p style="position:absolute;top:397px;left:624px;white-space:nowrap" class="ft40">7</p>
<p style="position:absolute;top:112px;left:22px;white-space:nowrap" class="ft41">Object&#160;Relational&#160;Mapping&#160;(continued)&#160;</p>
<p style="position:absolute;top:152px;left:33px;white-space:nowrap" class="ft42">●</p>
<p style="position:absolute;top:152px;left:54px;white-space:nowrap" class="ft43">ORM&#160;Problem:</p>
<p style="position:absolute;top:170px;left:59px;white-space:nowrap" class="ft44">○</p>
<p style="position:absolute;top:170px;left:79px;white-space:nowrap" class="ft412">Database&#160;interaction&#160;requires&#160;a&#160;great&#160;deal&#160;of&#160;code,&#160;and&#160;is&#160;therefore&#160;a&#160;burden&#160;on&#160;data&#160;<br/>maintenance.&#160;Database&#160;interaction&#160;also&#160;requires&#160;a&#160;proprietary&#160;framework.</p>
<p style="position:absolute;top:202px;left:33px;white-space:nowrap" class="ft42">●</p>
<p style="position:absolute;top:202px;left:54px;white-space:nowrap" class="ft43">Solution:</p>
<p style="position:absolute;top:219px;left:59px;white-space:nowrap" class="ft44">○</p>
<p style="position:absolute;top:219px;left:79px;white-space:nowrap" class="ft412">Most&#160;contemporary&#160;applications&#160;use&#160;relational&#160;database&#160;to&#160;store&#160;data.&#160;Recently,&#160;many&#160;vendors&#160;<br/>switched&#160;to&#160;a&#160;relational&#160;database&#160;to&#160;reduce&#160;the&#160;burden&#160;on&#160;data&#160;maintenance.&#160;This&#160;means&#160;that&#160;<br/>object&#160;databases&#160;or&#160;object&#160;relational&#160;technologies&#160;take&#160;care&#160;of&#160;storing,&#160;retrieving,&#160;and&#160;updating&#160;<br/>data,&#160;as&#160;well&#160;as&#160;maintaining&#160;data.&#160;The&#160;core&#160;part&#160;of&#160;the&#160;object&#160;relational&#160;technologies&#160;is&#160;the&#160;<br/>mapping&#160;file.&#160;As&#160;a&#160;mapping&#160;file&#160;does&#160;not&#160;require&#160;compilation,&#160;we&#160;can&#160;easily&#160;make&#160;changes&#160;to&#160;<br/>multiple&#160;data&#160;sources&#160;with&#160;less&#160;administration.</p>
<p style="position:absolute;top:323px;left:59px;white-space:nowrap" class="ft44">○</p>
<p style="position:absolute;top:323px;left:79px;white-space:nowrap" class="ft412">ORM&#160;is&#160;a&#160;programming&#160;ability&#160;or&#160;approach&#160;to&#160;mapping&#160;Java&#160;objects&#160;to&#160;database&#160;tables,&#160;and&#160;<br/>vice&#160;versa.&#160;JPA&#160;is&#160;one&#160;possible&#160;approach&#160;to&#160;ORM.&#160;By&#160;using&#160;JPA,&#160;the&#160;developer&#160;can&#160;map,&#160;store,&#160;<br/>update,&#160;delete,&#160;and&#160;retrieve&#160;data&#160;from&#160;relational&#160;databases&#160;to&#160;Java&#160;objects,&#160;and&#160;vice&#160;versa.</p>
<p style="position:absolute;top:813px;left:624px;white-space:nowrap" class="ft40">8</p>
<p style="position:absolute;top:520px;left:25px;white-space:nowrap" class="ft46">Overview&#160;of&#160;Java&#160;Hibernate</p>
<p style="position:absolute;top:550px;left:34px;white-space:nowrap" class="ft413">Hibernate&#160;is&#160;an&#160;implementation&#160;of&#160;JPA&#160;API,&#160;and&#160;uses&#160;ORM&#160;techniques.&#160;In&#160;that,&#160;we&#160;can&#160;use&#160;the&#160;standard&#160;JPA&#160;API,&#160;<br/>and&#160;configure&#160;applications&#160;to&#160;use&#160;Hibernate&#160;as&#160;the&#160;provider&#160;of&#160;the&#160;specification&#160;under&#160;the&#160;covers.&#160;Hibernate&#160;<br/>provides&#160;more&#160;features&#160;beyond&#160;what&#160;JPA&#160;specifies:</p>
<p style="position:absolute;top:599px;left:26px;white-space:nowrap" class="ft48">●</p>
<p style="position:absolute;top:600px;left:40px;white-space:nowrap" class="ft413">The&#160;Hibernate&#160;framework&#160;consists&#160;of&#160;several&#160;components&#160;such&#160;as&#160;<i>Hibernate&#160;ORM,&#160;Hibernate&#160;Search,&#160;<br/>Hibernate&#160;Validator,&#160;Hibernate&#160;CGM,&#160;and&#160;Hibernate&#160;Tools</i>.&#160;In&#160;this&#160;course,&#160;we&#160;will&#160;use&#160;Hibernate&#160;ORM,&#160;which&#160;<br/>is&#160;the&#160;core&#160;component&#160;of&#160;the&#160;Hibernate&#160;framework&#160;for&#160;mapping&#160;Java&#160;model&#160;classes.</p>
<p style="position:absolute;top:649px;left:26px;white-space:nowrap" class="ft48">●</p>
<p style="position:absolute;top:650px;left:40px;white-space:nowrap" class="ft415">JDBC&#160;is&#160;not&#160;object-oriented;&#160;rather,&#160;we&#160;are&#160;dealing&#160;with&#160;values&#160;means&#160;of&#160;primitive&#160;data.&#160;In&#160;Hibernate,&#160;each&#160;record&#160;<br/>is&#160;represented&#160;as&#160;a&#160;Object,&#160;but&#160;in&#160;JDBC,&#160;each&#160;record&#160;is&#160;no&#160;more&#160;than&#160;data,&#160;which&#160;is&#160;nothing&#160;but&#160;primitive&#160;values.</p>
<p style="position:absolute;top:685px;left:27px;white-space:nowrap" class="ft410">●</p>
<p style="position:absolute;top:685px;left:40px;white-space:nowrap" class="ft413">Hibernate&#160;maps&#160;Java&#160;classes&#160;to&#160;database&#160;tables,&#160;and&#160;from&#160;Java&#160;data&#160;types&#160;to&#160;SQL&#160;data&#160;types.&#160;Hibernate&#160;<br/>maps&#160;also&#160;relieve&#160;the&#160;developer&#160;from&#160;the&#160;majority&#160;of&#160;common&#160;data&#160;persistence-related&#160;programming&#160;tasks.&#160;This&#160;<br/>is&#160;especially&#160;beneficial&#160;for&#160;developers&#160;with&#160;limited&#160;knowledge&#160;of&#160;SQL.</p>
<p style="position:absolute;top:818px;left:435px;white-space:nowrap" class="ft411">&#160;image&#160;source:&#160;https://learnjava.co.in</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft50{font-size:6px;font-family:Times;color:#595959;}
	.ft51{font-size:18px;font-family:Times;color:#0079c0;}
	.ft52{font-size:10px;font-family:Times;color:#212121;}
	.ft53{font-size:10px;font-family:Times;color:#e69138;}
	.ft54{font-size:9px;font-family:Times;color:#e69138;}
	.ft55{font-size:9px;font-family:Times;color:#09507c;}
	.ft56{font-size:9px;font-family:Times;color:#212121;}
	.ft57{font-size:9px;font-family:Times;color:#1d1c1d;}
	.ft58{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
	.ft59{font-size:9px;line-height:14px;font-family:Times;color:#212121;}
	.ft510{font-size:9px;line-height:14px;font-family:Times;color:#1d1c1d;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page5-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target005.png" alt="background image"/>
<p style="position:absolute;top:397px;left:624px;white-space:nowrap" class="ft50">9</p>
<p style="position:absolute;top:104px;left:25px;white-space:nowrap" class="ft51">Why&#160;Hibernate?</p>
<p style="position:absolute;top:144px;left:37px;white-space:nowrap" class="ft58">At&#160;runtime,&#160;the&#160;Hibernate&#160;framework&#160;will&#160;generate&#160;all&#160;of&#160;the&#160;SQL&#160;required&#160;to&#160;interact&#160;with&#160;the&#160;backend&#160;<br/>database,&#160;including:&#160;</p>
<p style="position:absolute;top:187px;left:45px;white-space:nowrap" class="ft53">●</p>
<p style="position:absolute;top:187px;left:69px;white-space:nowrap" class="ft58">Reducing&#160;lines&#160;of&#160;code&#160;by&#160;maintaining&#160;object-table&#160;mapping&#160;itself&#160;and&#160;returning&#160;the&#160;result&#160;to&#160;the&#160;<br/>application&#160;in&#160;the&#160;form&#160;of&#160;Java&#160;objects,&#160;hence&#160;reducing&#160;the&#160;development&#160;time&#160;and&#160;maintenance&#160;<br/>cost&#160;code.</p>
<p style="position:absolute;top:248px;left:45px;white-space:nowrap" class="ft53">●</p>
<p style="position:absolute;top:248px;left:69px;white-space:nowrap" class="ft52">Making&#160;an&#160;application&#160;portable&#160;to&#160;all&#160;RDBMS(SQL)&#160;databases.</p>
<p style="position:absolute;top:274px;left:45px;white-space:nowrap" class="ft53">●</p>
<p style="position:absolute;top:274px;left:69px;white-space:nowrap" class="ft58">Handling&#160;all&#160;create-read-update-delete&#160;(CRUD)&#160;operations&#160;using&#160;simple&#160;API;&#160;no&#160;SQL.&#160;Hibernate&#160;<br/>takes&#160;care&#160;of&#160;this&#160;mapping&#160;/&#160;relationships&#160;using&#160;XML&#160;files&#160;so&#160;that&#160;the&#160;developer&#160;does&#160;not&#160;need&#160;to&#160;<br/>write&#160;code.</p>
<p style="position:absolute;top:335px;left:45px;white-space:nowrap" class="ft53">●</p>
<p style="position:absolute;top:335px;left:69px;white-space:nowrap" class="ft58">Interacting&#160;between&#160;the&#160;database&#160;and&#160;the&#160;Java&#160;program&#160;through&#160;a&#160;heavily-tested&#160;and&#160;robust&#160;<br/>framework.</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft50">10</p>
<p style="position:absolute;top:525px;left:28px;white-space:nowrap" class="ft51">Hibernate&#160;With&#160;JPA</p>
<p style="position:absolute;top:558px;left:48px;white-space:nowrap" class="ft54">●</p>
<p style="position:absolute;top:558px;left:71px;white-space:nowrap" class="ft55">JPA</p>
<p style="position:absolute;top:558px;left:93px;white-space:nowrap" class="ft56">&#160;is&#160;a&#160;specification&#160;for&#160;persistence&#160;providers&#160;to&#160;implement.&#160;Hibernate&#160;is&#160;one&#160;such&#160;implementation&#160;of&#160;</p>
<p style="position:absolute;top:573px;left:71px;white-space:nowrap" class="ft59">JPA&#160;specification.&#160;We&#160;can&#160;annotate&#160;our&#160;classes&#160;as&#160;much&#160;as&#160;we&#160;like&#160;with&#160;JPA&#160;annotations;&#160;however,&#160;<br/>without&#160;an&#160;implementation,&#160;nothing&#160;will&#160;happen.</p>
<p style="position:absolute;top:611px;left:48px;white-space:nowrap" class="ft54">●</p>
<p style="position:absolute;top:611px;left:71px;white-space:nowrap" class="ft59">Think&#160;of&#160;JPA&#160;as&#160;the&#160;guidelines/specification&#160;that&#160;must&#160;be&#160;followed,&#160;or&#160;as&#160;an&#160;interface&#160;while&#160;Hibernate’s&#160;<br/>JPA&#160;implementation&#160;is&#160;code&#160;that&#160;meets&#160;the&#160;API,&#160;as&#160;defined&#160;by&#160;JPA,&#160;providing&#160;&#160;the&#160;“under-the-hood”&#160;<br/>functionality.</p>
<p style="position:absolute;top:664px;left:48px;white-space:nowrap" class="ft54">●</p>
<p style="position:absolute;top:664px;left:71px;white-space:nowrap" class="ft59">When&#160;we&#160;use&#160;hibernate&#160;with&#160;JPA,&#160;we&#160;are&#160;actually&#160;using&#160;the&#160;Hibernate&#160;JPA&#160;implementation.&#160;The&#160;benefit&#160;<br/>of&#160;this&#160;is&#160;that&#160;we&#160;can&#160;swap&#160;out&#160;Hibernate’s&#160;implementation&#160;of&#160;JPA&#160;for&#160;another&#160;implementation&#160;of&#160;the&#160;<br/>JPA&#160;specification.</p>
<p style="position:absolute;top:717px;left:48px;white-space:nowrap" class="ft54">●</p>
<p style="position:absolute;top:717px;left:71px;white-space:nowrap" class="ft59">When&#160;we&#160;use&#160;straight&#160;Hibernate,&#160;we&#160;lock&#160;into&#160;the&#160;implementation&#160;because&#160;other&#160;ORMs&#160;may&#160;use&#160;<br/>different&#160;methods/configurations&#160;and&#160;annotations;&#160;therefore,&#160;we&#160;cannot&#160;just&#160;switch&#160;over&#160;to&#160;another&#160;<br/>ORM.</p>
<p style="position:absolute;top:769px;left:48px;white-space:nowrap" class="ft53">●</p>
<p style="position:absolute;top:771px;left:71px;white-space:nowrap" class="ft510">ORM&#160;is&#160;a&#160;set&#160;of&#160;design&#160;patterns&#160;for&#160;mapping&#160;Java&#160;objects&#160;to&#160;database&#160;tables,&#160;JPA&#160;is&#160;a&#160;specification&#160;for&#160;<br/>interacting&#160;with&#160;the&#160;database,&#160;and&#160;Hibernate&#160;is&#160;an&#160;implementation&#160;of&#160;the&#160;JPA&#160;specification.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft60{font-size:6px;font-family:Times;color:#595959;}
	.ft61{font-size:18px;font-family:Times;color:#0079c0;}
	.ft62{font-size:8px;font-family:Times;color:#222222;}
	.ft63{font-size:8px;font-family:Times;color:#e69138;}
	.ft64{font-size:8px;font-family:Times;color:#ffab40;}
	.ft65{font-size:4px;font-family:Times;color:#3f3f3f;}
	.ft66{font-size:9px;font-family:Times;color:#212121;}
	.ft67{font-size:9px;font-family:Times;color:#e69138;}
	.ft68{font-size:9px;font-family:Times;color:#212121;}
	.ft69{font-size:8px;line-height:12px;font-family:Times;color:#222222;}
	.ft610{font-size:8px;line-height:16px;font-family:Times;color:#222222;}
	.ft611{font-size:9px;line-height:13px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page6-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target006.png" alt="background image"/>
<p style="position:absolute;top:397px;left:620px;white-space:nowrap" class="ft60">11</p>
<p style="position:absolute;top:104px;left:26px;white-space:nowrap" class="ft61">Hibernate&#160;Application&#160;Architecture</p>
<p style="position:absolute;top:139px;left:35px;white-space:nowrap" class="ft610">Hibernate&#160;has&#160;a&#160;layered&#160;architecture,&#160;which&#160;helps&#160;the&#160;user&#160;to&#160;operate&#160;without&#160;<br/>having&#160;to&#160;know&#160;the&#160;underlying&#160;APIs.&#160;Hibernate&#160;makes&#160;use&#160;of&#160;the&#160;database&#160;and&#160;<br/>configuration&#160;data&#160;to&#160;provide&#160;persistence&#160;services&#160;(persistent&#160;objects)&#160;to&#160;the&#160;<br/>application.<br/>Let’s&#160;understand&#160;what&#160;each&#160;block&#160;represents&#160;in&#160;detail:</p>
<p style="position:absolute;top:216px;left:45px;white-space:nowrap" class="ft63">●</p>
<p style="position:absolute;top:216px;left:67px;white-space:nowrap" class="ft64">Persistent&#160;Objects:&#160;</p>
<p style="position:absolute;top:216px;left:168px;white-space:nowrap" class="ft62">Plain&#160;Old&#160;Java&#160;Objects&#160;(POJOs),&#160;which&#160;get&#160;persisted&#160;</p>
<p style="position:absolute;top:229px;left:67px;white-space:nowrap" class="ft69">as&#160;one&#160;of&#160;the&#160;rows&#160;in&#160;the&#160;related&#160;table&#160;in&#160;the&#160;database&#160;by&#160;Hibernate.&#160;They&#160;<br/>can&#160;be&#160;configured&#160;in&#160;configuration&#160;files&#160;(hibernate.cfg.xml&#160;or&#160;<br/>hibernate.properties)&#160;or&#160;annotated&#160;with&#160;@Entity&#160;annotations.</p>
<p style="position:absolute;top:384px;left:418px;white-space:nowrap" class="ft60">Visit&#160;Wiki&#160;document&#160;for&#160;Hibernate&#160;Cache</p>
<p style="position:absolute;top:360px;left:494px;white-space:nowrap" class="ft65">&#160;image&#160;source:&#160;https://learnjava.co.in</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft60">12</p>
<p style="position:absolute;top:526px;left:27px;white-space:nowrap" class="ft61">Persistent&#160;Objects</p>
<p style="position:absolute;top:561px;left:35px;white-space:nowrap" class="ft66">Persistent&#160;Objects:&#160;&#160;</p>
<p style="position:absolute;top:585px;left:44px;white-space:nowrap" class="ft67">●</p>
<p style="position:absolute;top:585px;left:67px;white-space:nowrap" class="ft611">Java&#160;classes&#160;whose&#160;objects&#160;or&#160;instances&#160;will&#160;be&#160;stored&#160;in&#160;database&#160;tables&#160;are&#160;called&#160;persistent&#160;classes&#160;in&#160;<br/>Hibernate.&#160;</p>
<p style="position:absolute;top:611px;left:44px;white-space:nowrap" class="ft67">●</p>
<p style="position:absolute;top:611px;left:67px;white-space:nowrap" class="ft66">It&#160;represents&#160;a&#160;row&#160;in&#160;a&#160;database&#160;table.</p>
<p style="position:absolute;top:624px;left:44px;white-space:nowrap" class="ft67">●</p>
<p style="position:absolute;top:624px;left:67px;white-space:nowrap" class="ft611">Hibernate&#160;works&#160;best&#160;if&#160;these&#160;classes&#160;follow&#160;some&#160;simple&#160;rules,&#160;also&#160;known&#160;as&#160;the&#160;POJO&#160;Programming&#160;<br/>Model.</p>
<p style="position:absolute;top:661px;left:35px;white-space:nowrap" class="ft66">These&#160;are&#160;some&#160;rules&#160;of&#160;persistent&#160;classes;&#160;however,&#160;none&#160;are&#160;hard&#160;requirements:</p>
<p style="position:absolute;top:685px;left:44px;white-space:nowrap" class="ft67">●</p>
<p style="position:absolute;top:685px;left:67px;white-space:nowrap" class="ft66">All&#160;Java&#160;classes&#160;that&#160;will&#160;be&#160;persisted&#160;need&#160;a&#160;default&#160;constructor.</p>
<p style="position:absolute;top:699px;left:44px;white-space:nowrap" class="ft67">●</p>
<p style="position:absolute;top:699px;left:67px;white-space:nowrap" class="ft611">All&#160;classes&#160;should&#160;contain&#160;an&#160;ID&#160;in&#160;order&#160;to&#160;allow&#160;easy&#160;identification&#160;of&#160;objects&#160;within&#160;Hibernate&#160;and&#160;the&#160;<br/>database.&#160;This&#160;property&#160;maps&#160;to&#160;the&#160;primary&#160;key&#160;column&#160;of&#160;a&#160;database&#160;table.</p>
<p style="position:absolute;top:725px;left:44px;white-space:nowrap" class="ft67">●</p>
<p style="position:absolute;top:725px;left:67px;white-space:nowrap" class="ft611">All&#160;attributes&#160;that&#160;will&#160;be&#160;persisted&#160;should&#160;be&#160;declared&#160;private&#160;and&#160;have&#160;getXXX&#160;and&#160;setXXX&#160;methods&#160;<br/>defined&#160;in&#160;the&#160;JavaBean&#160;style.</p>
<p style="position:absolute;top:752px;left:44px;white-space:nowrap" class="ft67">●</p>
<p style="position:absolute;top:752px;left:67px;white-space:nowrap" class="ft611">A&#160;central&#160;feature&#160;of&#160;Hibernate,&#160;called&#160;<i>proxies</i>,&#160;depends&#160;upon&#160;the&#160;persistent&#160;class&#160;being&#160;either&#160;non-final,&#160;or&#160;<br/>the&#160;implementation&#160;of&#160;an&#160;interface&#160;that&#160;declares&#160;all&#160;public&#160;methods.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft70{font-size:6px;font-family:Times;color:#595959;}
	.ft71{font-size:18px;font-family:Times;color:#0079c0;}
	.ft72{font-size:9px;font-family:Times;color:#212121;}
	.ft73{font-size:9px;font-family:Times;color:#273239;}
	.ft74{font-size:9px;font-family:Times;color:#b45f06;}
	.ft75{font-size:9px;font-family:Times;color:#a22b2b;}
	.ft76{font-size:9px;font-family:Times;color:#e69138;}
	.ft77{font-size:8px;font-family:Times;color:#273239;}
	.ft78{font-size:8px;font-family:Times;color:#ff0000;}
	.ft79{font-size:8px;font-family:Times;color:#000000;}
	.ft710{font-size:11px;font-family:Times;color:#000000;}
	.ft711{font-size:9px;font-family:Times;color:#000000;}
	.ft712{font-size:10px;font-family:Times;color:#212121;}
	.ft713{font-size:4px;font-family:Times;color:#3f3f3f;}
	.ft714{font-size:9px;line-height:15px;font-family:Times;color:#212121;}
	.ft715{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page7-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target007.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft70">13</p>
<p style="position:absolute;top:108px;left:25px;white-space:nowrap" class="ft71">Configuration</p>
<p style="position:absolute;top:141px;left:35px;white-space:nowrap" class="ft714">Configuration:&#160;A&#160;class,&#160;which&#160;is&#160;present&#160;in&#160;the&#160;org.hibernate.cfg&#160;package.&#160;The&#160;configuration&#160;object&#160;is&#160;the&#160;first&#160;<br/>Hibernate&#160;object;&#160;it&#160;activates&#160;the&#160;Hibernate&#160;framework&#160;and&#160;reads&#160;both&#160;configuration&#160;files&#160;and&#160;mapping&#160;files</p>
<p style="position:absolute;top:157px;left:573px;white-space:nowrap" class="ft73">.</p>
<p style="position:absolute;top:157px;left:576px;white-space:nowrap" class="ft72">&#160;It&#160;is&#160;</p>
<p style="position:absolute;top:172px;left:35px;white-space:nowrap" class="ft72">usually&#160;created&#160;only&#160;once&#160;during&#160;application&#160;initialization,&#160;and&#160;represents&#160;a&#160;</p>
<p style="position:absolute;top:172px;left:404px;white-space:nowrap" class="ft74"><i>configuration&#160;file</i></p>
<p style="position:absolute;top:172px;left:494px;white-space:nowrap" class="ft72">&#160;(hibernate.cfg.xml)&#160;or&#160;</p>
<p style="position:absolute;top:187px;left:35px;white-space:nowrap" class="ft74"><i>properties&#160;file</i></p>
<p style="position:absolute;top:189px;left:108px;white-space:nowrap" class="ft75">(</p>
<p style="position:absolute;top:187px;left:114px;white-space:nowrap" class="ft72">hibernate.properties</p>
<p style="position:absolute;top:189px;left:213px;white-space:nowrap" class="ft75">).</p>
<p style="position:absolute;top:214px;left:35px;white-space:nowrap" class="ft72">The&#160;configuration&#160;object&#160;provides&#160;two&#160;keys&#160;components:</p>
<p style="position:absolute;top:240px;left:44px;white-space:nowrap" class="ft76">●</p>
<p style="position:absolute;top:240px;left:67px;white-space:nowrap" class="ft714">Database&#160;Connection&#160;−&#160;Handled&#160;through&#160;one&#160;or&#160;more&#160;configuration&#160;files&#160;supported&#160;by&#160;Hibernate;&#160;these&#160;files&#160;are&#160;<br/>hibernate.properties&#160;or&#160;hibernate.cfg.xml.</p>
<p style="position:absolute;top:270px;left:44px;white-space:nowrap" class="ft76">●</p>
<p style="position:absolute;top:270px;left:67px;white-space:nowrap" class="ft72">Class&#160;Mapping&#160;Setup&#160;−&#160;Creates&#160;the&#160;connection&#160;between&#160;the&#160;Java&#160;classes&#160;and&#160;database&#160;tables.</p>
<p style="position:absolute;top:297px;left:67px;white-space:nowrap" class="ft77">Configuration cfg = new Configuration();&#160;</p>
<p style="position:absolute;top:297px;left:308px;white-space:nowrap" class="ft78"><i>&#160;//This line activate Hibernate Framework</i></p>
<p style="position:absolute;top:319px;left:67px;white-space:nowrap" class="ft77">cfg.configure();&#160;</p>
<p style="position:absolute;top:319px;left:167px;white-space:nowrap" class="ft78"><i>//&#160;read both cfg file and mapping files</i></p>
<p style="position:absolute;top:347px;left:43px;white-space:nowrap" class="ft79">The&#160;configuration&#160;object&#160;is&#160;used&#160;to&#160;create&#160;a&#160;SessionFactory&#160;object&#160;in&#160;Hibernate.</p>
<p style="position:absolute;top:378px;left:153px;white-space:nowrap" class="ft710">&#160; &#160; &#160; &#160;&#160;</p>
<p style="position:absolute;top:379px;left:185px;white-space:nowrap" class="ft711">SessionFactory&#160;factory=cfg.buildSessionFactory();&#160;&#160;</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft70">14</p>
<p style="position:absolute;top:523px;left:25px;white-space:nowrap" class="ft71">Hibernate&#160;Configuration&#160;Files</p>
<p style="position:absolute;top:558px;left:37px;white-space:nowrap" class="ft715">Database&#160;details&#160;and&#160;mapping&#160;file&#160;details&#160;are&#160;specified&#160;in&#160;hibernate.cfg.xml&#160;file.&#160;The&#160;Hibernate&#160;engine&#160;<br/>uses&#160;these&#160;files&#160;to&#160;generate&#160;Database&#160;specific&#160;SQL&#160;syntax.</p>
<p style="position:absolute;top:747px;left:445px;white-space:nowrap" class="ft713">&#160;image&#160;source:&#160;https://learnjava.co.in</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft80{font-size:6px;font-family:Times;color:#595959;}
	.ft81{font-size:18px;font-family:Times;color:#0079c0;}
	.ft82{font-size:6px;font-family:Times;color:#e8bf6a;}
	.ft83{font-size:6px;font-family:Times;color:#bababa;}
	.ft84{font-size:6px;font-family:Times;color:#6a8759;}
	.ft85{font-size:6px;font-family:Times;color:#a9b7c6;}
	.ft86{font-size:12px;font-family:Times;color:#212121;}
	.ft87{font-size:20px;font-family:Times;color:#0079c0;}
	.ft88{font-size:11px;font-family:Times;color:#000000;}
	.ft89{font-size:11px;font-family:Times;color:#ffab40;}
	.ft810{font-size:10px;font-family:Times;color:#333333;}
	.ft811{font-size:6px;line-height:11px;font-family:Times;color:#e8bf6a;}
	.ft812{font-size:11px;line-height:16px;font-family:Times;color:#000000;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page8-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target008.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft80">15</p>
<p style="position:absolute;top:105px;left:25px;white-space:nowrap" class="ft81">Hibernate&#160;Configuration&#160;Files&#160;(continued)</p>
<p style="position:absolute;top:194px;left:30px;white-space:nowrap" class="ft82">&lt;?</p>
<p style="position:absolute;top:194px;left:39px;white-space:nowrap" class="ft83">xml version</p>
<p style="position:absolute;top:194px;left:88px;white-space:nowrap" class="ft84">=&#34;1.0&#34;&#160;</p>
<p style="position:absolute;top:194px;left:119px;white-space:nowrap" class="ft83">encoding</p>
<p style="position:absolute;top:194px;left:154px;white-space:nowrap" class="ft84">=&#34;UTF-8&#34;</p>
<p style="position:absolute;top:194px;left:189px;white-space:nowrap" class="ft82">?&gt;&lt;!DOCTYPE&#160;</p>
<p style="position:absolute;top:194px;left:242px;white-space:nowrap" class="ft83">hibernate-configuration&#160;</p>
<p style="position:absolute;top:194px;left:348px;white-space:nowrap" class="ft82">PUBLIC&#160;</p>
<p style="position:absolute;top:194px;left:379px;white-space:nowrap" class="ft84">&#34;-//Hibernate/Hibernate Configuration DTD 5.3//EN&#34;&#160;</p>
<p style="position:absolute;top:205px;left:30px;white-space:nowrap" class="ft84">http://www.hibernate.org/dtd/hibernate-configuration-5.3.dtd&#34;</p>
<p style="position:absolute;top:205px;left:300px;white-space:nowrap" class="ft82">&gt;</p>
<p style="position:absolute;top:217px;left:30px;white-space:nowrap" class="ft811">&lt;hibernate-configuration&gt; &#160; &lt;session-factory&gt;<br/>&#160; &#160; &#160; &#160;&lt;property&#160;</p>
<p style="position:absolute;top:228px;left:106px;white-space:nowrap" class="ft83">name</p>
<p style="position:absolute;top:228px;left:123px;white-space:nowrap" class="ft84">=&#34;hibernate.hbm2ddl.auto&#34;</p>
<p style="position:absolute;top:228px;left:234px;white-space:nowrap" class="ft82">&gt;&#160;</p>
<p style="position:absolute;top:228px;left:242px;white-space:nowrap" class="ft85">update&#160;</p>
<p style="position:absolute;top:228px;left:273px;white-space:nowrap" class="ft82">&lt;/property&gt;</p>
<p style="position:absolute;top:239px;left:30px;white-space:nowrap" class="ft82">&#160; &#160; &#160; &#160;&lt;property&#160;</p>
<p style="position:absolute;top:239px;left:106px;white-space:nowrap" class="ft83">name</p>
<p style="position:absolute;top:239px;left:123px;white-space:nowrap" class="ft84">=&#34;connection.driver_class&#34;</p>
<p style="position:absolute;top:239px;left:238px;white-space:nowrap" class="ft82">&gt;</p>
<p style="position:absolute;top:239px;left:242px;white-space:nowrap" class="ft85">com.mysql.cj.jdbc.Driver</p>
<p style="position:absolute;top:239px;left:348px;white-space:nowrap" class="ft82">&lt;/property&gt;</p>
<p style="position:absolute;top:250px;left:30px;white-space:nowrap" class="ft82">&#160; &#160; &#160; &#160;&lt;property&#160;</p>
<p style="position:absolute;top:250px;left:106px;white-space:nowrap" class="ft83">name</p>
<p style="position:absolute;top:250px;left:123px;white-space:nowrap" class="ft84">=&#34;connection.url&#34;</p>
<p style="position:absolute;top:250px;left:198px;white-space:nowrap" class="ft82">&gt;</p>
<p style="position:absolute;top:250px;left:203px;white-space:nowrap" class="ft85">jdbc:mysql://localhost:3306/databasename</p>
<p style="position:absolute;top:250px;left:379px;white-space:nowrap" class="ft82">&lt;/property&gt;</p>
<p style="position:absolute;top:261px;left:30px;white-space:nowrap" class="ft82">&#160; &#160; &#160; &#160;&lt;property&#160;</p>
<p style="position:absolute;top:261px;left:106px;white-space:nowrap" class="ft83">name</p>
<p style="position:absolute;top:261px;left:123px;white-space:nowrap" class="ft84">=&#34;connection.username&#34;</p>
<p style="position:absolute;top:261px;left:220px;white-space:nowrap" class="ft82">&gt;</p>
<p style="position:absolute;top:261px;left:225px;white-space:nowrap" class="ft85">root</p>
<p style="position:absolute;top:261px;left:242px;white-space:nowrap" class="ft82">&lt;/property&gt;</p>
<p style="position:absolute;top:272px;left:30px;white-space:nowrap" class="ft82">&#160; &#160; &#160; &#160;&lt;property&#160;</p>
<p style="position:absolute;top:272px;left:106px;white-space:nowrap" class="ft83">name</p>
<p style="position:absolute;top:272px;left:123px;white-space:nowrap" class="ft84">=&#34;connection.password&#34;</p>
<p style="position:absolute;top:272px;left:220px;white-space:nowrap" class="ft82">&gt;</p>
<p style="position:absolute;top:272px;left:225px;white-space:nowrap" class="ft85">password</p>
<p style="position:absolute;top:272px;left:260px;white-space:nowrap" class="ft82">&lt;/property&gt;</p>
<p style="position:absolute;top:283px;left:30px;white-space:nowrap" class="ft82">&#160; &#160; &#160; &#160;&lt;property&#160;</p>
<p style="position:absolute;top:283px;left:106px;white-space:nowrap" class="ft83">name</p>
<p style="position:absolute;top:283px;left:123px;white-space:nowrap" class="ft84">=&#34;dialect&#34;</p>
<p style="position:absolute;top:283px;left:167px;white-space:nowrap" class="ft82">&gt;</p>
<p style="position:absolute;top:283px;left:172px;white-space:nowrap" class="ft85">org.hibernate.dialect.MySQL5Dialect</p>
<p style="position:absolute;top:283px;left:326px;white-space:nowrap" class="ft82">&lt;/property&gt;</p>
<p style="position:absolute;top:294px;left:30px;white-space:nowrap" class="ft82">&#160; &#160; &#160; &#160;&lt;property&#160;</p>
<p style="position:absolute;top:294px;left:106px;white-space:nowrap" class="ft83">name</p>
<p style="position:absolute;top:294px;left:123px;white-space:nowrap" class="ft84">=&#34;hibernate.show_sql&#34;&#160;</p>
<p style="position:absolute;top:294px;left:220px;white-space:nowrap" class="ft82">&gt;</p>
<p style="position:absolute;top:294px;left:225px;white-space:nowrap" class="ft85">true&#160;</p>
<p style="position:absolute;top:294px;left:247px;white-space:nowrap" class="ft82">&lt;/property&gt;</p>
<p style="position:absolute;top:305px;left:30px;white-space:nowrap" class="ft82">&#160; &#160; &#160; &#160;&lt;property&#160;</p>
<p style="position:absolute;top:305px;left:106px;white-space:nowrap" class="ft83">name</p>
<p style="position:absolute;top:305px;left:123px;white-space:nowrap" class="ft84">=&#34;hibernate.format_sql&#34;&#160;</p>
<p style="position:absolute;top:305px;left:229px;white-space:nowrap" class="ft82">&gt;</p>
<p style="position:absolute;top:305px;left:234px;white-space:nowrap" class="ft85">true&#160;</p>
<p style="position:absolute;top:305px;left:256px;white-space:nowrap" class="ft82">&lt;/property&gt;</p>
<p style="position:absolute;top:316px;left:30px;white-space:nowrap" class="ft82">&#160; &#160; &#160; &#160;&lt;mapping&#160;</p>
<p style="position:absolute;top:316px;left:101px;white-space:nowrap" class="ft83">class</p>
<p style="position:absolute;top:316px;left:123px;white-space:nowrap" class="ft84">=&#34;com.test.hib.model.User&#34;</p>
<p style="position:absolute;top:316px;left:238px;white-space:nowrap" class="ft82">/&gt;</p>
<p style="position:absolute;top:327px;left:30px;white-space:nowrap" class="ft82">&#160; &#160; &#160; &#160;&lt;mapping&#160;</p>
<p style="position:absolute;top:327px;left:101px;white-space:nowrap" class="ft83">class</p>
<p style="position:absolute;top:327px;left:123px;white-space:nowrap" class="ft84">=&#34;com.perscholas.model.Employee&#34;</p>
<p style="position:absolute;top:327px;left:265px;white-space:nowrap" class="ft82">/&gt;&#160; &#160; &#160;&#160;</p>
<p style="position:absolute;top:338px;left:30px;white-space:nowrap" class="ft811">&#160; &#160;&lt;/session-factory&gt;<br/>&lt;/hibernate-configuration&gt;</p>
<p style="position:absolute;top:156px;left:37px;white-space:nowrap" class="ft86">Example&#160;-&#160;hibernate.cfg.xml</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft80">16</p>
<p style="position:absolute;top:527px;left:28px;white-space:nowrap" class="ft87">Specify&#160;Exact&#160;Database&#160;Dialect</p>
<p style="position:absolute;top:569px;left:39px;white-space:nowrap" class="ft812">Hibernate&#160;uses&#160;the&#160;SQL&#160;syntax&#160;of&#160;the&#160;target&#160;database&#160;to&#160;create&#160;tables&#160;accordingly;&#160;therefore,&#160;<br/>it&#160;is&#160;important&#160;to&#160;specify&#160;database&#160;dialect&#160;information&#160;exactly&#160;to&#160;match&#160;the&#160;type&#160;of&#160;underlying&#160;<br/>database.&#160;Otherwise,&#160;you&#160;will&#160;get&#160;an&#160;error&#160;or&#160;an&#160;undesired&#160;behavior.</p>
<p style="position:absolute;top:626px;left:39px;white-space:nowrap" class="ft89">❏</p>
<p style="position:absolute;top:626px;left:64px;white-space:nowrap" class="ft88">Example&#160;for&#160;MySQL&#160;database:</p>
<p style="position:absolute;top:705px;left:59px;white-space:nowrap" class="ft810">&lt;property name=&#34;dialect&#34;&gt;org.hibernate.dialect.MySQL5Dialect&lt;/property&gt;</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft90{font-size:6px;font-family:Times;color:#595959;}
	.ft91{font-size:20px;font-family:Times;color:#0079c0;}
	.ft92{font-size:10px;font-family:Times;color:#212121;}
	.ft93{font-size:10px;font-family:Times;color:#ffab40;}
	.ft94{font-size:8px;font-family:Times;color:#273239;}
	.ft95{font-size:8px;font-family:Times;color:#273239;}
	.ft96{font-size:18px;font-family:Times;color:#0079c0;}
	.ft97{font-size:10px;font-family:Times;color:#e69138;}
	.ft98{font-size:10px;font-family:Times;color:#000000;}
	.ft99{font-size:8px;font-family:Times;color:#000000;}
	.ft910{font-size:5px;font-family:Times;color:#b45f06;}
	.ft911{font-size:6px;font-family:Times;color:#b45f06;}
	.ft912{font-size:10px;line-height:14px;font-family:Times;color:#212121;}
	.ft913{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page9-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target009.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft90">17</p>
<p style="position:absolute;top:108px;left:26px;white-space:nowrap" class="ft91">Hibernate&#160;Configuration&#160;Property</p>
<p style="position:absolute;top:147px;left:39px;white-space:nowrap" class="ft912">To&#160;use&#160;Hibernate&#160;Forward&#160;Engineering,&#160;you&#160;need&#160;to&#160;specify&#160;the&#160;hibernate.hbm2ddl.auto&#160;property&#160;in&#160;<br/>the&#160;Hibernate&#160;configuration&#160;file.&#160;The&#160;“hibernate.hbm2ddl.auto”&#160;property&#160;accepts&#160;the&#160;following&#160;<br/>values:</p>
<p style="position:absolute;top:194px;left:64px;white-space:nowrap" class="ft92">●</p>
<p style="position:absolute;top:194px;left:88px;white-space:nowrap" class="ft93">create:</p>
<p style="position:absolute;top:194px;left:128px;white-space:nowrap" class="ft92">&#160;If&#160;the&#160;value&#160;is&#160;created,&#160;Hibernate&#160;creates&#160;a&#160;new&#160;table&#160;in&#160;the&#160;database&#160;when&#160;the&#160;</p>
<p style="position:absolute;top:209px;left:88px;white-space:nowrap" class="ft912">SessionFactory&#160;object&#160;is&#160;created.&#160;If&#160;a&#160;table&#160;exists&#160;in&#160;the&#160;database&#160;with&#160;the&#160;same&#160;name,&#160;it&#160;<br/>deletes&#160;the&#160;table&#160;along&#160;with&#160;the&#160;data&#160;and&#160;creates&#160;a&#160;new&#160;table.</p>
<p style="position:absolute;top:241px;left:64px;white-space:nowrap" class="ft92">●</p>
<p style="position:absolute;top:241px;left:88px;white-space:nowrap" class="ft93">update:</p>
<p style="position:absolute;top:241px;left:133px;white-space:nowrap" class="ft92">&#160;If&#160;the&#160;value&#160;is&#160;updated,&#160;Hibernate&#160;first&#160;validates&#160;whether&#160;the&#160;table&#160;is&#160;present&#160;in&#160;the&#160;</p>
<p style="position:absolute;top:256px;left:88px;white-space:nowrap" class="ft912">database&#160;or&#160;not.&#160;If&#160;it&#160;is&#160;present,&#160;Hibernate&#160;alters&#160;that&#160;table&#160;per&#160;the&#160;changes;&#160;if&#160;it&#160;is&#160;not&#160;present,&#160;<br/>Hibernate&#160;creates&#160;a&#160;new&#160;one.</p>
<p style="position:absolute;top:288px;left:64px;white-space:nowrap" class="ft92">●</p>
<p style="position:absolute;top:288px;left:88px;white-space:nowrap" class="ft93">validate:</p>
<p style="position:absolute;top:288px;left:138px;white-space:nowrap" class="ft92">&#160;If&#160;the&#160;value&#160;is&#160;validated,&#160;Hibernate&#160;only&#160;verifies&#160;whether&#160;the&#160;table&#160;is&#160;present.&#160;If&#160;the&#160;</p>
<p style="position:absolute;top:303px;left:88px;white-space:nowrap" class="ft92">table&#160;does&#160;not&#160;exist,&#160;Hibernate&#160;throws&#160;an&#160;exception.</p>
<p style="position:absolute;top:319px;left:64px;white-space:nowrap" class="ft92">●</p>
<p style="position:absolute;top:319px;left:88px;white-space:nowrap" class="ft93">create-drop:</p>
<p style="position:absolute;top:319px;left:160px;white-space:nowrap" class="ft92">&#160;If&#160;the&#160;value&#160;is&#160;create-drop,&#160;Hibernate&#160;creates&#160;a&#160;new&#160;table&#160;when&#160;SessionFactory&#160;</p>
<p style="position:absolute;top:334px;left:88px;white-space:nowrap" class="ft912">is&#160;created,&#160;performs&#160;the&#160;operation,&#160;and&#160;deletes&#160;the&#160;table&#160;when&#160;SessionFactory&#160;is&#160;destroyed.&#160;<br/>This&#160;value&#160;is&#160;used&#160;for&#160;testing&#160;the&#160;Hibernate&#160;code.</p>
<p style="position:absolute;top:366px;left:64px;white-space:nowrap" class="ft92">●</p>
<p style="position:absolute;top:366px;left:88px;white-space:nowrap" class="ft93">none:&#160;</p>
<p style="position:absolute;top:366px;left:125px;white-space:nowrap" class="ft92">It&#160;does&#160;not&#160;make&#160;any&#160;changes&#160;to&#160;the&#160;schema.</p>
<p style="position:absolute;top:398px;left:147px;white-space:nowrap" class="ft94">Example:&#160;<i>&lt;property name=”hibernate.hbm2ddl.auto”&gt;create&lt;/property&gt;</i></p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft90">18</p>
<p style="position:absolute;top:526px;left:26px;white-space:nowrap" class="ft96">Session&#160;Factory&#160;in&#160;Hibernate</p>
<p style="position:absolute;top:564px;left:36px;white-space:nowrap" class="ft92">SessionFactory()&#160;Object:</p>
<p style="position:absolute;top:589px;left:44px;white-space:nowrap" class="ft97">●</p>
<p style="position:absolute;top:589px;left:68px;white-space:nowrap" class="ft92">SessionFactory&#160;is&#160;an&#160;interface,&#160;which&#160;is&#160;present&#160;in&#160;the&#160;org.hibernate&#160;package.&#160;</p>
<p style="position:absolute;top:616px;left:44px;white-space:nowrap" class="ft97">●</p>
<p style="position:absolute;top:616px;left:68px;white-space:nowrap" class="ft913">Configuration&#160;object&#160;is&#160;used&#160;to&#160;create&#160;a&#160;SessionFactory&#160;object.&#160;The&#160;SessionFactory&#160;is&#160;a&#160;<br/>thread-safe&#160;object&#160;and&#160;is&#160;used&#160;by&#160;all&#160;of&#160;the&#160;threads&#160;of&#160;an&#160;application.</p>
<p style="position:absolute;top:659px;left:44px;white-space:nowrap" class="ft97">●</p>
<p style="position:absolute;top:659px;left:68px;white-space:nowrap" class="ft913">It&#160;is&#160;usually&#160;created&#160;during&#160;application&#160;start-up&#160;and&#160;kept&#160;for&#160;later&#160;use.&#160;We&#160;would&#160;need&#160;one&#160;<br/>SessionFactory&#160;object&#160;per&#160;database,&#160;using&#160;a&#160;separate&#160;configuration&#160;file.&#160;So,&#160;if&#160;you&#160;are&#160;using&#160;multiple&#160;<br/>databases,&#160;you&#160;would&#160;have&#160;to&#160;create&#160;multiple&#160;SessionFactory&#160;objects.&#160;</p>
<p style="position:absolute;top:720px;left:44px;white-space:nowrap" class="ft97">●</p>
<p style="position:absolute;top:720px;left:68px;white-space:nowrap" class="ft92">The&#160;SessionFactory&#160;in&#160;Hibernate&#160;is&#160;responsible&#160;for&#160;the&#160;creation&#160;of&#160;Session&#160;objects.</p>
<p style="position:absolute;top:754px;left:87px;white-space:nowrap" class="ft98">&#160; &#160; &#160; &#160;&#160;</p>
<p style="position:absolute;top:756px;left:119px;white-space:nowrap" class="ft99"><i>SessionFactory&#160;factory=cfg.buildSessionFactory();&#160;&#160;</i></p>
<p style="position:absolute;top:770px;left:87px;white-space:nowrap" class="ft910"><i>//&#160;</i></p>
<p style="position:absolute;top:768px;left:119px;white-space:nowrap" class="ft911"><i>buildSessionFactory()&#160;method&#160;gathers&#160;the&#160;meta-data&#160;which&#160;is&#160;in&#160;the&#160;cfg&#160;Object.&#160;</i></p>
<p style="position:absolute;top:779px;left:87px;white-space:nowrap" class="ft911"><i>&#160; &#160;&#160;</i></p>
<p style="position:absolute;top:779px;left:119px;white-space:nowrap" class="ft911"><i>From&#160;cfg&#160;object&#160;it&#160;takes&#160;the&#160;JDBC&#160;information&#160;and&#160;create&#160;a&#160;JDBC&#160;Connection.</i></p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft100{font-size:6px;font-family:Times;color:#595959;}
	.ft101{font-size:18px;font-family:Times;color:#0079c0;}
	.ft102{font-size:11px;font-family:Times;color:#212121;}
	.ft103{font-size:10px;font-family:Times;color:#e69138;}
	.ft104{font-size:10px;font-family:Times;color:#212121;}
	.ft105{font-size:10px;font-family:Times;color:#212121;}
	.ft106{font-size:9px;font-family:Times;color:#0d6efd;}
	.ft107{font-size:9px;font-family:Times;color:#212121;}
	.ft108{font-size:8px;font-family:Times;color:#273239;}
	.ft109{font-size:20px;font-family:Times;color:#0079c0;}
	.ft1010{font-size:5px;font-family:Times;color:#666666;}
	.ft1011{font-size:10px;font-family:Times;color:#000000;}
	.ft1012{font-size:10px;line-height:14px;font-family:Times;color:#212121;}
	.ft1013{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page10-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target010.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft100">19</p>
<p style="position:absolute;top:107px;left:25px;white-space:nowrap" class="ft101">Session&#160;in&#160;Hibernate</p>
<p style="position:absolute;top:140px;left:37px;white-space:nowrap" class="ft102">Session()&#160;Object:</p>
<p style="position:absolute;top:163px;left:46px;white-space:nowrap" class="ft103">●</p>
<p style="position:absolute;top:163px;left:69px;white-space:nowrap" class="ft1012">Session&#160;is&#160;an&#160;interface,&#160;which&#160;is&#160;present&#160;in&#160;the&#160;org.hibernate&#160;package.&#160;A&#160;Session&#160;is&#160;used&#160;to&#160;get&#160;<br/>a&#160;physical&#160;connection&#160;with&#160;a&#160;database.&#160;A&#160;Session&#160;object&#160;is&#160;created&#160;based&#160;upon&#160;a&#160;<br/><i>SessionFactory()&#160;</i>object.</p>
<p style="position:absolute;top:216px;left:46px;white-space:nowrap" class="ft103">●</p>
<p style="position:absolute;top:216px;left:69px;white-space:nowrap" class="ft104">A&#160;Session()&#160;is&#160;designed&#160;to&#160;be&#160;instantiated&#160;each&#160;time&#160;an&#160;interaction&#160;is&#160;needed&#160;with&#160;the&#160;database.</p>
<p style="position:absolute;top:238px;left:46px;white-space:nowrap" class="ft103">●</p>
<p style="position:absolute;top:238px;left:69px;white-space:nowrap" class="ft1012">The&#160;Session&#160;objects&#160;should&#160;not&#160;be&#160;kept&#160;open&#160;for&#160;a&#160;long&#160;time&#160;because&#160;they&#160;are&#160;not&#160;usually&#160;<br/>thread-safe,&#160;and&#160;they&#160;should&#160;be&#160;created&#160;and&#160;destroyed&#160;as&#160;needed.</p>
<p style="position:absolute;top:275px;left:46px;white-space:nowrap" class="ft103">●</p>
<p style="position:absolute;top:275px;left:69px;white-space:nowrap" class="ft1012">A&#160;Session&#160;object&#160;is&#160;used&#160;to&#160;perform&#160;Create,&#160;Read,&#160;Update,&#160;and&#160;Delete(CRUD)&#160;operations&#160;for&#160;<br/>instances&#160;of&#160;mapped&#160;entity&#160;classes.&#160;Instances&#160;may&#160;exist&#160;in&#160;one&#160;of&#160;three&#160;states:</p>
<p style="position:absolute;top:312px;left:62px;white-space:nowrap" class="ft106">○</p>
<p style="position:absolute;top:312px;left:85px;white-space:nowrap" class="ft107">Transient:&#160;never&#160;persistent;&#160;not&#160;associated&#160;with&#160;any&#160;Session.</p>
<p style="position:absolute;top:333px;left:62px;white-space:nowrap" class="ft106">○</p>
<p style="position:absolute;top:333px;left:85px;white-space:nowrap" class="ft107">Persistent:&#160;associated&#160;with&#160;a&#160;unique&#160;Session.</p>
<p style="position:absolute;top:354px;left:62px;white-space:nowrap" class="ft106">○</p>
<p style="position:absolute;top:354px;left:85px;white-space:nowrap" class="ft107">Detached:&#160;previously&#160;persistent;&#160;not&#160;associated&#160;with&#160;any&#160;Session.</p>
<p style="position:absolute;top:392px;left:273px;white-space:nowrap" class="ft108">Session session = factory.buildSession();</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft100">20</p>
<p style="position:absolute;top:528px;left:22px;white-space:nowrap" class="ft101">Session&#160;in&#160;Hibernate&#160;</p>
<p style="position:absolute;top:526px;left:230px;white-space:nowrap" class="ft109">(continued)</p>
<p style="position:absolute;top:805px;left:298px;white-space:nowrap" class="ft1010">image&#160;source:&#160;www.baeldung.com/</p>
<p style="position:absolute;top:607px;left:269px;white-space:nowrap" class="ft1011">Session&#160;States&#160;Life&#160;Cycle</p>
<p style="position:absolute;top:558px;left:30px;white-space:nowrap" class="ft1013">Here&#160;is&#160;a&#160;simplified&#160;state&#160;diagram&#160;with&#160;comments&#160;on&#160;Session&#160;methods&#160;that&#160;make&#160;the&#160;state&#160;transitions&#160;<br/>happen:</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft110{font-size:6px;font-family:Times;color:#595959;}
	.ft111{font-size:18px;font-family:Times;color:#0079c0;}
	.ft112{font-size:12px;font-family:Times;color:#212121;}
	.ft113{font-size:12px;font-family:Times;color:#e69138;}
	.ft114{font-size:10px;font-family:Times;color:#212121;}
	.ft115{font-size:10px;font-family:Times;color:#212121;}
	.ft116{font-size:10px;font-family:Times;color:#232323;}
	.ft117{font-size:8px;font-family:Times;color:#292929;}
	.ft118{font-size:10px;font-family:Times;color:#000000;}
	.ft119{font-size:10px;font-family:Times;color:#292929;}
	.ft1110{font-size:9px;font-family:Times;color:#212121;}
	.ft1111{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
	.ft1112{font-size:8px;line-height:12px;font-family:Times;color:#292929;}
	.ft1113{font-size:9px;line-height:12px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page11-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target011.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft110">21</p>
<p style="position:absolute;top:112px;left:28px;white-space:nowrap" class="ft111">Important&#160;“Session&#160;Methods”</p>
<p style="position:absolute;top:152px;left:30px;white-space:nowrap" class="ft112">The&#160;Session&#160;Interface&#160;has&#160;several&#160;methods&#160;that&#160;save&#160;the&#160;objects&#160;to&#160;the&#160;database:</p>
<p style="position:absolute;top:183px;left:38px;white-space:nowrap" class="ft113">●</p>
<p style="position:absolute;top:183px;left:62px;white-space:nowrap" class="ft112">persist()&#160;or&#160;save()&#160;</p>
<p style="position:absolute;top:202px;left:38px;white-space:nowrap" class="ft113">●</p>
<p style="position:absolute;top:202px;left:62px;white-space:nowrap" class="ft112">merge()&#160;or&#160;update()</p>
<p style="position:absolute;top:222px;left:38px;white-space:nowrap" class="ft113">●</p>
<p style="position:absolute;top:222px;left:62px;white-space:nowrap" class="ft112">Remove()&#160;or&#160;Delete()</p>
<p style="position:absolute;top:242px;left:38px;white-space:nowrap" class="ft113">●</p>
<p style="position:absolute;top:242px;left:62px;white-space:nowrap" class="ft112">SaveOrUpdate()</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft110">22</p>
<p style="position:absolute;top:527px;left:27px;white-space:nowrap" class="ft111">session.persist()&#160;method</p>
<p style="position:absolute;top:568px;left:30px;white-space:nowrap" class="ft1111">The&#160;<i>session.persist()</i>&#160;method&#160;is&#160;used&#160;to&#160;add&#160;a&#160;new&#160;entity&#160;instance&#160;to&#160;the&#160;persistence&#160;context,&#160;(i.e.&#160;<br/>transitioning&#160;an&#160;instance&#160;from&#160;a&#160;<i>transient</i>&#160;to&#160;a&#160;<i>persistent</i>&#160;state&#160;</p>
<p style="position:absolute;top:585px;left:371px;white-space:nowrap" class="ft116">when&#160;the&#160;transaction&#160;is&#160;committed).</p>
<p style="position:absolute;top:622px;left:32px;white-space:nowrap" class="ft1112">TestEntity&#160;testEntity = new&#160;TestEntity(&#34;Hello&#34;);<br/>session.persist(testEntity);</p>
<p style="position:absolute;top:621px;left:335px;white-space:nowrap" class="ft118">This&#160;statement&#160;makes&#160;the&#160;</p>
<p style="position:absolute;top:621px;left:481px;white-space:nowrap" class="ft119">testEntity&#160;</p>
<p style="position:absolute;top:636px;left:335px;white-space:nowrap" class="ft118">object&#160;a&#160;Persistent&#160;one.</p>
<p style="position:absolute;top:702px;left:36px;white-space:nowrap" class="ft1112">User&#160;hellouser = new&#160;User();<br/>hellouser.setName(&#34;ABC&#34;);<br/>session.persist(hellouser);</p>
<p style="position:absolute;top:707px;left:256px;white-space:nowrap" class="ft119">This&#160;statement&#160;persists&#160;the&#160;hellouser object&#160;to&#160;the&#160;</p>
<p style="position:absolute;top:722px;left:256px;white-space:nowrap" class="ft119">database.</p>
<p style="position:absolute;top:765px;left:109px;white-space:nowrap" class="ft1110">●</p>
<p style="position:absolute;top:767px;left:132px;white-space:nowrap" class="ft1110">persist()</p>
<p style="position:absolute;top:763px;left:189px;white-space:nowrap" class="ft1110">&#160;method&#160;INSERT&#160;records&#160;into&#160;database&#160;but&#160;return&#160;type&#160;of&#160;persist&#160;is&#160;void.</p>
<p style="position:absolute;top:779px;left:109px;white-space:nowrap" class="ft1110">●</p>
<p style="position:absolute;top:781px;left:132px;white-space:nowrap" class="ft1113">persist()&#160;method&#160;guarantees&#160;that&#160;it&#160;will&#160;not&#160;execute&#160;an&#160;INSERT&#160;statement&#160;if&#160;it&#160;is&#160;called&#160;<br/>outside&#160;of&#160;transaction&#160;boundaries.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft120{font-size:6px;font-family:Times;color:#595959;}
	.ft121{font-size:20px;font-family:Times;color:#0079c0;}
	.ft122{font-size:12px;font-family:Times;color:#e69138;}
	.ft123{font-size:10px;font-family:Times;color:#212121;}
	.ft124{font-size:8px;font-family:Times;color:#980000;}
	.ft125{font-size:9px;font-family:Times;color:#212121;}
	.ft126{font-size:13px;font-family:Times;color:#e69138;}
	.ft127{font-size:11px;font-family:Times;color:#ffab40;}
	.ft128{font-size:8px;font-family:Times;color:#292929;}
	.ft129{font-size:8px;font-family:Times;color:#000000;}
	.ft1210{font-size:8px;font-family:Times;color:#000000;}
	.ft1211{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
	.ft1212{font-size:10px;line-height:16px;font-family:Times;color:#212121;}
	.ft1213{font-size:8px;line-height:14px;font-family:Times;color:#980000;}
	.ft1214{font-size:9px;line-height:13px;font-family:Times;color:#212121;}
	.ft1215{font-size:8px;line-height:12px;font-family:Times;color:#292929;}
	.ft1216{font-size:8px;line-height:12px;font-family:Times;color:#000000;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page12-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target012.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft120">23</p>
<p style="position:absolute;top:103px;left:25px;white-space:nowrap" class="ft121">session.save()&#160;method</p>
<p style="position:absolute;top:153px;left:32px;white-space:nowrap" class="ft122">❖</p>
<p style="position:absolute;top:154px;left:62px;white-space:nowrap" class="ft1212">The&#160;session.save()&#160;method&#160;saves&#160;the&#160;transient&#160;entity&#160;and&#160;INSERT&#160;record&#160;into&#160;a&#160;database&#160;when&#160;the&#160;<br/>transaction&#160;is&#160;committed.&#160;This&#160;method&#160;is&#160;used&#160;to&#160;bring&#160;only&#160;a&#160;transient&#160;object&#160;to&#160;a&#160;persistent&#160;state.&#160;<br/>There&#160;are&#160;two&#160;overloading&#160;functions&#160;of&#160;this&#160;method:</p>
<p style="position:absolute;top:204px;left:50px;white-space:nowrap" class="ft122">●</p>
<p style="position:absolute;top:206px;left:75px;white-space:nowrap" class="ft1211">save(Object&#160;object):&#160;Generates&#160;a&#160;new&#160;identifier&#160;and&#160;INSERT&#160;record&#160;into&#160;a&#160;database.&#160;The&#160;<br/>insertion&#160;fails&#160;if&#160;the&#160;primary&#160;key&#160;already&#160;exists&#160;in&#160;the&#160;table.</p>
<p style="position:absolute;top:248px;left:50px;white-space:nowrap" class="ft122">●</p>
<p style="position:absolute;top:250px;left:75px;white-space:nowrap" class="ft123">save(String&#160;entityName,&#160;Object&#160;object):&#160;Accepts&#160;the&#160;entity&#160;name&#160;and&#160;instance&#160;of&#160;entity.</p>
<p style="position:absolute;top:277px;left:32px;white-space:nowrap" class="ft122">❖</p>
<p style="position:absolute;top:278px;left:62px;white-space:nowrap" class="ft1211">The&#160;purpose&#160;of&#160;the&#160;save()&#160;method&#160;is&#160;the&#160;same&#160;as&#160;the&#160;persist()&#160;method,&#160;but&#160;it&#160;has&#160;different&#160;<br/>implementation&#160;details.&#160;The&#160;save&#160;method&#160;always&#160;persists&#160;the&#160;instance&#160;to&#160;the&#160;database&#160;by&#160;<br/>generating&#160;a&#160;unique&#160;identifier.</p>
<p style="position:absolute;top:348px;left:165px;white-space:nowrap" class="ft1213">Note:&#160;Save()&#160;method&#160;is&#160;deprecated.&#160;Save()&#160;is&#160;no&#160;longer&#160;used;&#160;instead,&#160;we&#160;should&#160;<br/>use&#160;the&#160;persist()&#160;method.&#160;However,&#160;The&#160;save()&#160;method&#160;could&#160;be&#160;important&#160;for&#160;an&#160;<br/>interview&#160;because&#160;legacy&#160;applications&#160;still&#160;use&#160;it.</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft120">24</p>
<p style="position:absolute;top:525px;left:26px;white-space:nowrap" class="ft121">Session.merge()&#160;method</p>
<p style="position:absolute;top:559px;left:31px;white-space:nowrap" class="ft1214">The&#160;main&#160;intention&#160;of&#160;the&#160;merge()&#160;method&#160;is&#160;to&#160;update&#160;a&#160;persistent&#160;entity&#160;instance&#160;with&#160;new&#160;field&#160;values&#160;from&#160;a&#160;<br/>detached&#160;entity&#160;instance.</p>
<p style="position:absolute;top:594px;left:38px;white-space:nowrap" class="ft126">●</p>
<p style="position:absolute;top:598px;left:63px;white-space:nowrap" class="ft125">The&#160;merge()&#160;method&#160;is&#160;used&#160;to&#160;merge&#160;an&#160;object&#160;with&#160;a&#160;persistent&#160;object&#160;on&#160;the&#160;basis&#160;of&#160;the&#160;same&#160;identifier.&#160;</p>
<p style="position:absolute;top:609px;left:71px;white-space:nowrap" class="ft127">○</p>
<p style="position:absolute;top:611px;left:95px;white-space:nowrap" class="ft125">merge(Object&#160;object):&#160;Accepts&#160;the&#160;entity&#160;object&#160;and&#160;returns&#160;persistent&#160;object.</p>
<p style="position:absolute;top:622px;left:71px;white-space:nowrap" class="ft127">○</p>
<p style="position:absolute;top:624px;left:95px;white-space:nowrap" class="ft125">merge(String&#160;entityName,&#160;Object&#160;object):&#160;Pass&#160;entity&#160;name&#160;and&#160;entity&#160;object.</p>
<p style="position:absolute;top:655px;left:150px;white-space:nowrap" class="ft1215">User&#160;user = new&#160;User();<br/>user.setName(&#34;ABC&#34;);&#160;<br/>session.merge(user);&#160;<br/>session.evict(user);<br/>user.setName(&#34;XXX&#34;);&#160;<br/>User&#160;mergedUser = (User) session.merge(user);</p>
<p style="position:absolute;top:751px;left:84px;white-space:nowrap" class="ft1216">In&#160;the&#160;above&#160;example,&#160;we&#160;persisted&#160;the&#160;user&#160;object,&#160;and&#160;then&#160;detached&#160;it.&#160;Then&#160;we&#160;updated&#160;its&#160;<br/>name&#160;and&#160;merging&#160;to&#160;the&#160;session.&#160;The&#160;<i>mergedUser&#160;</i>object&#160;is&#160;the&#160;persisted&#160;entity&#160;that&#160;represents&#160;<br/>a&#160;row&#160;in&#160;the&#160;database&#160;and&#160;we&#160;have&#160;to&#160;discard&#160;the&#160;detached&#160;user&#160;object</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft130{font-size:6px;font-family:Times;color:#595959;}
	.ft131{font-size:20px;font-family:Times;color:#0079c0;}
	.ft132{font-size:9px;font-family:Times;color:#212121;}
	.ft133{font-size:10px;font-family:Times;color:#292929;}
	.ft134{font-size:10px;font-family:Times;color:#000000;}
	.ft135{font-size:19px;font-family:Times;color:#0079c0;}
	.ft136{font-size:11px;font-family:Times;color:#212121;}
	.ft137{font-size:11px;font-family:Times;color:#85200c;}
	.ft138{font-size:9px;line-height:13px;font-family:Times;color:#212121;}
	.ft139{font-size:10px;line-height:14px;font-family:Times;color:#292929;}
	.ft1310{font-size:10px;line-height:14px;font-family:Times;color:#000000;}
	.ft1311{font-size:11px;line-height:18px;font-family:Times;color:#212121;}
	.ft1312{font-size:11px;line-height:18px;font-family:Times;color:#85200c;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page13-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target013.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft130">25</p>
<p style="position:absolute;top:110px;left:26px;white-space:nowrap" class="ft131">Session.SaveOrUpdate()</p>
<p style="position:absolute;top:152px;left:40px;white-space:nowrap" class="ft138">The&#160;SaveorUpdate()&#160;method&#160;is&#160;similar&#160;to&#160;the&#160;Update&#160;method&#160;in&#160;reattaching&#160;the&#160;instances.&#160;The&#160;major&#160;<br/>difference&#160;is&#160;this&#160;method&#160;does&#160;not&#160;throw&#160;exceptions&#160;upon&#160;saving&#160;a&#160;transient&#160;instance&#160;instead&#160;it&#160;transitions&#160;the&#160;<br/>transient&#160;object&#160;to&#160;the&#160;persistent&#160;state.</p>
<p style="position:absolute;top:216px;left:209px;white-space:nowrap" class="ft139">User user = new User();<br/>user.setName(&#34;ABC&#34;);<br/>session.saveOrUpdate(user);</p>
<p style="position:absolute;top:279px;left:40px;white-space:nowrap" class="ft1310">The&#160;above&#160;code&#160;makes&#160;the&#160;transient&#160;user&#160;object&#160;a&#160;persistent&#160;one.&#160;This&#160;is&#160;a&#160;safer&#160;method&#160;to&#160;use&#160;when&#160;<br/>compared&#160;to&#160;the&#160;update&#160;method&#160;as&#160;it&#160;can&#160;work&#160;on&#160;both&#160;transient&#160;and&#160;detached&#160;objects.</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft130">26</p>
<p style="position:absolute;top:541px;left:26px;white-space:nowrap" class="ft135">Session.delete()&#160;method&#160;or&#160;Session.remove()&#160;method</p>
<p style="position:absolute;top:576px;left:41px;white-space:nowrap" class="ft1311">In&#160;Hibernate,&#160;an&#160;entity&#160;can&#160;be&#160;removed&#160;from&#160;a&#160;database&#160;by&#160;calling&#160;the&#160;Session.delete()&#160;or&#160;<br/>Session.remove().&#160;Using&#160;these&#160;methods,&#160;we&#160;can&#160;remove&#160;a&#160;transient&#160;or&#160;persistent&#160;object&#160;from&#160;<br/>datastore.</p>
<p style="position:absolute;top:671px;left:73px;white-space:nowrap" class="ft1312">Note:&#160;delete()&#160;method&#160;is&#160;deprecated,&#160;delete()&#160;is&#160;no&#160;longer&#160;used.&#160;Instead,&#160;we&#160;should&#160;use&#160;<br/>the&#160;remove()&#160;method.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft140{font-size:6px;font-family:Times;color:#595959;}
	.ft141{font-size:24px;font-family:Times;color:#0079c0;}
	.ft142{font-size:9px;font-family:Times;color:#212121;}
	.ft143{font-size:9px;font-family:Times;color:#333333;}
	.ft144{font-size:6px;font-family:Times;color:#273239;}
	.ft145{font-size:8px;font-family:Times;color:#ffab40;}
	.ft146{font-size:8px;font-family:Times;color:#212121;}
	.ft147{font-size:8px;font-family:Times;color:#212121;}
	.ft148{font-size:18px;font-family:Times;color:#0079c0;}
	.ft149{font-size:10px;font-family:Times;color:#212121;}
	.ft1410{font-size:10px;font-family:Times;color:#e69138;}
	.ft1411{font-size:10px;font-family:Times;color:#0097a7;}
	.ft1412{font-size:10px;font-family:Times;color:#212121;}
	.ft1413{font-size:8px;font-family:Times;color:#273239;}
	.ft1414{font-size:9px;line-height:13px;font-family:Times;color:#212121;}
	.ft1415{font-size:6px;line-height:10px;font-family:Times;color:#273239;}
	.ft1416{font-size:8px;line-height:12px;font-family:Times;color:#212121;}
	.ft1417{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page14-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target014.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft140">27</p>
<p style="position:absolute;top:107px;left:25px;white-space:nowrap" class="ft141">Transaction</p>
<p style="position:absolute;top:147px;left:34px;white-space:nowrap" class="ft1414">Transaction:&#160;Enables&#160;you&#160;to&#160;achieve&#160;data&#160;consistency&#160;and&#160;rollback&#160;<br/>in&#160;case&#160;something&#160;unexpected&#160;happens.&#160;A&#160;transaction&#160;can&#160;be&#160;<br/>described&#160;by&#160;Atomicity,&#160;Consistency,&#160;Isolation,&#160;and&#160;Durability&#160;(ACID)&#160;<br/>properties</p>
<p style="position:absolute;top:189px;left:85px;white-space:nowrap" class="ft143">.</p>
<p style="position:absolute;top:189px;left:88px;white-space:nowrap" class="ft142">&#160;It&#160;maintains&#160;abstraction&#160;from&#160;the&#160;transaction&#160;</p>
<p style="position:absolute;top:203px;left:34px;white-space:nowrap" class="ft142">implementation&#160;(JTA,JDBC).</p>
<p style="position:absolute;top:148px;left:405px;white-space:nowrap" class="ft1415">Transaction tx=session.beginTransaction();<br/>tx.commit();</p>
<p style="position:absolute;top:237px;left:32px;white-space:nowrap" class="ft142">The&#160;methods&#160;of&#160;Transaction&#160;interface&#160;are&#160;as&#160;follows:</p>
<p style="position:absolute;top:252px;left:39px;white-space:nowrap" class="ft145">1.</p>
<p style="position:absolute;top:252px;left:64px;white-space:nowrap" class="ft146">void&#160;begin()&#160;starts&#160;a&#160;new&#160;transaction.</p>
<p style="position:absolute;top:267px;left:39px;white-space:nowrap" class="ft145">2.</p>
<p style="position:absolute;top:267px;left:64px;white-space:nowrap" class="ft146">void&#160;commit()&#160;ends&#160;the&#160;unit&#160;of&#160;work&#160;unless&#160;we&#160;are&#160;in&#160;<i>FlushMode.NEVER</i>.</p>
<p style="position:absolute;top:282px;left:39px;white-space:nowrap" class="ft145">3.</p>
<p style="position:absolute;top:282px;left:64px;white-space:nowrap" class="ft146">void&#160;rollback()&#160;forces&#160;this&#160;transaction&#160;to&#160;rollback.</p>
<p style="position:absolute;top:296px;left:39px;white-space:nowrap" class="ft145">4.</p>
<p style="position:absolute;top:296px;left:64px;white-space:nowrap" class="ft1416">void&#160;setTimeout(int&#160;seconds)&#160;sets&#160;a&#160;transaction&#160;timeout&#160;for&#160;any&#160;transaction&#160;started&#160;by&#160;a&#160;subsequent&#160;call&#160;to&#160;begin&#160;<br/>on&#160;this&#160;instance.</p>
<p style="position:absolute;top:324px;left:39px;white-space:nowrap" class="ft145">5.</p>
<p style="position:absolute;top:324px;left:64px;white-space:nowrap" class="ft146">boolean&#160;isAlive()&#160;checks&#160;if&#160;the&#160;transaction&#160;is&#160;still&#160;alive.</p>
<p style="position:absolute;top:338px;left:39px;white-space:nowrap" class="ft145">6.</p>
<p style="position:absolute;top:338px;left:64px;white-space:nowrap" class="ft146">void&#160;registerSynchronization(Synchronization&#160;s)&#160;registers&#160;a&#160;user&#160;synchronization&#160;callback&#160;for&#160;this&#160;transaction.</p>
<p style="position:absolute;top:353px;left:39px;white-space:nowrap" class="ft145">7.</p>
<p style="position:absolute;top:353px;left:64px;white-space:nowrap" class="ft146">boolean&#160;wasCommited()&#160;checks&#160;if&#160;the&#160;transaction&#160;is&#160;committed&#160;successfully.</p>
<p style="position:absolute;top:368px;left:39px;white-space:nowrap" class="ft145">8.</p>
<p style="position:absolute;top:368px;left:64px;white-space:nowrap" class="ft146">boolean&#160;wasRolledBack()&#160;checks&#160;if&#160;the&#160;transaction&#160;is&#160;rolled&#160;back&#160;successfully.</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft140">28</p>
<p style="position:absolute;top:526px;left:25px;white-space:nowrap" class="ft148">Query&#160;Object</p>
<p style="position:absolute;top:561px;left:30px;white-space:nowrap" class="ft149">Query&#160;Interface:</p>
<p style="position:absolute;top:589px;left:38px;white-space:nowrap" class="ft1410">●</p>
<p style="position:absolute;top:589px;left:62px;white-space:nowrap" class="ft1411">Query&#160;</p>
<p style="position:absolute;top:589px;left:102px;white-space:nowrap" class="ft149">is&#160;an&#160;interface&#160;that&#160;presents&#160;inside&#160;org.hibernate&#160;package.</p>
<p style="position:absolute;top:615px;left:38px;white-space:nowrap" class="ft1410">●</p>
<p style="position:absolute;top:615px;left:62px;white-space:nowrap" class="ft149">A&#160;Query&#160;instance&#160;is&#160;obtained&#160;by&#160;calling&#160;<i>Session.createQuery()&#160;</i>method.</p>
<p style="position:absolute;top:641px;left:38px;white-space:nowrap" class="ft1410">●</p>
<p style="position:absolute;top:641px;left:62px;white-space:nowrap" class="ft1417">Query&#160;objects&#160;use&#160;SQL&#160;or&#160;Hibernate&#160;Query&#160;Language&#160;(HQL)&#160;to&#160;retrieve&#160;data&#160;from&#160;the&#160;database&#160;<br/>and&#160;create&#160;objects.&#160;A&#160;Query&#160;instance&#160;is&#160;used&#160;to&#160;bind&#160;query&#160;parameters,&#160;limit&#160;the&#160;number&#160;of&#160;results&#160;<br/>returned&#160;by&#160;the&#160;query,&#160;and&#160;finally,&#160;to&#160;execute&#160;the&#160;query.&#160;</p>
<p style="position:absolute;top:715px;left:220px;white-space:nowrap" class="ft1413">Query query=session.createQuery();</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft150{font-size:6px;font-family:Times;color:#595959;}
	.ft151{font-size:18px;font-family:Times;color:#0079c0;}
	.ft152{font-size:10px;font-family:Times;color:#212121;}
	.ft153{font-size:6px;font-family:Times;color:#000000;}
	.ft154{font-size:6px;font-family:Times;color:#b45f06;}
	.ft155{font-size:6px;font-family:Times;color:#212121;}
	.ft156{font-size:7px;font-family:Times;color:#333333;}
	.ft157{font-size:12px;font-family:Times;color:#e69138;}
	.ft158{font-size:12px;font-family:Times;color:#212121;}
	.ft159{font-size:10px;line-height:16px;font-family:Times;color:#212121;}
	.ft1510{font-size:6px;line-height:10px;font-family:Times;color:#b45f06;}
	.ft1511{font-size:6px;line-height:10px;font-family:Times;color:#212121;}
	.ft1512{font-size:7px;line-height:13px;font-family:Times;color:#333333;}
	.ft1513{font-size:12px;line-height:19px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page15-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target015.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft150">29</p>
<p style="position:absolute;top:107px;left:20px;white-space:nowrap" class="ft151">Setting&#160;up&#160;a&#160;Hibernate&#160;</p>
<p style="position:absolute;top:142px;left:25px;white-space:nowrap" class="ft159">1.&#160;Create&#160;Java&#160;Maven&#160;Project.<br/>2.&#160;Configure&#160;Maven&#160;Dependencies&#160;for&#160;Hibernate&#160;and&#160;Database&#160;in&#160;pom.xml&#160;file.</p>
<p style="position:absolute;top:189px;left:238px;white-space:nowrap" class="ft153">&lt;!-- https://mvnrepository.com/artifact/org.hibernate/hibernate-core --&gt;</p>
<p style="position:absolute;top:200px;left:238px;white-space:nowrap" class="ft1510">&lt;dependency&gt;<br/>&#160; &#160; &lt;groupId&gt;org.hibernate&lt;/groupId&gt;<br/>&#160; &#160; &lt;artifactId&gt;hibernate-core&lt;/artifactId&gt;<br/>&#160; &#160; &lt;version&gt;5.5.7.Final&lt;/version&gt;<br/>&lt;/dependency&gt;</p>
<p style="position:absolute;top:264px;left:238px;white-space:nowrap" class="ft1510">&lt;dependency&gt;<br/>&#160; &#160; &lt;groupId&gt;org.hibernate&lt;/groupId&gt;<br/>&#160; &#160; &lt;artifactId&gt;hibernate-annotations&lt;/artifactId&gt;<br/>&#160; &#160; &lt;version&gt;3.5.5-Final&lt;/version&gt;<br/>&lt;/dependency&gt;</p>
<p style="position:absolute;top:329px;left:238px;white-space:nowrap" class="ft153">&lt;!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java --&gt;</p>
<p style="position:absolute;top:339px;left:238px;white-space:nowrap" class="ft1511">&lt;dependency&gt;<br/>&#160; &#160; &lt;groupId&gt;mysql&lt;/groupId&gt;<br/>&#160; &#160; &lt;artifactId&gt;mysql-connector-java&lt;/artifactId&gt;<br/>&#160; &#160; &lt;version&gt;8.0.25&lt;/version&gt;<br/>&lt;/dependency&gt;</p>
<p style="position:absolute;top:226px;left:30px;white-space:nowrap" class="ft1512">Maven&#160;automatically&#160;downloads&#160;the&#160;<br/>required&#160;JAR&#160;files.&#160;</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft150">30</p>
<p style="position:absolute;top:525px;left:25px;white-space:nowrap" class="ft151">Entity/POJO/Model&#160;Classes</p>
<p style="position:absolute;top:568px;left:38px;white-space:nowrap" class="ft157">●</p>
<p style="position:absolute;top:568px;left:62px;white-space:nowrap" class="ft1513">Entity&#160;is&#160;a&#160;Java&#160;object&#160;that&#160;is&#160;going&#160;to&#160;be&#160;persisted.&#160;Entity&#160;classes&#160;are&#160;decorated&#160;with&#160;<br/>Java&#160;annotations&#160;such&#160;as&#160;@Entity,&#160;@Id,&#160;@Table,&#160;or&#160;@Column.</p>
<p style="position:absolute;top:616px;left:38px;white-space:nowrap" class="ft157">●</p>
<p style="position:absolute;top:616px;left:62px;white-space:nowrap" class="ft1513">Entities&#160;are&#160;nothing&#160;but&#160;beans&#160;or&#160;Models,&#160;and&#160;they&#160;contain&#160;default&#160;constructor,&#160;setter,&#160;<br/>and&#160;getter&#160;methods&#160;for&#160;the&#160;attributes&#160;or&#160;private&#160;variables&#160;(class&#160;variables).</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft160{font-size:6px;font-family:Times;color:#595959;}
	.ft161{font-size:20px;font-family:Times;color:#0079c0;}
	.ft162{font-size:11px;font-family:Times;color:#e69138;}
	.ft163{font-size:10px;font-family:Times;color:#212121;}
	.ft164{font-size:9px;font-family:Times;color:#ffab40;}
	.ft165{font-size:7px;font-family:Times;color:#ffab40;}
	.ft166{font-size:7px;font-family:Times;color:#212121;}
	.ft167{font-size:9px;font-family:Times;color:#212121;}
	.ft168{font-size:6px;font-family:Times;color:#212121;}
	.ft169{font-size:5px;font-family:Times;color:#212121;}
	.ft1610{font-size:10px;font-family:Times;color:#b45f06;}
	.ft1611{font-size:7px;font-family:Times;color:#e69138;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page16-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target016.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft160">31</p>
<p style="position:absolute;top:104px;left:22px;white-space:nowrap" class="ft161">Hibernate&#160;Application&#160;Workflow</p>
<p style="position:absolute;top:142px;left:28px;white-space:nowrap" class="ft162">❑</p>
<p style="position:absolute;top:142px;left:54px;white-space:nowrap" class="ft163">Phase&#160;1&#160;Composed&#160;of...</p>
<p style="position:absolute;top:166px;left:54px;white-space:nowrap" class="ft164">➢</p>
<p style="position:absolute;top:165px;left:78px;white-space:nowrap" class="ft163">Add&#160;Maven&#160;dependencies&#160;for&#160;Hibernate&#160;and&#160;mysql</p>
<p style="position:absolute;top:189px;left:54px;white-space:nowrap" class="ft164">➢</p>
<p style="position:absolute;top:188px;left:78px;white-space:nowrap" class="ft163">POJO&#160;Classes:&#160;</p>
<p style="position:absolute;top:204px;left:82px;white-space:nowrap" class="ft165">&#160;▶</p>
<p style="position:absolute;top:204px;left:89px;white-space:nowrap" class="ft166">&#160;</p>
<p style="position:absolute;top:202px;left:92px;white-space:nowrap" class="ft167">Creating&#160;Entities&#160;-&#160;Entities&#160;are&#160;nothing&#160;but&#160;beans&#160;or&#160;Models,&#160;and&#160;they&#160;contain&#160;default&#160;constructor,&#160;</p>
<p style="position:absolute;top:215px;left:82px;white-space:nowrap" class="ft167">setter,&#160;and&#160;getter&#160;methods&#160;of&#160;those&#160;attributes.</p>
<p style="position:absolute;top:229px;left:84px;white-space:nowrap" class="ft168">(Note:&#160;We&#160;will&#160;be&#160;handling&#160;this&#160;via&#160;annotations.)</p>
<p style="position:absolute;top:250px;left:54px;white-space:nowrap" class="ft164">➢</p>
<p style="position:absolute;top:248px;left:78px;white-space:nowrap" class="ft163">DAO/Service&#160;Classes&#160;containing&#160;service&#160;methods:</p>
<p style="position:absolute;top:265px;left:84px;white-space:nowrap" class="ft165">▶</p>
<p style="position:absolute;top:262px;left:98px;white-space:nowrap" class="ft167">Create,&#160;Update,&#160;Delete,&#160;or&#160;Find&#160;a&#160;record.</p>
<p style="position:absolute;top:287px;left:28px;white-space:nowrap" class="ft162">❑</p>
<p style="position:absolute;top:287px;left:54px;white-space:nowrap" class="ft163">Phase&#160;2&#160;Composed&#160;of…</p>
<p style="position:absolute;top:311px;left:54px;white-space:nowrap" class="ft164">➢</p>
<p style="position:absolute;top:310px;left:78px;white-space:nowrap" class="ft163">Mapping&#160;File:&#160;Hibernate&#160;Configuration:</p>
<p style="position:absolute;top:336px;left:84px;white-space:nowrap" class="ft165">▶</p>
<p style="position:absolute;top:333px;left:98px;white-space:nowrap" class="ft167">Configurations&#160;-&#160;Database&#160;connection&#160;information,&#160;schema&#160;level&#160;settings,&#160;and&#160;entity&#160;mapping,&#160;</p>
<p style="position:absolute;top:346px;left:84px;white-space:nowrap" class="ft167">using&#160;the&#160;mapping&#160;file&#160;(hibernate.properties&#160;or&#160;hibernate.cfg.xml).</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft160">32</p>
<p style="position:absolute;top:526px;left:19px;white-space:nowrap" class="ft161">Hibernate&#160;-&#160;Annotations</p>
<p style="position:absolute;top:686px;left:35px;white-space:nowrap" class="ft169">Annotation</p>
<p style="position:absolute;top:686px;left:112px;white-space:nowrap" class="ft169">Description</p>
<p style="position:absolute;top:707px;left:35px;white-space:nowrap" class="ft168">@Entity</p>
<p style="position:absolute;top:707px;left:112px;white-space:nowrap" class="ft168">This&#160;annotation&#160;indicates&#160;that&#160;the&#160;class&#160;is&#160;mapped&#160;to&#160;a&#160;database&#160;table.&#160;By&#160;default,&#160;the&#160;ORM&#160;framework&#160;understands&#160;that&#160;</p>
<p style="position:absolute;top:717px;left:112px;white-space:nowrap" class="ft168">the&#160;class&#160;name&#160;is&#160;the&#160;same&#160;as&#160;the&#160;table&#160;name.&#160;The&#160;@Entity&#160;annotation&#160;must&#160;be&#160;placed&#160;before&#160;the&#160;class&#160;definition.</p>
<p style="position:absolute;top:741px;left:29px;white-space:nowrap" class="ft168">&#160;&#160;@Table</p>
<p style="position:absolute;top:733px;left:112px;white-space:nowrap" class="ft168">This&#160;annotation&#160;is&#160;used&#160;if&#160;the&#160;class&#160;name&#160;is&#160;different&#160;than&#160;the&#160;database&#160;table&#160;name,&#160;and&#160;it&#160;must&#160;be&#160;placed&#160;before&#160;the&#160;class&#160;</p>
<p style="position:absolute;top:742px;left:112px;white-space:nowrap" class="ft168">definition.</p>
<p style="position:absolute;top:756px;left:35px;white-space:nowrap" class="ft168">@Id</p>
<p style="position:absolute;top:756px;left:112px;white-space:nowrap" class="ft168">This&#160;annotation&#160;specifies&#160;that&#160;a&#160;field&#160;is&#160;mapped&#160;to&#160;a&#160;primary&#160;key&#160;column&#160;in&#160;the&#160;table.</p>
<p style="position:absolute;top:775px;left:35px;white-space:nowrap" class="ft168">@Basic</p>
<p style="position:absolute;top:775px;left:112px;white-space:nowrap" class="ft168">This&#160;annotation&#160;tells&#160;JPA&#160;the&#160;below&#160;variable&#160;is&#160;a&#160;regular&#160;attribute.</p>
<p style="position:absolute;top:557px;left:25px;white-space:nowrap" class="ft1610">Annotations:</p>
<p style="position:absolute;top:583px;left:28px;white-space:nowrap" class="ft1611">❏</p>
<p style="position:absolute;top:580px;left:49px;white-space:nowrap" class="ft163">We&#160;can&#160;write&#160;configurations&#160;using&#160;annotations.&#160;The&#160;annotations&#160;are&#160;used&#160;for&#160;classes,&#160;properties,&#160;and&#160;</p>
<p style="position:absolute;top:592px;left:49px;white-space:nowrap" class="ft163">methods.&#160;Annotations&#160;start&#160;with&#160;the&#160;‘@’&#160;symbol,&#160;and&#160;are&#160;declared&#160;before&#160;the&#160;class,&#160;property,&#160;or&#160;</p>
<p style="position:absolute;top:604px;left:49px;white-space:nowrap" class="ft163">method&#160;is&#160;declared.&#160;</p>
<p style="position:absolute;top:628px;left:28px;white-space:nowrap" class="ft1611">❏</p>
<p style="position:absolute;top:625px;left:49px;white-space:nowrap" class="ft163">Hibernate&#160;Annotations&#160;is&#160;a&#160;powerful&#160;way&#160;to&#160;provide&#160;the&#160;metadata&#160;for&#160;the&#160;Object&#160;and&#160;Relational&#160;Table&#160;</p>
<p style="position:absolute;top:637px;left:49px;white-space:nowrap" class="ft163">mapping.&#160;</p>
<p style="position:absolute;top:658px;left:25px;white-space:nowrap" class="ft167">Annotations&#160;used&#160;in&#160;our&#160;examples:</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft170{font-size:6px;font-family:Times;color:#595959;}
	.ft171{font-size:19px;font-family:Times;color:#0079c0;}
	.ft172{font-size:17px;font-family:Times;color:#0079c0;}
	.ft173{font-size:8px;font-family:Times;color:#212121;}
	.ft174{font-size:6px;font-family:Times;color:#212121;}
	.ft175{font-size:7px;font-family:Times;color:#212121;}
	.ft176{font-size:24px;font-family:Times;color:#0079c0;}
	.ft177{font-size:12px;font-family:Times;color:#212121;}
	.ft178{font-size:12px;font-family:Times;color:#ffab40;}
	.ft179{font-size:12px;font-family:Times;color:#e69138;}
	.ft1710{font-size:10px;font-family:Times;color:#0d6efd;}
	.ft1711{font-size:10px;font-family:Times;color:#212121;}
	.ft1712{font-size:10px;font-family:Times;color:#0097a7;}
	.ft1713{font-size:7px;line-height:12px;font-family:Times;color:#212121;}
	.ft1714{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page17-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target017.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft170">33</p>
<p style="position:absolute;top:113px;left:26px;white-space:nowrap" class="ft171">Hibernate&#160;-&#160;Annotations&#160;</p>
<p style="position:absolute;top:115px;left:274px;white-space:nowrap" class="ft172">(continued)</p>
<p style="position:absolute;top:151px;left:33px;white-space:nowrap" class="ft173">Annotation</p>
<p style="position:absolute;top:151px;left:121px;white-space:nowrap" class="ft173">Description</p>
<p style="position:absolute;top:180px;left:33px;white-space:nowrap" class="ft174">@Column</p>
<p style="position:absolute;top:172px;left:121px;white-space:nowrap" class="ft174">This&#160;annotation&#160;is&#160;used&#160;to&#160;map&#160;an&#160;instance&#160;field&#160;of&#160;the&#160;class&#160;to&#160;a&#160;column&#160;in&#160;the&#160;database&#160;table,&#160;and&#160;it&#160;is&#160;</p>
<p style="position:absolute;top:183px;left:121px;white-space:nowrap" class="ft174">placed&#160;before&#160;the&#160;getter&#160;method&#160;of&#160;the&#160;field.&#160;By&#160;default,&#160;Hibernate&#160;can&#160;implicitly&#160;infer&#160;the&#160;mapping&#160;</p>
<p style="position:absolute;top:194px;left:121px;white-space:nowrap" class="ft174">based&#160;on&#160;field&#160;name&#160;and&#160;field&#160;type&#160;of&#160;the&#160;class,&#160;but&#160;if&#160;the&#160;field&#160;name&#160;and&#160;the&#160;corresponding&#160;column&#160;</p>
<p style="position:absolute;top:204px;left:121px;white-space:nowrap" class="ft174">name&#160;are&#160;different,&#160;we&#160;have&#160;to&#160;use&#160;this&#160;annotation&#160;explicitly:</p>
<p style="position:absolute;top:227px;left:121px;white-space:nowrap" class="ft1713">@Column(name = &#34;ACC_NO&#34;, unique = false, nullable = false, length = 100)<br/>private String accountNumber;</p>
<p style="position:absolute;top:270px;left:28px;white-space:nowrap" class="ft174">&#160;&#160;@JoinColumn</p>
<p style="position:absolute;top:262px;left:116px;white-space:nowrap" class="ft174">This&#160;annotation&#160;is&#160;used&#160;to&#160;map&#160;the&#160;foreign&#160;key&#160;column&#160;of&#160;a&#160;managed&#160;association&#160;(e.g.,&#160;one-to-one,&#160;</p>
<p style="position:absolute;top:273px;left:116px;white-space:nowrap" class="ft174">many-to-one,&#160;many-to-many).</p>
<p style="position:absolute;top:296px;left:33px;white-space:nowrap" class="ft174">@TableGenerator</p>
<p style="position:absolute;top:288px;left:121px;white-space:nowrap" class="ft174">This&#160;annotation&#160;is&#160;used&#160;to&#160;specify&#160;the&#160;value&#160;generator&#160;for&#160;the&#160;property&#160;specified&#160;in&#160;@GeneratedValue&#160;</p>
<p style="position:absolute;top:299px;left:121px;white-space:nowrap" class="ft174">annotation.&#160;It&#160;creates&#160;a&#160;table&#160;for&#160;value&#160;generation.</p>
<p style="position:absolute;top:322px;left:33px;white-space:nowrap" class="ft174">@ColumnResult</p>
<p style="position:absolute;top:314px;left:121px;white-space:nowrap" class="ft174">This&#160;annotation&#160;references&#160;the&#160;name&#160;of&#160;a&#160;column&#160;in&#160;the&#160;SQL&#160;query&#160;using&#160;the&#160;select&#160;clause.</p>
<p style="position:absolute;top:336px;left:33px;white-space:nowrap" class="ft174">@GeneratedValue</p>
<p style="position:absolute;top:336px;left:121px;white-space:nowrap" class="ft174">If&#160;the&#160;values&#160;of&#160;the&#160;primary&#160;column&#160;are&#160;auto-increment,&#160;we&#160;need&#160;to&#160;use&#160;this&#160;annotation&#160;along&#160;with&#160;one&#160;of&#160;</p>
<p style="position:absolute;top:346px;left:121px;white-space:nowrap" class="ft174">the&#160;following&#160;strategy&#160;types:&#160;AUTO,&#160;IDENTITY,&#160;SEQUENCE,&#160;and&#160;TABLE.&#160;In&#160;our&#160;case,&#160;we&#160;use&#160;the&#160;strategy&#160;</p>
<p style="position:absolute;top:357px;left:121px;white-space:nowrap" class="ft174">IDENTITY,&#160;which&#160;specifies&#160;that&#160;the&#160;generated&#160;values&#160;are&#160;unique&#160;at&#160;table&#160;level,&#160;whereas&#160;the&#160;strategy&#160;AUTO&#160;</p>
<p style="position:absolute;top:368px;left:121px;white-space:nowrap" class="ft174">implies&#160;that&#160;the&#160;generated&#160;values&#160;are&#160;unique&#160;at&#160;database&#160;level.</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft170">34</p>
<p style="position:absolute;top:516px;left:25px;white-space:nowrap" class="ft176">Hands-On&#160;Lab</p>
<p style="position:absolute;top:568px;left:30px;white-space:nowrap" class="ft177">Find&#160;the</p>
<p style="position:absolute;top:568px;left:86px;white-space:nowrap" class="ft178">&#160;GLAB&#160;-&#160;305.4.1&#160;-&#160;Hibernate&#160;Project&#160;Demonstration&#160;</p>
<p style="position:absolute;top:568px;left:430px;white-space:nowrap" class="ft177">on&#160;Canvas&#160;under&#160;the&#160;</p>
<p style="position:absolute;top:588px;left:30px;white-space:nowrap" class="ft177">Assignment&#160;section.</p>
<p style="position:absolute;top:648px;left:38px;white-space:nowrap" class="ft179">●</p>
<p style="position:absolute;top:648px;left:62px;white-space:nowrap" class="ft177">Important&#160;note:</p>
<p style="position:absolute;top:668px;left:71px;white-space:nowrap" class="ft1710">○</p>
<p style="position:absolute;top:668px;left:95px;white-space:nowrap" class="ft1711">The&#160;instructor&#160;should&#160;give&#160;demonstration&#160;of&#160;the&#160;above&#160;lab&#160;in&#160;the&#160;class.&#160;</p>
<p style="position:absolute;top:685px;left:71px;white-space:nowrap" class="ft1710">○</p>
<p style="position:absolute;top:685px;left:95px;white-space:nowrap" class="ft1714">If&#160;you&#160;have&#160;any&#160;technical&#160;questions&#160;while&#160;performing&#160;the&#160;lab&#160;activity,&#160;ask&#160;your&#160;instructors&#160;for&#160;<br/>assistance.</p>
<p style="position:absolute;top:703px;left:157px;white-space:nowrap" class="ft1712">&#160;</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft180{font-size:6px;font-family:Times;color:#595959;}
	.ft181{font-size:20px;font-family:Times;color:#0079c0;}
	.ft182{font-size:10px;font-family:Times;color:#ffab40;}
	.ft183{font-size:11px;font-family:Times;color:#212121;}
	.ft184{font-size:10px;font-family:Times;color:#212121;}
	.ft185{font-size:12px;font-family:Times;color:#0f3b68;}
	.ft186{font-size:11px;line-height:18px;font-family:Times;color:#212121;}
	.ft187{font-size:11px;line-height:16px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page18-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target018.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft180">35</p>
<p style="position:absolute;top:104px;left:22px;white-space:nowrap" class="ft181">Practice&#160;Assignment</p>
<p style="position:absolute;top:145px;left:23px;white-space:nowrap" class="ft182">❑</p>
<p style="position:absolute;top:144px;left:48px;white-space:nowrap" class="ft183">Using&#160;Hibernate,&#160;create&#160;a&#160;Department&#160;Model&#160;with&#160;the&#160;following&#160;&#160;attributes:</p>
<p style="position:absolute;top:163px;left:47px;white-space:nowrap" class="ft182">➢</p>
<p style="position:absolute;top:163px;left:73px;white-space:nowrap" class="ft184">int&#160;did</p>
<p style="position:absolute;top:181px;left:47px;white-space:nowrap" class="ft182">➢</p>
<p style="position:absolute;top:180px;left:73px;white-space:nowrap" class="ft184">String&#160;name</p>
<p style="position:absolute;top:199px;left:47px;white-space:nowrap" class="ft182">➢</p>
<p style="position:absolute;top:198px;left:73px;white-space:nowrap" class="ft184">String&#160;state</p>
<p style="position:absolute;top:218px;left:23px;white-space:nowrap" class="ft182">❑</p>
<p style="position:absolute;top:216px;left:48px;white-space:nowrap" class="ft186">Using&#160;Hibernate,&#160;create&#160;the&#160;relevant&#160;services&#160;for&#160;this&#160;Model&#160;(create,&#160;update&#160;name/state,&#160;find,&#160;<br/>and&#160;delete).</p>
<p style="position:absolute;top:254px;left:23px;white-space:nowrap" class="ft182">❑</p>
<p style="position:absolute;top:252px;left:48px;white-space:nowrap" class="ft183">Do&#160;not&#160;forget&#160;to&#160;add&#160;your&#160;model&#160;to&#160;the&#160;configuration&#160;file&#160;(hibernate.cfg.xml).</p>
<p style="position:absolute;top:285px;left:29px;white-space:nowrap" class="ft187">If&#160;you&#160;have&#160;any&#160;technical&#160;questions&#160;while&#160;performing&#160;this&#160;practice&#160;assignment,&#160;ask&#160;your&#160;<br/>instructors&#160;for&#160;assistance.</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft180">36</p>
<p style="position:absolute;top:525px;left:26px;white-space:nowrap" class="ft181">Knowledge&#160;Check</p>
<p style="position:absolute;top:568px;left:34px;white-space:nowrap" class="ft185">1.</p>
<p style="position:absolute;top:568px;left:62px;white-space:nowrap" class="ft185">What&#160;is&#160;the&#160;difference&#160;between&#160;ORM,&#160;JPA,&#160;and&#160;Hibernate?</p>
<p style="position:absolute;top:588px;left:34px;white-space:nowrap" class="ft185">2.</p>
<p style="position:absolute;top:588px;left:62px;white-space:nowrap" class="ft185">Why&#160;do&#160;we&#160;need&#160;Hibernate&#160;instead&#160;of&#160;JDBC&#160;?</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft190{font-size:6px;font-family:Times;color:#595959;}
	.ft191{font-size:24px;font-family:Times;color:#0079c0;}
	.ft192{font-size:12px;font-family:Times;color:#212121;}
	.ft193{font-size:18px;font-family:Times;color:#0079c0;}
	.ft194{font-size:12px;line-height:19px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page19-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target019.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft190">37</p>
<p style="position:absolute;top:101px;left:25px;white-space:nowrap" class="ft191">Summary</p>
<p style="position:absolute;top:152px;left:30px;white-space:nowrap" class="ft194">Object&#160;Relational&#160;Mapping&#160;(ORM)&#160;is&#160;a&#160;concept/process&#160;of&#160;converting&#160;the&#160;data&#160;from&#160;Object&#160;<br/>Oriented&#160;Language&#160;to&#160;relational&#160;database,&#160;and&#160;vice&#160;versa.&#160;In&#160;Java,&#160;this&#160;is&#160;performed&#160;with&#160;the&#160;<br/>help&#160;of&#160;reflection&#160;and&#160;JDBC.</p>
<p style="position:absolute;top:252px;left:30px;white-space:nowrap" class="ft194">Java&#160;Persistence&#160;API&#160;is&#160;one&#160;step&#160;above&#160;ORM.&#160;It&#160;has&#160;a&#160;high-level&#160;API&#160;and&#160;specification&#160;<br/>requirement&#160;so&#160;that&#160;different&#160;ORM&#160;tools&#160;can&#160;implement&#160;it&#160;and&#160;provide&#160;the&#160;flexibility&#160;to&#160;change&#160;<br/>the&#160;implementation&#160;from&#160;one&#160;ORM&#160;to&#160;another&#160;(e.g.,&#160;if&#160;an&#160;application&#160;uses&#160;the&#160;JPA&#160;API&#160;and&#160;<br/>implementation&#160;is&#160;hibernate).&#160;</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft190">38</p>
<p style="position:absolute;top:520px;left:24px;white-space:nowrap" class="ft193">Let’s&#160;Take&#160;A&#160;Break…</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft200{font-size:14px;font-family:Times;color:#b45f06;}
	.ft201{font-size:6px;font-family:Times;color:#595959;}
	.ft202{font-size:18px;font-family:Times;color:#0079c0;}
	.ft203{font-size:16px;font-family:Times;color:#0079c0;}
	.ft204{font-size:9px;font-family:Times;color:#e69138;}
	.ft205{font-size:11px;font-family:Times;color:#212121;}
	.ft206{font-size:11px;font-family:Times;color:#212121;}
	.ft207{font-size:8px;font-family:Times;color:#e69138;}
	.ft208{font-size:10px;font-family:Times;color:#1155cc;}
	.ft209{font-size:11px;line-height:18px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page20-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target020.png" alt="background image"/>
<p style="position:absolute;top:212px;left:143px;white-space:nowrap" class="ft200">Hibernate&#160;Query&#160;Object&#160;/&#160;Hibernate&#160;Query&#160;</p>
<p style="position:absolute;top:232px;left:271px;white-space:nowrap" class="ft200">Language</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft201">40</p>
<p style="position:absolute;top:530px;left:28px;white-space:nowrap" class="ft202">Hibernate&#160;Query&#160;Object&#160;/&#160;Hibernate&#160;Query&#160;Languag</p>
<p style="position:absolute;top:531px;left:525px;white-space:nowrap" class="ft203">e</p>
<p style="position:absolute;top:569px;left:39px;white-space:nowrap" class="ft204">●</p>
<p style="position:absolute;top:568px;left:62px;white-space:nowrap" class="ft209">Hibernate&#160;created&#160;a&#160;new&#160;language&#160;named&#160;Hibernate&#160;Query&#160;Language&#160;(HQL).&#160;The&#160;syntax&#160;is&#160;<br/>quite&#160;similar&#160;to&#160;database&#160;SQL&#160;language.&#160;The&#160;main&#160;difference&#160;is&#160;that&#160;HQL&#160;uses&#160;class&#160;name&#160;<br/>instead&#160;of&#160;table&#160;name,&#160;and&#160;property&#160;names&#160;instead&#160;of&#160;column&#160;names.</p>
<p style="position:absolute;top:634px;left:39px;white-space:nowrap" class="ft204">●</p>
<p style="position:absolute;top:632px;left:62px;white-space:nowrap" class="ft205">The&#160;Hibernate&#160;Query&#160;Object&#160;-&#160;HQL&#160;is&#160;used&#160;to&#160;retrieve&#160;data&#160;from&#160;the&#160;database.</p>
<p style="position:absolute;top:661px;left:39px;white-space:nowrap" class="ft204">●</p>
<p style="position:absolute;top:660px;left:62px;white-space:nowrap" class="ft209">Keywords,&#160;such&#160;as&#160;SELECT,&#160;FROM,&#160;and&#160;WHERE,&#160;etc.&#160;are&#160;<i>not&#160;</i>case-sensitive,&#160;but&#160;properties&#160;<br/>such&#160;as&#160;table&#160;and&#160;column&#160;names&#160;are&#160;case-sensitive&#160;in&#160;HQL.</p>
<p style="position:absolute;top:708px;left:40px;white-space:nowrap" class="ft207">●</p>
<p style="position:absolute;top:705px;left:62px;white-space:nowrap" class="ft209">HQL&#160;has&#160;many&#160;benefits,&#160;including&#160;that&#160;it&#160;is&#160;database&#160;independent,&#160;polymorphic&#160;queries&#160;are&#160;<br/>supported,&#160;and&#160;it&#160;is&#160;easy&#160;to&#160;learn&#160;for&#160;Java&#160;programmers.</p>
<p style="position:absolute;top:763px;left:149px;white-space:nowrap" class="ft208">Visit&#160;Wiki&#160;document&#160;</p>
<p style="position:absolute;top:761px;left:261px;white-space:nowrap" class="ft208">for&#160;more&#160;details&#160;and&#160;information&#160;about&#160;HQL.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft210{font-size:6px;font-family:Times;color:#595959;}
	.ft211{font-size:17px;font-family:Times;color:#0079c0;}
	.ft212{font-size:15px;font-family:Times;color:#0079c0;}
	.ft213{font-size:11px;font-family:Times;color:#212121;}
	.ft214{font-size:10px;font-family:Times;color:#e69138;}
	.ft215{font-size:10px;font-family:Times;color:#212121;}
	.ft216{font-size:10px;font-family:Times;color:#ffffff;}
	.ft217{font-size:24px;font-family:Times;color:#0079c0;}
	.ft218{font-size:10px;font-family:Times;color:#212121;}
	.ft219{font-size:10px;font-family:Times;color:#cc0000;}
	.ft2110{font-size:8px;font-family:Times;color:#273239;}
	.ft2111{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page21-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target021.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft210">41</p>
<p style="position:absolute;top:107px;left:26px;white-space:nowrap" class="ft211">Hibernate&#160;Query&#160;Object&#160;/&#160;Hibernate&#160;Query&#160;Languag</p>
<p style="position:absolute;top:108px;left:502px;white-space:nowrap" class="ft212">e&#160;(continued)&#160;</p>
<p style="position:absolute;top:145px;left:42px;white-space:nowrap" class="ft213">Commonly&#160;supported&#160;clauses&#160;in&#160;HQL:</p>
<p style="position:absolute;top:171px;left:32px;white-space:nowrap" class="ft214">1.</p>
<p style="position:absolute;top:171px;left:48px;white-space:nowrap" class="ft215">HQL&#160;From:&#160;HQL&#160;From&#160;is&#160;the&#160;same&#160;as&#160;a&#160;select&#160;clause&#160;in&#160;SQL.&#160;For&#160;example,</p>
<p style="position:absolute;top:171px;left:458px;white-space:nowrap" class="ft216">&#160;from Employee&#160;</p>
<p style="position:absolute;top:171px;left:553px;white-space:nowrap" class="ft215">is&#160;the&#160;</p>
<p style="position:absolute;top:192px;left:48px;white-space:nowrap" class="ft215">same&#160;as</p>
<p style="position:absolute;top:194px;left:95px;white-space:nowrap" class="ft216">&#160;select * from Employee</p>
<p style="position:absolute;top:192px;left:250px;white-space:nowrap" class="ft215">.We&#160;can&#160;also&#160;create&#160;aliases,&#160;such&#160;as&#160;</p>
<p style="position:absolute;top:194px;left:452px;white-space:nowrap" class="ft216">from Employee emp</p>
<p style="position:absolute;top:194px;left:567px;white-space:nowrap" class="ft215">&#160;or&#160;</p>
<p style="position:absolute;top:214px;left:48px;white-space:nowrap" class="ft216">from Employee as emp.</p>
<p style="position:absolute;top:241px;left:32px;white-space:nowrap" class="ft214">2.</p>
<p style="position:absolute;top:241px;left:48px;white-space:nowrap" class="ft215">HQL&#160;JOIN:&#160;HQL&#160;supports&#160;inner&#160;JOIN,&#160;left&#160;JOIN,&#160;right&#160;JOIN,&#160;and&#160;full&#160;JOIN&#160;(e.g.,</p>
<p style="position:absolute;top:243px;left:489px;white-space:nowrap" class="ft216">&#160;select e.name,&#160;</p>
<p style="position:absolute;top:256px;left:48px;white-space:nowrap" class="ft216">a.city from Employee e INNER JOIN e.address a)</p>
<p style="position:absolute;top:254px;left:359px;white-space:nowrap" class="ft215">.&#160;In&#160;this&#160;query,&#160;Employee&#160;Class&#160;should&#160;have&#160;a&#160;</p>
<p style="position:absolute;top:267px;left:48px;white-space:nowrap" class="ft215">variable&#160;named&#160;address.&#160;</p>
<p style="position:absolute;top:289px;left:32px;white-space:nowrap" class="ft214">3.</p>
<p style="position:absolute;top:289px;left:48px;white-space:nowrap" class="ft215">Aggregate&#160;Functions:&#160;HQL&#160;supports&#160;commonly&#160;used&#160;aggregate&#160;functions&#160;such&#160;as&#160;count(*),&#160;</p>
<p style="position:absolute;top:303px;left:48px;white-space:nowrap" class="ft215">count(distinct&#160;x),&#160;min(),&#160;max(),&#160;avg(),&#160;and&#160;sum().</p>
<p style="position:absolute;top:325px;left:32px;white-space:nowrap" class="ft214">4.</p>
<p style="position:absolute;top:325px;left:48px;white-space:nowrap" class="ft215">Expressions:&#160;HQL&#160;supports&#160;arithmetic&#160;expressions&#160;(+,&#160;-,&#160;*,&#160;/),&#160;binary&#160;comparison&#160;operators&#160;(=,&#160;&gt;=,&#160;</p>
<p style="position:absolute;top:338px;left:48px;white-space:nowrap" class="ft215">&lt;=,&#160;&lt;&gt;,&#160;!=),&#160;and&#160;logical&#160;operations&#160;(and,&#160;or,&#160;not),&#160;etc.</p>
<p style="position:absolute;top:360px;left:32px;white-space:nowrap" class="ft214">5.</p>
<p style="position:absolute;top:360px;left:48px;white-space:nowrap" class="ft215">HQL&#160;supports&#160;order&#160;by&#160;and&#160;group&#160;by&#160;clauses,&#160;subqueries&#160;such&#160;as&#160;SQL&#160;queries,&#160;and&#160;DDL&#160;and&#160;DML.</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft210">42</p>
<p style="position:absolute;top:522px;left:25px;white-space:nowrap" class="ft217">HQL&#160;-&#160;Interfaces</p>
<p style="position:absolute;top:563px;left:30px;white-space:nowrap" class="ft215">Query&#160;Interface:</p>
<p style="position:absolute;top:591px;left:39px;white-space:nowrap" class="ft214">●</p>
<p style="position:absolute;top:591px;left:62px;white-space:nowrap" class="ft215">Query&#160;is&#160;an&#160;interface&#160;that&#160;presents&#160;inside&#160;org.hibernate&#160;package.</p>
<p style="position:absolute;top:617px;left:39px;white-space:nowrap" class="ft214">●</p>
<p style="position:absolute;top:617px;left:62px;white-space:nowrap" class="ft215">A&#160;Query&#160;instance&#160;is&#160;obtained&#160;by&#160;calling&#160;<i>Session.createQuery().</i></p>
<p style="position:absolute;top:643px;left:39px;white-space:nowrap" class="ft214">●</p>
<p style="position:absolute;top:643px;left:62px;white-space:nowrap" class="ft2111">Query&#160;objects&#160;use&#160;SQL&#160;or&#160;Hibernate&#160;Query&#160;Language&#160;(HQL)&#160;strings&#160;to&#160;retrieve&#160;data&#160;from&#160;the&#160;<br/>database&#160;and&#160;create&#160;objects.&#160;A&#160;Query&#160;instance&#160;is&#160;used&#160;to&#160;bind&#160;query&#160;parameters,&#160;limit&#160;the&#160;number&#160;of&#160;<br/>results&#160;returned&#160;by&#160;the&#160;query,&#160;and&#160;finally,&#160;to&#160;execute&#160;the&#160;query.&#160;</p>
<p style="position:absolute;top:704px;left:39px;white-space:nowrap" class="ft214">●</p>
<p style="position:absolute;top:704px;left:62px;white-space:nowrap" class="ft215">Hibernate&#160;provides&#160;different&#160;techniques&#160;to&#160;query&#160;database,&#160;including,&#160;</p>
<p style="position:absolute;top:704px;left:478px;white-space:nowrap" class="ft219">TypedQuery</p>
<p style="position:absolute;top:704px;left:550px;white-space:nowrap" class="ft215">,&#160;</p>
<p style="position:absolute;top:721px;left:62px;white-space:nowrap" class="ft219">NamedQuery</p>
<p style="position:absolute;top:721px;left:140px;white-space:nowrap" class="ft215">&#160;and&#160;</p>
<p style="position:absolute;top:721px;left:169px;white-space:nowrap" class="ft219">Criteria&#160;API</p>
<p style="position:absolute;top:721px;left:237px;white-space:nowrap" class="ft215">.&#160;TypedQuery&#160;and&#160;NamedQuery&#160;are&#160;two&#160;additional&#160;Query&#160;sub-types.</p>
<p style="position:absolute;top:755px;left:220px;white-space:nowrap" class="ft2110">Query query=session.createQuery();</p>
<p style="position:absolute;top:771px;left:314px;white-space:nowrap" class="ft2110">or</p>
<p style="position:absolute;top:787px;left:200px;white-space:nowrap" class="ft2110">TypedQuery query = session.createQuery();</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft220{font-size:6px;font-family:Times;color:#595959;}
	.ft221{font-size:24px;font-family:Times;color:#0079c0;}
	.ft222{font-size:20px;font-family:Times;color:#0079c0;}
	.ft223{font-size:10px;font-family:Times;color:#212121;}
	.ft224{font-size:10px;font-family:Times;color:#0079c0;}
	.ft225{font-size:10px;font-family:Times;color:#990000;}
	.ft226{font-size:8px;font-family:Times;color:#09507c;}
	.ft227{font-size:8px;font-family:Times;color:#0079c0;}
	.ft228{font-size:8px;font-family:Times;color:#273239;}
	.ft229{font-size:8px;font-family:Times;color:#990000;}
	.ft2210{font-size:8px;font-family:Times;color:#351c75;}
	.ft2211{font-size:8px;font-family:Times;color:#000000;}
	.ft2212{font-size:22px;font-family:Times;color:#0079c0;}
	.ft2213{font-size:11px;font-family:Times;color:#212121;}
	.ft2214{font-size:14px;font-family:Times;color:#212121;}
	.ft2215{font-size:12px;font-family:Times;color:#e69138;}
	.ft2216{font-size:13px;font-family:Times;color:#212121;}
	.ft2217{font-size:10px;line-height:15px;font-family:Times;color:#212121;}
	.ft2218{font-size:10px;line-height:14px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page22-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target022.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft220">43</p>
<p style="position:absolute;top:108px;left:28px;white-space:nowrap" class="ft221">HQL&#160;-&#160;Interface&#160;</p>
<p style="position:absolute;top:112px;left:227px;white-space:nowrap" class="ft222">(continued)</p>
<p style="position:absolute;top:315px;left:36px;white-space:nowrap" class="ft2217">In&#160;the&#160;above&#160;code,&#160;the&#160;same&#160;HQL&#160;query&#160;retrieves&#160;all&#160;of&#160;the&#160;Employee&#160;objects&#160;in&#160;the&#160;database&#160;is&#160;<br/>represented&#160;by&#160;both&#160;q1&#160;and&#160;q2.&#160;When&#160;building&#160;a&#160;TypedQuery&#160;instance,&#160;the&#160;expected&#160;result&#160;type&#160;has&#160;to&#160;be&#160;<br/>passed&#160;as&#160;an&#160;additional&#160;argument,&#160;as&#160;demonstrated&#160;for&#160;</p>
<p style="position:absolute;top:346px;left:345px;white-space:nowrap" class="ft224">q2</p>
<p style="position:absolute;top:346px;left:358px;white-space:nowrap" class="ft223">,&#160;because&#160;in&#160;this&#160;case,&#160;the&#160;result&#160;type&#160;is&#160;known&#160;</p>
<p style="position:absolute;top:362px;left:36px;white-space:nowrap" class="ft223">(the&#160;query&#160;returns&#160;only&#160;</p>
<p style="position:absolute;top:364px;left:164px;white-space:nowrap" class="ft225">Employee&#160;</p>
<p style="position:absolute;top:362px;left:226px;white-space:nowrap" class="ft223">objects),&#160;a&#160;TypedQuery is preferred.</p>
<p style="position:absolute;top:270px;left:39px;white-space:nowrap" class="ft226">Query&#160;</p>
<p style="position:absolute;top:270px;left:74px;white-space:nowrap" class="ft227">q1 =&#160;</p>
<p style="position:absolute;top:270px;left:104px;white-space:nowrap" class="ft228">session</p>
<p style="position:absolute;top:270px;left:145px;white-space:nowrap" class="ft226">.createQuery(</p>
<p style="position:absolute;top:270px;left:221px;white-space:nowrap" class="ft229">&#34;SELECT c FROM Employee e&#34;</p>
<p style="position:absolute;top:270px;left:374px;white-space:nowrap" class="ft226">);&#160;</p>
<p style="position:absolute;top:290px;left:39px;white-space:nowrap" class="ft226">TypedQuery&lt;Employee&gt;&#160;</p>
<p style="position:absolute;top:290px;left:162px;white-space:nowrap" class="ft227">q2 =</p>
<p style="position:absolute;top:290px;left:186px;white-space:nowrap" class="ft228">session</p>
<p style="position:absolute;top:290px;left:227px;white-space:nowrap" class="ft226">.createQuery(</p>
<p style="position:absolute;top:290px;left:304px;white-space:nowrap" class="ft229">&#34;SELECT c FROM Employee e&#34;</p>
<p style="position:absolute;top:290px;left:457px;white-space:nowrap" class="ft226">,&#160;</p>
<p style="position:absolute;top:290px;left:469px;white-space:nowrap" class="ft229">Employee&#160;</p>
<p style="position:absolute;top:290px;left:522px;white-space:nowrap" class="ft226">.</p>
<p style="position:absolute;top:290px;left:528px;white-space:nowrap" class="ft2210">class</p>
<p style="position:absolute;top:290px;left:557px;white-space:nowrap" class="ft226">);&#160;</p>
<p style="position:absolute;top:152px;left:32px;white-space:nowrap" class="ft2218">Query&#160;interface&#160;should&#160;be&#160;used&#160;mainly&#160;when&#160;the&#160;query&#160;result&#160;type&#160;is&#160;unknown&#160;or&#160;when&#160;a&#160;query&#160;returns&#160;<br/>polymorphic&#160;results,&#160;and&#160;the&#160;lowest&#160;known&#160;common&#160;denominator&#160;of&#160;all&#160;the&#160;result&#160;objects&#160;is&#160;Object.&#160;<br/>When&#160;a&#160;more&#160;specific&#160;result&#160;type&#160;is&#160;expected,&#160;queries&#160;should&#160;usually&#160;use&#160;the&#160;TypedQuery&#160;interface.&#160;It&#160;is&#160;<br/>easier&#160;to&#160;run&#160;queries&#160;and&#160;process&#160;the&#160;query&#160;results&#160;in&#160;a&#160;type&#160;safe&#160;manner&#160;when&#160;using&#160;the&#160;TypedQuery&#160;<br/>interface.</p>
<p style="position:absolute;top:235px;left:39px;white-space:nowrap" class="ft2211">Example:&#160;Building&#160;Queries&#160;with&#160;createQuery</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft220">44</p>
<p style="position:absolute;top:525px;left:25px;white-space:nowrap" class="ft2212">Execution&#160;HQL&#160;Queries</p>
<p style="position:absolute;top:570px;left:38px;white-space:nowrap" class="ft2213">The&#160;</p>
<p style="position:absolute;top:569px;left:65px;white-space:nowrap" class="ft2214">Query&#160;</p>
<p style="position:absolute;top:570px;left:117px;white-space:nowrap" class="ft2213">interface&#160;defines&#160;two&#160;methods&#160;for&#160;running&#160;SELECT&#160;queries:</p>
<p style="position:absolute;top:604px;left:58px;white-space:nowrap" class="ft2215">●</p>
<p style="position:absolute;top:608px;left:78px;white-space:nowrap" class="ft223">Query.getSingleResult</p>
<p style="position:absolute;top:605px;left:223px;white-space:nowrap" class="ft2213">&#160;-&#160;use&#160;when&#160;exactly&#160;one&#160;result&#160;object&#160;is&#160;expected.</p>
<p style="position:absolute;top:622px;left:58px;white-space:nowrap" class="ft2215">●</p>
<p style="position:absolute;top:626px;left:78px;white-space:nowrap" class="ft223">Query.getResultList</p>
<p style="position:absolute;top:623px;left:209px;white-space:nowrap" class="ft2213">&#160;-&#160;use&#160;when&#160;multiple&#160;result&#160;objects&#160;are&#160;expected.</p>
<p style="position:absolute;top:660px;left:38px;white-space:nowrap" class="ft2213">The&#160;</p>
<p style="position:absolute;top:659px;left:65px;white-space:nowrap" class="ft2216">TypedQuery&#160;</p>
<p style="position:absolute;top:660px;left:160px;white-space:nowrap" class="ft2213">interface&#160;defines&#160;the&#160;following&#160;methods:</p>
<p style="position:absolute;top:693px;left:58px;white-space:nowrap" class="ft2215">●</p>
<p style="position:absolute;top:697px;left:78px;white-space:nowrap" class="ft223">TypedQuery.getSingleResult</p>
<p style="position:absolute;top:694px;left:257px;white-space:nowrap" class="ft2213">&#160;-&#160;use&#160;when&#160;exactly&#160;one&#160;result&#160;object&#160;is&#160;expected.</p>
<p style="position:absolute;top:711px;left:58px;white-space:nowrap" class="ft2215">●</p>
<p style="position:absolute;top:715px;left:78px;white-space:nowrap" class="ft223">TypedQuery.getResultList</p>
<p style="position:absolute;top:712px;left:243px;white-space:nowrap" class="ft2213">&#160;-&#160;use&#160;when&#160;multiple&#160;result&#160;objects&#160;are&#160;expected</p>
<p style="position:absolute;top:747px;left:38px;white-space:nowrap" class="ft2213">In&#160;addition,&#160;the&#160;Query&#160;interface&#160;defines&#160;a&#160;method&#160;for&#160;running&#160;DELETE&#160;and&#160;UPDATE&#160;queries:</p>
<p style="position:absolute;top:780px;left:58px;white-space:nowrap" class="ft2215">●</p>
<p style="position:absolute;top:784px;left:78px;white-space:nowrap" class="ft223">Query.executeUpdate</p>
<p style="position:absolute;top:781px;left:209px;white-space:nowrap" class="ft2213">&#160;-&#160;for&#160;running&#160;only&#160;DELETE&#160;and&#160;UPDATE&#160;queries.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft230{font-size:6px;font-family:Times;color:#595959;}
	.ft231{font-size:22px;font-family:Times;color:#0079c0;}
	.ft232{font-size:10px;font-family:Times;color:#222222;}
	.ft233{font-size:10px;font-family:Times;color:#e69138;}
	.ft234{font-size:10px;font-family:Times;color:#0f3b68;}
	.ft235{font-size:10px;font-family:Times;color:#232629;}
	.ft236{font-size:10px;font-family:Times;color:#ffab40;}
	.ft237{font-size:18px;font-family:Times;color:#0079c0;}
	.ft238{font-size:10px;font-family:Times;color:#212121;}
	.ft239{font-size:10px;font-family:Times;color:#1a3d3c;}
	.ft2310{font-size:6px;font-family:Times;color:#212121;}
	.ft2311{font-size:9px;font-family:Times;color:#0097a7;}
	.ft2312{font-size:7px;font-family:Times;color:#1d1c1d;}
	.ft2313{font-size:6px;font-family:Times;color:#e69138;}
	.ft2314{font-size:10px;line-height:14px;font-family:Times;color:#222222;}
	.ft2315{font-size:10px;line-height:17px;font-family:Times;color:#222222;}
	.ft2316{font-size:10px;line-height:14px;font-family:Times;color:#212121;}
	.ft2317{font-size:7px;line-height:12px;font-family:Times;color:#1d1c1d;}
	.ft2318{font-size:6px;line-height:11px;font-family:Times;color:#e69138;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page23-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target023.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft230">45</p>
<p style="position:absolute;top:109px;left:25px;white-space:nowrap" class="ft231">HQL&#160;Methods</p>
<p style="position:absolute;top:143px;left:37px;white-space:nowrap" class="ft232">Here&#160;are&#160;a&#160;few&#160;important&#160;Query&#160;methods,&#160;which&#160;will&#160;be&#160;used&#160;often&#160;in&#160;Hibernate&#160;implementations:</p>
<p style="position:absolute;top:160px;left:39px;white-space:nowrap" class="ft233">●</p>
<p style="position:absolute;top:160px;left:62px;white-space:nowrap" class="ft234">getSingleResult:</p>
<p style="position:absolute;top:160px;left:161px;white-space:nowrap" class="ft232">&#160;Execute&#160;a&#160;SELECT&#160;query&#160;that&#160;returns&#160;a&#160;single&#160;result.</p>
<p style="position:absolute;top:184px;left:39px;white-space:nowrap" class="ft233">●</p>
<p style="position:absolute;top:184px;left:62px;white-space:nowrap" class="ft234">list():&#160;</p>
<p style="position:absolute;top:184px;left:96px;white-space:nowrap" class="ft232">Return&#160;the&#160;query&#160;results&#160;as&#160;a&#160;list.&#160;If&#160;the&#160;query&#160;contains&#160;multiple&#160;results&#160;per&#160;row,&#160;the&#160;results&#160;</p>
<p style="position:absolute;top:199px;left:62px;white-space:nowrap" class="ft2314">are&#160;returned&#160;in&#160;an&#160;instance&#160;of&#160;Object[],&#160;but&#160;use&#160;the&#160;getResultList()&#160;method&#160;instead&#160;of&#160;list()&#160;<br/>because&#160;this&#160;is&#160;a&#160;deprecated&#160;(out&#160;of&#160;date)&#160;method.</p>
<p style="position:absolute;top:238px;left:39px;white-space:nowrap" class="ft233">●</p>
<p style="position:absolute;top:238px;left:62px;white-space:nowrap" class="ft234">getResultList():</p>
<p style="position:absolute;top:238px;left:154px;white-space:nowrap" class="ft235">&#160;</p>
<p style="position:absolute;top:238px;left:157px;white-space:nowrap" class="ft232">The&#160;default&#160;implementation&#160;of&#160;the&#160;getResultList()&#160;method&#160;is&#160;in&#160;org.hibernate&#160;</p>
<p style="position:absolute;top:253px;left:62px;white-space:nowrap" class="ft2314">package.&#160;Query&#160;calls&#160;the&#160;list()&#160;method.&#160;Execute&#160;a&#160;SELECT&#160;query&#160;and&#160;return&#160;the&#160;query&#160;results&#160;as&#160;<br/>an&#160;untyped&#160;list.</p>
<p style="position:absolute;top:292px;left:37px;white-space:nowrap" class="ft232">General&#160;rule&#160;for&#160;the&#160;getResultList():</p>
<p style="position:absolute;top:320px;left:39px;white-space:nowrap" class="ft236">●</p>
<p style="position:absolute;top:320px;left:62px;white-space:nowrap" class="ft232">If&#160;select&#160;contains&#160;a&#160;single&#160;expression&#160;and&#160;it&#160;is&#160;an&#160;entity,&#160;the&#160;result&#160;is&#160;that&#160;entity.</p>
<p style="position:absolute;top:337px;left:39px;white-space:nowrap" class="ft236">●</p>
<p style="position:absolute;top:337px;left:62px;white-space:nowrap" class="ft232">If&#160;select&#160;contains&#160;a&#160;single&#160;expression&#160;and&#160;it&#160;is&#160;a&#160;primitive,&#160;the&#160;result&#160;is&#160;that&#160;primitive.</p>
<p style="position:absolute;top:355px;left:39px;white-space:nowrap" class="ft236">●</p>
<p style="position:absolute;top:355px;left:62px;white-space:nowrap" class="ft2315">If&#160;select&#160;contains&#160;multiple&#160;expressions,&#160;the&#160;result&#160;is&#160;List&lt;Object[]&gt;,&#160;containing&#160;the&#160;corresponding&#160;<br/>primitives/entities.&#160;</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft230">46</p>
<p style="position:absolute;top:524px;left:24px;white-space:nowrap" class="ft231">HQL&#160;Methods&#160;</p>
<p style="position:absolute;top:528px;left:193px;white-space:nowrap" class="ft237">(continued)</p>
<p style="position:absolute;top:565px;left:34px;white-space:nowrap" class="ft233">●</p>
<p style="position:absolute;top:565px;left:47px;white-space:nowrap" class="ft234">executeUpdate():&#160;</p>
<p style="position:absolute;top:565px;left:152px;white-space:nowrap" class="ft238">This&#160;method&#160;is&#160;used&#160;to&#160;run&#160;the&#160;update/delete&#160;query</p>
<p style="position:absolute;top:565px;left:434px;white-space:nowrap" class="ft239">.</p>
<p style="position:absolute;top:565px;left:437px;white-space:nowrap" class="ft238">&#160;It&#160;returns&#160;the&#160;number&#160;of&#160;</p>
<p style="position:absolute;top:580px;left:47px;white-space:nowrap" class="ft238">entities&#160;updated&#160;or&#160;deleted.</p>
<p style="position:absolute;top:604px;left:34px;white-space:nowrap" class="ft233">●</p>
<p style="position:absolute;top:604px;left:47px;white-space:nowrap" class="ft234">setParameter():&#160;</p>
<p style="position:absolute;top:604px;left:142px;white-space:nowrap" class="ft238">Bind&#160;a&#160;value&#160;to&#160;a&#160;JDBC-style&#160;query&#160;parameter.&#160;The&#160;Hibernate&#160;type&#160;of&#160;the&#160;parameter&#160;</p>
<p style="position:absolute;top:619px;left:47px;white-space:nowrap" class="ft2316">is&#160;first&#160;detected&#160;via&#160;the&#160;usage/position&#160;in&#160;the&#160;query,&#160;and&#160;if&#160;not&#160;sufficient,&#160;secondly&#160;guessed&#160;from&#160;the&#160;<br/>class&#160;of&#160;the&#160;given&#160;object.</p>
<p style="position:absolute;top:658px;left:34px;white-space:nowrap" class="ft233">●</p>
<p style="position:absolute;top:658px;left:47px;white-space:nowrap" class="ft234">setMaxResults():&#160;</p>
<p style="position:absolute;top:658px;left:150px;white-space:nowrap" class="ft238">Set&#160;the&#160;maximum&#160;number&#160;of&#160;rows&#160;to&#160;retrieve.&#160;If&#160;not&#160;set,&#160;there&#160;is&#160;no&#160;limit&#160;to&#160;the&#160;</p>
<p style="position:absolute;top:673px;left:47px;white-space:nowrap" class="ft238">number&#160;of&#160;rows&#160;retrieved.</p>
<p style="position:absolute;top:689px;left:34px;white-space:nowrap" class="ft2310">Visit&#160;the&#160;link&#160;below&#160;for&#160;more&#160;information.</p>
<p style="position:absolute;top:703px;left:34px;white-space:nowrap" class="ft2311">https://docs.jboss.org/hibernate/jpa/2.1/api/javax/persistence/Query.html#getResultList()</p>
<p style="position:absolute;top:738px;left:29px;white-space:nowrap" class="ft2317"><i>Note:&#160;Because&#160;Hibernate&#160;libraries&#160;are&#160;often&#160;updated,&#160;some&#160;methods,&#160;interfaces,&#160;and&#160;constructors&#160;may&#160;be&#160;deprecated.&#160;View&#160;the&#160;official&#160;<br/>document&#160;link&#160;below&#160;to&#160;view&#160;the&#160;deprecated&#160;list,&#160;and&#160;replace&#160;the&#160;version:</i></p>
<p style="position:absolute;top:763px;left:29px;white-space:nowrap" class="ft2318">https://docs.jboss.org/hibernate/orm/3.2/api/org/hibernate/engine/NamedSQLQueryDefinition.html#NamedSQLQueryDefinition(java.lang.String,%2<br/>0java.lang.String,%20java.util.List,%20boolean,%20java.lang.String,%20java.lang.Integer,%20java.lang.Integer,%20org.hibernate.FlushMode,%20j<br/>ava.util.Map,%20boolean)</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft240{font-size:6px;font-family:Times;color:#595959;}
	.ft241{font-size:20px;font-family:Times;color:#0079c0;}
	.ft242{font-size:12px;font-family:Times;color:#212121;}
	.ft243{font-size:10px;font-family:Times;color:#212121;}
	.ft244{font-size:6px;font-family:Times;color:#cc7832;}
	.ft245{font-size:6px;font-family:Times;color:#ffc66d;}
	.ft246{font-size:6px;font-family:Times;color:#a9b7c6;}
	.ft247{font-size:6px;font-family:Times;color:#a8c023;}
	.ft248{font-size:6px;font-family:Times;color:#6a8759;}
	.ft249{font-size:7px;font-family:Times;color:#610b4b;}
	.ft2410{font-size:6px;font-family:Times;color:#9876aa;}
	.ft2411{font-size:18px;font-family:Times;color:#0079c0;}
	.ft2412{font-size:9px;font-family:Times;color:#212121;}
	.ft2413{font-size:6px;font-family:Times;color:#610b4b;}
	.ft2414{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
	.ft2415{font-size:9px;line-height:13px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page24-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target024.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft240">47</p>
<p style="position:absolute;top:113px;left:27px;white-space:nowrap" class="ft241">Clauses&#160;in&#160;the&#160;HQL</p>
<p style="position:absolute;top:152px;left:41px;white-space:nowrap" class="ft242">FROM&#160;Clause:</p>
<p style="position:absolute;top:182px;left:41px;white-space:nowrap" class="ft2414">You&#160;will&#160;use&#160;the&#160;FROM&#160;clause&#160;if&#160;you&#160;want&#160;to&#160;load&#160;a&#160;complete&#160;persistent&#160;object&#160;into&#160;memory.&#160;Following&#160;<br/>is&#160;the&#160;simple&#160;syntax&#160;of&#160;using&#160;the&#160;FROM&#160;clause:</p>
<p style="position:absolute;top:238px;left:40px;white-space:nowrap" class="ft244">public static void&#160;</p>
<p style="position:absolute;top:238px;left:133px;white-space:nowrap" class="ft245">main</p>
<p style="position:absolute;top:238px;left:152px;white-space:nowrap" class="ft246">(String[] args) {</p>
<p style="position:absolute;top:248px;left:40px;white-space:nowrap" class="ft246">&#160;</p>
<p style="position:absolute;top:248px;left:45px;white-space:nowrap" class="ft247"><i>&#160;&#160;</i></p>
<p style="position:absolute;top:248px;left:54px;white-space:nowrap" class="ft246">SessionFactory factory =&#160;</p>
<p style="position:absolute;top:248px;left:177px;white-space:nowrap" class="ft244">new&#160;</p>
<p style="position:absolute;top:248px;left:197px;white-space:nowrap" class="ft246">Configuration().configure().buildSessionFactory()</p>
<p style="position:absolute;top:248px;left:437px;white-space:nowrap" class="ft244">;</p>
<p style="position:absolute;top:259px;left:40px;white-space:nowrap" class="ft244">&#160; &#160;</p>
<p style="position:absolute;top:259px;left:54px;white-space:nowrap" class="ft246">Session session = factory.openSession()</p>
<p style="position:absolute;top:259px;left:246px;white-space:nowrap" class="ft244">;</p>
<p style="position:absolute;top:270px;left:40px;white-space:nowrap" class="ft244">&#160; &#160;</p>
<p style="position:absolute;top:270px;left:54px;white-space:nowrap" class="ft246">String hql =&#160;</p>
<p style="position:absolute;top:270px;left:118px;white-space:nowrap" class="ft248">&#34;FROM User&#34;</p>
<p style="position:absolute;top:270px;left:172px;white-space:nowrap" class="ft244">;&#160;</p>
<p style="position:absolute;top:270px;left:182px;white-space:nowrap" class="ft249"><i>// Example of HQL to get all records of user class</i></p>
<p style="position:absolute;top:281px;left:40px;white-space:nowrap" class="ft244">&#160; &#160;</p>
<p style="position:absolute;top:281px;left:54px;white-space:nowrap" class="ft246">TypedQuery query = session.createQuery(hql)</p>
<p style="position:absolute;top:281px;left:265px;white-space:nowrap" class="ft244">;</p>
<p style="position:absolute;top:292px;left:40px;white-space:nowrap" class="ft244">&#160; &#160;</p>
<p style="position:absolute;top:292px;left:54px;white-space:nowrap" class="ft246">List&lt;User&gt; results = query.getResultList()</p>
<p style="position:absolute;top:292px;left:260px;white-space:nowrap" class="ft244">;</p>
<p style="position:absolute;top:302px;left:40px;white-space:nowrap" class="ft244">&#160; &#160;for&#160;</p>
<p style="position:absolute;top:302px;left:74px;white-space:nowrap" class="ft246">(User u : results) {</p>
<p style="position:absolute;top:313px;left:40px;white-space:nowrap" class="ft246">&#160; &#160; &#160; &#160;System.</p>
<p style="position:absolute;top:313px;left:108px;white-space:nowrap" class="ft2410"><i>out</i></p>
<p style="position:absolute;top:313px;left:123px;white-space:nowrap" class="ft246">.println(</p>
<p style="position:absolute;top:313px;left:167px;white-space:nowrap" class="ft248">&#34;User Id: &#34;&#160;</p>
<p style="position:absolute;top:313px;left:226px;white-space:nowrap" class="ft246">+u.getId() +&#160;</p>
<p style="position:absolute;top:313px;left:290px;white-space:nowrap" class="ft248">&#34;|&#34;&#160;</p>
<p style="position:absolute;top:313px;left:309px;white-space:nowrap" class="ft246">+&#160;</p>
<p style="position:absolute;top:313px;left:319px;white-space:nowrap" class="ft248">&#34; Full name:&#34;&#160;</p>
<p style="position:absolute;top:313px;left:388px;white-space:nowrap" class="ft246">+ u.getFullname() +</p>
<p style="position:absolute;top:313px;left:481px;white-space:nowrap" class="ft248">&#34;|&#34;</p>
<p style="position:absolute;top:313px;left:496px;white-space:nowrap" class="ft246">+&#160;</p>
<p style="position:absolute;top:313px;left:506px;white-space:nowrap" class="ft248">&#34;Email: &#34;&#160;</p>
<p style="position:absolute;top:313px;left:555px;white-space:nowrap" class="ft246">+&#160;</p>
<p style="position:absolute;top:324px;left:40px;white-space:nowrap" class="ft246">u.getEmail() +</p>
<p style="position:absolute;top:324px;left:108px;white-space:nowrap" class="ft248">&#34;|&#34;</p>
<p style="position:absolute;top:324px;left:123px;white-space:nowrap" class="ft246">+&#160;</p>
<p style="position:absolute;top:324px;left:133px;white-space:nowrap" class="ft248">&#34;password&#34;&#160;</p>
<p style="position:absolute;top:324px;left:187px;white-space:nowrap" class="ft246">+ u.getPassword())</p>
<p style="position:absolute;top:324px;left:275px;white-space:nowrap" class="ft244">;</p>
<p style="position:absolute;top:335px;left:40px;white-space:nowrap" class="ft244">&#160; &#160;</p>
<p style="position:absolute;top:335px;left:54px;white-space:nowrap" class="ft246">}</p>
<p style="position:absolute;top:345px;left:40px;white-space:nowrap" class="ft246">}</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft240">48</p>
<p style="position:absolute;top:520px;left:25px;white-space:nowrap" class="ft2411">Clauses&#160;in&#160;the&#160;HQL&#160;</p>
<p style="position:absolute;top:519px;left:219px;white-space:nowrap" class="ft241">(continued)</p>
<p style="position:absolute;top:552px;left:30px;white-space:nowrap" class="ft243">WHERE&#160;Clause:&#160;</p>
<p style="position:absolute;top:553px;left:129px;white-space:nowrap" class="ft2412">If&#160;you&#160;want&#160;to&#160;narrow&#160;the&#160;specific&#160;objects&#160;that&#160;are&#160;returned&#160;from&#160;storage,&#160;use&#160;the&#160;WHERE&#160;</p>
<p style="position:absolute;top:567px;left:30px;white-space:nowrap" class="ft2412">clause.&#160;Following&#160;is&#160;the&#160;simple&#160;syntax&#160;of&#160;using&#160;the&#160;WHERE&#160;clause:</p>
<p style="position:absolute;top:594px;left:25px;white-space:nowrap" class="ft244">public static void&#160;</p>
<p style="position:absolute;top:594px;left:109px;white-space:nowrap" class="ft245">main</p>
<p style="position:absolute;top:594px;left:127px;white-space:nowrap" class="ft246">(String[] args) {</p>
<p style="position:absolute;top:603px;left:25px;white-space:nowrap" class="ft246">&#160;</p>
<p style="position:absolute;top:603px;left:30px;white-space:nowrap" class="ft247"><i>&#160;&#160;</i></p>
<p style="position:absolute;top:603px;left:38px;white-space:nowrap" class="ft246">SessionFactory factory =&#160;</p>
<p style="position:absolute;top:603px;left:149px;white-space:nowrap" class="ft244">new&#160;</p>
<p style="position:absolute;top:603px;left:166px;white-space:nowrap" class="ft246">Configuration().configure().buildSessionFactory()</p>
<p style="position:absolute;top:603px;left:383px;white-space:nowrap" class="ft244">;</p>
<p style="position:absolute;top:613px;left:25px;white-space:nowrap" class="ft244">&#160; &#160;</p>
<p style="position:absolute;top:613px;left:38px;white-space:nowrap" class="ft246">Session session = factory.openSession()</p>
<p style="position:absolute;top:613px;left:211px;white-space:nowrap" class="ft244">;</p>
<p style="position:absolute;top:622px;left:25px;white-space:nowrap" class="ft244">&#160; &#160;</p>
<p style="position:absolute;top:622px;left:38px;white-space:nowrap" class="ft246">String hql =&#160;</p>
<p style="position:absolute;top:622px;left:96px;white-space:nowrap" class="ft248">&#34;FROM User u WHERE u.id = 2&#34;&#160;</p>
<p style="position:absolute;top:622px;left:224px;white-space:nowrap" class="ft244">;&#160;</p>
<p style="position:absolute;top:622px;left:233px;white-space:nowrap" class="ft2413"><i>// Example of HQL to get all records of user class</i></p>
<p style="position:absolute;top:632px;left:25px;white-space:nowrap" class="ft244">&#160; &#160;</p>
<p style="position:absolute;top:632px;left:38px;white-space:nowrap" class="ft246">TypedQuery query = session.createQuery(hql)</p>
<p style="position:absolute;top:632px;left:228px;white-space:nowrap" class="ft244">;</p>
<p style="position:absolute;top:642px;left:25px;white-space:nowrap" class="ft244">&#160; &#160;</p>
<p style="position:absolute;top:642px;left:38px;white-space:nowrap" class="ft246">List&lt;User&gt; results = query.getResultList()</p>
<p style="position:absolute;top:642px;left:224px;white-space:nowrap" class="ft244">;</p>
<p style="position:absolute;top:651px;left:25px;white-space:nowrap" class="ft244">&#160; &#160;for&#160;</p>
<p style="position:absolute;top:651px;left:56px;white-space:nowrap" class="ft246">(User u : results) {</p>
<p style="position:absolute;top:661px;left:25px;white-space:nowrap" class="ft246">&#160; &#160; &#160; &#160;System.</p>
<p style="position:absolute;top:661px;left:87px;white-space:nowrap" class="ft2410"><i>out</i></p>
<p style="position:absolute;top:661px;left:100px;white-space:nowrap" class="ft246">.println(</p>
<p style="position:absolute;top:661px;left:140px;white-space:nowrap" class="ft248">&#34;User Id: &#34;&#160;</p>
<p style="position:absolute;top:661px;left:193px;white-space:nowrap" class="ft246">+u.getId() +&#160;</p>
<p style="position:absolute;top:661px;left:250px;white-space:nowrap" class="ft248">&#34;|&#34;&#160;</p>
<p style="position:absolute;top:661px;left:268px;white-space:nowrap" class="ft246">+&#160;</p>
<p style="position:absolute;top:661px;left:277px;white-space:nowrap" class="ft248">&#34; Full name:&#34;&#160;</p>
<p style="position:absolute;top:661px;left:339px;white-space:nowrap" class="ft246">+ u.getFullname() +</p>
<p style="position:absolute;top:661px;left:423px;white-space:nowrap" class="ft248">&#34;|&#34;</p>
<p style="position:absolute;top:661px;left:436px;white-space:nowrap" class="ft246">+&#160;</p>
<p style="position:absolute;top:661px;left:445px;white-space:nowrap" class="ft248">&#34;Email: &#34;&#160;</p>
<p style="position:absolute;top:661px;left:489px;white-space:nowrap" class="ft246">+ u.getEmail() +</p>
<p style="position:absolute;top:661px;left:559px;white-space:nowrap" class="ft248">&#34;|&#34;</p>
<p style="position:absolute;top:661px;left:573px;white-space:nowrap" class="ft246">+&#160;</p>
<p style="position:absolute;top:671px;left:25px;white-space:nowrap" class="ft248">&#34;password&#34;&#160;</p>
<p style="position:absolute;top:671px;left:74px;white-space:nowrap" class="ft246">+ u.getPassword())</p>
<p style="position:absolute;top:671px;left:153px;white-space:nowrap" class="ft244">;</p>
<p style="position:absolute;top:680px;left:25px;white-space:nowrap" class="ft244">&#160; &#160;</p>
<p style="position:absolute;top:680px;left:38px;white-space:nowrap" class="ft246">}}</p>
<p style="position:absolute;top:706px;left:134px;white-space:nowrap" class="ft2412">HQL&#160;supports&#160;the&#160;list&#160;of&#160;expressions&#160;that&#160;are&#160;used&#160;in&#160;the&#160;where&#160;clause:</p>
<p style="position:absolute;top:720px;left:143px;white-space:nowrap" class="ft2412">●</p>
<p style="position:absolute;top:720px;left:166px;white-space:nowrap" class="ft2412">math&#160;operators:&#160;+,&#160;-,&#160;*,&#160;/</p>
<p style="position:absolute;top:734px;left:143px;white-space:nowrap" class="ft2412">●</p>
<p style="position:absolute;top:734px;left:166px;white-space:nowrap" class="ft2412">comparison&#160;operators:&#160;=,&#160;&gt;=,&#160;&lt;=,&#160;&lt;&gt;,&#160;!=,&#160;like</p>
<p style="position:absolute;top:748px;left:143px;white-space:nowrap" class="ft2412">●</p>
<p style="position:absolute;top:748px;left:166px;white-space:nowrap" class="ft2412">logical&#160;operations:&#160;and,&#160;or,&#160;not</p>
<p style="position:absolute;top:762px;left:143px;white-space:nowrap" class="ft2412">●</p>
<p style="position:absolute;top:762px;left:166px;white-space:nowrap" class="ft2415">is&#160;empty,&#160;is&#160;not&#160;empty,&#160;in,&#160;not&#160;in,&#160;between,&#160;is&#160;null,&#160;is&#160;not&#160;null,&#160;member&#160;of&#160;and&#160;not&#160;<br/>member&#160;of</p>
<p style="position:absolute;top:790px;left:143px;white-space:nowrap" class="ft2412">●</p>
<p style="position:absolute;top:790px;left:166px;white-space:nowrap" class="ft2412">string&#160;concatenation&#160;concat(…,…)</p>
<p style="position:absolute;top:804px;left:143px;white-space:nowrap" class="ft2412">●</p>
<p style="position:absolute;top:804px;left:166px;white-space:nowrap" class="ft2412">current_date(),&#160;current_time(),&#160;and&#160;current_timestamp()</p>
<p style="position:absolute;top:818px;left:143px;white-space:nowrap" class="ft2412">●</p>
<p style="position:absolute;top:818px;left:166px;white-space:nowrap" class="ft2412">str()&#160;for&#160;converting&#160;values&#160;to&#160;a&#160;readable&#160;string</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft250{font-size:6px;font-family:Times;color:#595959;}
	.ft251{font-size:18px;font-family:Times;color:#0079c0;}
	.ft252{font-size:20px;font-family:Times;color:#0079c0;}
	.ft253{font-size:12px;font-family:Times;color:#212121;}
	.ft254{font-size:10px;font-family:Times;color:#212121;}
	.ft255{font-size:7px;font-family:Times;color:#cc7832;}
	.ft256{font-size:7px;font-family:Times;color:#ffc66d;}
	.ft257{font-size:7px;font-family:Times;color:#a9b7c6;}
	.ft258{font-size:7px;font-family:Times;color:#a8c023;}
	.ft259{font-size:7px;font-family:Times;color:#6a8759;}
	.ft2510{font-size:7px;font-family:Times;color:#9876aa;}
	.ft2511{font-size:6px;font-family:Times;color:#a9b7c6;}
	.ft2512{font-size:6px;font-family:Times;color:#6a8759;}
	.ft2513{font-size:6px;font-family:Times;color:#cc7832;}
	.ft2514{font-size:7px;font-family:Times;color:#6897bb;}
	.ft2515{font-size:10px;font-family:Times;color:#000000;}
	.ft2516{font-size:11px;font-family:Times;color:#09507c;}
	.ft2517{font-size:10px;font-family:Times;color:#741b47;}
	.ft2518{font-size:10px;font-family:Times;color:#09507c;}
	.ft2519{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
	.ft2520{font-size:10px;line-height:15px;font-family:Times;color:#000000;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page25-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target025.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft250">49</p>
<p style="position:absolute;top:105px;left:25px;white-space:nowrap" class="ft251">Clauses&#160;in&#160;the&#160;HQL&#160;</p>
<p style="position:absolute;top:103px;left:219px;white-space:nowrap" class="ft252">(continued)</p>
<p style="position:absolute;top:142px;left:29px;white-space:nowrap" class="ft253">ORDER&#160;BY&#160;Clause:</p>
<p style="position:absolute;top:165px;left:29px;white-space:nowrap" class="ft2519">To&#160;sort&#160;your&#160;HQL&#160;query&#160;results,&#160;you&#160;will&#160;need&#160;to&#160;use&#160;the&#160;ORDER&#160;BY&#160;clause.&#160;You&#160;can&#160;order&#160;the&#160;results&#160;by&#160;<br/>any&#160;property&#160;on&#160;the&#160;objects&#160;in&#160;the&#160;result&#160;set&#160;(ascending&#160;[ASC]&#160;or&#160;descending&#160;[DESC]).&#160;Following&#160;is&#160;the&#160;<br/>simple&#160;syntax&#160;of&#160;using&#160;ORDER&#160;BY&#160;clause:</p>
<p style="position:absolute;top:249px;left:47px;white-space:nowrap" class="ft255">public static void&#160;</p>
<p style="position:absolute;top:249px;left:149px;white-space:nowrap" class="ft256">main</p>
<p style="position:absolute;top:249px;left:171px;white-space:nowrap" class="ft257">(String[] args) {</p>
<p style="position:absolute;top:260px;left:47px;white-space:nowrap" class="ft257">&#160;</p>
<p style="position:absolute;top:260px;left:52px;white-space:nowrap" class="ft258"><i>&#160;&#160;</i></p>
<p style="position:absolute;top:260px;left:63px;white-space:nowrap" class="ft257">SessionFactory factory =&#160;</p>
<p style="position:absolute;top:260px;left:198px;white-space:nowrap" class="ft255">new&#160;</p>
<p style="position:absolute;top:260px;left:220px;white-space:nowrap" class="ft257">Configuration().configure().buildSessionFactory()</p>
<p style="position:absolute;top:260px;left:484px;white-space:nowrap" class="ft255">;</p>
<p style="position:absolute;top:272px;left:47px;white-space:nowrap" class="ft255">&#160; &#160;</p>
<p style="position:absolute;top:272px;left:63px;white-space:nowrap" class="ft257">Session session = factory.openSession()</p>
<p style="position:absolute;top:272px;left:273px;white-space:nowrap" class="ft255">;</p>
<p style="position:absolute;top:284px;left:47px;white-space:nowrap" class="ft255">&#160; &#160;</p>
<p style="position:absolute;top:284px;left:63px;white-space:nowrap" class="ft257">String hql =&#160;</p>
<p style="position:absolute;top:284px;left:133px;white-space:nowrap" class="ft259">&#34;FROM User E WHERE E.id &gt; 3 ORDER BY E.salary DESC&#34;</p>
<p style="position:absolute;top:284px;left:408px;white-space:nowrap" class="ft255">;</p>
<p style="position:absolute;top:296px;left:47px;white-space:nowrap" class="ft255">&#160; &#160;</p>
<p style="position:absolute;top:296px;left:63px;white-space:nowrap" class="ft257">TypedQuery query = session.createQuery(hql)</p>
<p style="position:absolute;top:296px;left:295px;white-space:nowrap" class="ft255">;</p>
<p style="position:absolute;top:308px;left:47px;white-space:nowrap" class="ft255">&#160; &#160;</p>
<p style="position:absolute;top:308px;left:63px;white-space:nowrap" class="ft257">List&lt;User&gt; results = query.getResultList()</p>
<p style="position:absolute;top:308px;left:290px;white-space:nowrap" class="ft255">;</p>
<p style="position:absolute;top:319px;left:47px;white-space:nowrap" class="ft255">&#160; &#160;for&#160;</p>
<p style="position:absolute;top:319px;left:85px;white-space:nowrap" class="ft257">(User u : results) {</p>
<p style="position:absolute;top:331px;left:47px;white-space:nowrap" class="ft257">&#160; &#160; &#160; &#160;System.</p>
<p style="position:absolute;top:331px;left:122px;white-space:nowrap" class="ft2510"><i>out</i></p>
<p style="position:absolute;top:331px;left:139px;white-space:nowrap" class="ft257">.println(</p>
<p style="position:absolute;top:331px;left:187px;white-space:nowrap" class="ft259">&#34;User Id: &#34;&#160;</p>
<p style="position:absolute;top:331px;left:252px;white-space:nowrap" class="ft257">+u.getId() +&#160;</p>
<p style="position:absolute;top:331px;left:322px;white-space:nowrap" class="ft259">&#34;|&#34;&#160;</p>
<p style="position:absolute;top:331px;left:344px;white-space:nowrap" class="ft257">+&#160;</p>
<p style="position:absolute;top:331px;left:354px;white-space:nowrap" class="ft259">&#34; Full name:&#34;&#160;</p>
<p style="position:absolute;top:331px;left:430px;white-space:nowrap" class="ft257">+ u.getFullname() +</p>
<p style="position:absolute;top:331px;left:532px;white-space:nowrap" class="ft259">&#34;|&#34;</p>
<p style="position:absolute;top:331px;left:549px;white-space:nowrap" class="ft257">+&#160;</p>
<p style="position:absolute;top:331px;left:559px;white-space:nowrap" class="ft259">&#34;Email: &#34;&#160;</p>
<p style="position:absolute;top:343px;left:47px;white-space:nowrap" class="ft257">+ u.getEmail() +</p>
<p style="position:absolute;top:343px;left:133px;white-space:nowrap" class="ft259">&#34;|&#34;</p>
<p style="position:absolute;top:343px;left:149px;white-space:nowrap" class="ft257">+&#160;</p>
<p style="position:absolute;top:343px;left:160px;white-space:nowrap" class="ft259">&#34;password&#34;&#160;</p>
<p style="position:absolute;top:343px;left:220px;white-space:nowrap" class="ft257">+ u.getPassword())</p>
<p style="position:absolute;top:343px;left:317px;white-space:nowrap" class="ft255">;</p>
<p style="position:absolute;top:355px;left:47px;white-space:nowrap" class="ft255">&#160; &#160;</p>
<p style="position:absolute;top:355px;left:63px;white-space:nowrap" class="ft257">}</p>
<p style="position:absolute;top:366px;left:47px;white-space:nowrap" class="ft257">}</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft250">50</p>
<p style="position:absolute;top:520px;left:25px;white-space:nowrap" class="ft251">Multiple&#160;SELECT&#160;Expressions</p>
<p style="position:absolute;top:555px;left:21px;white-space:nowrap" class="ft254">The&#160;SELECT&#160;clause&#160;may&#160;also&#160;define&#160;composite&#160;results:&#160;</p>
<p style="position:absolute;top:687px;left:19px;white-space:nowrap" class="ft255">public static void&#160;</p>
<p style="position:absolute;top:687px;left:121px;white-space:nowrap" class="ft256">main</p>
<p style="position:absolute;top:687px;left:143px;white-space:nowrap" class="ft257">(String[] args) {</p>
<p style="position:absolute;top:699px;left:19px;white-space:nowrap" class="ft258"><i>&#160; &#160;</i></p>
<p style="position:absolute;top:699px;left:35px;white-space:nowrap" class="ft257">SessionFactory factory =&#160;</p>
<p style="position:absolute;top:699px;left:170px;white-space:nowrap" class="ft255">new&#160;</p>
<p style="position:absolute;top:699px;left:191px;white-space:nowrap" class="ft257">Configuration().configure().buildSessionFactory()</p>
<p style="position:absolute;top:699px;left:456px;white-space:nowrap" class="ft255">;</p>
<p style="position:absolute;top:711px;left:19px;white-space:nowrap" class="ft255">&#160; &#160;</p>
<p style="position:absolute;top:711px;left:35px;white-space:nowrap" class="ft257">Session session = factory.openSession()</p>
<p style="position:absolute;top:711px;left:245px;white-space:nowrap" class="ft255">;</p>
<p style="position:absolute;top:722px;left:19px;white-space:nowrap" class="ft257">&#160;</p>
<p style="position:absolute;top:723px;left:24px;white-space:nowrap" class="ft2511">&#160;&#160;TypedQuery&lt;Object[]&gt; queryy = session.createQuery(&#160;</p>
<p style="position:absolute;top:723px;left:284px;white-space:nowrap" class="ft2512">&#34;SELECT U.salary, U.fullname FROM User AS U&#34;</p>
<p style="position:absolute;top:723px;left:500px;white-space:nowrap" class="ft2513">,&#160;</p>
<p style="position:absolute;top:723px;left:510px;white-space:nowrap" class="ft2511">Object[].</p>
<p style="position:absolute;top:723px;left:554px;white-space:nowrap" class="ft2513">class</p>
<p style="position:absolute;top:723px;left:578px;white-space:nowrap" class="ft2511">)</p>
<p style="position:absolute;top:723px;left:583px;white-space:nowrap" class="ft2513">;</p>
<p style="position:absolute;top:734px;left:19px;white-space:nowrap" class="ft257">List&lt;Object[]&gt; resultss = queryy.getResultList()</p>
<p style="position:absolute;top:734px;left:278px;white-space:nowrap" class="ft255">;</p>
<p style="position:absolute;top:746px;left:19px;white-space:nowrap" class="ft255">for&#160;</p>
<p style="position:absolute;top:746px;left:40px;white-space:nowrap" class="ft257">(Object[] a : resultss) {</p>
<p style="position:absolute;top:758px;left:19px;white-space:nowrap" class="ft257">&#160; &#160;System.</p>
<p style="position:absolute;top:758px;left:73px;white-space:nowrap" class="ft2510"><i>out</i></p>
<p style="position:absolute;top:758px;left:89px;white-space:nowrap" class="ft257">.println(</p>
<p style="position:absolute;top:758px;left:137px;white-space:nowrap" class="ft259">&#34;Salary: &#34;&#160;</p>
<p style="position:absolute;top:758px;left:197px;white-space:nowrap" class="ft257">+ a[</p>
<p style="position:absolute;top:758px;left:218px;white-space:nowrap" class="ft2514">0</p>
<p style="position:absolute;top:758px;left:224px;white-space:nowrap" class="ft257">] +&#160;</p>
<p style="position:absolute;top:758px;left:245px;white-space:nowrap" class="ft259">&#34;, Full name: &#34;&#160;</p>
<p style="position:absolute;top:758px;left:332px;white-space:nowrap" class="ft257">+ a[</p>
<p style="position:absolute;top:758px;left:353px;white-space:nowrap" class="ft2514">1</p>
<p style="position:absolute;top:758px;left:359px;white-space:nowrap" class="ft257">])</p>
<p style="position:absolute;top:758px;left:369px;white-space:nowrap" class="ft255">;</p>
<p style="position:absolute;top:758px;left:375px;white-space:nowrap" class="ft257">&#160;}</p>
<p style="position:absolute;top:770px;left:19px;white-space:nowrap" class="ft257">}}</p>
<p style="position:absolute;top:622px;left:20px;white-space:nowrap" class="ft2520">The&#160;result&#160;list&#160;of&#160;above&#160;query&#160;contains&#160;Object[]&#160;elements,&#160;one&#160;per&#160;result.&#160;The&#160;length&#160;of&#160;each&#160;result&#160;<br/>Object[]&#160;element&#160;is&#160;2.&#160;The&#160;first&#160;array&#160;cell&#160;contains&#160;the&#160;salary&#160;(</p>
<p style="position:absolute;top:637px;left:359px;white-space:nowrap" class="ft2516">U.salary</p>
<p style="position:absolute;top:638px;left:408px;white-space:nowrap" class="ft2515">)&#160;and&#160;the&#160;second&#160;array&#160;cell&#160;</p>
<p style="position:absolute;top:654px;left:20px;white-space:nowrap" class="ft2515">contains&#160;the&#160;Full&#160;name&#160;(</p>
<p style="position:absolute;top:653px;left:153px;white-space:nowrap" class="ft2516">U.fullname</p>
<p style="position:absolute;top:654px;left:217px;white-space:nowrap" class="ft2515">).&#160;The&#160;following&#160;code&#160;demonstrates&#160;the&#160;above&#160;query</p>
<p style="position:absolute;top:587px;left:27px;white-space:nowrap" class="ft2517">SELECT&#160;</p>
<p style="position:absolute;top:587px;left:76px;white-space:nowrap" class="ft2518">U.salary,</p>
<p style="position:absolute;top:587px;left:137px;white-space:nowrap" class="ft2517">&#160;</p>
<p style="position:absolute;top:587px;left:144px;white-space:nowrap" class="ft2518">U.fullname&#160;</p>
<p style="position:absolute;top:587px;left:220px;white-space:nowrap" class="ft2517">FROM</p>
<p style="position:absolute;top:587px;left:247px;white-space:nowrap" class="ft2518">&#160;User AS U;</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft260{font-size:6px;font-family:Times;color:#595959;}
	.ft261{font-size:18px;font-family:Times;color:#0079c0;}
	.ft262{font-size:20px;font-family:Times;color:#0079c0;}
	.ft263{font-size:12px;font-family:Times;color:#212121;}
	.ft264{font-size:10px;font-family:Times;color:#212121;}
	.ft265{font-size:6px;font-family:Times;color:#cc7832;}
	.ft266{font-size:6px;font-family:Times;color:#ffc66d;}
	.ft267{font-size:6px;font-family:Times;color:#a9b7c6;}
	.ft268{font-size:6px;font-family:Times;color:#a8c023;}
	.ft269{font-size:6px;font-family:Times;color:#6a8759;}
	.ft2610{font-size:6px;font-family:Times;color:#9876aa;}
	.ft2611{font-size:6px;font-family:Times;color:#6897bb;}
	.ft2612{font-size:22px;font-family:Times;color:#0079c0;}
	.ft2613{font-size:8px;font-family:Times;color:#cc7832;}
	.ft2614{font-size:8px;font-family:Times;color:#a9b7c6;}
	.ft2615{font-size:8px;font-family:Times;color:#6a8759;}
	.ft2616{font-size:8px;font-family:Times;color:#6897bb;}
	.ft2617{font-size:8px;font-family:Times;color:#9876aa;}
	.ft2618{font-size:10px;line-height:17px;font-family:Times;color:#212121;}
	.ft2619{font-size:6px;line-height:10px;font-family:Times;color:#a9b7c6;}
	.ft2620{font-size:10px;line-height:14px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page26-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target026.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft260">51</p>
<p style="position:absolute;top:112px;left:25px;white-space:nowrap" class="ft261">Clauses&#160;in&#160;the&#160;HQL&#160;</p>
<p style="position:absolute;top:110px;left:219px;white-space:nowrap" class="ft262">(continued)</p>
<p style="position:absolute;top:152px;left:30px;white-space:nowrap" class="ft263">GROUP&#160;BY&#160;Clause&#160;and&#160;Aggregate&#160;function</p>
<p style="position:absolute;top:175px;left:30px;white-space:nowrap" class="ft2618">This&#160;clause&#160;lets&#160;Hibernate&#160;pull&#160;information&#160;from&#160;the&#160;database&#160;and&#160;group&#160;it&#160;based&#160;on&#160;a&#160;value&#160;of&#160;an&#160;<br/>attribute;&#160;and&#160;typically,&#160;uses&#160;the&#160;result&#160;to&#160;include&#160;an&#160;aggregate&#160;value.&#160;Following&#160;is&#160;the&#160;simple&#160;syntax&#160;of&#160;<br/>using&#160;GROUP&#160;BY&#160;clause:</p>
<p style="position:absolute;top:239px;left:33px;white-space:nowrap" class="ft265">public static void&#160;</p>
<p style="position:absolute;top:239px;left:127px;white-space:nowrap" class="ft266">main</p>
<p style="position:absolute;top:239px;left:146px;white-space:nowrap" class="ft267">(String[] args) {</p>
<p style="position:absolute;top:250px;left:33px;white-space:nowrap" class="ft267">&#160;&#160;</p>
<p style="position:absolute;top:250px;left:43px;white-space:nowrap" class="ft268"><i>&#160;</i></p>
<p style="position:absolute;top:250px;left:48px;white-space:nowrap" class="ft267">SessionFactory factory =&#160;</p>
<p style="position:absolute;top:250px;left:171px;white-space:nowrap" class="ft265">new&#160;</p>
<p style="position:absolute;top:250px;left:190px;white-space:nowrap" class="ft267">Configuration().configure().buildSessionFactory()</p>
<p style="position:absolute;top:250px;left:431px;white-space:nowrap" class="ft265">;</p>
<p style="position:absolute;top:261px;left:33px;white-space:nowrap" class="ft265">&#160; &#160;</p>
<p style="position:absolute;top:261px;left:48px;white-space:nowrap" class="ft267">Session session = factory.openSession()</p>
<p style="position:absolute;top:261px;left:239px;white-space:nowrap" class="ft265">;</p>
<p style="position:absolute;top:272px;left:33px;white-space:nowrap" class="ft267">&#160; &#160;String hql =&#160;</p>
<p style="position:absolute;top:272px;left:112px;white-space:nowrap" class="ft269">&#34;SELECT SUM(U.salary), U.city FROM User U GROUP BY U.city&#34;</p>
<p style="position:absolute;top:272px;left:396px;white-space:nowrap" class="ft265">;&#160;</p>
<p style="position:absolute;top:282px;left:33px;white-space:nowrap" class="ft265">&#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;//The query returns Object[] arrays of length 2</p>
<p style="position:absolute;top:293px;left:33px;white-space:nowrap" class="ft267">&#160; &#160;TypedQuery query = session.createQuery(hql)</p>
<p style="position:absolute;top:293px;left:259px;white-space:nowrap" class="ft265">;</p>
<p style="position:absolute;top:304px;left:33px;white-space:nowrap" class="ft267">&#160; &#160;List&lt;Object[]&gt; result =query.getResultList()</p>
<p style="position:absolute;top:304px;left:264px;white-space:nowrap" class="ft265">;</p>
<p style="position:absolute;top:314px;left:33px;white-space:nowrap" class="ft265">&#160; &#160;for&#160;</p>
<p style="position:absolute;top:314px;left:68px;white-space:nowrap" class="ft267">(Object[] o : result) {</p>
<p style="position:absolute;top:325px;left:33px;white-space:nowrap" class="ft267">&#160; &#160; &#160;System.</p>
<p style="position:absolute;top:325px;left:92px;white-space:nowrap" class="ft2610"><i>out</i></p>
<p style="position:absolute;top:325px;left:107px;white-space:nowrap" class="ft267">.println(</p>
<p style="position:absolute;top:325px;left:151px;white-space:nowrap" class="ft269">&#34;Total salary &#34;&#160;</p>
<p style="position:absolute;top:325px;left:230px;white-space:nowrap" class="ft267">+o[</p>
<p style="position:absolute;top:325px;left:244px;white-space:nowrap" class="ft2611">0</p>
<p style="position:absolute;top:325px;left:249px;white-space:nowrap" class="ft267">] +</p>
<p style="position:absolute;top:325px;left:264px;white-space:nowrap" class="ft269">&#34; | city: &#34;</p>
<p style="position:absolute;top:325px;left:318px;white-space:nowrap" class="ft267">+ o[</p>
<p style="position:absolute;top:325px;left:337px;white-space:nowrap" class="ft2611">1</p>
<p style="position:absolute;top:325px;left:342px;white-space:nowrap" class="ft267">] )</p>
<p style="position:absolute;top:325px;left:357px;white-space:nowrap" class="ft265">;&#160;</p>
<p style="position:absolute;top:336px;left:33px;white-space:nowrap" class="ft2619">&#160; &#160;&#160;}<br/>} }</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft260">52</p>
<p style="position:absolute;top:527px;left:28px;white-space:nowrap" class="ft2612">Hibernate&#160;Parameterized&#160;Query</p>
<p style="position:absolute;top:568px;left:30px;white-space:nowrap" class="ft263">Using&#160;Named&#160;Parameters:</p>
<p style="position:absolute;top:589px;left:30px;white-space:nowrap" class="ft2620">Hibernate&#160;supports&#160;named&#160;parameters&#160;in&#160;its&#160;HQL&#160;queries.&#160;Following&#160;is&#160;the&#160;simple&#160;syntax&#160;of&#160;using&#160;named&#160;<br/>parameters:</p>
<p style="position:absolute;top:636px;left:35px;white-space:nowrap" class="ft2613">&#160; &#160;</p>
<p style="position:absolute;top:636px;left:53px;white-space:nowrap" class="ft2614">String hql =&#160;</p>
<p style="position:absolute;top:636px;left:129px;white-space:nowrap" class="ft2615">&#34;FROM User u WHERE u.id = :id&#34;</p>
<p style="position:absolute;top:636px;left:306px;white-space:nowrap" class="ft2613">;</p>
<p style="position:absolute;top:649px;left:35px;white-space:nowrap" class="ft2613">&#160; &#160;</p>
<p style="position:absolute;top:649px;left:53px;white-space:nowrap" class="ft2614">TypedQuery query = session.createQuery(hql)</p>
<p style="position:absolute;top:649px;left:306px;white-space:nowrap" class="ft2613">;</p>
<p style="position:absolute;top:661px;left:35px;white-space:nowrap" class="ft2613">&#160; &#160;</p>
<p style="position:absolute;top:661px;left:53px;white-space:nowrap" class="ft2614">query.setParameter(</p>
<p style="position:absolute;top:661px;left:165px;white-space:nowrap" class="ft2615">&#34;id&#34;</p>
<p style="position:absolute;top:661px;left:188px;white-space:nowrap" class="ft2613">,&#160;</p>
<p style="position:absolute;top:661px;left:200px;white-space:nowrap" class="ft2616">4</p>
<p style="position:absolute;top:661px;left:206px;white-space:nowrap" class="ft2614">)</p>
<p style="position:absolute;top:661px;left:212px;white-space:nowrap" class="ft2613">;</p>
<p style="position:absolute;top:674px;left:35px;white-space:nowrap" class="ft2613">&#160; &#160;</p>
<p style="position:absolute;top:674px;left:53px;white-space:nowrap" class="ft2614">List&lt;User&gt; result = query.getResultList()</p>
<p style="position:absolute;top:674px;left:294px;white-space:nowrap" class="ft2613">;</p>
<p style="position:absolute;top:687px;left:35px;white-space:nowrap" class="ft2613">&#160; &#160;for&#160;</p>
<p style="position:absolute;top:687px;left:76px;white-space:nowrap" class="ft2614">(User u : result) {</p>
<p style="position:absolute;top:700px;left:35px;white-space:nowrap" class="ft2614">&#160; &#160; &#160; &#160;System.</p>
<p style="position:absolute;top:700px;left:118px;white-space:nowrap" class="ft2617"><i>out</i></p>
<p style="position:absolute;top:700px;left:135px;white-space:nowrap" class="ft2614">.println(</p>
<p style="position:absolute;top:700px;left:188px;white-space:nowrap" class="ft2615">&#34;User Id: &#34;&#160;</p>
<p style="position:absolute;top:700px;left:259px;white-space:nowrap" class="ft2614">+ u.getId() +&#160;</p>
<p style="position:absolute;top:700px;left:341px;white-space:nowrap" class="ft2615">&#34;|&#34;&#160;</p>
<p style="position:absolute;top:700px;left:365px;white-space:nowrap" class="ft2614">+&#160;</p>
<p style="position:absolute;top:700px;left:377px;white-space:nowrap" class="ft2615">&#34; Full name:&#34;&#160;</p>
<p style="position:absolute;top:700px;left:459px;white-space:nowrap" class="ft2614">+ u.getFullname() +&#160;</p>
<p style="position:absolute;top:713px;left:35px;white-space:nowrap" class="ft2615">&#34;|&#34;&#160;</p>
<p style="position:absolute;top:713px;left:59px;white-space:nowrap" class="ft2614">+&#160;</p>
<p style="position:absolute;top:713px;left:71px;white-space:nowrap" class="ft2615">&#34;Email: &#34;&#160;</p>
<p style="position:absolute;top:713px;left:129px;white-space:nowrap" class="ft2614">+ u.getEmail() +&#160;</p>
<p style="position:absolute;top:713px;left:230px;white-space:nowrap" class="ft2615">&#34;|&#34;&#160;</p>
<p style="position:absolute;top:713px;left:253px;white-space:nowrap" class="ft2614">+&#160;</p>
<p style="position:absolute;top:713px;left:265px;white-space:nowrap" class="ft2615">&#34;password&#34;&#160;</p>
<p style="position:absolute;top:713px;left:330px;white-space:nowrap" class="ft2614">+ u.getPassword())</p>
<p style="position:absolute;top:726px;left:35px;white-space:nowrap" class="ft2613">&#160; &#160;</p>
<p style="position:absolute;top:726px;left:53px;white-space:nowrap" class="ft2614">}</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft270{font-size:6px;font-family:Times;color:#595959;}
	.ft271{font-size:20px;font-family:Times;color:#0079c0;}
	.ft272{font-size:14px;font-family:Times;color:#212121;}
	.ft273{font-size:14px;font-family:Times;color:#cc7832;}
	.ft274{font-size:14px;font-family:Times;color:#78909c;}
	.ft275{font-size:12px;font-family:Times;color:#212121;}
	.ft276{font-size:18px;font-family:Times;color:#0079c0;}
	.ft277{font-size:12px;line-height:19px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page27-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target027.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft270">53</p>
<p style="position:absolute;top:111px;left:25px;white-space:nowrap" class="ft271">Hands-On&#160;Lab</p>
<p style="position:absolute;top:152px;left:47px;white-space:nowrap" class="ft272">Find&#160;the&#160;</p>
<p style="position:absolute;top:152px;left:115px;white-space:nowrap" class="ft273">GLAB 305.4.2&#160;- Demonstration of&#160;Hibernate&#160;Query&#160;Language&#160;-&#160;</p>
<p style="position:absolute;top:179px;left:41px;white-space:nowrap" class="ft273">HQL</p>
<p style="position:absolute;top:179px;left:72px;white-space:nowrap" class="ft274">&#160;</p>
<p style="position:absolute;top:179px;left:77px;white-space:nowrap" class="ft272">on Canvas&#160;under the&#160;Assignment section.</p>
<p style="position:absolute;top:244px;left:41px;white-space:nowrap" class="ft277">If&#160;you&#160;have&#160;any&#160;technical&#160;questions&#160;while&#160;performing&#160;the&#160;lab&#160;activity,&#160;ask&#160;your&#160;instructors&#160;<br/>for&#160;assistance.&#160;</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft270">54</p>
<p style="position:absolute;top:530px;left:27px;white-space:nowrap" class="ft276">Problem&#160;with&#160;HQL&#160;and&#160;SQL</p>
<p style="position:absolute;top:568px;left:36px;white-space:nowrap" class="ft275">Problem:</p>
<p style="position:absolute;top:598px;left:36px;white-space:nowrap" class="ft277">A&#160;major&#160;disadvantage&#160;of&#160;having&#160;HQL&#160;and&#160;SQL&#160;scattered&#160;across&#160;data&#160;access&#160;objects&#160;is&#160;<br/>that&#160;it&#160;makes&#160;the&#160;code&#160;unreadable.&#160;Hence,&#160;it&#160;might&#160;make&#160;sense&#160;to&#160;group&#160;all&#160;HQL&#160;and&#160;<br/>SQL&#160;in&#160;one&#160;place&#160;and&#160;use&#160;only&#160;their&#160;reference&#160;in&#160;the&#160;actual&#160;data&#160;access&#160;code.&#160;</p>
<p style="position:absolute;top:699px;left:36px;white-space:nowrap" class="ft275">Solution:</p>
<p style="position:absolute;top:729px;left:36px;white-space:nowrap" class="ft275">Fortunately,&#160;Hibernate&#160;allows&#160;us&#160;to&#160;do&#160;this&#160;with&#160;Named&#160;queries.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft280{font-size:6px;font-family:Times;color:#595959;}
	.ft281{font-size:19px;font-family:Times;color:#0079c0;}
	.ft282{font-size:10px;font-family:Times;color:#212121;}
	.ft283{font-size:10px;font-family:Times;color:#e69138;}
	.ft284{font-size:8px;font-family:Times;color:#0d6efd;}
	.ft285{font-size:8px;font-family:Times;color:#212121;}
	.ft286{font-size:9px;font-family:Times;color:#212121;}
	.ft287{font-size:9px;font-family:Times;color:#212121;}
	.ft288{font-size:6px;font-family:Times;color:#0079c0;}
	.ft289{font-size:4px;font-family:Times;color:#212121;}
	.ft2810{font-size:5px;font-family:Times;color:#212121;}
	.ft2811{font-size:6px;font-family:Times;color:#212121;}
	.ft2812{font-size:6px;font-family:Times;color:#98c379;}
	.ft2813{font-size:7px;font-family:Times;color:#abb2bf;}
	.ft2814{font-size:7px;font-family:Times;color:#98c379;}
	.ft2815{font-size:11px;font-family:Times;color:#ffab40;}
	.ft2816{font-size:9px;font-family:Times;color:#ffab40;}
	.ft2817{font-size:10px;line-height:14px;font-family:Times;color:#212121;}
	.ft2818{font-size:10px;line-height:20px;font-family:Times;color:#212121;}
	.ft2819{font-size:9px;line-height:13px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page28-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target028.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft280">55</p>
<p style="position:absolute;top:110px;left:27px;white-space:nowrap" class="ft281">Overview&#160;of&#160;Hibernate&#160;Named&#160;Query</p>
<p style="position:absolute;top:152px;left:39px;white-space:nowrap" class="ft2817">Hibernate&#160;provides&#160;“NamedQuery”&#160;that&#160;can&#160;be&#160;defined&#160;at&#160;a&#160;central&#160;location&#160;and&#160;can&#160;be&#160;used&#160;<br/>anywhere&#160;in&#160;the&#160;code.&#160;The&#160;Hibernate&#160;named&#160;query&#160;is&#160;a&#160;way&#160;to&#160;use&#160;any&#160;query&#160;by&#160;some&#160;meaningful&#160;<br/>name.&#160;It&#160;is&#160;like&#160;using&#160;alias&#160;names,&#160;whereas&#160;the&#160;Hibernate&#160;framework&#160;provides&#160;the&#160;concept&#160;of&#160;named&#160;<br/>queries&#160;so&#160;that&#160;application&#160;programmers&#160;need&#160;not&#160;scatter&#160;queries&#160;to&#160;all&#160;the&#160;Java&#160;code.<br/>We&#160;can&#160;create&#160;a&#160;NamedQuery&#160;&#160;for&#160;both&#160;HQL&#160;and&#160;Native&#160;SQL.&#160;A&#160;named&#160;query&#160;is&#160;a&#160;statically&#160;defined&#160;<br/>query&#160;with&#160;a&#160;predefined,&#160;unchangeable&#160;query&#160;string.&#160;Named&#160;queries&#160;are&#160;compiled&#160;when&#160;<br/>SessionFactory&#160;is&#160;instantiated&#160;(essentially,&#160;when&#160;your&#160;application&#160;starts&#160;up).&#160;They&#160;are&#160;validated&#160;when&#160;<br/>the&#160;session&#160;factory&#160;is&#160;created.</p>
<p style="position:absolute;top:283px;left:47px;white-space:nowrap" class="ft283">●</p>
<p style="position:absolute;top:283px;left:71px;white-space:nowrap" class="ft2817">If&#160;you&#160;want&#160;to&#160;use&#160;named&#160;query&#160;in&#160;Hibernate,&#160;you&#160;need&#160;to&#160;have&#160;knowledge&#160;of&#160;@NamedQueries&#160;<br/>annotations&#160;and&#160;@NamedQuery&#160;annotations:</p>
<p style="position:absolute;top:318px;left:80px;white-space:nowrap" class="ft284">○</p>
<p style="position:absolute;top:318px;left:103px;white-space:nowrap" class="ft285">@NameQuery&#160;annotation&#160;is&#160;used&#160;to&#160;define&#160;the&#160;single-named&#160;query.</p>
<p style="position:absolute;top:336px;left:80px;white-space:nowrap" class="ft284">○</p>
<p style="position:absolute;top:336px;left:103px;white-space:nowrap" class="ft285">@NameQueries&#160;annotation&#160;is&#160;used&#160;to&#160;define&#160;the&#160;multiple-named&#160;queries.</p>
<p style="position:absolute;top:354px;left:39px;white-space:nowrap" class="ft2819">Note:&#160;You&#160;cannot&#160;have&#160;two&#160;named&#160;queries&#160;with&#160;the&#160;same&#160;name&#160;in&#160;Hibernate.&#160;Hibernate&#160;shows&#160;<i>fail-fast</i>&#160;<br/>behavior&#160;in&#160;this&#160;regard,&#160;and&#160;will&#160;show&#160;an&#160;error&#160;in&#160;the&#160;application&#160;start-up.</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft280">56</p>
<p style="position:absolute;top:524px;left:23px;white-space:nowrap" class="ft281">Example&#160;-&#160;Hibernate&#160;Named&#160;Query</p>
<p style="position:absolute;top:632px;left:52px;white-space:nowrap" class="ft288">User&#160;Entity&#160;Class&#160;-&#160;&#160;Hibernate&#160;Named&#160;Query&#160;@NamedQuery&#160;Annotation</p>
<p style="position:absolute;top:645px;left:52px;white-space:nowrap" class="ft289">&#160;</p>
<p style="position:absolute;top:645px;left:53px;white-space:nowrap" class="ft2810">@</p>
<p style="position:absolute;top:644px;left:60px;white-space:nowrap" class="ft2811">NamedQueries({</p>
<p style="position:absolute;top:653px;left:52px;white-space:nowrap" class="ft2811">&#160;&#160;&#160;&#160;&#160;&#160;@NamedQuery(&#160;&#160;name&#160;=&#160;&#34;</p>
<p style="position:absolute;top:653px;left:164px;white-space:nowrap" class="ft2812">findStockByStockCode</p>
<p style="position:absolute;top:653px;left:253px;white-space:nowrap" class="ft2811">&#34;,&#160;query&#160;=&#160;&#34;from&#160;Stock&#160;s&#160;where&#160;s.stockCode&#160;=&#160;:stockCode&#34;&#160;)&#160;</p>
<p style="position:absolute;top:653px;left:534px;white-space:nowrap" class="ft2811">})</p>
<p style="position:absolute;top:674px;left:52px;white-space:nowrap" class="ft2811">&#160;</p>
<p style="position:absolute;top:674px;left:116px;white-space:nowrap" class="ft2811">@Entity</p>
<p style="position:absolute;top:683px;left:52px;white-space:nowrap" class="ft2811">&#160;</p>
<p style="position:absolute;top:683px;left:116px;white-space:nowrap" class="ft2811">@Table(name&#160;=&#160;&#34;stock&#34;)</p>
<p style="position:absolute;top:704px;left:52px;white-space:nowrap" class="ft2811">&#160;</p>
<p style="position:absolute;top:704px;left:116px;white-space:nowrap" class="ft2811">public&#160;class&#160;Stock&#160;implements&#160;java.io.Serializable&#160;{&#160;&#160;.&#160;.&#160;………..</p>
<p style="position:absolute;top:734px;left:79px;white-space:nowrap" class="ft285">Call&#160;a&#160;named&#160;query&#160;-&#160;We&#160;can&#160;call&#160;the&#160;named&#160;query&#160;via&#160;getNamedQuery&#160;method.</p>
<p style="position:absolute;top:754px;left:79px;white-space:nowrap" class="ft2813">Query query = session.getNamedQuery(</p>
<p style="position:absolute;top:754px;left:274px;white-space:nowrap" class="ft2814">&#34;findStockByStockCode&#34;</p>
<p style="position:absolute;top:754px;left:392px;white-space:nowrap" class="ft2813">)</p>
<p style="position:absolute;top:766px;left:79px;white-space:nowrap" class="ft2813">.setString(</p>
<p style="position:absolute;top:766px;left:139px;white-space:nowrap" class="ft2814">&#34;stockCode&#34;</p>
<p style="position:absolute;top:766px;left:198px;white-space:nowrap" class="ft2813">,&#160;</p>
<p style="position:absolute;top:766px;left:209px;white-space:nowrap" class="ft2814">&#34;7277&#34;</p>
<p style="position:absolute;top:766px;left:241px;white-space:nowrap" class="ft2813">);</p>
<p style="position:absolute;top:791px;left:79px;white-space:nowrap" class="ft2813">Query query = session.getNamedQuery(</p>
<p style="position:absolute;top:791px;left:274px;white-space:nowrap" class="ft2814">&#34;findStockByStockCodeNativeSQL&#34;</p>
<p style="position:absolute;top:791px;left:441px;white-space:nowrap" class="ft2813">)</p>
<p style="position:absolute;top:803px;left:84px;white-space:nowrap" class="ft2813">.setString(</p>
<p style="position:absolute;top:803px;left:143px;white-space:nowrap" class="ft2814">&#34;stockCode&#34;</p>
<p style="position:absolute;top:803px;left:202px;white-space:nowrap" class="ft2813">,&#160;</p>
<p style="position:absolute;top:803px;left:213px;white-space:nowrap" class="ft2814">&#34;7277&#34;</p>
<p style="position:absolute;top:803px;left:246px;white-space:nowrap" class="ft2813">);</p>
<p style="position:absolute;top:556px;left:33px;white-space:nowrap" class="ft2815">❖</p>
<p style="position:absolute;top:558px;left:63px;white-space:nowrap" class="ft285">NamedQuery&#160;can&#160;be&#160;defined&#160;in&#160;Hibernate&#160;mapping&#160;files&#160;or&#160;by&#160;using&#160;Hibernate&#160;annotations.</p>
<p style="position:absolute;top:572px;left:33px;white-space:nowrap" class="ft2815">❖</p>
<p style="position:absolute;top:574px;left:63px;white-space:nowrap" class="ft285">Named&#160;query&#160;definition&#160;has&#160;two&#160;important&#160;attributes:</p>
<p style="position:absolute;top:588px;left:67px;white-space:nowrap" class="ft2816">➢</p>
<p style="position:absolute;top:588px;left:95px;white-space:nowrap" class="ft285">name:&#160;The&#160;name&#160;attribute&#160;of&#160;a&#160;named&#160;query&#160;by&#160;which&#160;it&#160;will&#160;be&#160;located&#160;using&#160;hibernate&#160;session.</p>
<p style="position:absolute;top:602px;left:67px;white-space:nowrap" class="ft2816">➢</p>
<p style="position:absolute;top:602px;left:95px;white-space:nowrap" class="ft285">query:&#160;Here&#160;you&#160;will&#160;give&#160;the&#160;HQL&#160;statement&#160;to&#160;get&#160;executed&#160;in&#160;the&#160;database.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft290{font-size:6px;font-family:Times;color:#595959;}
	.ft291{font-size:20px;font-family:Times;color:#0079c0;}
	.ft292{font-size:19px;font-family:Times;color:#0079c0;}
	.ft293{font-size:8px;font-family:Times;color:#212121;}
	.ft294{font-size:8px;font-family:Times;color:#c7254e;}
	.ft295{font-size:8px;font-family:Times;color:#78909c;}
	.ft296{font-size:7px;font-family:Times;color:#666666;}
	.ft297{font-size:8px;font-family:Times;color:#666666;}
	.ft298{font-size:8px;font-family:Times;color:#09507c;}
	.ft299{font-size:8px;font-family:Times;color:#e69138;}
	.ft2910{font-size:22px;font-family:Times;color:#0079c0;}
	.ft2911{font-size:11px;font-family:Times;color:#243b53;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page29-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target029.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft290">57</p>
<p style="position:absolute;top:103px;left:25px;white-space:nowrap" class="ft291">Example&#160;-&#160;Hibernate&#160;Named&#160;Query&#160;</p>
<p style="position:absolute;top:104px;left:397px;white-space:nowrap" class="ft292">(continued)</p>
<p style="position:absolute;top:152px;left:25px;white-space:nowrap" class="ft293">public void UpdateEmployee()</p>
<p style="position:absolute;top:162px;left:90px;white-space:nowrap" class="ft293">{ &#160; SessionFactory factory = new Configuration().configure().buildSessionFactory();</p>
<p style="position:absolute;top:172px;left:90px;white-space:nowrap" class="ft293">&#160; &#160; Session session = factory.openSession();</p>
<p style="position:absolute;top:182px;left:90px;white-space:nowrap" class="ft293">&#160; &#160; Transaction t = session.beginTransaction();</p>
<p style="position:absolute;top:193px;left:90px;white-space:nowrap" class="ft293">&#160; &#160; &#160; &#160; &#160;TypedQuery query = session.getNamedQuery(&#34;</p>
<p style="position:absolute;top:193px;left:390px;white-space:nowrap" class="ft294">updateEmpById</p>
<p style="position:absolute;top:193px;left:467px;white-space:nowrap" class="ft293">&#34;);</p>
<p style="position:absolute;top:203px;left:90px;white-space:nowrap" class="ft293">&#160; &#160; query.setParameter(&#34;</p>
<p style="position:absolute;top:203px;left:231px;white-space:nowrap" class="ft295">name</p>
<p style="position:absolute;top:203px;left:255px;white-space:nowrap" class="ft293">&#34;, &#34;Elded Leon&#34;);</p>
<p style="position:absolute;top:213px;left:90px;white-space:nowrap" class="ft293">&#160; &#160; query.setParameter(&#34;</p>
<p style="position:absolute;top:213px;left:231px;white-space:nowrap" class="ft295">id</p>
<p style="position:absolute;top:213px;left:243px;white-space:nowrap" class="ft293">&#34;, 7);</p>
<p style="position:absolute;top:224px;left:90px;white-space:nowrap" class="ft293">&#160; &#160; int rowsAffected = query.executeUpdate();</p>
<p style="position:absolute;top:234px;left:90px;white-space:nowrap" class="ft293">&#160; &#160; if (rowsAffected &gt; 0) {</p>
<p style="position:absolute;top:244px;left:122px;white-space:nowrap" class="ft293">System.out.println(rowsAffected + &#34;(s) were inserted&#34;);</p>
<p style="position:absolute;top:254px;left:90px;white-space:nowrap" class="ft293">&#160; &#160; &#160;}</p>
<p style="position:absolute;top:265px;left:90px;white-space:nowrap" class="ft293">&#160; &#160; t.commit();</p>
<p style="position:absolute;top:275px;left:90px;white-space:nowrap" class="ft293">&#160; &#160; System.out.println(&#34;successfully saved&#34;);&#160;</p>
<p style="position:absolute;top:285px;left:90px;white-space:nowrap" class="ft293">}</p>
<p style="position:absolute;top:332px;left:25px;white-space:nowrap" class="ft296"><i>&#160;</i></p>
<p style="position:absolute;top:331px;left:31px;white-space:nowrap" class="ft297"><i>//Using @NamedQueries for multiple &#160;HQL</i></p>
<p style="position:absolute;top:344px;left:25px;white-space:nowrap" class="ft298">&#160;</p>
<p style="position:absolute;top:344px;left:31px;white-space:nowrap" class="ft293">@NamedQueries({</p>
<p style="position:absolute;top:357px;left:25px;white-space:nowrap" class="ft293">&#160; &#160;@NamedQuery(name=&#34;</p>
<p style="position:absolute;top:357px;left:149px;white-space:nowrap" class="ft294">updateEmpById</p>
<p style="position:absolute;top:357px;left:226px;white-space:nowrap" class="ft293">&#34;, query = &#34;update Employee set Name =&#160;</p>
<p style="position:absolute;top:357px;left:455px;white-space:nowrap" class="ft299">:name</p>
<p style="position:absolute;top:357px;left:485px;white-space:nowrap" class="ft293">&#160;where id =&#160;</p>
<p style="position:absolute;top:357px;left:555px;white-space:nowrap" class="ft299">:id</p>
<p style="position:absolute;top:357px;left:573px;white-space:nowrap" class="ft293">&#34;),</p>
<p style="position:absolute;top:370px;left:25px;white-space:nowrap" class="ft293">)}</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft290">58</p>
<p style="position:absolute;top:525px;left:27px;white-space:nowrap" class="ft2910">Advantages&#160;of&#160;Named&#160;Queries</p>
<p style="position:absolute;top:566px;left:73px;white-space:nowrap" class="ft2911">1.&#160;Fail&#160;fast:&#160;Their&#160;syntax&#160;is&#160;checked&#160;when&#160;the&#160;session&#160;factory&#160;is&#160;created,&#160;making&#160;the&#160;</p>
<p style="position:absolute;top:586px;left:101px;white-space:nowrap" class="ft2911">application&#160;fail&#160;fast&#160;in&#160;case&#160;of&#160;an&#160;error.</p>
<p style="position:absolute;top:612px;left:73px;white-space:nowrap" class="ft2911">2.&#160;Reusable:&#160;They&#160;can&#160;be&#160;accessed&#160;and&#160;used&#160;from&#160;several&#160;places,&#160;which&#160;increases&#160;</p>
<p style="position:absolute;top:632px;left:101px;white-space:nowrap" class="ft2911">reusability.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft300{font-size:6px;font-family:Times;color:#595959;}
	.ft301{font-size:20px;font-family:Times;color:#0079c0;}
	.ft302{font-size:12px;font-family:Times;color:#212121;}
	.ft303{font-size:13px;font-family:Times;color:#b45f06;}
	.ft304{font-size:13px;font-family:Times;color:#78909c;}
	.ft305{font-size:12px;font-family:Times;color:#0097a7;}
	.ft306{font-size:22px;font-family:Times;color:#0079c0;}
	.ft307{font-size:11px;font-family:Times;color:#e69138;}
	.ft308{font-size:11px;font-family:Times;color:#212121;}
	.ft309{font-size:11px;font-family:Times;color:#ffab40;}
	.ft3010{font-size:12px;line-height:19px;font-family:Times;color:#212121;}
	.ft3011{font-size:11px;line-height:15px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page30-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target030.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft300">59</p>
<p style="position:absolute;top:103px;left:25px;white-space:nowrap" class="ft301">Hands-On&#160;Lab</p>
<p style="position:absolute;top:153px;left:40px;white-space:nowrap" class="ft302">Find&#160;</p>
<p style="position:absolute;top:152px;left:72px;white-space:nowrap" class="ft303">GLAB&#160;305.4.3&#160;-&#160;Demonstration&#160;-&#160;Named&#160;Queries&#160;in&#160;Hibernate</p>
<p style="position:absolute;top:152px;left:507px;white-space:nowrap" class="ft304">&#160;</p>
<p style="position:absolute;top:153px;left:511px;white-space:nowrap" class="ft302">on&#160;Canvas&#160;</p>
<p style="position:absolute;top:173px;left:40px;white-space:nowrap" class="ft302">under&#160;the&#160;Guided&#160;Lab&#160;section.</p>
<p style="position:absolute;top:234px;left:40px;white-space:nowrap" class="ft3010">If&#160;you&#160;have&#160;any&#160;technical&#160;questions&#160;while&#160;performing&#160;the&#160;lab&#160;activity,&#160;ask&#160;your&#160;instructors&#160;<br/>for&#160;assistance.</p>
<p style="position:absolute;top:254px;left:132px;white-space:nowrap" class="ft305">&#160;</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft300">60</p>
<p style="position:absolute;top:525px;left:26px;white-space:nowrap" class="ft306">JDBC&#160;vs.&#160;Java&#160;Hibernate</p>
<p style="position:absolute;top:567px;left:38px;white-space:nowrap" class="ft307">●</p>
<p style="position:absolute;top:567px;left:62px;white-space:nowrap" class="ft3011">Hibernate&#160;performs&#160;an&#160;Object-Relational&#160;Mapping&#160;(ORM)&#160;framework,&#160;while&#160;JDBC&#160;is&#160;simply&#160;a&#160;<br/>database&#160;connectivity&#160;API.&#160;</p>
<p style="position:absolute;top:607px;left:38px;white-space:nowrap" class="ft307">●</p>
<p style="position:absolute;top:607px;left:62px;white-space:nowrap" class="ft3011">With&#160;JDBC,&#160;you&#160;need&#160;to&#160;write&#160;database-speciﬁc&#160;SQL&#160;statements&#160;for&#160;your&#160;DDL,&#160;DML,&#160;and&#160;<br/>queries.&#160;JPA&#160;allows&#160;you&#160;to&#160;avoid&#160;writing&#160;database-speciﬁc&#160;DDL&#160;or&#160;DML,&#160;and&#160;to&#160;express&#160;<br/>queries&#160;in&#160;terms&#160;of&#160;the&#160;Java&#160;entities&#160;rather&#160;than&#160;native&#160;SQL&#160;tables&#160;and&#160;columns.</p>
<p style="position:absolute;top:661px;left:38px;white-space:nowrap" class="ft307">●</p>
<p style="position:absolute;top:661px;left:62px;white-space:nowrap" class="ft3011">JDBC&#160;is&#160;database&#160;dependent&#160;(i.e.,&#160;one&#160;needs&#160;to&#160;write&#160;different&#160;codes&#160;for&#160;different&#160;databases).&#160;<br/>Hibernate&#160;is&#160;database-independent&#160;and&#160;the&#160;same&#160;code&#160;can&#160;work&#160;for&#160;many&#160;databases&#160;with&#160;<br/>minor&#160;changes.</p>
<p style="position:absolute;top:716px;left:38px;white-space:nowrap" class="ft307">●</p>
<p style="position:absolute;top:716px;left:62px;white-space:nowrap" class="ft308">The&#160;benefits&#160;of&#160;using&#160;Hibernate&#160;over&#160;JDBC&#160;include:</p>
<p style="position:absolute;top:731px;left:70px;white-space:nowrap" class="ft309">○</p>
<p style="position:absolute;top:731px;left:95px;white-space:nowrap" class="ft308">There&#160;is&#160;reduced&#160;boilerplate&#160;code.</p>
<p style="position:absolute;top:747px;left:70px;white-space:nowrap" class="ft309">○</p>
<p style="position:absolute;top:747px;left:95px;white-space:nowrap" class="ft308">There&#160;is&#160;automatic&#160;Object&#160;Mapping.</p>
<p style="position:absolute;top:762px;left:70px;white-space:nowrap" class="ft309">○</p>
<p style="position:absolute;top:762px;left:95px;white-space:nowrap" class="ft308">It&#160;is&#160;easy&#160;to&#160;migrate&#160;to&#160;a&#160;new&#160;database.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft310{font-size:6px;font-family:Times;color:#595959;}
	.ft311{font-size:20px;font-family:Times;color:#0079c0;}
	.ft312{font-size:12px;font-family:Times;color:#e69138;}
	.ft313{font-size:12px;font-family:Times;color:#212121;}
	.ft314{font-size:12px;line-height:19px;font-family:Times;color:#212121;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page31-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target031.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft310">61</p>
<p style="position:absolute;top:111px;left:26px;white-space:nowrap" class="ft311">Knowledge&#160;Check</p>
<p style="position:absolute;top:152px;left:38px;white-space:nowrap" class="ft312">●</p>
<p style="position:absolute;top:152px;left:62px;white-space:nowrap" class="ft313">What&#160;are&#160;the&#160;advantages&#160;of&#160;Hibernate&#160;over&#160;JDBC?</p>
<p style="position:absolute;top:172px;left:38px;white-space:nowrap" class="ft312">●</p>
<p style="position:absolute;top:172px;left:62px;white-space:nowrap" class="ft313">What&#160;are&#160;some&#160;of&#160;the&#160;important&#160;interfaces&#160;of&#160;Hibernate&#160;framework?</p>
<p style="position:absolute;top:192px;left:38px;white-space:nowrap" class="ft312">●</p>
<p style="position:absolute;top:192px;left:62px;white-space:nowrap" class="ft313">What&#160;is&#160;a&#160;Session&#160;in&#160;Hibernate?</p>
<p style="position:absolute;top:211px;left:38px;white-space:nowrap" class="ft312">●</p>
<p style="position:absolute;top:211px;left:62px;white-space:nowrap" class="ft313">Explain&#160;a&#160;Hibernate&#160;mapping&#160;file.</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft310">62</p>
<p style="position:absolute;top:526px;left:25px;white-space:nowrap" class="ft311">Summary</p>
<p style="position:absolute;top:568px;left:42px;white-space:nowrap" class="ft314">Hibernate&#160;ORM&#160;stands&#160;for&#160;Object&#160;Relational&#160;Mapping.&#160;This&#160;is&#160;a&#160;mapping&#160;tool&#160;pattern&#160;<br/>mainly&#160;used&#160;for&#160;converting&#160;data&#160;stored&#160;in&#160;a&#160;relational&#160;database&#160;to&#160;an&#160;object&#160;used&#160;in&#160;<br/>object-oriented&#160;programming&#160;constructs.&#160;This&#160;tool&#160;also&#160;helps&#160;greatly&#160;in&#160;simplifying&#160;data&#160;<br/>retrieval,&#160;creation,&#160;and&#160;manipulation.</p>
<p style="position:absolute;top:657px;left:42px;white-space:nowrap" class="ft314">Hibernate&#160;Query&#160;Language&#160;(HQL)&#160;is&#160;used&#160;as&#160;an&#160;extension&#160;of&#160;SQL.&#160;It&#160;is&#160;very&#160;simple,&#160;<br/>efficient,&#160;and&#160;very&#160;flexible&#160;for&#160;performing&#160;complex&#160;operations&#160;on&#160;relational&#160;databases&#160;<br/>without&#160;writing&#160;complicated&#160;queries.&#160;HQL&#160;is&#160;the&#160;object-oriented&#160;representation&#160;of&#160;query&#160;<br/>language,&#160;(i.e.,&#160;instead&#160;of&#160;using&#160;table&#160;name,&#160;we&#160;make&#160;use&#160;of&#160;the&#160;class&#160;name&#160;which&#160;<br/>makes&#160;this&#160;language&#160;independent&#160;of&#160;any&#160;database).This&#160;makes&#160;use&#160;of&#160;the&#160;Query&#160;<br/>interface&#160;provided&#160;by&#160;Hibernate.&#160;The&#160;Query&#160;object&#160;is&#160;obtained&#160;by&#160;calling&#160;the&#160;<br/>createQuery()&#160;method&#160;of&#160;the&#160;hibernate&#160;Session&#160;interface.</p>
</div>
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
<title></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
 <br/>
<style type="text/css">
<!--
	p {margin: 0; padding: 0;}	.ft320{font-size:6px;font-family:Times;color:#595959;}
	.ft321{font-size:24px;font-family:Times;color:#0079c0;}
	.ft322{font-size:10px;font-family:Times;color:#595959;}
	.ft323{font-size:6px;font-family:Times;color:#999999;}
	.ft324{font-size:18px;font-family:Times;color:#0079c0;}
	.ft325{font-size:16px;font-family:Times;color:#ffffff;}
	.ft326{font-size:10px;line-height:19px;font-family:Times;color:#595959;}
-->
</style>
</head>
<body bgcolor="#A0A0A0" vlink="blue" link="blue">
<div id="page32-div" style="position:relative;width:630px;height:892px;">
<img width="630" height="892" src="target032.png" alt="background image"/>
<p style="position:absolute;top:397px;left:619px;white-space:nowrap" class="ft320">63</p>
<p style="position:absolute;top:101px;left:24px;white-space:nowrap" class="ft321">References</p>
<p style="position:absolute;top:148px;left:41px;white-space:nowrap" class="ft326">https://www.vogella.com/tutorials/JavaPersistenceAPI/article.html#jpaintro_overview<br/>https://docs.jboss.org/hibernate/core/3.3/reference/en-US/html/objectstate.html<br/>https://www.interviewbit.com/hibernate-interview-questions/<br/>https://www.w3schools.blog/hibernate-association-mappings<br/>https://rti.etf.bg.ac.rs/rti/temp/is1_proba/materijali/vezbe/JPA/JPA%20-%20Queries.pdf</p>
<p style="position:absolute;top:309px;left:29px;white-space:nowrap" class="ft323">Apache®,&#160;&#160;Apache&#160;&#160;Tomcat®,&#160;&#160;Apache&#160;&#160;Kafka®,&#160;&#160;Apache&#160;&#160;Cassandra™,&#160;&#160;and&#160;&#160;Apache&#160;&#160;Geode™&#160;&#160;are&#160;&#160;trademarks&#160;&#160;or&#160;&#160;registered&#160;&#160;trademarks&#160;&#160;of&#160;&#160;the&#160;&#160;Apache&#160;&#160;Software&#160;</p>
<p style="position:absolute;top:318px;left:29px;white-space:nowrap" class="ft323">Foundation&#160;in&#160;the&#160;United&#160;States&#160;and/or&#160;other&#160;countries.&#160;Java™,&#160;Java™&#160;SE,&#160;Java™&#160;EE,&#160;and&#160;OpenJDK™&#160;are&#160;trademarks&#160;of&#160;Oracle&#160;and/or&#160;its&#160;aﬃliates.&#160;Kubernetes®&#160;</p>
<p style="position:absolute;top:327px;left:29px;white-space:nowrap" class="ft323">is&#160;a&#160;registered&#160;trademark&#160;of&#160;the&#160;Linux&#160;Foundation&#160;in&#160;the&#160;United&#160;States&#160;and&#160;other&#160;countries.&#160;Linux®&#160;is&#160;the&#160;registered&#160;trademark&#160;of&#160;Linus&#160;Torvalds&#160;in&#160;the&#160;United&#160;</p>
<p style="position:absolute;top:336px;left:29px;white-space:nowrap" class="ft323">States&#160;and&#160;other&#160;countries.&#160;Windows®&#160;and&#160;Microsoft®&#160;Azure&#160;are&#160;registered&#160;trademarks&#160;of&#160;Microsoft&#160;Corporation.&#160;“AWS”&#160;and&#160;“Amazon&#160;Web&#160;Services”&#160;are&#160;</p>
<p style="position:absolute;top:346px;left:29px;white-space:nowrap" class="ft323">trademarks&#160;or&#160;registered&#160;trademarks&#160;of&#160;Amazon.com&#160;Inc.&#160;or&#160;its&#160;aﬃliates.&#160;All&#160;other&#160;trademarks&#160;and&#160;copyrights&#160;are&#160;property&#160;of&#160;their&#160;respective&#160;owners&#160;and&#160;are&#160;</p>
<p style="position:absolute;top:355px;left:29px;white-space:nowrap" class="ft323">only&#160;mentioned&#160;for&#160;informative&#160;purposes.&#160;Other&#160;names&#160;may&#160;be&#160;trademarks&#160;of&#160;their&#160;respective&#160;owners.</p>
<p style="position:absolute;top:813px;left:619px;white-space:nowrap" class="ft320">64</p>
<p style="position:absolute;top:520px;left:24px;white-space:nowrap" class="ft324">End&#160;of&#160;Module</p>
<p style="position:absolute;top:824px;left:445px;white-space:nowrap" class="ft325">64</p>
</div>
</body>
</html>
