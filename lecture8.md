<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:40.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:40.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_009{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture8/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:84.62px;top:37.28px" class="cls_002"><span class="cls_002">Project Planning and Control</span></div>
<div style="position:absolute;left:35.45px;top:366.04px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:35.45px;top:408.04px" class="cls_004"><span class="cls_004">§</span><span class="cls_003"> How to plan a project?</span></div>
<div style="position:absolute;left:35.45px;top:450.04px" class="cls_004"><span class="cls_004">§</span><span class="cls_003"> How to control it?</span></div>
<div style="position:absolute;left:7.20px;top:501.60px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:519.60px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">System’s view of project control</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Irregular variables: cannot be controlled (e.g.</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_008"><span class="cls_008">experience of the user)</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Goal variables: things one wants to achieve (e.g.</span></div>
<div style="position:absolute;left:75.85px;top:237.12px" class="cls_008"><span class="cls_008">minimize downtime, lowest cost)</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Control variables: things that can be varied (e.g.</span></div>
<div style="position:absolute;left:75.85px;top:300.24px" class="cls_008"><span class="cls_008">project staffing, tools to be used)</span></div>
<div style="position:absolute;left:61.20px;top:369.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Distribution of variables over categories is not rigid</span></div>
<div style="position:absolute;left:75.85px;top:398.16px" class="cls_008"><span class="cls_008">(staffing may be irregular, cost can be a control</span></div>
<div style="position:absolute;left:75.85px;top:427.20px" class="cls_008"><span class="cls_008">variable, etc)</span></div>
<div style="position:absolute;left:61.20px;top:461.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> You have to know the category of each variable</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">2</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">System’s view of project control, conditions</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Goals of the system are known</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Sufficient control variety</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Information on state, input and output of the</span></div>
<div style="position:absolute;left:75.85px;top:242.16px" class="cls_008"><span class="cls_008">system</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Conceptual control  model: knowledge of how and</span></div>
<div style="position:absolute;left:75.85px;top:306.24px" class="cls_008"><span class="cls_008">extent to which variables depend on and influence</span></div>
<div style="position:absolute;left:75.85px;top:334.32px" class="cls_008"><span class="cls_008">each other</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">3</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Classes of project characteristics</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Product, process, and resource characteristics</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Interested in degree of certainty</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Product certainty:</span></div>
<div style="position:absolute;left:90.80px;top:247.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Clear requirements, known upfront: product certainty is high</span></div>
<div style="position:absolute;left:90.80px;top:276.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> User requirements change frequently: product certainty is low</span></div>
<div style="position:absolute;left:61.20px;top:306.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Process certainty:</span></div>
<div style="position:absolute;left:90.80px;top:339.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> E.g., much knowledge about effect of control actions: high</span></div>
<div style="position:absolute;left:90.80px;top:368.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> E.g., use of unknown tools: low</span></div>
<div style="position:absolute;left:61.20px;top:398.16px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Resource certainty:</span></div>
<div style="position:absolute;left:90.80px;top:431.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Depends on availability of appropriately qualified personnel</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">4</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Archetypical control situations</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Realization problem: all certainties are high</span></div>
<div style="position:absolute;left:90.80px;top:178.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Ideal situation, just make sure work gets done</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Allocation problem: resource certainty low, others</span></div>
<div style="position:absolute;left:75.85px;top:237.12px" class="cls_008"><span class="cls_008">high</span></div>
<div style="position:absolute;left:90.80px;top:270.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Major issue: controlling capacity</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Design problem: product certainty high, others low</span></div>
<div style="position:absolute;left:90.80px;top:334.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> How to design the project (milestones, personnel, assign</span></div>
<div style="position:absolute;left:105.95px;top:358.24px" class="cls_011"><span class="cls_011">responsibilities, etc)</span></div>
<div style="position:absolute;left:61.20px;top:387.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Exploration problem: all certainties low</span></div>
<div style="position:absolute;left:90.80px;top:421.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Major issue: get commitment of all people involved</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">5</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Control situation: realization</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Primary goal in control:</span></div>
<div style="position:absolute;left:90.80px;top:178.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Optimize resource usage, efficiency and schedule</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Coordination/management style:</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Standardization, hierarchy, separation style</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Development strategy:</span></div>
<div style="position:absolute;left:90.80px;top:305.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Waterfall</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Cost estimation:</span></div>
<div style="position:absolute;left:90.80px;top:368.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Models, guard process</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">6</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Control situation: allocation</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Primary goal in control:</span></div>
<div style="position:absolute;left:90.80px;top:178.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Acquisition, training personnel</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Coordination/management style:</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Standardization of product and process</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Development strategy:</span></div>
<div style="position:absolute;left:90.80px;top:305.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Waterfall</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Cost estimation:</span></div>
<div style="position:absolute;left:90.80px;top:368.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Models, sensitivity analysis</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">7</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Control situation: design</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Primary goal in control:</span></div>
<div style="position:absolute;left:90.80px;top:178.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Control of process</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Coordination/management style:</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Standardization of process</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Development strategy:</span></div>
<div style="position:absolute;left:90.80px;top:305.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Incremental</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Cost estimation:</span></div>
<div style="position:absolute;left:90.80px;top:368.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Expert, sensitivity analysis</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">8</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Control situation: exploration</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Primary goal in control:</span></div>
<div style="position:absolute;left:90.80px;top:178.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Maximize results, lower risks</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Coordination/management style:</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Mutual adjustment, commitment, relation style</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Development strategy:</span></div>
<div style="position:absolute;left:90.80px;top:305.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Incremental, prototyping, agile</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Cost estimation:</span></div>
<div style="position:absolute;left:90.80px;top:368.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Agile, risk analysis, provide guidance</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">9</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Risk management</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Risk management is project management for</span></div>
<div style="position:absolute;left:75.85px;top:208.32px" class="cls_008"><span class="cls_008">adults</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> In software development, we tend to ignore risks:</span></div>
<div style="position:absolute;left:90.80px;top:311.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> We’ll solve the problem on time</span></div>
<div style="position:absolute;left:90.80px;top:339.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Requirements will be stable</span></div>
<div style="position:absolute;left:90.80px;top:368.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> No one will leave the project</span></div>
<div style="position:absolute;left:90.80px;top:397.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">…</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">10</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Top ten risk factors</span></div>
<div style="position:absolute;left:61.20px;top:141.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Personnel shortfall</span></div>
<div style="position:absolute;left:61.20px;top:173.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Unrealistic schedule/budget</span></div>
<div style="position:absolute;left:61.20px;top:205.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Wrong functionality</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Wrong user interface</span></div>
<div style="position:absolute;left:61.20px;top:268.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Goldplating</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Requirements volatility</span></div>
<div style="position:absolute;left:61.20px;top:332.16px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Bad external components</span></div>
<div style="position:absolute;left:61.20px;top:363.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Bad external tasks</span></div>
<div style="position:absolute;left:61.20px;top:395.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Real-time shortfalls</span></div>
<div style="position:absolute;left:61.20px;top:427.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Capability shortfalls</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">11</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Risk management strategy</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_012"><span class="cls_012">1.</span><span class="cls_008">  Identify risk factors</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_012"><span class="cls_012">2.</span><span class="cls_008">  Determine risk exposure (probability * effect)</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_012"><span class="cls_012">3.</span><span class="cls_008">  Develop strategies to mitigate risks</span></div>
<div style="position:absolute;left:90.85px;top:316.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">  Avoid, transfer, or accept</span></div>
<div style="position:absolute;left:61.20px;top:381.12px" class="cls_012"><span class="cls_012">4.</span><span class="cls_008">  Handle risks</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">12</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Categories of risks</span></div>
<div style="position:absolute;left:360.04px;top:141.36px" class="cls_008"><span class="cls_008">Level of control</span></div>
<div style="position:absolute;left:307.17px;top:202.48px" class="cls_011"><span class="cls_011">low</span></div>
<div style="position:absolute;left:550.29px;top:202.24px" class="cls_011"><span class="cls_011">high</span></div>
<div style="position:absolute;left:127.54px;top:276.40px" class="cls_011"><span class="cls_011">low</span></div>
<div style="position:absolute;left:127.54px;top:387.04px" class="cls_011"><span class="cls_011">high</span></div>
<div style="position:absolute;left:218.29px;top:476.32px" class="cls_011"><span class="cls_011">Order of handling: first C3, then C2, then C4 and C1</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">13</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Techniques for project planning and control</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Work breakdown structure (WBS)</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> PERT chart</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Gantt chart</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Agile planning and control</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">14</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Work Breakdown Structure</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">15</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">PERT chart</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">16</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Gantt chart</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">17</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Why task-oriented planning is problematic</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Activities never finish early</span></div>
<div style="position:absolute;left:90.80px;top:178.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Parkinson’s law: work fills the time available</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Lateness is passed down the schedule</span></div>
<div style="position:absolute;left:90.80px;top:276.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> If either design or coding is late, subsequent testing will be late</span></div>
<div style="position:absolute;left:61.20px;top:340.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Tasks are not independent</span></div>
<div style="position:absolute;left:90.80px;top:374.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> If design takes more time, so will implementation</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">18</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture8/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Agile planning factors</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Estimate value of features</span></div>
<div style="position:absolute;left:90.80px;top:178.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> e.g. the MoSCoW way</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Cost of implementing features</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Cost of doing it now versus cost of doing it later</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">New knowledge acquired</span></div>
<div style="position:absolute;left:90.80px;top:305.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> First do features that bring a lot of new knowledge</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Risk removed by implementing feature</span></div>
<div style="position:absolute;left:90.80px;top:368.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> First high-value-low risk features, then low risk-low value</span></div>
<div style="position:absolute;left:105.95px;top:392.32px" class="cls_011"><span class="cls_011">features</span></div>
<div style="position:absolute;left:90.80px;top:421.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Avoid high value-high risk features</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">19</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>

</body>
</html>
