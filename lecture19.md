<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:"Calibri",serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:"Calibri",serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:28.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:28.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:"Calibri",serif;font-size:28.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:"Calibri",serif;font-size:28.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:"Calibri",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:"Calibri",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:"Calibri",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:"Verdana",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:"Verdana",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:"Calibri",serif;font-size:36.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:"Calibri",serif;font-size:36.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:"Verdana",serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:"Verdana",serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:"Calibri Italic",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_022{font-family:"Calibri Italic",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_023{font-family:"Calibri Italic",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_023{font-family:"Calibri Italic",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_024{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:"Calibri Italic",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_025{font-family:"Calibri Italic",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_026{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:22.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:"Verdana",serif;font-size:20.1px;color:rgb(220,34,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_029{font-family:"Verdana",serif;font-size:20.1px;color:rgb(220,34,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_030{font-family:"Verdana",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:"Verdana",serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Arial,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Arial,serif;font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:"Verdana",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:"Verdana",serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_034{font-family:"Calibri",serif;font-size:24.1px;color:rgb(220,34,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:"Calibri",serif;font-size:24.1px;color:rgb(220,34,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_035{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_036{font-family:Arial,serif;font-size:20.1px;color:rgb(220,34,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_036{font-family:Arial,serif;font-size:20.1px;color:rgb(220,34,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_037{font-family:"Calibri Italic",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_037{font-family:"Calibri Italic",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_038{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_038{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_039{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_039{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_040{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_040{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_041{font-family:Arial,serif;font-size:14.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_041{font-family:Arial,serif;font-size:14.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_042{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_042{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_044{font-family:Arial,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_044{font-family:Arial,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture19/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:203.99px;top:35.36px" class="cls_002"><span class="cls_002">Service Orientation</span></div>
<div style="position:absolute;left:103.57px;top:349.84px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:103.57px;top:391.84px" class="cls_003"><span class="cls_003">• What’s special about services?</span></div>
<div style="position:absolute;left:103.57px;top:434.80px" class="cls_003"><span class="cls_003">• Essentials of service-oriented SE</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:275.30px;top:37.12px" class="cls_005"><span class="cls_005">Overview</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Services, service description, service</span></div>
<div style="position:absolute;left:70.20px;top:165.52px" class="cls_003"><span class="cls_003">communication</span></div>
<div style="position:absolute;left:43.20px;top:211.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Service-Oriented Architecture (SOA)</span></div>
<div style="position:absolute;left:43.20px;top:258.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Web services</span></div>
<div style="position:absolute;left:43.20px;top:304.48px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> SOSE: Service-Oriented Software Engineering</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_007"><span class="cls_007">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:132.35px;top:37.12px" class="cls_005"><span class="cls_005">Italian restaurant analogy</span></div>
<div style="position:absolute;left:43.20px;top:121.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> Restaurant provides food: a service</span></div>
<div style="position:absolute;left:43.20px;top:193.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> After the order is taken, food is produced, served,</span></div>
<div style="position:absolute;left:70.20px;top:221.52px" class="cls_009"><span class="cls_009">…: service may consist of other services</span></div>
<div style="position:absolute;left:43.20px;top:293.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> The menu indicates the service provided: a</span></div>
<div style="position:absolute;left:70.20px;top:322.56px" class="cls_009"><span class="cls_009">service description</span></div>
<div style="position:absolute;left:43.20px;top:394.56px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> The order is written down, or yelled at, the cook:</span></div>
<div style="position:absolute;left:70.20px;top:423.60px" class="cls_009"><span class="cls_009">services communicate through messages</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_007"><span class="cls_007">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:208.74px;top:37.12px" class="cls_005"><span class="cls_005">Main ingredients</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Services</span></div>
<div style="position:absolute;left:43.20px;top:219.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Service descriptions</span></div>
<div style="position:absolute;left:43.20px;top:265.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Messages</span></div>
<div style="position:absolute;left:43.20px;top:357.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Implementation: through web services</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_007"><span class="cls_007">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:227.48px;top:37.12px" class="cls_005"><span class="cls_005">Other example</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Citizen looking for a house:</span></div>
<div style="position:absolute;left:79.20px;top:173.60px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Check personal data  </span><span class="cls_012">Þ</span><span class="cls_013"> System X</span></div>
<div style="position:absolute;left:79.20px;top:213.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Check tax history</span></div>
<div style="position:absolute;left:346.02px;top:213.68px" class="cls_012"><span class="cls_012">Þ</span><span class="cls_013"> System Y</span></div>
<div style="position:absolute;left:79.20px;top:253.52px" class="cls_010"><span class="cls_010">–</span><span class="cls_011"> Check credit history      </span><span class="cls_012">Þ</span><span class="cls_013"> System Z</span></div>
<div style="position:absolute;left:79.20px;top:294.56px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Search rental agencies    </span><span class="cls_012">Þ</span><span class="cls_013"> System A,B</span></div>
<div style="position:absolute;left:79.20px;top:334.64px" class="cls_010"><span class="cls_010">-</span><span class="cls_011">…</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_007"><span class="cls_007">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:214.33px;top:37.12px" class="cls_005"><span class="cls_005">What’s a service</span></div>
<div style="position:absolute;left:43.20px;top:128.56px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Platform-independent computational entity that can be used in a</span></div>
<div style="position:absolute;left:70.20px;top:152.56px" class="cls_015"><span class="cls_015">platform-independent way</span></div>
<div style="position:absolute;left:43.20px;top:209.68px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Callable entities or application functionalities accessed via exchange of</span></div>
<div style="position:absolute;left:70.20px;top:233.68px" class="cls_015"><span class="cls_015">messages</span></div>
<div style="position:absolute;left:43.20px;top:291.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Component capable of performing a task</span></div>
<div style="position:absolute;left:43.20px;top:349.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Often just used in connection with something else: SOA, Web services, …</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_007"><span class="cls_007">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:159.52px;top:37.12px" class="cls_005"><span class="cls_005">What’s a service, cnt’d</span></div>
<div style="position:absolute;left:43.20px;top:123.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Shift from producing software to using</span></div>
<div style="position:absolute;left:70.20px;top:158.56px" class="cls_003"><span class="cls_003">software</span></div>
<div style="position:absolute;left:79.20px;top:200.48px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> You need not host the software</span></div>
<div style="position:absolute;left:79.20px;top:237.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Or keep track of versions, releases</span></div>
<div style="position:absolute;left:79.20px;top:274.64px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Need not make sure it evolves</span></div>
<div style="position:absolute;left:79.20px;top:311.60px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Etc</span></div>
<div style="position:absolute;left:43.20px;top:348.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Software is “somewhere”, deployed on as-</span></div>
<div style="position:absolute;left:70.20px;top:382.48px" class="cls_003"><span class="cls_003">needed basis</span></div>
<div style="position:absolute;left:43.20px;top:425.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> SaaS: Software as a Service</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_007"><span class="cls_007">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:256.23px;top:37.12px" class="cls_005"><span class="cls_005">Key aspects</span></div>
<div style="position:absolute;left:43.20px;top:149.52px" class="cls_016"><span class="cls_016">•</span><span class="cls_017">  Services can be discovered</span></div>
<div style="position:absolute;left:43.20px;top:173.52px" class="cls_016"><span class="cls_016">•</span><span class="cls_017">  Services can be composed to form larger services</span></div>
<div style="position:absolute;left:43.20px;top:197.52px" class="cls_016"><span class="cls_016">•</span><span class="cls_017">  Services adhere to a service contract</span></div>
<div style="position:absolute;left:43.20px;top:221.52px" class="cls_016"><span class="cls_016">•</span><span class="cls_017">  Services are loosely coupled</span></div>
<div style="position:absolute;left:43.20px;top:244.56px" class="cls_016"><span class="cls_016">•</span><span class="cls_017">  Services are stateless</span></div>
<div style="position:absolute;left:43.20px;top:268.56px" class="cls_016"><span class="cls_016">•</span><span class="cls_017">  Services are autonomous</span></div>
<div style="position:absolute;left:43.20px;top:292.56px" class="cls_016"><span class="cls_016">•</span><span class="cls_017">  Services hide their logic</span></div>
<div style="position:absolute;left:43.20px;top:316.56px" class="cls_016"><span class="cls_016">•</span><span class="cls_017">  Services are reusable</span></div>
<div style="position:absolute;left:43.20px;top:340.56px" class="cls_016"><span class="cls_016">•</span><span class="cls_017">  Services use open standards</span></div>
<div style="position:absolute;left:43.20px;top:363.60px" class="cls_016"><span class="cls_016">•</span><span class="cls_017">  Services facilitate interoperability</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_007"><span class="cls_007">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:206.49px;top:37.12px" class="cls_005"><span class="cls_005">Service discovery</span></div>
<div style="position:absolute;left:308.63px;top:170.40px" class="cls_018"><span class="cls_018">Service</span></div>
<div style="position:absolute;left:310.31px;top:191.28px" class="cls_018"><span class="cls_018">registry</span></div>
<div style="position:absolute;left:193.20px;top:261.60px" class="cls_018"><span class="cls_018">lookup</span></div>
<div style="position:absolute;left:433.20px;top:261.60px" class="cls_018"><span class="cls_018">publish</span></div>
<div style="position:absolute;left:152.63px;top:356.40px" class="cls_018"><span class="cls_018">Service</span></div>
<div style="position:absolute;left:464.63px;top:356.40px" class="cls_018"><span class="cls_018">Service</span></div>
<div style="position:absolute;left:145.63px;top:377.28px" class="cls_018"><span class="cls_018">requestor</span></div>
<div style="position:absolute;left:463.56px;top:377.28px" class="cls_018"><span class="cls_018">provider</span></div>
<div style="position:absolute;left:325.20px;top:387.60px" class="cls_018"><span class="cls_018">bind</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_007"><span class="cls_007">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:206.49px;top:37.12px" class="cls_005"><span class="cls_005">Service discovery</span></div>
<div style="position:absolute;left:275.28px;top:181.20px" class="cls_018"><span class="cls_018">Rental agency 1</span></div>
<div style="position:absolute;left:85.20px;top:243.60px" class="cls_018"><span class="cls_018">Apartment</span></div>
<div style="position:absolute;left:433.20px;top:261.60px" class="cls_018"><span class="cls_018">publish</span></div>
<div style="position:absolute;left:85.20px;top:264.48px" class="cls_018"><span class="cls_018">(immediate, cheap)</span></div>
<div style="position:absolute;left:281.95px;top:282.00px" class="cls_018"><span class="cls_018">Agency 1</span></div>
<div style="position:absolute;left:305.95px;top:342.00px" class="cls_018"><span class="cls_018">Apartment?</span></div>
<div style="position:absolute;left:132.36px;top:356.40px" class="cls_018"><span class="cls_018">Municipality</span></div>
<div style="position:absolute;left:440.28px;top:367.20px" class="cls_018"><span class="cls_018">Rental agency 1</span></div>
<div style="position:absolute;left:156.63px;top:377.28px" class="cls_018"><span class="cls_018">system</span></div>
<div style="position:absolute;left:265.20px;top:393.60px" class="cls_018"><span class="cls_018">Rental agreement</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:206.49px;top:37.12px" class="cls_005"><span class="cls_005">Service discovery</span></div>
<div style="position:absolute;left:43.20px;top:128.56px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Discovery is dynamic, each invocation may select a different one</span></div>
<div style="position:absolute;left:43.20px;top:185.68px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Primary criterion in selection: contract</span></div>
<div style="position:absolute;left:43.20px;top:243.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Selection may be based on workload, complexity of the question, etc  </span><span class="cls_014">Þ</span></div>
<div style="position:absolute;left:70.20px;top:267.52px" class="cls_015"><span class="cls_015">optimize compute resources</span></div>
<div style="position:absolute;left:43.20px;top:325.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  If answer fails, or takes too long  </span><span class="cls_014">Þ</span><span class="cls_015"> select another service  </span><span class="cls_014">Þ</span><span class="cls_015"> more fault-</span></div>
<div style="position:absolute;left:70.20px;top:349.60px" class="cls_015"><span class="cls_015">tolerance</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:664.34px;top:507.60px" class="cls_007"><span class="cls_007">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:149.22px;top:37.12px" class="cls_005"><span class="cls_005">Is discovery really new?</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Many design patterns loosen coupling</span></div>
<div style="position:absolute;left:70.20px;top:211.60px" class="cls_003"><span class="cls_003">between classes</span></div>
<div style="position:absolute;left:43.20px;top:304.48px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Factory pattern: creates object without</span></div>
<div style="position:absolute;left:70.20px;top:342.64px" class="cls_003"><span class="cls_003">specifying the exact class of the object.</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:125.79px;top:37.12px" class="cls_005"><span class="cls_005">Services can be composed</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Service can be a building block for larger</span></div>
<div style="position:absolute;left:70.20px;top:211.60px" class="cls_003"><span class="cls_003">services</span></div>
<div style="position:absolute;left:43.20px;top:304.48px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Not different from CBSE and other approaches</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:218.93px;top:35.76px" class="cls_019"><span class="cls_019">Services adhere to a contract</span></div>
<div style="position:absolute;left:43.20px;top:125.68px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Request to registry should contain everything needed, not just</span></div>
<div style="position:absolute;left:70.20px;top:147.52px" class="cls_015"><span class="cls_015">functionality</span></div>
<div style="position:absolute;left:43.20px;top:200.56px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  For “normal” components, much is implicit:</span></div>
<div style="position:absolute;left:79.20px;top:226.56px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> Platform characteristics</span></div>
<div style="position:absolute;left:79.20px;top:250.56px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> Quality information</span></div>
<div style="position:absolute;left:79.20px;top:274.56px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> Tacit design decisions</span></div>
<div style="position:absolute;left:43.20px;top:324.64px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Trust promises?</span></div>
<div style="position:absolute;left:43.20px;top:377.68px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Quality of Services (QoC), levels thereof</span></div>
<div style="position:absolute;left:43.20px;top:429.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Service Level Agreement (SLA)</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:206.49px;top:37.12px" class="cls_005"><span class="cls_005">Service discovery</span></div>
<div style="position:absolute;left:272.28px;top:170.40px" class="cls_018"><span class="cls_018">Rental agency 1</span></div>
<div style="position:absolute;left:272.28px;top:191.28px" class="cls_018"><span class="cls_018">Rental agency 2</span></div>
<div style="position:absolute;left:91.20px;top:243.60px" class="cls_018"><span class="cls_018">Apartment</span></div>
<div style="position:absolute;left:91.20px;top:264.48px" class="cls_018"><span class="cls_018">(immediate, cheap)</span></div>
<div style="position:absolute;left:281.95px;top:282.00px" class="cls_018"><span class="cls_018">Agency 1</span></div>
<div style="position:absolute;left:305.95px;top:342.00px" class="cls_018"><span class="cls_018">Apartment?</span></div>
<div style="position:absolute;left:132.36px;top:356.40px" class="cls_018"><span class="cls_018">Municipality</span></div>
<div style="position:absolute;left:440.28px;top:367.20px" class="cls_018"><span class="cls_018">Rental agency 1</span></div>
<div style="position:absolute;left:156.63px;top:377.28px" class="cls_018"><span class="cls_018">system</span></div>
<div style="position:absolute;left:265.20px;top:393.60px" class="cls_018"><span class="cls_018">Rental agreement</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:108.96px;top:37.12px" class="cls_005"><span class="cls_005">Services are loosely coupled</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Rental agencies come and go</span></div>
<div style="position:absolute;left:43.20px;top:265.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> No assumptions possible</span></div>
<div style="position:absolute;left:43.20px;top:357.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Stronger than CBSE loose coupling</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:171.10px;top:37.12px" class="cls_005"><span class="cls_005">Services are stateless</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Rental agency cannot retain information: it</span></div>
<div style="position:absolute;left:70.20px;top:211.60px" class="cls_003"><span class="cls_003">doesn’t know if and when it will be invoked</span></div>
<div style="position:absolute;left:70.20px;top:250.48px" class="cls_003"><span class="cls_003">again, and by whom</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:159.16px;top:50.16px" class="cls_019"><span class="cls_019">Services are autonomous, hide their logic</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Rental agency has its own rules on how to</span></div>
<div style="position:absolute;left:70.20px;top:165.52px" class="cls_003"><span class="cls_003">structure its process</span></div>
<div style="position:absolute;left:43.20px;top:258.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Its logic does not depend on the municipality</span></div>
<div style="position:absolute;left:70.20px;top:296.56px" class="cls_003"><span class="cls_003">service it is invoked by</span></div>
<div style="position:absolute;left:43.20px;top:388.48px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> This works two ways: outside doesn’t know</span></div>
<div style="position:absolute;left:70.20px;top:426.64px" class="cls_003"><span class="cls_003">the inside, and vice versa</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:171.63px;top:37.12px" class="cls_005"><span class="cls_005">Services are reusable</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Service models a business process:</span></div>
<div style="position:absolute;left:79.20px;top:172.64px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Not very fine grained</span></div>
<div style="position:absolute;left:79.20px;top:213.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Collecting debt status from one credit company is</span></div>
<div style="position:absolute;left:101.70px;top:246.56px" class="cls_011"><span class="cls_011">not a service, checking credit status is</span></div>
<div style="position:absolute;left:43.20px;top:333.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Deciding on proper granularity raises lots of</span></div>
<div style="position:absolute;left:70.20px;top:372.64px" class="cls_003"><span class="cls_003">debate</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:116.67px;top:37.12px" class="cls_005"><span class="cls_005">Service use open standards</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Proprietary standards </span><span class="cls_006">Þ</span><span class="cls_003"> vendor lockin</span></div>
<div style="position:absolute;left:43.20px;top:219.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> There are lots of open standards:</span></div>
<div style="position:absolute;left:79.20px;top:264.56px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> How services are described</span></div>
<div style="position:absolute;left:79.20px;top:305.60px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> How services communicate</span></div>
<div style="position:absolute;left:79.20px;top:345.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> How services exchange data</span></div>
<div style="position:absolute;left:79.20px;top:385.52px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> etc</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:117.22px;top:42.96px" class="cls_020"><span class="cls_020">Services facilitate interoperability</span></div>
<div style="position:absolute;left:43.20px;top:120.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Because of open standards, explicit contracts</span></div>
<div style="position:absolute;left:70.20px;top:151.60px" class="cls_003"><span class="cls_003">and loose coupling</span></div>
<div style="position:absolute;left:43.20px;top:228.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Classical CBSE solutions pose problems:</span></div>
<div style="position:absolute;left:79.20px;top:266.48px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Proprietary formats</span></div>
<div style="position:absolute;left:79.20px;top:300.56px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Platform differences</span></div>
<div style="position:absolute;left:79.20px;top:333.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Etc</span></div>
<div style="position:absolute;left:43.20px;top:405.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Interoperability within an organization (EAI)</span></div>
<div style="position:absolute;left:70.20px;top:436.48px" class="cls_003"><span class="cls_003">and between (B2B)</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:275.30px;top:37.12px" class="cls_005"><span class="cls_005">Overview</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Services, service description, service</span></div>
<div style="position:absolute;left:70.20px;top:165.52px" class="cls_003"><span class="cls_003">communication</span></div>
<div style="position:absolute;left:43.20px;top:258.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Service-Oriented Architecture (SOA)</span></div>
<div style="position:absolute;left:43.20px;top:350.56px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Web services</span></div>
<div style="position:absolute;left:43.20px;top:396.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> SOSE: Service-Oriented Software Engineering</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:275.30px;top:37.12px" class="cls_005"><span class="cls_005">Overview</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Services, service description, service</span></div>
<div style="position:absolute;left:70.20px;top:165.52px" class="cls_003"><span class="cls_003">communication</span></div>
<div style="position:absolute;left:43.20px;top:211.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Service-Oriented Architecture (SOA)</span></div>
<div style="position:absolute;left:43.20px;top:304.48px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Web services</span></div>
<div style="position:absolute;left:43.20px;top:396.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> SOSE: Service-Oriented Software Engineering</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:242.41px;top:37.12px" class="cls_005"><span class="cls_005">Web services</span></div>
<div style="position:absolute;left:43.20px;top:128.56px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Implementation means to realize services</span></div>
<div style="position:absolute;left:43.20px;top:157.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Based on open standards:</span></div>
<div style="position:absolute;left:79.20px;top:185.52px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> XML</span></div>
<div style="position:absolute;left:79.20px;top:211.68px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> SOAP: Simple Object Access Protocol</span></div>
<div style="position:absolute;left:79.20px;top:237.60px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> WSDL: Web Services Description Language</span></div>
<div style="position:absolute;left:79.20px;top:263.52px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> UDDI: Universal Description, Discovery and Integration</span></div>
<div style="position:absolute;left:79.20px;top:289.68px" class="cls_016"><span class="cls_016">-</span><span class="cls_017"> BPEL4WS: Business Process Execution Language for Web Services</span></div>
<div style="position:absolute;left:43.20px;top:315.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_015">  Main standardization bodies: OASIS, W3C</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:320.50px;top:37.12px" class="cls_005"><span class="cls_005">XML</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Looks like HTML</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Language/vocabulary defined in schema:</span></div>
<div style="position:absolute;left:70.20px;top:211.60px" class="cls_003"><span class="cls_003">collection of trees</span></div>
<div style="position:absolute;left:43.20px;top:258.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Only syntax</span></div>
<div style="position:absolute;left:43.20px;top:304.48px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Semantic Web, Web 2.0: semantics as well:</span></div>
<div style="position:absolute;left:70.20px;top:342.64px" class="cls_003"><span class="cls_003">OWL and descendants</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:311.43px;top:37.12px" class="cls_005"><span class="cls_005">SOAP</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Message inside an envelope</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Envelop has optional header (~address), and</span></div>
<div style="position:absolute;left:70.20px;top:211.60px" class="cls_003"><span class="cls_003">mandatory body: actual container of data</span></div>
<div style="position:absolute;left:43.20px;top:258.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> SOAP message is unidirectional: it’s NOT a</span></div>
<div style="position:absolute;left:70.20px;top:296.56px" class="cls_003"><span class="cls_003">conversation</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:307.61px;top:37.12px" class="cls_005"><span class="cls_005">WSDL</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Four parts:</span></div>
<div style="position:absolute;left:79.20px;top:172.64px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Web service interfaces</span></div>
<div style="position:absolute;left:79.20px;top:213.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Message definitions</span></div>
<div style="position:absolute;left:79.20px;top:253.52px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Bindings: transport, format details</span></div>
<div style="position:absolute;left:79.20px;top:293.60px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Services: endpoints for accessing service.</span></div>
<div style="position:absolute;left:101.70px;top:327.68px" class="cls_011"><span class="cls_011">Endpoint = (binding, network address)</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:313.19px;top:37.12px" class="cls_005"><span class="cls_005">UDDI</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Three (main) parts:</span></div>
<div style="position:absolute;left:79.20px;top:172.64px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Info about organization that publishes the services</span></div>
<div style="position:absolute;left:79.20px;top:213.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Descriptive info about each service</span></div>
<div style="position:absolute;left:79.20px;top:253.52px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Technical info to link services to implementation</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:250.50px;top:37.12px" class="cls_005"><span class="cls_005">UDDI (cnt’d)</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Original dream: one global registry</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Reality: many registries, with different levels</span></div>
<div style="position:absolute;left:70.20px;top:211.60px" class="cls_003"><span class="cls_003">of visibility</span></div>
<div style="position:absolute;left:79.20px;top:257.60px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Mapping problems</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">29</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:275.30px;top:37.12px" class="cls_005"><span class="cls_005">Overview</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Services, service description, service</span></div>
<div style="position:absolute;left:70.20px;top:165.52px" class="cls_003"><span class="cls_003">communication</span></div>
<div style="position:absolute;left:43.20px;top:211.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Service-Oriented Architecture (SOA)</span></div>
<div style="position:absolute;left:43.20px;top:258.64px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Web services</span></div>
<div style="position:absolute;left:43.20px;top:350.56px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> SOSE: Service-Oriented Software Engineering</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">30</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:232.95px;top:28.96px" class="cls_005"><span class="cls_005">SOSE life cycle</span></div>
<div style="position:absolute;left:64.75px;top:179.92px" class="cls_021"><span class="cls_021">Service oriented</span></div>
<div style="position:absolute;left:330.81px;top:179.92px" class="cls_021"><span class="cls_021">Service</span></div>
<div style="position:absolute;left:554.81px;top:179.92px" class="cls_021"><span class="cls_021">Service</span></div>
<div style="position:absolute;left:106.38px;top:204.88px" class="cls_021"><span class="cls_021">analysis</span></div>
<div style="position:absolute;left:302.65px;top:204.88px" class="cls_021"><span class="cls_021">development</span></div>
<div style="position:absolute;left:532.57px;top:204.88px" class="cls_021"><span class="cls_021">deployment</span></div>
<div style="position:absolute;left:64.75px;top:318.64px" class="cls_021"><span class="cls_021">Service oriented</span></div>
<div style="position:absolute;left:330.81px;top:318.64px" class="cls_021"><span class="cls_021">Service</span></div>
<div style="position:absolute;left:554.81px;top:318.64px" class="cls_021"><span class="cls_021">Service</span></div>
<div style="position:absolute;left:113.81px;top:343.84px" class="cls_021"><span class="cls_021">design</span></div>
<div style="position:absolute;left:333.31px;top:343.84px" class="cls_021"><span class="cls_021">testing</span></div>
<div style="position:absolute;left:519.55px;top:343.84px" class="cls_021"><span class="cls_021">administration</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">31</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:249.77px;top:17.44px" class="cls_005"><span class="cls_005">Terminology</span></div>
<div style="position:absolute;left:25.50px;top:117.76px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> service oriented environment (or service</span></div>
<div style="position:absolute;left:51.50px;top:157.84px" class="cls_003"><span class="cls_003">oriented </span><span class="cls_022">ecosystem)</span></div>
<div style="position:absolute;left:25.50px;top:207.76px" class="cls_006"><span class="cls_006">•</span><span class="cls_022"> business process + supporting services</span></div>
<div style="position:absolute;left:61.50px;top:258.80px" class="cls_010"><span class="cls_010">-</span><span class="cls_023"> application (infrastructure) service</span></div>
<div style="position:absolute;left:61.50px;top:303.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_023"> business service</span></div>
<div style="position:absolute;left:97.50px;top:345.84px" class="cls_024"><span class="cls_024">•</span><span class="cls_025"> Task-centric business service</span></div>
<div style="position:absolute;left:97.50px;top:379.68px" class="cls_024"><span class="cls_024">•</span><span class="cls_025"> Entity-centric business service</span></div>
<div style="position:absolute;left:61.50px;top:417.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_023"> hybrid service</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">32</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:249.83px;top:20.08px" class="cls_005"><span class="cls_005">Terminology</span></div>
<div style="position:absolute;left:501.00px;top:118.72px" class="cls_014"><span class="cls_014">entity centric</span></div>
<div style="position:absolute;left:358.68px;top:151.92px" class="cls_026"><span class="cls_026">order</span></div>
<div style="position:absolute;left:557.46px;top:153.04px" class="cls_028"><span class="cls_028">entity-centric</span></div>
<div style="position:absolute;left:343.69px;top:168.96px" class="cls_026"><span class="cls_026">fulfilment</span></div>
<div style="position:absolute;left:26.84px;top:169.28px" class="cls_027"><span class="cls_027">hybrid services</span></div>
<div style="position:absolute;left:54.31px;top:179.68px" class="cls_014"><span class="cls_014">hybrid services</span></div>
<div style="position:absolute;left:351.19px;top:186.96px" class="cls_026"><span class="cls_026">service</span></div>
<div style="position:absolute;left:567.50px;top:190.96px" class="cls_014"><span class="cls_014">task centric</span></div>
<div style="position:absolute;left:601.71px;top:207.68px" class="cls_027"><span class="cls_027">task-centric</span></div>
<div style="position:absolute;left:342.69px;top:264.24px" class="cls_026"><span class="cls_026">purchase</span></div>
<div style="position:absolute;left:464.31px;top:263.28px" class="cls_026"><span class="cls_026">customer</span></div>
<div style="position:absolute;left:590.18px;top:263.28px" class="cls_026"><span class="cls_026">verify</span></div>
<div style="position:absolute;left:26.84px;top:268.40px" class="cls_027"><span class="cls_027">business services</span></div>
<div style="position:absolute;left:358.68px;top:281.28px" class="cls_026"><span class="cls_026">order</span></div>
<div style="position:absolute;left:476.81px;top:280.08px" class="cls_026"><span class="cls_026">profile</span></div>
<div style="position:absolute;left:598.68px;top:280.32px" class="cls_026"><span class="cls_026">PO</span></div>
<div style="position:absolute;left:42.06px;top:281.68px" class="cls_014"><span class="cls_014">business services</span></div>
<div style="position:absolute;left:351.19px;top:299.28px" class="cls_026"><span class="cls_026">service</span></div>
<div style="position:absolute;left:472.81px;top:298.08px" class="cls_026"><span class="cls_026">service</span></div>
<div style="position:absolute;left:583.19px;top:298.32px" class="cls_026"><span class="cls_026">service</span></div>
<div style="position:absolute;left:360.18px;top:407.04px" class="cls_026"><span class="cls_026">send</span></div>
<div style="position:absolute;left:251.06px;top:415.92px" class="cls_026"><span class="cls_026">wrapper</span></div>
<div style="position:absolute;left:440.69px;top:415.92px" class="cls_026"><span class="cls_026">notification</span></div>
<div style="position:absolute;left:26.84px;top:415.28px" class="cls_027"><span class="cls_027">infrastructure services</span></div>
<div style="position:absolute;left:359.18px;top:424.08px" class="cls_026"><span class="cls_026">utility</span></div>
<div style="position:absolute;left:255.06px;top:432.96px" class="cls_026"><span class="cls_026">service</span></div>
<div style="position:absolute;left:455.19px;top:432.96px" class="cls_026"><span class="cls_026">service</span></div>
<div style="position:absolute;left:351.19px;top:442.08px" class="cls_026"><span class="cls_026">service</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">33</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:159.87px;top:3.76px" class="cls_005"><span class="cls_005">Strategies for life cycle</span></div>
<div style="position:absolute;left:249.59px;top:35.68px" class="cls_005"><span class="cls_005">organization</span></div>
<div style="position:absolute;left:54.00px;top:139.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Top-down strategy</span></div>
<div style="position:absolute;left:54.00px;top:189.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Bottom-up strategy</span></div>
<div style="position:absolute;left:54.00px;top:241.60px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Agile strategy</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">34</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:194.12px;top:28.96px" class="cls_005"><span class="cls_005">Top-down strategy</span></div>
<div style="position:absolute;left:64.75px;top:179.92px" class="cls_021"><span class="cls_021">Service oriented</span></div>
<div style="position:absolute;left:330.81px;top:179.92px" class="cls_021"><span class="cls_021">Service</span></div>
<div style="position:absolute;left:554.81px;top:179.92px" class="cls_021"><span class="cls_021">Service</span></div>
<div style="position:absolute;left:106.38px;top:204.88px" class="cls_021"><span class="cls_021">analysis</span></div>
<div style="position:absolute;left:302.65px;top:204.88px" class="cls_021"><span class="cls_021">development</span></div>
<div style="position:absolute;left:532.57px;top:204.88px" class="cls_021"><span class="cls_021">deployment</span></div>
<div style="position:absolute;left:64.75px;top:318.64px" class="cls_021"><span class="cls_021">Service oriented</span></div>
<div style="position:absolute;left:330.81px;top:318.64px" class="cls_021"><span class="cls_021">Service</span></div>
<div style="position:absolute;left:113.81px;top:343.84px" class="cls_021"><span class="cls_021">design</span></div>
<div style="position:absolute;left:333.31px;top:343.84px" class="cls_021"><span class="cls_021">testing</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">35</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:166.56px;top:20.08px" class="cls_005"><span class="cls_005">Top-down SO analysis</span></div>
<div style="position:absolute;left:57.81px;top:139.84px" class="cls_014"><span class="cls_014">step 1</span></div>
<div style="position:absolute;left:276.19px;top:139.84px" class="cls_014"><span class="cls_014">step 2</span></div>
<div style="position:absolute;left:60.13px;top:179.92px" class="cls_021"><span class="cls_021">Define enterprise</span></div>
<div style="position:absolute;left:509.88px;top:179.92px" class="cls_021"><span class="cls_021">Service oriented</span></div>
<div style="position:absolute;left:296.38px;top:192.16px" class="cls_021"><span class="cls_021">Compose SOA</span></div>
<div style="position:absolute;left:63.94px;top:204.88px" class="cls_021"><span class="cls_021">business models</span></div>
<div style="position:absolute;left:558.93px;top:204.88px" class="cls_021"><span class="cls_021">design</span></div>
<div style="position:absolute;left:55.06px;top:278.80px" class="cls_014"><span class="cls_014">step 3</span></div>
<div style="position:absolute;left:276.19px;top:278.80px" class="cls_014"><span class="cls_014">step 4</span></div>
<div style="position:absolute;left:60.13px;top:318.64px" class="cls_021"><span class="cls_021">Define enterprise</span></div>
<div style="position:absolute;left:289.43px;top:318.64px" class="cls_021"><span class="cls_021">Perform service</span></div>
<div style="position:absolute;left:77.06px;top:343.84px" class="cls_021"><span class="cls_021">service model</span></div>
<div style="position:absolute;left:282.56px;top:343.84px" class="cls_021"><span class="cls_021">oriented analysis</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">36</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:186.98px;top:20.32px" class="cls_005"><span class="cls_005">Bottom-up strategy</span></div>
<div style="position:absolute;left:327.15px;top:167.68px" class="cls_021"><span class="cls_021">Develop</span></div>
<div style="position:absolute;left:59.31px;top:179.92px" class="cls_021"><span class="cls_021">Model application</span></div>
<div style="position:absolute;left:557.07px;top:179.92px" class="cls_021"><span class="cls_021">Deploy</span></div>
<div style="position:absolute;left:313.38px;top:192.64px" class="cls_029"><span class="cls_029">application</span></div>
<div style="position:absolute;left:102.62px;top:204.88px" class="cls_021"><span class="cls_021">services</span></div>
<div style="position:absolute;left:551.25px;top:204.88px" class="cls_021"><span class="cls_021">services</span></div>
<div style="position:absolute;left:327.25px;top:216.64px" class="cls_029"><span class="cls_029">services</span></div>
<div style="position:absolute;left:54.63px;top:318.64px" class="cls_021"><span class="cls_021">Design </span><span class="cls_029">application</span></div>
<div style="position:absolute;left:347.45px;top:318.64px" class="cls_021"><span class="cls_021">Test</span></div>
<div style="position:absolute;left:111.31px;top:343.84px" class="cls_029"><span class="cls_029">service</span></div>
<div style="position:absolute;left:327.25px;top:343.84px" class="cls_021"><span class="cls_021">services</span></div>
<div style="position:absolute;left:110.44px;top:465.76px" class="cls_014"><span class="cls_014">application service = infrastructure service</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">37</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:239.26px;top:20.08px" class="cls_005"><span class="cls_005">Agile strategy</span></div>
<div style="position:absolute;left:86.39px;top:89.20px" class="cls_021"><span class="cls_021">Top-down</span></div>
<div style="position:absolute;left:95.06px;top:114.16px" class="cls_021"><span class="cls_021">analysis</span></div>
<div style="position:absolute;left:438.19px;top:112.72px" class="cls_021"><span class="cls_021">SO analysis</span></div>
<div style="position:absolute;left:264.13px;top:137.52px" class="cls_016"><span class="cls_016">align with</span></div>
<div style="position:absolute;left:273.63px;top:157.44px" class="cls_016"><span class="cls_016">current</span></div>
<div style="position:absolute;left:282.13px;top:177.36px" class="cls_016"><span class="cls_016">state</span></div>
<div style="position:absolute;left:445.63px;top:175.12px" class="cls_021"><span class="cls_021">SO design</span></div>
<div style="position:absolute;left:266.13px;top:197.52px" class="cls_016"><span class="cls_016">business</span></div>
<div style="position:absolute;left:272.63px;top:217.44px" class="cls_016"><span class="cls_016">models</span></div>
<div style="position:absolute;left:412.15px;top:232.00px" class="cls_021"><span class="cls_021">Develop services</span></div>
<div style="position:absolute;left:381.56px;top:285.76px" class="cls_021"><span class="cls_021">Test service operations</span></div>
<div style="position:absolute;left:418.07px;top:348.16px" class="cls_021"><span class="cls_021">Deploy services</span></div>
<div style="position:absolute;left:416.68px;top:398.56px" class="cls_021"><span class="cls_021">Revisit business</span></div>
<div style="position:absolute;left:264.13px;top:423.84px" class="cls_016"><span class="cls_016">align with</span></div>
<div style="position:absolute;left:383.38px;top:423.52px" class="cls_021"><span class="cls_021">(and process) services</span></div>
<div style="position:absolute;left:102.21px;top:441.04px" class="cls_028"><span class="cls_028">on-going</span></div>
<div style="position:absolute;left:273.63px;top:443.76px" class="cls_016"><span class="cls_016">current</span></div>
<div style="position:absolute;left:282.13px;top:463.68px" class="cls_016"><span class="cls_016">state</span></div>
<div style="position:absolute;left:266.13px;top:483.84px" class="cls_016"><span class="cls_016">business</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:272.63px;top:503.76px" class="cls_016"><span class="cls_016">models</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">38</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background39.jpg" width=720 height=540></div>
<div style="position:absolute;left:140.26px;top:20.32px" class="cls_005"><span class="cls_005">Service oriented analysis</span></div>
<div style="position:absolute;left:34.00px;top:257.92px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> The process of determining how business</span></div>
<div style="position:absolute;left:60.00px;top:297.76px" class="cls_003"><span class="cls_003">automation requirements can be represented</span></div>
<div style="position:absolute;left:60.00px;top:337.84px" class="cls_003"><span class="cls_003">through service orientation</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">39</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:21450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background40.jpg" width=720 height=540></div>
<div style="position:absolute;left:182.52px;top:20.08px" class="cls_005"><span class="cls_005">Goals of SO analysis</span></div>
<div style="position:absolute;left:91.94px;top:103.04px" class="cls_030"><span class="cls_030">Service operation</span></div>
<div style="position:absolute;left:375.69px;top:103.04px" class="cls_030"><span class="cls_030">Service candidates</span></div>
<div style="position:absolute;left:129.25px;top:138.08px" class="cls_030"><span class="cls_030">candidates</span></div>
<div style="position:absolute;left:383.81px;top:138.08px" class="cls_030"><span class="cls_030">(logical contexts)</span></div>
<div style="position:absolute;left:57.60px;top:265.84px" class="cls_031"><span class="cls_031">l</span><span class="cls_015">    Appropriateness for intended use</span></div>
<div style="position:absolute;left:57.60px;top:296.80px" class="cls_031"><span class="cls_031">l</span><span class="cls_015">    Identify preliminary issues that may challenge required service</span></div>
<div style="position:absolute;left:82.75px;top:324.88px" class="cls_015"><span class="cls_015">autonomy</span></div>
<div style="position:absolute;left:57.60px;top:356.80px" class="cls_031"><span class="cls_031">l</span><span class="cls_015">    Define known preliminary composition models</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">40</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background41.jpg" width=720 height=540></div>
<div style="position:absolute;left:181.92px;top:27.76px" class="cls_005"><span class="cls_005">3 Analysis sub-steps</span></div>
<div style="position:absolute;left:376.34px;top:118.00px" class="cls_014"><span class="cls_014">step 1</span></div>
<div style="position:absolute;left:101.63px;top:160.00px" class="cls_021"><span class="cls_021">Service oriented</span></div>
<div style="position:absolute;left:431.88px;top:160.00px" class="cls_021"><span class="cls_021">Define</span></div>
<div style="position:absolute;left:143.25px;top:184.96px" class="cls_021"><span class="cls_021">analysis</span></div>
<div style="position:absolute;left:391.75px;top:184.96px" class="cls_021"><span class="cls_021">analysis scope</span></div>
<div style="position:absolute;left:376.34px;top:231.28px" class="cls_014"><span class="cls_014">step 2</span></div>
<div style="position:absolute;left:425.23px;top:263.92px" class="cls_021"><span class="cls_021">Identify</span></div>
<div style="position:absolute;left:406.88px;top:289.12px" class="cls_021"><span class="cls_021">automation</span></div>
<div style="position:absolute;left:101.63px;top:298.96px" class="cls_032"><span class="cls_032">Service oriented</span></div>
<div style="position:absolute;left:422.88px;top:313.12px" class="cls_021"><span class="cls_021">systems</span></div>
<div style="position:absolute;left:150.68px;top:323.92px" class="cls_032"><span class="cls_032">design</span></div>
<div style="position:absolute;left:376.34px;top:341.92px" class="cls_014"><span class="cls_014">step 3</span></div>
<div style="position:absolute;left:434.56px;top:386.80px" class="cls_021"><span class="cls_021">Model</span></div>
<div style="position:absolute;left:371.50px;top:412.00px" class="cls_021"><span class="cls_021">candidate services</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">41</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background42.jpg" width=720 height=540></div>
<div style="position:absolute;left:103.07px;top:27.76px" class="cls_005"><span class="cls_005">Step 1: Define analysis scope</span></div>
<div style="position:absolute;left:72.00px;top:158.96px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Mature and understood business requirements</span></div>
<div style="position:absolute;left:108.00px;top:196.08px" class="cls_024"><span class="cls_024">•</span><span class="cls_019"> S = ∑i Si, where smaller services may still be quite</span></div>
<div style="position:absolute;left:126.00px;top:222.00px" class="cls_019"><span class="cls_019">complex</span></div>
<div style="position:absolute;left:72.00px;top:290.96px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Can lead to</span></div>
<div style="position:absolute;left:108.00px;top:326.88px" class="cls_024"><span class="cls_024">•</span><span class="cls_019"> process-agnostic services/service operations (</span><span class="cls_034">generic</span></div>
<div style="position:absolute;left:126.00px;top:353.04px" class="cls_019"><span class="cls_019">service portfolio)</span></div>
<div style="position:absolute;left:108.00px;top:384.96px" class="cls_024"><span class="cls_024">•</span><span class="cls_019"> services delivering </span><span class="cls_034">business-specific</span><span class="cls_019"> tasks</span></div>
<div style="position:absolute;left:72.00px;top:453.92px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Models: UML use case or activity diagrams</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">42</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background43.jpg" width=720 height=540></div>
<div style="position:absolute;left:134.24px;top:27.76px" class="cls_005"><span class="cls_005">Order Fulfillment Process</span></div>
<div style="position:absolute;left:228.44px;top:101.92px" class="cls_014"><span class="cls_014">start</span></div>
<div style="position:absolute;left:431.56px;top:174.16px" class="cls_014"><span class="cls_014">Transform</span></div>
<div style="position:absolute;left:178.56px;top:188.32px" class="cls_014"><span class="cls_014">receive PO</span></div>
<div style="position:absolute;left:462.25px;top:196.00px" class="cls_014"><span class="cls_014">PO</span></div>
<div style="position:absolute;left:176.31px;top:263.44px" class="cls_014"><span class="cls_014">validate PO</span></div>
<div style="position:absolute;left:448.44px;top:276.16px" class="cls_014"><span class="cls_014">Import</span></div>
<div style="position:absolute;left:462.25px;top:298.00px" class="cls_014"><span class="cls_014">PO</span></div>
<div style="position:absolute;left:298.21px;top:336.64px" class="cls_014"><span class="cls_014">yes</span></div>
<div style="position:absolute;left:212.38px;top:351.28px" class="cls_014"><span class="cls_014">PO</span></div>
<div style="position:absolute;left:206.19px;top:364.24px" class="cls_014"><span class="cls_014">valid</span></div>
<div style="position:absolute;left:436.13px;top:378.16px" class="cls_014"><span class="cls_014">Send PO</span></div>
<div style="position:absolute;left:241.59px;top:401.68px" class="cls_014"><span class="cls_014">no</span></div>
<div style="position:absolute;left:437.81px;top:400.24px" class="cls_014"><span class="cls_014">to queue</span></div>
<div style="position:absolute;left:204.56px;top:453.04px" class="cls_014"><span class="cls_014">Send</span></div>
<div style="position:absolute;left:457.81px;top:459.76px" class="cls_014"><span class="cls_014">stop</span></div>
<div style="position:absolute;left:180.13px;top:474.88px" class="cls_014"><span class="cls_014">notification</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">43</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background44.jpg" width=720 height=540></div>
<div style="position:absolute;left:39.15px;top:36.88px" class="cls_005"><span class="cls_005">Step 2: Identify automation systems</span></div>
<div style="position:absolute;left:54.00px;top:185.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> What is already implemented?</span></div>
<div style="position:absolute;left:90.00px;top:232.64px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> encapsulate</span></div>
<div style="position:absolute;left:90.00px;top:272.72px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> replace</span></div>
<div style="position:absolute;left:54.00px;top:359.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Models: UML deployment diagram, mapping</span></div>
<div style="position:absolute;left:80.00px;top:398.56px" class="cls_003"><span class="cls_003">tables</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">44</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background45.jpg" width=720 height=540></div>
<div style="position:absolute;left:134.24px;top:27.76px" class="cls_005"><span class="cls_005">Order Fulfillment Process</span></div>
<div style="position:absolute;left:7.09px;top:75.76px" class="cls_035"><span class="cls_035">already</span></div>
<div style="position:absolute;left:7.09px;top:97.84px" class="cls_035"><span class="cls_035">automated</span></div>
<div style="position:absolute;left:157.56px;top:101.92px" class="cls_014"><span class="cls_014">start</span></div>
<div style="position:absolute;left:7.09px;top:119.68px" class="cls_035"><span class="cls_035">by</span></div>
<div style="position:absolute;left:7.09px;top:142.72px" class="cls_035"><span class="cls_035">Order</span></div>
<div style="position:absolute;left:473.46px;top:153.28px" class="cls_035"><span class="cls_035">(XML -> native format)</span></div>
<div style="position:absolute;left:7.09px;top:164.80px" class="cls_035"><span class="cls_035">fulfillment</span></div>
<div style="position:absolute;left:360.68px;top:174.16px" class="cls_014"><span class="cls_014">Transform</span></div>
<div style="position:absolute;left:473.46px;top:175.36px" class="cls_035"><span class="cls_035">(currently custom</span></div>
<div style="position:absolute;left:7.09px;top:186.88px" class="cls_035"><span class="cls_035">service</span><span class="cls_014">    receive PO</span></div>
<div style="position:absolute;left:391.38px;top:196.00px" class="cls_014"><span class="cls_014">PO</span></div>
<div style="position:absolute;left:473.46px;top:197.20px" class="cls_035"><span class="cls_035">component)</span></div>
<div style="position:absolute;left:473.46px;top:220.24px" class="cls_036"><span class="cls_036">service candidate</span></div>
<div style="position:absolute;left:10.46px;top:256.48px" class="cls_035"><span class="cls_035">same as</span></div>
<div style="position:absolute;left:105.44px;top:263.44px" class="cls_014"><span class="cls_014">validate PO</span></div>
<div style="position:absolute;left:468.84px;top:261.04px" class="cls_035"><span class="cls_035">(into accounting sys.)</span></div>
<div style="position:absolute;left:10.46px;top:278.56px" class="cls_035"><span class="cls_035">previous</span></div>
<div style="position:absolute;left:377.56px;top:276.16px" class="cls_014"><span class="cls_014">Import</span></div>
<div style="position:absolute;left:468.84px;top:283.12px" class="cls_035"><span class="cls_035">service candidate</span></div>
<div style="position:absolute;left:391.38px;top:298.00px" class="cls_014"><span class="cls_014">PO</span></div>
<div style="position:absolute;left:468.84px;top:304.96px" class="cls_035"><span class="cls_035">(currently custom legacy)</span></div>
<div style="position:absolute;left:468.84px;top:328.00px" class="cls_036"><span class="cls_036">service candidate</span></div>
<div style="position:absolute;left:227.34px;top:336.64px" class="cls_014"><span class="cls_014">yes</span></div>
<div style="position:absolute;left:13.21px;top:352.96px" class="cls_035"><span class="cls_035">same as</span></div>
<div style="position:absolute;left:142.75px;top:350.56px" class="cls_014"><span class="cls_014">PO</span></div>
<div style="position:absolute;left:136.56px;top:363.52px" class="cls_014"><span class="cls_014">valid</span></div>
<div style="position:absolute;left:13.21px;top:375.04px" class="cls_035"><span class="cls_035">previous</span></div>
<div style="position:absolute;left:365.25px;top:378.16px" class="cls_014"><span class="cls_014">Send PO</span></div>
<div style="position:absolute;left:473.46px;top:377.20px" class="cls_035"><span class="cls_035">(to accounting clerk's</span></div>
<div style="position:absolute;left:170.71px;top:401.68px" class="cls_014"><span class="cls_014">no</span></div>
<div style="position:absolute;left:366.93px;top:400.24px" class="cls_014"><span class="cls_014">to queue</span></div>
<div style="position:absolute;left:473.46px;top:399.28px" class="cls_035"><span class="cls_035">work queue)</span></div>
<div style="position:absolute;left:473.46px;top:421.36px" class="cls_036"><span class="cls_036">same as previous</span></div>
<div style="position:absolute;left:133.68px;top:453.04px" class="cls_014"><span class="cls_014">Send</span></div>
<div style="position:absolute;left:386.94px;top:459.76px" class="cls_014"><span class="cls_014">stop</span></div>
<div style="position:absolute;left:109.25px;top:474.88px" class="cls_014"><span class="cls_014">notification</span></div>
<div style="position:absolute;left:7.20px;top:503.52px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">45</span></div>
<div style="position:absolute;left:7.20px;top:520.56px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background46.jpg" width=720 height=540></div>
<div style="position:absolute;left:68.68px;top:36.88px" class="cls_037"><span class="cls_037">Step 3: Model candidate services</span></div>
<div style="position:absolute;left:36.00px;top:155.04px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> How to compose services?</span></div>
<div style="position:absolute;left:36.00px;top:228.00px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> Service (candidates) conceptual model</span></div>
<div style="position:absolute;left:72.00px;top:264.88px" class="cls_038"><span class="cls_038">-</span><span class="cls_039"> operations + service contexts</span></div>
<div style="position:absolute;left:72.00px;top:297.04px" class="cls_038"><span class="cls_038">-</span><span class="cls_039"> SO principles</span></div>
<div style="position:absolute;left:36.00px;top:365.04px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> Focus on task- and entity-centred services</span></div>
<div style="position:absolute;left:36.00px;top:438.96px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> Models: BPM, UML use case or class diag.</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">46</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:25300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background47.jpg" width=720 height=540></div>
<div style="position:absolute;left:53.71px;top:39.44px" class="cls_002"><span class="cls_002">Example service operation candidates</span></div>
<div style="position:absolute;left:274.38px;top:148.08px" class="cls_024"><span class="cls_024">Receive PO document</span></div>
<div style="position:absolute;left:111.84px;top:179.28px" class="cls_016"><span class="cls_016">&lt;&lt;include>></span></div>
<div style="position:absolute;left:275.27px;top:218.88px" class="cls_024"><span class="cls_024">Validate PO document</span></div>
<div style="position:absolute;left:54.94px;top:252.24px" class="cls_024"><span class="cls_024">PO processing</span></div>
<div style="position:absolute;left:95.62px;top:279.36px" class="cls_024"><span class="cls_024">service</span></div>
<div style="position:absolute;left:286.50px;top:306.24px" class="cls_016"><span class="cls_016">(If PO document is invalid,)</span></div>
<div style="position:absolute;left:290.50px;top:326.16px" class="cls_016"><span class="cls_016">send rejection notification</span></div>
<div style="position:absolute;left:321.99px;top:346.08px" class="cls_016"><span class="cls_016">(and end process)</span></div>
<div style="position:absolute;left:122.34px;top:358.32px" class="cls_016"><span class="cls_016">&lt;&lt;include>></span></div>
<div style="position:absolute;left:293.83px;top:414.00px" class="cls_016"><span class="cls_016">Transform PO document</span></div>
<div style="position:absolute;left:350.99px;top:433.92px" class="cls_016"><span class="cls_016">into native</span></div>
<div style="position:absolute;left:312.49px;top:453.84px" class="cls_016"><span class="cls_016">electronic PO format</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">47</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:25850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background48.jpg" width=720 height=540></div>
<div style="position:absolute;left:83.50px;top:27.76px" class="cls_005"><span class="cls_005">Example business process logic</span></div>
<div style="position:absolute;left:54.00px;top:232.72px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> Not service operation candidates</span></div>
<div style="position:absolute;left:90.00px;top:279.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> if PO document is valid, proceed with the</span></div>
<div style="position:absolute;left:111.50px;top:312.56px" class="cls_011"><span class="cls_011">transform PO document step</span></div>
<div style="position:absolute;left:90.00px;top:353.60px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> if the PO document is invalid, end process</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">48</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:26400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background49.jpg" width=720 height=540></div>
<div style="position:absolute;left:45.12px;top:36.88px" class="cls_005"><span class="cls_005">Task- versus entity-centred services</span></div>
<div style="position:absolute;left:54.00px;top:174.72px" class="cls_024"><span class="cls_024">•</span><span class="cls_019"> Task-centred</span></div>
<div style="position:absolute;left:370.75px;top:174.72px" class="cls_024"><span class="cls_024">•</span><span class="cls_019"> Entity-centred</span></div>
<div style="position:absolute;left:90.00px;top:209.68px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> (+) direct mapping of</span></div>
<div style="position:absolute;left:406.75px;top:209.68px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> (+) agility</span></div>
<div style="position:absolute;left:111.50px;top:233.68px" class="cls_015"><span class="cls_015">business requirements</span></div>
<div style="position:absolute;left:406.75px;top:238.72px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> (-) upfront analysis</span></div>
<div style="position:absolute;left:90.00px;top:262.72px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> (-) dependent on specific</span></div>
<div style="position:absolute;left:406.75px;top:267.52px" class="cls_014"><span class="cls_014">-</span><span class="cls_015"> (-) dependent on controllers</span></div>
<div style="position:absolute;left:111.50px;top:286.72px" class="cls_015"><span class="cls_015">process</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">49</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:26950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background50.jpg" width=720 height=540></div>
<div style="position:absolute;left:72.27px;top:27.76px" class="cls_005"><span class="cls_005">Benefits of business-centric SOA</span></div>
<div style="position:absolute;left:54.00px;top:185.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> introduce agility</span></div>
<div style="position:absolute;left:54.00px;top:279.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> prepare for orchestration</span></div>
<div style="position:absolute;left:54.00px;top:371.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> enable reuse</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">50</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:27500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background51.jpg" width=720 height=540></div>
<div style="position:absolute;left:67.03px;top:6.08px" class="cls_002"><span class="cls_002">Service-oriented design: design sub-</span></div>
<div style="position:absolute;left:317.85px;top:54.08px" class="cls_002"><span class="cls_002">steps</span></div>
<div style="position:absolute;left:62.88px;top:90.40px" class="cls_032"><span class="cls_032">Service oriented</span></div>
<div style="position:absolute;left:331.96px;top:110.80px" class="cls_028"><span class="cls_028">step 1</span></div>
<div style="position:absolute;left:104.50px;top:115.60px" class="cls_032"><span class="cls_032">analysis</span></div>
<div style="position:absolute;left:407.50px;top:150.40px" class="cls_021"><span class="cls_021">Compose SOA</span></div>
<div style="position:absolute;left:331.96px;top:181.60px" class="cls_028"><span class="cls_028">step 2</span></div>
<div style="position:absolute;left:62.88px;top:192.40px" class="cls_021"><span class="cls_021">Service oriented</span></div>
<div style="position:absolute;left:111.93px;top:217.60px" class="cls_021"><span class="cls_021">design</span></div>
<div style="position:absolute;left:370.93px;top:215.92px" class="cls_021"><span class="cls_021">Design entity-centric</span></div>
<div style="position:absolute;left:391.75px;top:240.88px" class="cls_021"><span class="cls_021">business services</span></div>
<div style="position:absolute;left:331.96px;top:260.80px" class="cls_028"><span class="cls_028">step 3</span></div>
<div style="position:absolute;left:330.30px;top:302.32px" class="cls_021"><span class="cls_021">Design infrastructure services</span></div>
<div style="position:absolute;left:331.96px;top:334.72px" class="cls_028"><span class="cls_028">step 4</span></div>
<div style="position:absolute;left:382.42px;top:370.24px" class="cls_021"><span class="cls_021">Design task-centric</span></div>
<div style="position:absolute;left:391.75px;top:395.20px" class="cls_021"><span class="cls_021">business services</span></div>
<div style="position:absolute;left:331.96px;top:419.68px" class="cls_028"><span class="cls_028">step 5</span></div>
<div style="position:absolute;left:422.94px;top:455.20px" class="cls_021"><span class="cls_021">Design SO</span></div>
<div style="position:absolute;left:394.06px;top:480.16px" class="cls_021"><span class="cls_021">business process</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">56</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:28050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background52.jpg" width=720 height=540></div>
<div style="position:absolute;left:83.00px;top:27.76px" class="cls_005"><span class="cls_005">Entity-centric business services</span></div>
<div style="position:absolute;left:108.88px;top:109.20px" class="cls_024"><span class="cls_024">•</span><span class="cls_019"> Goal: entity-centric business service layer + parent</span></div>
<div style="position:absolute;left:126.88px;top:138.24px" class="cls_019"><span class="cls_019">orchestration layer</span></div>
<div style="position:absolute;left:69.19px;top:198.64px" class="cls_040"><span class="cls_040">Invoice</span></div>
<div style="position:absolute;left:253.59px;top:200.08px" class="cls_041"><span class="cls_041">1</span></div>
<div style="position:absolute;left:292.81px;top:198.64px" class="cls_040"><span class="cls_040">PO</span></div>
<div style="position:absolute;left:138.46px;top:203.68px" class="cls_041"><span class="cls_041">*</span></div>
<div style="position:absolute;left:538.31px;top:206.40px" class="cls_004"><span class="cls_004">Receive PO document</span></div>
<div style="position:absolute;left:138.21px;top:222.88px" class="cls_041"><span class="cls_041">*</span></div>
<div style="position:absolute;left:427.21px;top:224.64px" class="cls_004"><span class="cls_004">&lt;&lt;include>></span></div>
<div style="position:absolute;left:58.09px;top:232.16px" class="cls_042"><span class="cls_042">Customer</span></div>
<div style="position:absolute;left:253.09px;top:227.44px" class="cls_041"><span class="cls_041">*</span></div>
<div style="position:absolute;left:58.09px;top:243.20px" class="cls_042"><span class="cls_042">Hours billed</span></div>
<div style="position:absolute;left:538.76px;top:251.76px" class="cls_004"><span class="cls_004">Validate PO document</span></div>
<div style="position:absolute;left:256.09px;top:253.60px" class="cls_041"><span class="cls_041">*</span></div>
<div style="position:absolute;left:140.09px;top:256.96px" class="cls_041"><span class="cls_041">1</span></div>
<div style="position:absolute;left:312.34px;top:269.20px" class="cls_041"><span class="cls_041">*</span></div>
<div style="position:absolute;left:398.75px;top:275.04px" class="cls_004"><span class="cls_004">PO processing</span></div>
<div style="position:absolute;left:293.71px;top:277.36px" class="cls_041"><span class="cls_041">1</span></div>
<div style="position:absolute;left:419.06px;top:288.00px" class="cls_004"><span class="cls_004">service</span></div>
<div style="position:absolute;left:140.21px;top:294.88px" class="cls_041"><span class="cls_041">1</span></div>
<div style="position:absolute;left:250.96px;top:297.52px" class="cls_041"><span class="cls_041">1</span></div>
<div style="position:absolute;left:271.88px;top:298.00px" class="cls_040"><span class="cls_040">Employee</span></div>
<div style="position:absolute;left:73.50px;top:304.72px" class="cls_040"><span class="cls_040">Order</span></div>
<div style="position:absolute;left:526.25px;top:304.32px" class="cls_004"><span class="cls_004">(If PO document is invalid,)</span></div>
<div style="position:absolute;left:529.07px;top:317.28px" class="cls_004"><span class="cls_004">send rejection notification</span></div>
<div style="position:absolute;left:140.21px;top:321.76px" class="cls_041"><span class="cls_041">*</span></div>
<div style="position:absolute;left:271.34px;top:332.48px" class="cls_042"><span class="cls_042">Email</span></div>
<div style="position:absolute;left:550.00px;top:330.24px" class="cls_004"><span class="cls_004">(and end process)</span></div>
<div style="position:absolute;left:271.34px;top:343.52px" class="cls_042"><span class="cls_042">Weekly hours</span></div>
<div style="position:absolute;left:433.71px;top:339.36px" class="cls_004"><span class="cls_004">&lt;&lt;include>></span></div>
<div style="position:absolute;left:293.71px;top:364.48px" class="cls_041"><span class="cls_041">1</span></div>
<div style="position:absolute;left:531.10px;top:373.44px" class="cls_004"><span class="cls_004">Transform PO document</span></div>
<div style="position:absolute;left:569.25px;top:386.40px" class="cls_004"><span class="cls_004">into native</span></div>
<div style="position:absolute;left:312.34px;top:387.28px" class="cls_041"><span class="cls_041">*</span></div>
<div style="position:absolute;left:254.46px;top:399.52px" class="cls_041"><span class="cls_041">1</span></div>
<div style="position:absolute;left:272.69px;top:399.28px" class="cls_040"><span class="cls_040">Customer</span></div>
<div style="position:absolute;left:543.56px;top:399.36px" class="cls_004"><span class="cls_004">electronic PO format</span></div>
<div style="position:absolute;left:254.34px;top:435.04px" class="cls_041"><span class="cls_041">1</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">57</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:28600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background53.jpg" width=720 height=540></div>
<div style="position:absolute;left:160.66px;top:27.76px" class="cls_005"><span class="cls_005">Infrastructure services</span></div>
<div style="position:absolute;left:205.87px;top:145.52px" class="cls_044"><span class="cls_044">PO</span></div>
<div style="position:absolute;left:178.81px;top:161.60px" class="cls_044"><span class="cls_044">processing</span></div>
<div style="position:absolute;left:192.12px;top:178.64px" class="cls_044"><span class="cls_044">service</span></div>
<div style="position:absolute;left:466.40px;top:193.44px" class="cls_018"><span class="cls_018">Orchestration/coordination</span></div>
<div style="position:absolute;left:663.76px;top:215.52px" class="cls_018"><span class="cls_018">layer</span></div>
<div style="position:absolute;left:83.68px;top:264.24px" class="cls_026"><span class="cls_026">Verify</span></div>
<div style="position:absolute;left:200.18px;top:272.88px" class="cls_026"><span class="cls_026">PO</span></div>
<div style="position:absolute;left:93.18px;top:281.28px" class="cls_026"><span class="cls_026">PO</span></div>
<div style="position:absolute;left:184.69px;top:289.92px" class="cls_026"><span class="cls_026">service</span></div>
<div style="position:absolute;left:77.69px;top:299.28px" class="cls_026"><span class="cls_026">service</span></div>
<div style="position:absolute;left:508.51px;top:315.84px" class="cls_018"><span class="cls_018">Business service layer</span></div>
<div style="position:absolute;left:513.15px;top:366.96px" class="cls_018"><span class="cls_018">Infrastructure service</span></div>
<div style="position:absolute;left:369.61px;top:379.52px" class="cls_044"><span class="cls_044">Transform</span></div>
<div style="position:absolute;left:262.81px;top:384.32px" class="cls_044"><span class="cls_044">Notification</span></div>
<div style="position:absolute;left:663.76px;top:388.80px" class="cls_018"><span class="cls_018">layer</span></div>
<div style="position:absolute;left:380.50px;top:395.60px" class="cls_044"><span class="cls_044">service</span></div>
<div style="position:absolute;left:277.00px;top:400.16px" class="cls_044"><span class="cls_044">service</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">58</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:29150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background54.jpg" width=720 height=540></div>
<div style="position:absolute;left:96.99px;top:27.76px" class="cls_005"><span class="cls_005">Task-centric business services</span></div>
<div style="position:absolute;left:31.13px;top:125.20px" class="cls_006"><span class="cls_006">•</span><span class="cls_003"> UML sequence diagram</span></div>
<div style="position:absolute;left:67.13px;top:172.16px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> express and refine order of invocations implicit in</span></div>
<div style="position:absolute;left:88.63px;top:206.24px" class="cls_011"><span class="cls_011">the UML use case diagram</span></div>
<div style="position:absolute;left:411.61px;top:261.04px" class="cls_014"><span class="cls_014">Verify PO</span></div>
<div style="position:absolute;left:547.13px;top:261.04px" class="cls_014"><span class="cls_014">PO</span></div>
<div style="position:absolute;left:616.94px;top:261.04px" class="cls_014"><span class="cls_014">Notification</span></div>
<div style="position:absolute;left:186.48px;top:276.24px" class="cls_004"><span class="cls_004">Receive PO document</span></div>
<div style="position:absolute;left:422.13px;top:283.12px" class="cls_014"><span class="cls_014">service</span></div>
<div style="position:absolute;left:529.88px;top:283.12px" class="cls_014"><span class="cls_014">service</span></div>
<div style="position:absolute;left:634.75px;top:283.12px" class="cls_014"><span class="cls_014">service</span></div>
<div style="position:absolute;left:74.95px;top:294.48px" class="cls_004"><span class="cls_004">&lt;&lt;include>></span></div>
<div style="position:absolute;left:472.71px;top:310.32px" class="cls_016"><span class="cls_016">get_PO</span></div>
<div style="position:absolute;left:186.93px;top:321.60px" class="cls_004"><span class="cls_004">Validate PO document</span></div>
<div style="position:absolute;left:474.84px;top:335.52px" class="cls_016"><span class="cls_016">[PO data]</span></div>
<div style="position:absolute;left:46.53px;top:344.88px" class="cls_004"><span class="cls_004">PO processing</span></div>
<div style="position:absolute;left:66.84px;top:357.84px" class="cls_004"><span class="cls_004">service</span></div>
<div style="position:absolute;left:472.71px;top:364.32px" class="cls_016"><span class="cls_016">verify</span></div>
<div style="position:absolute;left:174.42px;top:374.16px" class="cls_004"><span class="cls_004">(If PO document is invalid,)</span></div>
<div style="position:absolute;left:177.23px;top:387.12px" class="cls_004"><span class="cls_004">send rejection notification</span></div>
<div style="position:absolute;left:198.16px;top:400.32px" class="cls_004"><span class="cls_004">(and end process)</span></div>
<div style="position:absolute;left:81.46px;top:409.20px" class="cls_004"><span class="cls_004">&lt;&lt;include>></span></div>
<div style="position:absolute;left:461.34px;top:423.84px" class="cls_016"><span class="cls_016">send_reject</span></div>
<div style="position:absolute;left:179.27px;top:443.28px" class="cls_004"><span class="cls_004">Transform PO document</span></div>
<div style="position:absolute;left:217.42px;top:456.24px" class="cls_004"><span class="cls_004">into native</span></div>
<div style="position:absolute;left:191.73px;top:469.20px" class="cls_004"><span class="cls_004">electronic PO format</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">59</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:29700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture19/background55.jpg" width=720 height=540></div>
<div style="position:absolute;left:274.89px;top:37.12px" class="cls_005"><span class="cls_005">Summary</span></div>
<div style="position:absolute;left:43.20px;top:121.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> Services have a long history (telephony)</span></div>
<div style="position:absolute;left:43.20px;top:157.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> Most important characteristic: dynamic discovery</span></div>
<div style="position:absolute;left:70.20px;top:185.52px" class="cls_009"><span class="cls_009">of services</span></div>
<div style="position:absolute;left:43.20px;top:221.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> SOA as architectural style</span></div>
<div style="position:absolute;left:43.20px;top:257.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> Today’s Web services mostly syntax-based</span></div>
<div style="position:absolute;left:43.20px;top:294.48px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> Key design decisions in SOSE concern service</span></div>
<div style="position:absolute;left:70.20px;top:323.52px" class="cls_009"><span class="cls_009">layering, industry standards, and relevant SO</span></div>
<div style="position:absolute;left:70.20px;top:353.52px" class="cls_009"><span class="cls_009">principles</span></div>
<div style="position:absolute;left:43.20px;top:389.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_009"> SOSE differentiates from traditional life cycles</span></div>
<div style="position:absolute;left:70.20px;top:418.56px" class="cls_009"><span class="cls_009">mainly in the analysis and design phases</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_007"><span class="cls_007">60</span></div>
</div>

</body>
</html>
