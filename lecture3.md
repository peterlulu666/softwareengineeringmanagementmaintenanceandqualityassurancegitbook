<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture3/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:156.94px;top:34.96px" class="cls_002"><span class="cls_002">Software Life Cycle</span></div>
<div style="position:absolute;left:75.20px;top:366.04px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:75.20px;top:408.04px" class="cls_004"><span class="cls_004">§</span><span class="cls_003"> Discussion of different life cycle models</span></div>
<div style="position:absolute;left:75.20px;top:450.04px" class="cls_004"><span class="cls_004">§</span><span class="cls_003"> Maintenance or evolution</span></div>
<div style="position:absolute;left:7.20px;top:503.52px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:520.56px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Not this life cycle</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_008"><span class="cls_008">2</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Introduction</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> software development projects are large and</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">complex</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> a phased approach to control it is necessary</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> traditional models are document-driven: there is a</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_010"><span class="cls_010">new pile of paper after each phase is completed</span></div>
<div style="position:absolute;left:61.20px;top:306.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> evolutionary models recognize that much of what</span></div>
<div style="position:absolute;left:88.20px;top:334.32px" class="cls_010"><span class="cls_010">is called maintenance is inevitable</span></div>
<div style="position:absolute;left:61.20px;top:369.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> latest fashion: agile methods, eXtreme</span></div>
<div style="position:absolute;left:88.20px;top:398.16px" class="cls_010"><span class="cls_010">Programming</span></div>
<div style="position:absolute;left:61.20px;top:432.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> life cycle models can be explicitly modeled, in a</span></div>
<div style="position:absolute;left:88.20px;top:461.28px" class="cls_010"><span class="cls_010">process modeling language</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_008"><span class="cls_008">3</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Simple life cycle model</span></div>
<div style="position:absolute;left:317.33px;top:177.68px" class="cls_011"><span class="cls_011">problem</span></div>
<div style="position:absolute;left:367.20px;top:207.68px" class="cls_011"><span class="cls_011">requirements engineering</span></div>
<div style="position:absolute;left:284.08px;top:237.68px" class="cls_011"><span class="cls_011">reqs specification</span></div>
<div style="position:absolute;left:372.70px;top:267.68px" class="cls_011"><span class="cls_011">design</span></div>
<div style="position:absolute;left:322.95px;top:297.68px" class="cls_011"><span class="cls_011">design</span></div>
<div style="position:absolute;left:367.20px;top:327.68px" class="cls_011"><span class="cls_011">implementation</span></div>
<div style="position:absolute;left:317.33px;top:357.68px" class="cls_011"><span class="cls_011">system</span></div>
<div style="position:absolute;left:367.20px;top:387.68px" class="cls_011"><span class="cls_011">testing</span></div>
<div style="position:absolute;left:289.70px;top:417.68px" class="cls_011"><span class="cls_011">working system</span></div>
<div style="position:absolute;left:367.20px;top:447.68px" class="cls_011"><span class="cls_011">maintenance</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_008"><span class="cls_008">4</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Point to ponder #1</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Why does the model look like this?</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Is this how we go about?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_008"><span class="cls_008">5</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Simple Life Cycle Model</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> document driven, planning driven, heavyweight</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> milestones are reached if the appropriate</span></div>
<div style="position:absolute;left:88.20px;top:242.16px" class="cls_010"><span class="cls_010">documentation is delivered (e.g., requirements</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_010"><span class="cls_010">specification, design specification, program, test</span></div>
<div style="position:absolute;left:88.20px;top:300.24px" class="cls_010"><span class="cls_010">document)</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> much planning upfront, often heavy contracts are</span></div>
<div style="position:absolute;left:88.20px;top:363.12px" class="cls_010"><span class="cls_010">signed</span></div>
<div style="position:absolute;left:61.20px;top:398.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> problems</span></div>
<div style="position:absolute;left:97.20px;top:431.20px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> feedback is not taken into account</span></div>
<div style="position:absolute;left:97.20px;top:460.24px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> maintenance does not imply evolution</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_008"><span class="cls_008">6</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Waterfall Model</span></div>
<div style="position:absolute;left:168.25px;top:159.68px" class="cls_014"><span class="cls_014">reqs engineering</span></div>
<div style="position:absolute;left:204.50px;top:180.56px" class="cls_014"><span class="cls_014">V & V</span></div>
<div style="position:absolute;left:287.38px;top:225.68px" class="cls_014"><span class="cls_014">design</span></div>
<div style="position:absolute;left:293.13px;top:249.68px" class="cls_014"><span class="cls_014">V & V</span></div>
<div style="position:absolute;left:334.81px;top:297.68px" class="cls_014"><span class="cls_014">implementation</span></div>
<div style="position:absolute;left:365.13px;top:321.68px" class="cls_014"><span class="cls_014">V & V</span></div>
<div style="position:absolute;left:442.38px;top:369.68px" class="cls_014"><span class="cls_014">testing</span></div>
<div style="position:absolute;left:448.25px;top:390.32px" class="cls_014"><span class="cls_014">V & V</span></div>
<div style="position:absolute;left:501.63px;top:435.68px" class="cls_014"><span class="cls_014">maintenance</span></div>
<div style="position:absolute;left:525.75px;top:459.68px" class="cls_014"><span class="cls_014">V & V</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_008"><span class="cls_008">7</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Another waterfall model</span></div>
<div style="position:absolute;left:517.06px;top:211.68px" class="cls_015"><span class="cls_015">testing</span></div>
<div style="position:absolute;left:219.88px;top:255.60px" class="cls_015"><span class="cls_015">feedback</span></div>
<div style="position:absolute;left:461.75px;top:297.60px" class="cls_015"><span class="cls_015">implementation</span></div>
<div style="position:absolute;left:530.81px;top:339.60px" class="cls_015"><span class="cls_015">design</span></div>
<div style="position:absolute;left:523.25px;top:387.60px" class="cls_015"><span class="cls_015">requirements</span></div>
<div style="position:absolute;left:528.75px;top:408.48px" class="cls_015"><span class="cls_015">engineering</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_008"><span class="cls_008">8</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">V-Model</span></div>
<div style="position:absolute;left:612.56px;top:166.64px" class="cls_014"><span class="cls_014">acceptance</span></div>
<div style="position:absolute;left:57.06px;top:174.24px" class="cls_015"><span class="cls_015">reqs eng</span></div>
<div style="position:absolute;left:629.87px;top:189.68px" class="cls_014"><span class="cls_014">testing</span></div>
<div style="position:absolute;left:526.38px;top:251.84px" class="cls_014"><span class="cls_014">integration</span></div>
<div style="position:absolute;left:136.44px;top:261.12px" class="cls_015"><span class="cls_015">global design</span></div>
<div style="position:absolute;left:540.13px;top:274.64px" class="cls_014"><span class="cls_014">testing</span></div>
<div style="position:absolute;left:242.81px;top:348.00px" class="cls_015"><span class="cls_015">det. design</span></div>
<div style="position:absolute;left:430.44px;top:347.04px" class="cls_015"><span class="cls_015">unit testing</span></div>
<div style="position:absolute;left:357.56px;top:432.00px" class="cls_015"><span class="cls_015">coding</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_008"><span class="cls_008">9</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:63.20px;top:54.08px" class="cls_007"><span class="cls_007">Waterfall Model (cntd)</span></div>
<div style="position:absolute;left:61.20px;top:154.56px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> includes iteration and feedback</span></div>
<div style="position:absolute;left:61.20px;top:188.64px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> validation (</span><span class="cls_016">are we building the right system</span><span class="cls_010">?) and</span></div>
<div style="position:absolute;left:88.20px;top:217.68px" class="cls_010"><span class="cls_010">verification (</span><span class="cls_016">are we building the system right</span><span class="cls_010">?)</span></div>
<div style="position:absolute;left:88.20px;top:246.48px" class="cls_010"><span class="cls_010">after each step</span></div>
<div style="position:absolute;left:61.20px;top:281.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> user requirements are fixed as early as possible</span></div>
<div style="position:absolute;left:61.20px;top:315.60px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> problems</span></div>
<div style="position:absolute;left:97.20px;top:349.60px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> too rigid</span></div>
<div style="position:absolute;left:97.20px;top:378.64px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> developers cannot move between various abstraction levels</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">10</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Activity versus phase</span></div>
<div style="position:absolute;left:452.06px;top:153.60px" class="cls_015"><span class="cls_015">Integration   Acceptance</span></div>
<div style="position:absolute;left:148.75px;top:165.60px" class="cls_017"><span class="cls_017">Phase</span></div>
<div style="position:absolute;left:237.19px;top:164.40px" class="cls_015"><span class="cls_015">Design   Implementation</span></div>
<div style="position:absolute;left:468.06px;top:174.48px" class="cls_015"><span class="cls_015">testing</span></div>
<div style="position:absolute;left:577.43px;top:174.48px" class="cls_015"><span class="cls_015">testing</span></div>
<div style="position:absolute;left:98.63px;top:177.60px" class="cls_017"><span class="cls_017">Activity</span></div>
<div style="position:absolute;left:83.69px;top:231.60px" class="cls_015"><span class="cls_015">Integration</span></div>
<div style="position:absolute;left:255.19px;top:236.64px" class="cls_010"><span class="cls_010">4.7</span></div>
<div style="position:absolute;left:352.31px;top:236.64px" class="cls_010"><span class="cls_010">43.4</span></div>
<div style="position:absolute;left:464.38px;top:236.64px" class="cls_010"><span class="cls_010">26.1</span></div>
<div style="position:absolute;left:575.38px;top:236.64px" class="cls_010"><span class="cls_010">25.8</span></div>
<div style="position:absolute;left:99.68px;top:252.48px" class="cls_015"><span class="cls_015">testing</span></div>
<div style="position:absolute;left:64.75px;top:327.60px" class="cls_015"><span class="cls_015">Implementation</span></div>
<div style="position:absolute;left:255.19px;top:331.20px" class="cls_010"><span class="cls_010">6.9</span></div>
<div style="position:absolute;left:352.31px;top:331.20px" class="cls_010"><span class="cls_010">70.3</span></div>
<div style="position:absolute;left:464.38px;top:331.20px" class="cls_010"><span class="cls_010">15.9</span></div>
<div style="position:absolute;left:582.00px;top:331.20px" class="cls_010"><span class="cls_010">6.9</span></div>
<div style="position:absolute;left:68.25px;top:348.48px" class="cls_015"><span class="cls_015">(& unit testing)</span></div>
<div style="position:absolute;left:248.63px;top:413.52px" class="cls_010"><span class="cls_010">49.2</span></div>
<div style="position:absolute;left:352.31px;top:413.52px" class="cls_010"><span class="cls_010">34.1</span></div>
<div style="position:absolute;left:464.50px;top:413.52px" class="cls_010"><span class="cls_010">10.3</span></div>
<div style="position:absolute;left:582.00px;top:413.52px" class="cls_010"><span class="cls_010">6.4</span></div>
<div style="position:absolute;left:98.25px;top:423.60px" class="cls_015"><span class="cls_015">Design</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">11</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Lightweight (agile) approaches</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> prototyping</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> incremental development</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> RAD, DSDM</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> XP</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">12</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">The Agile Manifesto</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Individuals and interactions over processes and</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">tools</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Working software over comprehensive</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_010"><span class="cls_010">documentation</span></div>
<div style="position:absolute;left:61.20px;top:306.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Customer collaboration over contract negotiation</span></div>
<div style="position:absolute;left:61.20px;top:340.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Responding to change over following a plan</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">13</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Prototyping</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> requirements elicitation is difficult</span></div>
<div style="position:absolute;left:97.20px;top:178.24px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> software is developed because the present situation is</span></div>
<div style="position:absolute;left:119.70px;top:202.24px" class="cls_013"><span class="cls_013">unsatisfactory</span></div>
<div style="position:absolute;left:97.20px;top:231.28px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> however, the desirable new situation is as yet unknown</span></div>
<div style="position:absolute;left:61.20px;top:261.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> prototyping is used to obtain the requirements of</span></div>
<div style="position:absolute;left:88.20px;top:289.20px" class="cls_010"><span class="cls_010">some aspects of the system</span></div>
<div style="position:absolute;left:61.20px;top:324.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> prototyping should be a relatively cheap process</span></div>
<div style="position:absolute;left:97.20px;top:358.24px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> use rapid prototyping languages and tools</span></div>
<div style="position:absolute;left:97.20px;top:386.32px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> not all functionality needs to be implemented</span></div>
<div style="position:absolute;left:97.20px;top:415.12px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> production quality is not required</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">14</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:22.40px" class="cls_007"><span class="cls_007">Prototyping as a tool for</span></div>
<div style="position:absolute;left:61.20px;top:56.48px" class="cls_007"><span class="cls_007">requirements engineering</span></div>
<div style="position:absolute;left:173.25px;top:171.60px" class="cls_015"><span class="cls_015">reqs engineering</span></div>
<div style="position:absolute;left:209.56px;top:237.60px" class="cls_015"><span class="cls_015">design</span></div>
<div style="position:absolute;left:486.50px;top:237.60px" class="cls_015"><span class="cls_015">design</span></div>
<div style="position:absolute;left:179.25px;top:309.60px" class="cls_015"><span class="cls_015">implementation</span></div>
<div style="position:absolute;left:445.13px;top:309.60px" class="cls_015"><span class="cls_015">implementation</span></div>
<div style="position:absolute;left:209.56px;top:381.60px" class="cls_015"><span class="cls_015">testing</span></div>
<div style="position:absolute;left:486.50px;top:381.60px" class="cls_015"><span class="cls_015">testing</span></div>
<div style="position:absolute;left:456.88px;top:453.60px" class="cls_015"><span class="cls_015">maintenance</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">15</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Prototyping (cntd)</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> throwaway prototyping</span><span class="cls_010">: the n-th prototype is</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">followed by a waterfall-like  process (as depicted</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_010"><span class="cls_010">on previous slide)</span></div>
<div style="position:absolute;left:61.20px;top:306.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> evolutionary prototyping</span><span class="cls_010">: the nth prototype is</span></div>
<div style="position:absolute;left:88.20px;top:334.32px" class="cls_010"><span class="cls_010">delivered</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">16</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Point to ponder #2</span></div>
<div style="position:absolute;left:68.87px;top:249.60px" class="cls_010"><span class="cls_010">What are the pros and cons of the two approaches?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">17</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Prototyping, advantages</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The resulting system is easier to use</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> User needs are better accommodated</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The resulting system has fewer features</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Problems are detected earlier</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The design is of higher quality</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The resulting system is easier to maintain</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The development incurs less effort</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">18</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Prototyping, disadvantages</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The resulting system has more features</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The performance of the resulting system is worse</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The design is of less quality</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The resulting system is harder to maintain</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The prototyping approach requires more</span></div>
<div style="position:absolute;left:88.20px;top:346.32px" class="cls_010"><span class="cls_010">experienced team members</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">19</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Prototyping, recommendations</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> the users and the designers must be well aware of</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">the issues and the pitfalls</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> use prototyping when the requirements are</span></div>
<div style="position:absolute;left:88.20px;top:306.24px" class="cls_010"><span class="cls_010">unclear</span></div>
<div style="position:absolute;left:61.20px;top:375.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> prototyping needs to be planned and controlled</span></div>
<div style="position:absolute;left:88.20px;top:404.16px" class="cls_010"><span class="cls_010">as well</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">20</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Incremental Development</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> a software system is delivered in small</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">increments, thereby avoiding the Big Bang effect</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> the waterfall model is employed in each phase</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> the user is closely involved in directing the next</span></div>
<div style="position:absolute;left:88.20px;top:340.32px" class="cls_010"><span class="cls_010">steps</span></div>
<div style="position:absolute;left:61.20px;top:409.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> incremental development prevents</span></div>
<div style="position:absolute;left:88.20px;top:438.24px" class="cls_010"><span class="cls_010">overfunctionality</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">21</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">RAD: Rapid Application Development</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> evolutionary development, with </span><span class="cls_016">time boxes</span><span class="cls_010">: fixed</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">time frames within which activities are done;</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> time frame is decided upon first, then one tries to</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_010"><span class="cls_010">realize as much as possible within that time</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_010"><span class="cls_010">frame;</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> other elements: Joint Requirements Planning</span></div>
<div style="position:absolute;left:88.20px;top:329.28px" class="cls_010"><span class="cls_010">(JRD) and Joint Application Design (JAD),</span></div>
<div style="position:absolute;left:88.20px;top:357.12px" class="cls_010"><span class="cls_010">workshops in which users participate;</span></div>
<div style="position:absolute;left:61.20px;top:392.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> requirements prioritization through a </span><span class="cls_016">triage</span><span class="cls_010">;</span></div>
<div style="position:absolute;left:61.20px;top:427.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> development in a SWAT team: Skilled Workers</span></div>
<div style="position:absolute;left:88.20px;top:455.28px" class="cls_010"><span class="cls_010">with Advanced Tools</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">22</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">DSDM</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Dynamic Systems Development Method, #1 RAD</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">framework in UK</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Fundamental idea: fix time and resources</span></div>
<div style="position:absolute;left:88.20px;top:306.24px" class="cls_018"><span class="cls_018">(</span><span class="cls_016">timebox</span><span class="cls_018">),</span><span class="cls_010"> adjust functionality accordingly</span></div>
<div style="position:absolute;left:61.20px;top:375.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> One needs to be a member of the DSDM</span></div>
<div style="position:absolute;left:88.20px;top:404.16px" class="cls_010"><span class="cls_010">consortium</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">23</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">DSDM phases</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Feasibility: delivers feasibility report and outline</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">plan, optionally fast prototype (few weeks)</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Business study: analyze characteristics of</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_010"><span class="cls_010">business and technology (in workshops), delivers</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_010"><span class="cls_010">a.o. System Architecture Definition</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Functional model iteration: timeboxed iterative,</span></div>
<div style="position:absolute;left:88.20px;top:329.28px" class="cls_010"><span class="cls_010">incremental phase, yields requirements</span></div>
<div style="position:absolute;left:61.20px;top:363.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Design and build iteration</span></div>
<div style="position:absolute;left:61.20px;top:398.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Implementation: transfer to production</span></div>
<div style="position:absolute;left:88.20px;top:427.20px" class="cls_010"><span class="cls_010">environment</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">24</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">DSDM practices</span></div>
<div style="position:absolute;left:61.20px;top:141.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Active user involvement is imperative</span></div>
<div style="position:absolute;left:61.20px;top:173.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Empowered teams</span></div>
<div style="position:absolute;left:61.20px;top:205.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Frequent delivery of products</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Acceptance determined by fitness for business</span></div>
<div style="position:absolute;left:88.20px;top:262.32px" class="cls_010"><span class="cls_010">purpose</span></div>
<div style="position:absolute;left:61.20px;top:294.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Iterative, incremental development</span></div>
<div style="position:absolute;left:61.20px;top:326.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> All changes are reversible</span></div>
<div style="position:absolute;left:61.20px;top:357.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Requirements baselined at high level</span></div>
<div style="position:absolute;left:61.20px;top:389.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Testing integrated in life cycle</span></div>
<div style="position:absolute;left:61.20px;top:421.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Collaborative, cooperative approach shared by all</span></div>
<div style="position:absolute;left:88.20px;top:447.12px" class="cls_010"><span class="cls_010">stakeholders is essential</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">25</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">XP - eXtreme Programming</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Everything is done in small steps</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> The system always compiles, always runs</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Client as the center of development team</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Developers have same responsibility w.r.t.</span></div>
<div style="position:absolute;left:88.20px;top:415.20px" class="cls_010"><span class="cls_010">software and methodology</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">26</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">13 practices of XP</span></div>
<div style="position:absolute;left:61.20px;top:144.32px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Whole team: client part</span></div>
<div style="position:absolute;left:375.95px;top:144.32px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Test-driven</span></div>
<div style="position:absolute;left:88.20px;top:171.20px" class="cls_020"><span class="cls_020">of the team</span></div>
<div style="position:absolute;left:402.95px;top:171.20px" class="cls_020"><span class="cls_020">development: tests</span></div>
<div style="position:absolute;left:402.95px;top:197.12px" class="cls_020"><span class="cls_020">developed first</span></div>
<div style="position:absolute;left:61.20px;top:202.16px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Metaphor: common</span></div>
<div style="position:absolute;left:88.20px;top:229.28px" class="cls_020"><span class="cls_020">analogy for the system</span></div>
<div style="position:absolute;left:375.95px;top:229.28px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Design improvement</span></div>
<div style="position:absolute;left:402.95px;top:255.20px" class="cls_020"><span class="cls_020">(refactoring)</span></div>
<div style="position:absolute;left:61.20px;top:261.20px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> The planning game,</span></div>
<div style="position:absolute;left:88.20px;top:287.12px" class="cls_020"><span class="cls_020">based on user stories</span></div>
<div style="position:absolute;left:375.95px;top:287.12px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Collective code</span></div>
<div style="position:absolute;left:402.95px;top:313.28px" class="cls_020"><span class="cls_020">ownership</span></div>
<div style="position:absolute;left:61.20px;top:319.28px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Simple design</span></div>
<div style="position:absolute;left:375.95px;top:345.20px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Continuous integration:</span></div>
<div style="position:absolute;left:61.20px;top:350.24px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Small releases (e.g. 2</span></div>
<div style="position:absolute;left:402.95px;top:371.12px" class="cls_020"><span class="cls_020">system always runs</span></div>
<div style="position:absolute;left:88.20px;top:377.12px" class="cls_020"><span class="cls_020">weeks)</span></div>
<div style="position:absolute;left:375.95px;top:403.28px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Sustainable pace: no</span></div>
<div style="position:absolute;left:61.20px;top:408.32px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Customer tests</span></div>
<div style="position:absolute;left:402.95px;top:430.16px" class="cls_020"><span class="cls_020">overtime</span></div>
<div style="position:absolute;left:61.20px;top:440.24px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Pair programming</span></div>
<div style="position:absolute;left:375.95px;top:461.12px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Coding standards</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">27</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">RUP</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Rational Unified Process</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Complement to UML (Unified Modeling Language)</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Iterative approach for object-oriented systems,</span></div>
<div style="position:absolute;left:88.20px;top:311.28px" class="cls_010"><span class="cls_010">strongly embraces use cases for modeling</span></div>
<div style="position:absolute;left:88.20px;top:340.32px" class="cls_010"><span class="cls_010">requirements</span></div>
<div style="position:absolute;left:61.20px;top:409.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Tool-supported (UML-tools, ClearCase)</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">28</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">RUP phases</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Inception</span><span class="cls_010">: establish scope, boundaries, critical</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">use cases, candidate architectures, schedule and</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_010"><span class="cls_010">cost estimates</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Elaboration</span><span class="cls_010">: foundation of architecture, establish</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_010"><span class="cls_010">tool support, get al use cases</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Construction</span><span class="cls_010">: manufactoring process, one or</span></div>
<div style="position:absolute;left:88.20px;top:329.28px" class="cls_010"><span class="cls_010">more releases</span></div>
<div style="position:absolute;left:61.20px;top:363.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Transition</span><span class="cls_010">: release to user community, often</span></div>
<div style="position:absolute;left:88.20px;top:392.16px" class="cls_010"><span class="cls_010">several releases</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">29</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:37.20px;top:21.20px" class="cls_007"><span class="cls_007">Two-dimensional process structure of RUP</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">30</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Differences for developers</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Agile</span><span class="cls_010">: knowledgeable, collocated, collaborative</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Heavyweight</span><span class="cls_010">: plan-driven, adequate skills, access</span></div>
<div style="position:absolute;left:88.20px;top:346.32px" class="cls_010"><span class="cls_010">to external knowledge</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">31</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Differences for customers</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Agile</span><span class="cls_010">: dedicated, knowledgeable, collocated,</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">collaborative, representative, empowered</span></div>
<div style="position:absolute;left:61.20px;top:346.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Heavyweight</span><span class="cls_010">: access to knowledgeable,</span></div>
<div style="position:absolute;left:88.20px;top:375.12px" class="cls_010"><span class="cls_010">collaborative, representative, empowered</span></div>
<div style="position:absolute;left:88.20px;top:404.16px" class="cls_010"><span class="cls_010">customers</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">32</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Differences for requirements</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Agile</span><span class="cls_010">: largely emergent, rapid change</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Heavyweight</span><span class="cls_010">: knowable early, largely stable</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">33</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Differences for architecture</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Agile</span><span class="cls_010">: designed for current requirements</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Heavyweight</span><span class="cls_010">: designed for current and</span></div>
<div style="position:absolute;left:88.20px;top:311.28px" class="cls_010"><span class="cls_010">foreseeable requirements</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">34</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Differences for size</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Agile</span><span class="cls_010">: smaller teams and products</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Heavyweight</span><span class="cls_010">: larger teams and products</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">35</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Differences for primary objective</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Agile</span><span class="cls_010">: rapid value</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_016"> Heavyweight</span><span class="cls_010">: high assurance</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">36</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">The advantages of screen wipers</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">37</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">MDA - Model Driven Architecture</span></div>
<div style="position:absolute;left:217.45px;top:156.64px" class="cls_021"><span class="cls_021">model</span></div>
<div style="position:absolute;left:433.20px;top:156.64px" class="cls_021"><span class="cls_021">maintenance</span></div>
<div style="position:absolute;left:289.20px;top:278.56px" class="cls_021"><span class="cls_021">transformation</span></div>
<div style="position:absolute;left:226.32px;top:408.64px" class="cls_021"><span class="cls_021">code</span></div>
<div style="position:absolute;left:427.20px;top:408.64px" class="cls_021"><span class="cls_021">maintenance</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">38</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background39.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Essence of MDA</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Platform Independent Model (PIM)</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Model transformation and refinement</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Resulting in a Platform Specific Model (PSM)</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">39</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:21450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background40.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Maintenance or Evolution</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> some observations</span></div>
<div style="position:absolute;left:97.20px;top:178.24px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> systems are not built from scratch</span></div>
<div style="position:absolute;left:97.20px;top:207.28px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> there is time pressure on maintenance</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> the five laws of software evolution</span></div>
<div style="position:absolute;left:97.20px;top:305.20px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> law of continuing change</span></div>
<div style="position:absolute;left:97.20px;top:334.24px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> law of increasingly complexity</span></div>
<div style="position:absolute;left:97.20px;top:362.32px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> law of program evolution</span></div>
<div style="position:absolute;left:97.20px;top:391.12px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> law of invariant work rate</span></div>
<div style="position:absolute;left:97.20px;top:420.16px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> law of incremental growth limit</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">40</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background41.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Illustration third law of Software Evolution</span></div>
<div style="position:absolute;left:150.81px;top:165.60px" class="cls_015"><span class="cls_015">system</span></div>
<div style="position:absolute;left:142.31px;top:186.48px" class="cls_015"><span class="cls_015">attributes</span></div>
<div style="position:absolute;left:526.00px;top:441.60px" class="cls_015"><span class="cls_015">time</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">41</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background42.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Software Product Lines</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> developers are not inclined to make a</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">maintainable and reusable product, it has</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_010"><span class="cls_010">additional costs</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> this viewpoint is changed somewhat if the </span><span class="cls_016">product</span></div>
<div style="position:absolute;left:88.20px;top:300.24px" class="cls_016"><span class="cls_016">family</span><span class="cls_010"> is the focus of attention rather than</span></div>
<div style="position:absolute;left:88.20px;top:329.28px" class="cls_010"><span class="cls_010">producing a single version of a product</span></div>
<div style="position:absolute;left:61.20px;top:363.12px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> two processes result: </span><span class="cls_016">domain engineering</span><span class="cls_010">, and</span></div>
<div style="position:absolute;left:88.20px;top:392.16px" class="cls_016"><span class="cls_016">application engineering</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">42</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background43.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Process modeling</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> we may describe a software-development</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_010"><span class="cls_010">process, or parts thereof, in the form of a</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_010"><span class="cls_010">“program” too. E.G.:</span></div>
<div style="position:absolute;left:97.20px;top:264.16px" class="cls_022"><span class="cls_022">function</span><span class="cls_013"> review(document, threshold): boolean;</span></div>
<div style="position:absolute;left:97.20px;top:293.20px" class="cls_022"><span class="cls_022">begin</span><span class="cls_013"> prepare-review;</span></div>
<div style="position:absolute;left:119.70px;top:322.24px" class="cls_013"><span class="cls_013">hold-review{document, no-of-problems);</span></div>
<div style="position:absolute;left:119.70px;top:351.28px" class="cls_013"><span class="cls_013">make-report;</span></div>
<div style="position:absolute;left:119.70px;top:380.32px" class="cls_022"><span class="cls_022">return</span><span class="cls_013"> no-of-problems &lt; threshold</span></div>
<div style="position:absolute;left:97.20px;top:408.16px" class="cls_022"><span class="cls_022">end</span><span class="cls_013"> review;</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">43</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background44.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">STD of review process</span></div>
<div style="position:absolute;left:181.88px;top:189.60px" class="cls_015"><span class="cls_015">coding</span></div>
<div style="position:absolute;left:569.81px;top:189.60px" class="cls_015"><span class="cls_015">ready for</span></div>
<div style="position:absolute;left:185.88px;top:210.48px" class="cls_015"><span class="cls_015">ready</span></div>
<div style="position:absolute;left:568.81px;top:210.48px" class="cls_015"><span class="cls_015">next step</span></div>
<div style="position:absolute;left:220.63px;top:243.60px" class="cls_015"><span class="cls_015">submit</span></div>
<div style="position:absolute;left:262.00px;top:294.48px" class="cls_015"><span class="cls_015">review</span></div>
<div style="position:absolute;left:75.88px;top:303.60px" class="cls_015"><span class="cls_015">re-review</span></div>
<div style="position:absolute;left:176.00px;top:342.48px" class="cls_015"><span class="cls_015">prepare</span></div>
<div style="position:absolute;left:343.81px;top:342.48px" class="cls_015"><span class="cls_015">do</span></div>
<div style="position:absolute;left:459.13px;top:342.48px" class="cls_015"><span class="cls_015">make</span></div>
<div style="position:absolute;left:265.25px;top:360.48px" class="cls_015"><span class="cls_015">ready</span></div>
<div style="position:absolute;left:398.38px;top:360.48px" class="cls_015"><span class="cls_015">done</span></div>
<div style="position:absolute;left:506.94px;top:360.48px" class="cls_015"><span class="cls_015">report ready</span></div>
<div style="position:absolute;left:459.00px;top:381.60px" class="cls_015"><span class="cls_015">report</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">44</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background45.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Petri-net view of the review process</span></div>
<div style="position:absolute;left:146.88px;top:189.60px" class="cls_015"><span class="cls_015">code</span></div>
<div style="position:absolute;left:229.68px;top:195.60px" class="cls_015"><span class="cls_015">hold</span></div>
<div style="position:absolute;left:143.88px;top:210.48px" class="cls_015"><span class="cls_015">ready</span></div>
<div style="position:absolute;left:304.31px;top:211.20px" class="cls_015"><span class="cls_015">code</span></div>
<div style="position:absolute;left:220.69px;top:216.48px" class="cls_015"><span class="cls_015">review</span></div>
<div style="position:absolute;left:375.69px;top:217.20px" class="cls_015"><span class="cls_015">update</span></div>
<div style="position:absolute;left:81.44px;top:231.60px" class="cls_015"><span class="cls_015">from</span></div>
<div style="position:absolute;left:458.63px;top:243.60px" class="cls_015"><span class="cls_015">revised</span></div>
<div style="position:absolute;left:548.31px;top:243.60px" class="cls_015"><span class="cls_015">end</span></div>
<div style="position:absolute;left:625.69px;top:243.60px" class="cls_015"><span class="cls_015">next</span></div>
<div style="position:absolute;left:468.13px;top:264.48px" class="cls_015"><span class="cls_015">code</span></div>
<div style="position:absolute;left:625.69px;top:264.48px" class="cls_015"><span class="cls_015">step</span></div>
<div style="position:absolute;left:72.94px;top:267.60px" class="cls_015"><span class="cls_015">coding</span></div>
<div style="position:absolute;left:81.44px;top:351.60px" class="cls_015"><span class="cls_015">from</span></div>
<div style="position:absolute;left:46.94px;top:381.60px" class="cls_015"><span class="cls_015">management</span></div>
<div style="position:absolute;left:125.38px;top:411.60px" class="cls_015"><span class="cls_015">scheduled</span></div>
<div style="position:absolute;left:292.25px;top:411.60px" class="cls_015"><span class="cls_015">minutes</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">45</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background46.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Purposes of process modeling</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> facilitates understanding and communication by</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">providing a shared view of the process</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> supports management and improvement; it can be</span></div>
<div style="position:absolute;left:88.20px;top:306.24px" class="cls_010"><span class="cls_010">used to assign tasks, track progress, and identify</span></div>
<div style="position:absolute;left:88.20px;top:334.32px" class="cls_010"><span class="cls_010">trouble spots</span></div>
<div style="position:absolute;left:61.20px;top:404.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> serves as a basis for automated support (usually</span></div>
<div style="position:absolute;left:88.20px;top:432.24px" class="cls_010"><span class="cls_010">not fully automatic)</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">46</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:25300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background47.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Caveats of process modeling</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> not all aspects of software development can be</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_010"><span class="cls_010">caught in an algorithm</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> a model is a model, thus a simplification of reality</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> progression of stages differs from what is actually</span></div>
<div style="position:absolute;left:88.20px;top:306.24px" class="cls_010"><span class="cls_010">done</span></div>
<div style="position:absolute;left:61.20px;top:340.32px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> some processes (e.g. learning the domain) tend to</span></div>
<div style="position:absolute;left:88.20px;top:369.12px" class="cls_010"><span class="cls_010">be ignored</span></div>
<div style="position:absolute;left:61.20px;top:404.16px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> no support for transfer across projects</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">47</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:25850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture3/background48.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_007"><span class="cls_007">Summary</span></div>
<div style="position:absolute;left:58.20px;top:153.36px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Traditional models focus on </span><span class="cls_016">control</span><span class="cls_010"> of the</span></div>
<div style="position:absolute;left:85.20px;top:182.40px" class="cls_010"><span class="cls_010">process</span></div>
<div style="position:absolute;left:58.20px;top:217.44px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> There is no one-size-fits-all model; each situation</span></div>
<div style="position:absolute;left:85.20px;top:246.48px" class="cls_010"><span class="cls_010">requires its own approach</span></div>
<div style="position:absolute;left:58.20px;top:280.56px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> A pure project approach inhibits reuse and</span></div>
<div style="position:absolute;left:85.20px;top:309.36px" class="cls_010"><span class="cls_010">maintenance</span></div>
<div style="position:absolute;left:58.20px;top:343.44px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> There has been quite some attention for process</span></div>
<div style="position:absolute;left:85.20px;top:372.48px" class="cls_010"><span class="cls_010">modeling, and tools based on such process</span></div>
<div style="position:absolute;left:85.20px;top:401.52px" class="cls_010"><span class="cls_010">models</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_006"><span class="cls_006">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_008"><span class="cls_008">48</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_006"><span class="cls_006"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>

</body>
</html>
