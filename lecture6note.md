## Quiz      

1. Quality **cannot** be added as an **afterthought**. It has to be **built** in from the very **beginning** 

2. A measurement framework 
    - Entity 
        - An entity is an object in the ‘real’ world of which we want to know or predict certain propertie 
    - Attribute 
        - Entities have certain properties which we call attribute 
    - Attribute relation 
        - Different attributes of one or more entities can be related 
    - Value 
        - The former three constituents of the model reside in the ‘real’ world 
        - measuring attributes 
        - assigning values to them 
    - Unit 
        - this value is expressed in a certain unit 
    - Scale types 
        - Nominal 
            - Attributes are merely **classified**: the color of my hair is gray, white or black 
        - Ordinal 
            - There is a (linear) **ordering** in the possible values of an attribute: one type of material is **harder** than another, one program is **more complex** than another 
        - Interval 
            - The same as ordinal, but **the ‘distance’ between successive values** of an attribute is the same, as in a calendar, or the temperature measured in degrees Fahrenheit 
        - Ratio 
            - The same as interval, with the additional requirement that there exists a value 0, as in the age of a software system, or the temperature measured in degrees Kelvin 
        - Absolute 
            - In this case we simply **count the number of occurrences**, as in the number of errors detected in a program 

3. If we observe that car A drives faster than car B, then we would rather like our function S which maps the speed observed to some number to be such that S(A) > S(B). This is called the **representation condition**. If a measure satisfies the representation condition, it is said to be a **valid measure**. For example, we earlier proposed to measure the complexity of a program text by counting the number of if-statements. For this (indirect) measure to be valid we have to ascertain that 
    - any two programs with the same number of if-statements are equally complex, and 
    - if program A has more if-statements than program B, then A is more complex than B 

4. The **quality attributes** with McCall’s taxonomy 
    - **Higher-level** quality attributes, known as **quality factors**, are **external** attributes and can, therefore, be measured only **indirectly** 
        - Users and managers tend to be interested in the **higher-level**, **external** quality attributes 
        - Most quality attributes are external  
    - McCall introduced a second level of quality attributes, termed **quality criteria**. Quality criteria can be measured either **subjectivel** or objectively** 
    - The **quality factor** and **quality attributes** 
        - Product operation 
        - Product revision 
        - Product transition 

5. In ISO standard 9126 
    - ISO 9126 is about **product** 
    - a similar effort has been made to define a set of **quality characteristics** and **sub-characteristics** 
    - ISO scheme is hierarchical: each **sub-characteristic** is **related** to exactly one **characteristic** 
    - The ISO **quality characteristics** strictly refer to a **software product** 
    - The ISO **characteristics** and **subcharacteristics**, together with an extensive set of measures, make up ISO˘s **external** and **internal** quality model 
        - Internal quality refers to the **product** itself, ultimately the source **code** 
            - the **average number of statements** in a method is a measure of internal quality 
        - External quality refers to the **quality** when the **software** is executed 
            - the **number of defects** encountered during **testing** is a measure of external quality 
    - the user is interested in the **quality in use**, defined in (ISO9126, 2001) as ”the user’s view of the quality of the software product when it is executed in a specific environment and a specific context of use.” It measures the extent to which users can achieve their **goals**, **rather than** mere **properties** of the software 
        - **Quality in use** is **modeled** in four **characteristics** 
            - effectiveness 
            - productivity 
            - safety 
            - satisfaction 
        - **Quality in use** can in general only be measured **indirectly** 
    - **quality model** of ISO 9126 
        - The **quality attribute** 
            - Functionality 
            - Reliability 
            - Usability 
            - Efficiency 
            - Maintainability 
            - Portability 

6. Perspectives on Quality 
    - Users will judge the quality of a software system by the **degree to which it helps them accomplish tasks** and **by the sheer joy they have in using it**  
    - These **different viewpoints** are all valid. They are also **difficult to reconcile** 
    - definitions of software quality 
        - Transcendent definition 
            - **innate excellence** 
            - a **good feeling** for this type of quality 
            - I really **like** this program 
        - User-based definition 
            - fitness for use 
            - addresses the user’s need 
            - **average user** prefer **simple** word-processing package while a **computer scientist** prefer **complex** system like LATEX 
            - definition used during **acceptance testing** 
        - Product-based definition 
            - In the product-based definition, quality relates to **attributes of the software** 
            - the **research** into software quality concerns this type of quality 
        - Manufacturing-based definition 
            - The manufacturing-based definition concerns **conformance to specifications** 
            - definition used during **system testing** 
        - Value-based definition 
            - the value-based definition deals with **costs** and **profits** 
            - It concerns balancing **time** and **cost** on the one hand, and **profit** on the other hand 
        - **Software developers** tend to concentrate on the **product-based** and **manufacturing- based** definitions of quality 

7. The Quality System 
    - ISO 9001 is a generic standard 
        - It can be applied to any product 
        - demonstrate its ability 
        - meet requirement 
        - enhance satisfaction 
    - ISO 9001 **registration** is granted when a third-party **accredited** body assesses the quality system 
    - ISO 9001 **Reregistration** is required **every three years** 
    - ISO 9001 surveillance **audits** are required **every six months** 

8. We will try to assess the quality by reading documents, by inspections, by walkthroughs and by peer reviews 

9. The Capability Maturity Model (CMM) 
    - maturity levels 
        - Initial 
            - Tools are **not adequately** integrated 
            - Many problems are **overlooked** 
            - **not planning** 
            - Software development at this level can be characterized as being **ad-hoc** 
        - Repeatable 
            - the repeatable level provides control over the way plans and commitments are established 
        - Defined 
            - At the defined process level, a set of **standard processes** for the development and maintenance of software is in place 
        - Quantitatively managed 
            - **data** is **gathered** and **analyzed** on a routine basis 
        - Optimizing 
            - a **stable base** has been reached from which further **improvements** can be made 
            - **data** is **gathered** and **improve** the process 
    - Critical Note 
        - The SEI’s Capability Maturity Model seems most **appropriate** for the really **big companies**. It is doubtful whether small companies can afford the time and money required by a process improvement program as advocated by CMM 
        - Requirements analysis and design ask for a certain amount of **creativity**, and a pure CMM approach may have a **stifling** effect here 
        - **crude five-point scale** 




































