<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_010{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:23.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:23.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:27.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:27.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture7/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:93.44px;top:34.96px" class="cls_002"><span class="cls_002">Software Cost Estimation</span></div>
<div style="position:absolute;left:35.45px;top:366.04px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:35.45px;top:408.04px" class="cls_004"><span class="cls_004">§</span><span class="cls_003"> What factors determine cost/effort?</span></div>
<div style="position:absolute;left:35.45px;top:450.04px" class="cls_004"><span class="cls_004">§</span><span class="cls_003"> How to relate effort to development time?</span></div>
<div style="position:absolute;left:7.20px;top:501.60px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:519.60px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Cost estimation, topics</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Quantitative models (E = 2.5 KLOC</span><span class="cls_009"><sup>1.05</sup></span><span class="cls_008">)</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Qualitative models (e.g. expert estimation)</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Agile cost estimation</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Relate cost to development time</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">2</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">On productivity</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Even if quantitative models are not that good, the</span></div>
<div style="position:absolute;left:75.85px;top:277.20px" class="cls_008"><span class="cls_008">cost drivers of these models learn us about</span></div>
<div style="position:absolute;left:75.85px;top:306.24px" class="cls_008"><span class="cls_008">productivity:</span></div>
<div style="position:absolute;left:90.80px;top:339.28px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Writing less code helps</span></div>
<div style="position:absolute;left:90.80px;top:368.32px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Reuse helps</span></div>
<div style="position:absolute;left:90.80px;top:397.12px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Quality of people is important</span></div>
<div style="position:absolute;left:90.80px;top:426.16px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Tools help</span></div>
<div style="position:absolute;left:90.80px;top:455.20px" class="cls_011"><span class="cls_011">§</span><span class="cls_012">…</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">3</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Algorithmic models</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Base formula: E = a + bKLOC</span><span class="cls_009"><sup>c</sup></span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">C usually is around 1</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">C > 1: diseconomy of scale</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">C &lt; 1: economy of scale</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> This nominal cost is multiplied by a number of cost</span></div>
<div style="position:absolute;left:75.85px;top:346.32px" class="cls_008"><span class="cls_008">drivers (volatility of requirements, amount of</span></div>
<div style="position:absolute;left:75.85px;top:375.12px" class="cls_008"><span class="cls_008">documentation required, CMM level, quality of</span></div>
<div style="position:absolute;left:75.85px;top:404.16px" class="cls_008"><span class="cls_008">people, …)</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">4</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Walston-Felix</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> One of the early algorithmic models (1977)</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Many factors (29 out of 51 projects)</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Only three alternatives (high, medium, low) per</span></div>
<div style="position:absolute;left:75.85px;top:242.16px" class="cls_008"><span class="cls_008">factor</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Its form influenced many later models</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">5</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">COCOMO (COnstructive COst MOdel)</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Very well-documented (Boehm book, 1981)</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Basic form: E = bKLOC</span><span class="cls_009"><sup>c</sup></span><span class="cls_008">, where b (~3) and c (1+ε)</span></div>
<div style="position:absolute;left:75.85px;top:208.32px" class="cls_008"><span class="cls_008">depend on the type of project (mode):</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Organic: relatively small and well-known</span></div>
<div style="position:absolute;left:90.80px;top:270.16px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Embedded: inflexible environment with many constraints</span></div>
<div style="position:absolute;left:90.80px;top:299.20px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Semidetached: somewhere in between</span></div>
<div style="position:absolute;left:61.20px;top:329.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> More complex form: takes into account 15</span></div>
<div style="position:absolute;left:75.85px;top:357.12px" class="cls_008"><span class="cls_008">multiplicative cost drivers</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">6</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Putnam model, Rayleigh curve</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">7</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Function Point Analysis (FPA)</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Size (=cost) is based on number of data structures</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_008"><span class="cls_008">used:</span></div>
<div style="position:absolute;left:90.80px;top:207.28px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> I: number of input types</span></div>
<div style="position:absolute;left:90.80px;top:236.32px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> O: number of output types</span></div>
<div style="position:absolute;left:90.80px;top:264.16px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> E: number of enquiry types</span></div>
<div style="position:absolute;left:90.80px;top:293.20px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> L: number of logical internal types</span></div>
<div style="position:absolute;left:90.80px;top:322.24px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> F: number of interfaces</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Then, magically, UFP = 4I + 5O + 10E + 4L + 7F</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">8</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">FPA cnt’d</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Somewhat more complex model: constants depend</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_008"><span class="cls_008">on complexity level (simple, average, complex)</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Cost drivers (application characteristics) next</span></div>
<div style="position:absolute;left:75.85px;top:237.12px" class="cls_008"><span class="cls_008">adjust the value of UFP by at most +/- 40%</span></div>
<div style="position:absolute;left:61.20px;top:306.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Extensive guidelines for counting Function Points</span></div>
<div style="position:absolute;left:61.20px;top:375.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Reflects data-centric world of the 1970’s</span></div>
<div style="position:absolute;left:61.20px;top:444.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Widely used</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">9</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">COCOMO2</span></div>
<div style="position:absolute;left:61.20px;top:141.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Successor to COCOMO</span></div>
<div style="position:absolute;left:61.20px;top:173.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Three, increasingly complex models:</span></div>
<div style="position:absolute;left:90.80px;top:204.16px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Application composition model; counting components of large</span></div>
<div style="position:absolute;left:105.95px;top:226.24px" class="cls_012"><span class="cls_012">granularity, using object points (objects are: screens, reports,</span></div>
<div style="position:absolute;left:105.95px;top:248.32px" class="cls_012"><span class="cls_012">and the like); FPA-like, with 3 levels of complexity for each</span></div>
<div style="position:absolute;left:105.95px;top:269.20px" class="cls_012"><span class="cls_012">object.</span></div>
<div style="position:absolute;left:90.80px;top:296.32px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Early design model: same, but uses 7 cost drivers (project</span></div>
<div style="position:absolute;left:105.95px;top:317.20px" class="cls_012"><span class="cls_012">characteristics, combinations of cost drivers from the post-</span></div>
<div style="position:absolute;left:105.95px;top:339.28px" class="cls_012"><span class="cls_012">architecture version) instead of 3 simple complexity levels</span></div>
<div style="position:absolute;left:90.80px;top:365.20px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Post-architecture model: like COCOMO, with updated set of 17</span></div>
<div style="position:absolute;left:105.95px;top:387.28px" class="cls_012"><span class="cls_012">cost drivers</span></div>
<div style="position:absolute;left:61.20px;top:414.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Rather than having 3 modes (COCOMO),</span></div>
<div style="position:absolute;left:75.85px;top:440.16px" class="cls_008"><span class="cls_008">COCOMO2 has a more elaborate scaling model</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">10</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Use Case Points</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> FPA-like model, starting from use cases</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Counting depends on the use case</span></div>
<div style="position:absolute;left:90.80px;top:213.28px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> How many steps in success scenario</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> How many classes in the implementation</span></div>
<div style="position:absolute;left:90.80px;top:270.16px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Complexity of the actors involved</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Next, corrections for the technical and</span></div>
<div style="position:absolute;left:75.85px;top:329.28px" class="cls_008"><span class="cls_008">environmental complexity</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">11</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:22.40px" class="cls_006"><span class="cls_006">Difficulties with applying</span></div>
<div style="position:absolute;left:61.20px;top:56.48px" class="cls_006"><span class="cls_006">these models</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> People do not collect numbers, so:</span></div>
<div style="position:absolute;left:90.80px;top:282.16px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> This project costs the same as the last project</span></div>
<div style="position:absolute;left:90.80px;top:311.20px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> We have 6 months, so it will take 6 months</span></div>
<div style="position:absolute;left:90.80px;top:339.28px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> …and other political estimates</span></div>
<div style="position:absolute;left:61.20px;top:404.16px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> These models require calibration</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">12</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Guidelines</span></div>
<div style="position:absolute;left:61.20px;top:214.26px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Estimation </span><span class="cls_013">¹</span><span class="cls_008"> planning </span><span class="cls_013">¹</span><span class="cls_008"> bidding</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Combine methods</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Ask justification</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Select experts with similar experience</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Accept and assess uncertainty</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Provide earning opportunities</span></div>
<div style="position:absolute;left:61.20px;top:421.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Try to avoid, or postpone, effort estimation</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">13</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Cone of uncertainty</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Son</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">14</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.14px" class="cls_006"><span class="cls_006">From total effort to  </span><span class="cls_014">Þ</span><span class="cls_006">  number of months</span></div>
<div style="position:absolute;left:61.20px;top:144.18px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">A lot of consensus between models: T </span><span class="cls_013">@</span><span class="cls_008"> 2.5E</span><span class="cls_009"><sup>1/3</sup></span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Compressing this value has a price:</span></div>
<div style="position:absolute;left:90.80px;top:213.28px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> Team larger  Þ more communication</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_011"><span class="cls_011">§</span><span class="cls_012"> New people first slows down productivity</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Combined: Brooks’ law:</span></div>
<div style="position:absolute;left:86.32px;top:340.32px" class="cls_008"><span class="cls_008">Adding manpower to a late project makes it later</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">15</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Impossible region</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">16</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Agile cost estimation</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Estimate size of features in </span><span class="cls_015">story points</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> These are relative sizes: one feature is twice as</span></div>
<div style="position:absolute;left:75.85px;top:242.16px" class="cls_008"><span class="cls_008">large as another one, etc.</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Use a few simple relative sizes, e.g., 1, 2, 4, and 8</span></div>
<div style="position:absolute;left:61.20px;top:381.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008">Use a Delphi-like procedure to get consensus</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">17</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture7/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Agile cost estimation, cnt’d</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Translation of story points to real time: </span><span class="cls_015">velocity</span><span class="cls_008">:</span></div>
<div style="position:absolute;left:75.85px;top:242.16px" class="cls_008"><span class="cls_008">number of function points completed in one</span></div>
<div style="position:absolute;left:75.85px;top:271.20px" class="cls_008"><span class="cls_008">iteration</span></div>
<div style="position:absolute;left:61.20px;top:306.24px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Start: </span><span class="cls_015">yesterday’s weather</span><span class="cls_008">: productivity is the same</span></div>
<div style="position:absolute;left:75.85px;top:334.32px" class="cls_008"><span class="cls_008">as that for the last project</span></div>
<div style="position:absolute;left:61.20px;top:369.12px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> If the outcome is wrong: adjust the velocity, </span><span class="cls_015">not</span><span class="cls_008"> the</span></div>
<div style="position:absolute;left:75.85px;top:398.16px" class="cls_008"><span class="cls_008">story points</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">18</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>

</body>
</html>
