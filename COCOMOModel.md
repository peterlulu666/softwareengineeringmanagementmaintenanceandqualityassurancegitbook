## Quiz        

1. The Source Lines of Code (SLOC) 
    - Only Source lines that are DELIVERED as part of the product are included 
        - **test drivers** and other **support** software is **excluded**  
        - **comment** and **documentation** is **excluded**  
    - SOURCE lines are created by the project staff 
        - **code** created by **applications** generators is **excluded** 
        - the **open source code** is **excluded** 
    - One SLOC is **one** logical **line** of code 
    - **Comments** are not counted as SLOC 

2. The Scale Drivers 
    - Precedentedness 
        - you done the project before 
    - Development Flexibility 
        - the flexible schedule 
    - Architecture / Risk Resolution    
        - pair program 
    - Team Cohesion 
    - Process Maturity 
        - plan 
        - identical product 



