## Quiz        

1. Despite these **discrepancies**, the various **cost estimation models** do have a number of characteristics **in common** 
    - Writing less code 
    - Getting the best from people 
    - Avoiding rework 
    - Developing and using integrated project support environments 

2. Walston--Felix 
    - The base equation of Walston and Felix’ model (Walston and Felix, 1977) 
    - For a number of projects (51) Walston and Felix determined the level of each of these 29 variables, together with the productivity obtained (in terms of lines of code per man-month) in those project 
    - The **number of factors** considered in this model is rather **high** (29 factors out of 51 projects) 
    - the number of **alternatives per factor** is only **three**, and does not seem to offer enough choice in practical situations 
    - the approach taken by Walston and Felix and their list of cost drivers have played a very **important role** in directing later **research** in this area  

3. The COCOMO 
    - Boehm 
    - the 1981 COCOMO model 
    - COCOMO distinguishes three classes of project 
        - Organic 
            - A relatively **small team** develops software in **a known environment** 
        - Embedded 
            - The product will be embedded in an environment which is very **inflexible** and poses severe **constraints** 
        - Semidetached 
            - This is an **intermediate** form 
    - Both these models **take into account 15 cost drivers** --attributes that affect productivity, and hence cost 

4. **Norden** studied the distribution of manpower over time in a number of software development projects in the 1960s. He found that this distribution often had a very characteristic shape which is well-approximated by a **Rayleigh** distribution. Based upon this finding, **Putnam** developed a cost estimation model in which the manpower required ( MR) at time t 

5. 40% of the total effort is spent on the **actual development**, while 60% is spent on **maintenance** 

6. The Function Point Analysis (FPA) 
    - Function point analysis (FPA) is a method of **estimating costs** in which the problems associated with determining the expected amount of code are circumvented. FPA is based on counting the number of different **data structures** that are used 
    - FPA is particularly **suitable** for projects aimed at realizing business applications for, in these applications, the structure of the **data** plays a **very** dominant **role** 
    - The method is **less suited** to projects in which the structure of the **data** plays a **less** prominent **role**, and the emphasis is on **algorithms** 
    - The five entities play a central role in the FPA-model
        - Number of input types (I) 
        - Number of output types (O) 
        - Number of inquiry types (E) 
        - Number of logical internal files (L) 
        - Number of interfaces (F) 
    - The International Function Point User Group (IFPUG) has published **extensive guidelines** on how to classify and count the various entities involved 
    - FPA distinguishes **three** levels of component **complexity** only (simple, average, complex) 

7. The COCOMO 2 
    - tuned to the life cycle practices of the 1990s and 2000s 
    - the **Application Composition model**, mainly intended for **prototyping** effort 
        - **counting system components of a large granularity**
    - the **Early Design model**, aimed at the architectural **design stage** 
        - an **FPA-like model**
        - The Early Design model uses **unadjusted function points (UFPs)** as its basic size measure. These unadjusted function points are counted in the same way they are counted in FPA 
    - the **Post-Architecture model** for the actual **development stage** of a software product 
        - **update** of the original COCOMO model
    - The **Post-Architecture model** can be considered an **update** of the original COCOMO model; the **Early Design model** is an **FPA-like model**; and the **Application Composition model** is based on **counting system components of a large granularity**, such as screens and report 

8. The reuse
    - The original **COCOMO** model allows us to handle reuse in the following way. The three main development phases, design, coding and integration, are estimated to take 40%, 30% and 30% of the average effort, respectively. **Reuse** can be catered for by separately considering the fractions of the system that require redesign ( DM ), recoding ( CM ) and re-integration ( IM ) 
    - **COCOMO 2** uses a more **elaborate** scheme to handle **reuse** effects. This scheme reflects two additional factors that impact the cost of reuse: the quality of the code being reused and the amount of effort needed to test the applicability of the component to be reused 



























































