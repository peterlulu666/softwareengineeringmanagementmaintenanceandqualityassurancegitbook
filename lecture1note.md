## Quiz      

1. The software development problem
    - To many people, programming was still an **art** and had never become a craft. However, in reality, it is not an art
    - An additional problem was that many programmers had **not** been formally **educated** in the field

2. The term Software Engineering
    - This type of problem really became manifest in the 1960s. Under the auspices of **NATO**, two conferences were devoted to the topic in 1968 and 1969 (Naur and Randell, 1968), (Buxton and Randell, 1969). Here, the term ‘software engineering’ was coined in a somewhat provocative sense
    - definition was given at the first NATO conference
        - Software engineering is the establishment and use of sound engineering principles in order to obtain economically software that is reliable and works efficiently on real machines
    - IEEE Standard Glossary
        - Software engineering is the application of a systematic, disciplined, quantifiable approach to the development, operation, and maintenance of software; that is, the application of engineering to software

3. Build **software** is in the way one builds **bridges** and houses, starting from a theoretical basis and using sound and proven design and construction techniques

4. The errors
    - The financial consequences

5. The programming-in-the-small
    - refers to programs written by one person in a relatively short period of time
    - Traditional programming techniques and tools are primarily aimed at supporting programming-in-the-small

6. The The programming-in-the-large
    - refers to multi-person jobs that span, say, more than half a year

7. The software and the physical products
    - Viewing software engineering as a branch of engineering is problematic
    - maintain physical products
        - wear out
    - maintain software
        - caused by errors detected late or by changing requirements of the user

8. The view of software development
    - Design
        - During the design phase we try to separate the what from the how
    - Implementation
        - a well-documented, reliable, easy to read, flexible, correct, program
        - **not** to produce a very efficient program full of **tricks**
    - Testing
        - it is wrong to say that testing is a phase following implementation
        - verification
            - phases is correct
            - building the system right
        - validation
            - fulfilling user requirements
            - building the right system
    - Maintenance
        - repair errors
        - requests for changes and enhancements
        - all activities needed to keep the system operational **after** it has been **delivered** to the user
        - maintenance activities
            - corrective maintenance -- the repair of actual errors
            - adaptive maintenance -- **adapting** the software to **changes** in the **environment**, such as new hardware or the next release of an operating or database system
            - perfective maintenance -- adapting the software to new or changed user requirements, such as extra functions to be provided by the system. Perfective maintenance also includes work to increase the system’s performance or to enhance its user interface
            - preventive maintenance -- increasing the system’s future maintainability. Updating documentation, adding comments, or improving the modular struc- ture of a system are examples of preventive maintenance activities
        - Only the first category may rightfully be termed maintenance
        - A large percentage of what we are used to calling maintenance is actually evolution

9. Commercial Off-The-Shelves (COTS)
    - It is not to build the editor and then coding

10. The Unified Modeling Language 
    - At a later stage, **OMG** --- the **Object Management Group**, an open consortium of companies --- took over. They now control the activities around **UML**, and adopted it as one of their standards. The **current version** is known as **UML 2** 







    





