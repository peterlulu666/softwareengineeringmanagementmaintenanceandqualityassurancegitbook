<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Calibri",serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Calibri",serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:"Calibri Italic",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_009{font-family:"Calibri Italic",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:"Calibri Italic",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_012{font-family:"Calibri Italic",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:"Calibri Italic",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
div.cls_015{font-family:"Calibri Italic",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:italic;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture17/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:177.49px;top:32.80px" class="cls_002"><span class="cls_002">Software Reusability</span></div>
<div style="position:absolute;left:103.20px;top:304.96px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:103.20px;top:346.96px" class="cls_003"><span class="cls_003">• Why is reuse so difficult</span></div>
<div style="position:absolute;left:103.20px;top:389.92px" class="cls_003"><span class="cls_003">• How to realize reuse</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:68.95px;top:52.72px" class="cls_002"><span class="cls_002">Reuse dimensions</span></div>
<div style="position:absolute;left:59.58px;top:229.92px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Things being reused: components, concepts, …</span></div>
<div style="position:absolute;left:59.58px;top:265.92px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Scope: horizontal vs vertical</span></div>
<div style="position:absolute;left:59.58px;top:301.92px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Approach: systematic or opportunistic</span></div>
<div style="position:absolute;left:59.58px;top:337.92px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Technique: compositional or generative</span></div>
<div style="position:absolute;left:59.58px;top:373.92px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Use: black-box or white-box</span></div>
<div style="position:absolute;left:59.58px;top:409.92px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Product being reused: source code, design, …</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_004"><span class="cls_004">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:110.24px;top:15.68px" class="cls_007"><span class="cls_007">Success criteria for component</span></div>
<div style="position:absolute;left:294.91px;top:63.68px" class="cls_007"><span class="cls_007">libraries</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Well-developed field, standard terminology</span></div>
<div style="position:absolute;left:43.20px;top:265.60px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Small interfaces</span></div>
<div style="position:absolute;left:43.20px;top:357.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Standardized data formats</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_004"><span class="cls_004">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:151.78px;top:16.64px" class="cls_007"><span class="cls_007">Requirements for</span></div>
<div style="position:absolute;left:130.07px;top:64.64px" class="cls_007"><span class="cls_007">component libraries</span></div>
<div style="position:absolute;left:109.32px;top:179.28px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Searching for components</span></div>
<div style="position:absolute;left:109.32px;top:258.24px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Understanding/evaluating components found</span></div>
<div style="position:absolute;left:109.32px;top:337.20px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Adapt components if necessary</span></div>
<div style="position:absolute;left:109.32px;top:417.12px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Compose systems from components</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_004"><span class="cls_004">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:116.06px;top:15.68px" class="cls_007"><span class="cls_007">Component evaluation, useful</span></div>
<div style="position:absolute;left:264.10px;top:63.68px" class="cls_007"><span class="cls_007">information</span></div>
<div style="position:absolute;left:43.20px;top:121.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Quality information</span></div>
<div style="position:absolute;left:43.20px;top:193.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Administrative information (name developer,</span></div>
<div style="position:absolute;left:70.20px;top:221.52px" class="cls_006"><span class="cls_006">modification history, etc)</span></div>
<div style="position:absolute;left:43.20px;top:293.52px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Documentation</span></div>
<div style="position:absolute;left:43.20px;top:365.52px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Interface information</span></div>
<div style="position:absolute;left:43.20px;top:437.52px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Test information</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_004"><span class="cls_004">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:162.99px;top:37.12px" class="cls_002"><span class="cls_002">Reuse process models</span></div>
<div style="position:absolute;left:43.20px;top:165.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Software development </span><span class="cls_009">with</span><span class="cls_003"> reuse</span></div>
<div style="position:absolute;left:79.20px;top:207.68px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Passive</span></div>
<div style="position:absolute;left:79.20px;top:244.64px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Component library evolves haphazardly</span></div>
<div style="position:absolute;left:43.20px;top:324.64px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Software development </span><span class="cls_009">for</span><span class="cls_003"> reuse</span></div>
<div style="position:absolute;left:79.20px;top:366.56px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Active</span></div>
<div style="position:absolute;left:79.20px;top:403.52px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Reusable assets are </span><span class="cls_012">developed</span><span class="cls_011">, rather than found</span></div>
<div style="position:absolute;left:101.70px;top:433.52px" class="cls_011"><span class="cls_011">by accident</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_004"><span class="cls_004">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:57.04px;top:37.12px" class="cls_002"><span class="cls_002">Software development with reuse</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_004"><span class="cls_004">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:71.25px;top:37.12px" class="cls_002"><span class="cls_002">Software development for reuse</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_004"><span class="cls_004">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:71.25px;top:37.12px" class="cls_002"><span class="cls_002">Software development for reuse</span></div>
<div style="position:absolute;left:43.20px;top:123.60px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Often two separate development processes:</span></div>
<div style="position:absolute;left:79.20px;top:163.60px" class="cls_013"><span class="cls_013">-</span><span class="cls_014"> Development of components (involving domain</span></div>
<div style="position:absolute;left:101.70px;top:191.68px" class="cls_014"><span class="cls_014">analysis)</span></div>
<div style="position:absolute;left:79.20px;top:225.52px" class="cls_013"><span class="cls_013">-</span><span class="cls_014"> Development of applications, using the available</span></div>
<div style="position:absolute;left:101.70px;top:253.60px" class="cls_014"><span class="cls_014">components</span></div>
<div style="position:absolute;left:43.20px;top:327.60px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Specific forms hereof:</span></div>
<div style="position:absolute;left:79.20px;top:367.60px" class="cls_013"><span class="cls_013">-</span><span class="cls_014"> Component-based software development</span></div>
<div style="position:absolute;left:79.20px;top:401.68px" class="cls_013"><span class="cls_013">-</span><span class="cls_014"> Software factory</span></div>
<div style="position:absolute;left:79.20px;top:436.48px" class="cls_013"><span class="cls_013">-</span><span class="cls_014"> Software product lines</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_004"><span class="cls_004">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:36.84px;top:49.84px" class="cls_002"><span class="cls_002">Reuse tools and techniques</span></div>
<div style="position:absolute;left:59.58px;top:211.12px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Languages to describe compositions</span></div>
<div style="position:absolute;left:95.58px;top:256.16px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Module Interconnection Language (MIL)</span></div>
<div style="position:absolute;left:95.58px;top:297.20px" class="cls_010"><span class="cls_010">-</span><span class="cls_011"> Architecture Description Language (ADL)</span></div>
<div style="position:absolute;left:59.58px;top:384.16px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Middleware (CORBA, JavaBeans, .NET)</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_004"><span class="cls_004">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:84.73px;top:15.68px" class="cls_007"><span class="cls_007">Characteristics of successful reuse</span></div>
<div style="position:absolute;left:282.85px;top:63.68px" class="cls_007"><span class="cls_007">programs</span></div>
<div style="position:absolute;left:43.20px;top:165.52px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Extensive management support</span></div>
<div style="position:absolute;left:43.20px;top:208.48px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Organizational support structure</span></div>
<div style="position:absolute;left:43.20px;top:250.48px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Incremental implementation</span></div>
<div style="position:absolute;left:43.20px;top:292.48px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Significant success</span></div>
<div style="position:absolute;left:43.20px;top:334.48px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> High incentives</span></div>
<div style="position:absolute;left:43.20px;top:377.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Domain analysis done</span></div>
<div style="position:absolute;left:43.20px;top:419.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Attention to architectural issues</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:664.34px;top:507.60px" class="cls_004"><span class="cls_004">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:84.62px;top:15.68px" class="cls_007"><span class="cls_007">Non-technical aspects of software</span></div>
<div style="position:absolute;left:315.08px;top:63.68px" class="cls_007"><span class="cls_007">reuse</span></div>
<div style="position:absolute;left:43.20px;top:173.68px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Economics: it is a long term investment</span></div>
<div style="position:absolute;left:43.20px;top:265.60px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Management: it does not happen</span></div>
<div style="position:absolute;left:70.20px;top:304.48px" class="cls_003"><span class="cls_003">spontaneously</span></div>
<div style="position:absolute;left:43.20px;top:396.64px" class="cls_008"><span class="cls_008">•</span><span class="cls_003"> Psychology: people do not want to reuse</span></div>
<div style="position:absolute;left:70.20px;top:434.56px" class="cls_003"><span class="cls_003">someone else’s code</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_004"><span class="cls_004">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:201.27px;top:37.12px" class="cls_002"><span class="cls_002">Reuse devil’s loop</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_004"><span class="cls_004">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture17/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:274.89px;top:37.12px" class="cls_002"><span class="cls_002">Summary</span></div>
<div style="position:absolute;left:43.20px;top:163.68px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> We can reuse different things: code, design, …</span></div>
<div style="position:absolute;left:43.20px;top:242.64px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Reuse can be </span><span class="cls_015">systematic </span><span class="cls_006">(software development</span></div>
<div style="position:absolute;left:70.20px;top:275.52px" class="cls_015"><span class="cls_015">for</span><span class="cls_006"> reuse), or </span><span class="cls_015">opportunistic</span><span class="cls_006"> (software</span></div>
<div style="position:absolute;left:70.20px;top:307.68px" class="cls_006"><span class="cls_006">development </span><span class="cls_015">with</span><span class="cls_006"> reuse)</span></div>
<div style="position:absolute;left:43.20px;top:386.64px" class="cls_005"><span class="cls_005">•</span><span class="cls_006"> Reuse does not just happen; it needs to be</span></div>
<div style="position:absolute;left:70.20px;top:419.52px" class="cls_006"><span class="cls_006">planned</span></div>
<div style="position:absolute;left:7.20px;top:489.84px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:506.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_004"><span class="cls_004">14</span></div>
</div>

</body>
</html>
