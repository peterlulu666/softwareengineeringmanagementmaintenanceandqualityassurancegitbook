<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_003{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_002{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:23.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:23.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture2/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:133.75px;top:8.56px" class="cls_003"><span class="cls_003">Software Engineering</span></div>
<div style="position:absolute;left:225.50px;top:61.60px" class="cls_003"><span class="cls_003">Management</span></div>
<div style="position:absolute;left:44.08px;top:369.08px" class="cls_004"><span class="cls_004">Main issues:</span></div>
<div style="position:absolute;left:44.08px;top:405.08px" class="cls_005"><span class="cls_005">§</span><span class="cls_004">Plan -</span><span class="cls_006"> as much as possible, but not too much, up front</span></div>
<div style="position:absolute;left:44.08px;top:441.08px" class="cls_005"><span class="cls_005">§</span><span class="cls_004">Control -</span><span class="cls_006"> continuously</span></div>
<div style="position:absolute;left:7.20px;top:500.64px" class="cls_002"><span class="cls_002">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:517.68px" class="cls_002"><span class="cls_002"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">A broader view on software development</span></div>
<div style="position:absolute;left:282.94px;top:153.60px" class="cls_009"><span class="cls_009">information planning</span></div>
<div style="position:absolute;left:360.50px;top:189.60px" class="cls_009"><span class="cls_009">boundary conditions</span></div>
<div style="position:absolute;left:245.88px;top:255.60px" class="cls_009"><span class="cls_009">documentation</span></div>
<div style="position:absolute;left:458.75px;top:279.60px" class="cls_009"><span class="cls_009">people</span></div>
<div style="position:absolute;left:369.63px;top:303.60px" class="cls_009"><span class="cls_009">software</span></div>
<div style="position:absolute;left:286.63px;top:315.60px" class="cls_009"><span class="cls_009">program</span></div>
<div style="position:absolute;left:88.31px;top:321.60px" class="cls_009"><span class="cls_009">input</span></div>
<div style="position:absolute;left:575.25px;top:321.60px" class="cls_009"><span class="cls_009">output</span></div>
<div style="position:absolute;left:369.63px;top:351.60px" class="cls_009"><span class="cls_009">program</span></div>
<div style="position:absolute;left:318.94px;top:423.60px" class="cls_009"><span class="cls_009">procedures</span></div>
<div style="position:absolute;left:5.67px;top:503.70px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">2</span></div>
<div style="position:absolute;left:5.67px;top:518.58px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:22.40px" class="cls_008"><span class="cls_008">Example: information plan of a university</span></div>
<div style="position:absolute;left:61.20px;top:56.48px" class="cls_008"><span class="cls_008">registration of student data</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Relations to other systems: personal data,</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_011"><span class="cls_011">courses, course results, alumni, …</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Use both by central administration, at faculty</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_011"><span class="cls_011">level, and possibly by students themselves</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Requires training courses to administrative</span></div>
<div style="position:absolute;left:88.20px;top:300.24px" class="cls_011"><span class="cls_011">personnel</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Authorization/security procedures</span></div>
<div style="position:absolute;left:61.20px;top:369.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Auditing procedures</span></div>
<div style="position:absolute;left:61.20px;top:404.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> External links, e.g. to scholarship funding</span></div>
<div style="position:absolute;left:88.20px;top:432.24px" class="cls_011"><span class="cls_011">agencies, ministry of education</span></div>
<div style="position:absolute;left:5.67px;top:503.70px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">3</span></div>
<div style="position:absolute;left:5.67px;top:518.58px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Contents of project plan</span></div>
<div style="position:absolute;left:375.70px;top:141.28px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> Staffing</span></div>
<div style="position:absolute;left:80.83px;top:144.88px" class="cls_012"><span class="cls_012">§</span><span class="cls_013"> Introduction</span></div>
<div style="position:absolute;left:375.70px;top:175.12px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:402.70px;top:175.12px" class="cls_013"><span class="cls_013">Methods and</span></div>
<div style="position:absolute;left:80.83px;top:183.04px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:107.82px;top:183.04px" class="cls_013"><span class="cls_013">Process model</span></div>
<div style="position:absolute;left:402.70px;top:204.16px" class="cls_013"><span class="cls_013">techniques</span></div>
<div style="position:absolute;left:80.83px;top:220.00px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:107.82px;top:220.00px" class="cls_013"><span class="cls_013">Organization of</span></div>
<div style="position:absolute;left:375.70px;top:238.24px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:402.70px;top:238.24px" class="cls_013"><span class="cls_013">Quality assurance</span></div>
<div style="position:absolute;left:107.82px;top:250.96px" class="cls_013"><span class="cls_013">project</span></div>
<div style="position:absolute;left:375.70px;top:272.32px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:402.70px;top:272.32px" class="cls_013"><span class="cls_013">Work packages</span></div>
<div style="position:absolute;left:80.83px;top:288.88px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:107.82px;top:288.88px" class="cls_013"><span class="cls_013">Standards,</span></div>
<div style="position:absolute;left:375.70px;top:307.12px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:402.70px;top:307.12px" class="cls_013"><span class="cls_013">Resources</span></div>
<div style="position:absolute;left:107.82px;top:320.08px" class="cls_013"><span class="cls_013">guidelines,</span></div>
<div style="position:absolute;left:375.70px;top:341.20px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:402.70px;top:341.20px" class="cls_013"><span class="cls_013">Budget and</span></div>
<div style="position:absolute;left:107.82px;top:351.04px" class="cls_013"><span class="cls_013">procedures</span></div>
<div style="position:absolute;left:402.70px;top:369.28px" class="cls_013"><span class="cls_013">schedule</span></div>
<div style="position:absolute;left:80.83px;top:388.96px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:107.82px;top:388.96px" class="cls_013"><span class="cls_013">Management</span></div>
<div style="position:absolute;left:375.70px;top:403.12px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:402.70px;top:403.12px" class="cls_013"><span class="cls_013">Changes</span></div>
<div style="position:absolute;left:107.82px;top:419.92px" class="cls_013"><span class="cls_013">activities</span></div>
<div style="position:absolute;left:375.70px;top:438.16px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:402.70px;top:438.16px" class="cls_013"><span class="cls_013">Delivery</span></div>
<div style="position:absolute;left:80.83px;top:456.88px" class="cls_012"><span class="cls_012">§</span></div>
<div style="position:absolute;left:107.82px;top:456.88px" class="cls_013"><span class="cls_013">Risks</span></div>
<div style="position:absolute;left:5.67px;top:503.70px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">4</span></div>
<div style="position:absolute;left:5.67px;top:518.58px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Project control</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Time, </span><span class="cls_014">both the number of man-months and the</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_014"><span class="cls_014">schedule</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Information, </span><span class="cls_014">mostly the documentation</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Organization, </span><span class="cls_014">people and team aspects</span></div>
<div style="position:absolute;left:61.20px;top:381.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Quality, </span><span class="cls_014">not an add-on feature; it has to be built in</span></div>
<div style="position:absolute;left:61.20px;top:450.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Money, </span><span class="cls_014">largely personnel</span></div>
<div style="position:absolute;left:5.67px;top:503.70px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">5</span></div>
<div style="position:absolute;left:5.67px;top:518.58px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Managing time</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Measuring progress is hard (“we spent half the</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_011"><span class="cls_011">money, so we must be halfway”)</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">Development models serve to manage time</span></div>
<div style="position:absolute;left:61.20px;top:311.22px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> More people  </span><span class="cls_015">Þ</span><span class="cls_011"> less time?</span></div>
<div style="position:absolute;left:90.80px;top:345.28px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Brooks’ law: adding people to a lae project makes it later</span></div>
<div style="position:absolute;left:5.67px;top:503.70px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">6</span></div>
<div style="position:absolute;left:5.67px;top:518.58px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Managing information</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">Documentation</span></div>
<div style="position:absolute;left:90.80px;top:178.24px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Technical documentation</span></div>
<div style="position:absolute;left:90.80px;top:207.28px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Current state of projects</span></div>
<div style="position:absolute;left:90.80px;top:236.32px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Changes agree upon</span></div>
<div style="position:absolute;left:90.80px;top:264.16px" class="cls_016"><span class="cls_016">§</span><span class="cls_017">…</span></div>
<div style="position:absolute;left:61.20px;top:294.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">Agile projects: less attention to explicit</span></div>
<div style="position:absolute;left:75.85px;top:323.28px" class="cls_011"><span class="cls_011">documentation, more on tacit knowledge held by</span></div>
<div style="position:absolute;left:75.85px;top:352.32px" class="cls_011"><span class="cls_011">people</span></div>
<div style="position:absolute;left:5.67px;top:503.70px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">7</span></div>
<div style="position:absolute;left:5.67px;top:518.58px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Managing people</span></div>
<div style="position:absolute;left:61.20px;top:144.32px" class="cls_018"><span class="cls_018">§</span><span class="cls_019"> Managing expectations</span></div>
<div style="position:absolute;left:61.20px;top:208.16px" class="cls_018"><span class="cls_018">§</span><span class="cls_019"> Building a team</span></div>
<div style="position:absolute;left:61.20px;top:271.28px" class="cls_018"><span class="cls_018">§</span><span class="cls_019"> Coordination of work</span></div>
<div style="position:absolute;left:5.67px;top:500.82px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:515.70px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Managing quality</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Quality has to be designed in</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Quality is not an afterthought</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Quality requirements often conflict with each other</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">Requires frequent interaction with stakeholders</span></div>
<div style="position:absolute;left:5.67px;top:503.70px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">9</span></div>
<div style="position:absolute;left:5.67px;top:518.58px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Managing cost</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Which factors influence cost?</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> What influences productivity?</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">Relation between cost and schedule</span></div>
<div style="position:absolute;left:5.67px;top:503.70px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">10</span></div>
<div style="position:absolute;left:5.67px;top:518.58px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_008"><span class="cls_008">Summary</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Project control concerns</span></div>
<div style="position:absolute;left:90.80px;top:178.24px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Time</span></div>
<div style="position:absolute;left:90.80px;top:207.28px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Information</span></div>
<div style="position:absolute;left:90.80px;top:236.32px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Organization</span></div>
<div style="position:absolute;left:90.80px;top:264.16px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Quality</span></div>
<div style="position:absolute;left:90.80px;top:293.20px" class="cls_016"><span class="cls_016">§</span><span class="cls_017"> Money</span></div>
<div style="position:absolute;left:61.20px;top:357.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011">Agile projects do less planning than document-</span></div>
<div style="position:absolute;left:75.85px;top:386.16px" class="cls_011"><span class="cls_011">driven projects</span></div>
<div style="position:absolute;left:5.67px;top:503.70px" class="cls_007"><span class="cls_007">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">11</span></div>
<div style="position:absolute;left:5.67px;top:518.58px" class="cls_007"><span class="cls_007"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>

</body>
</html>
