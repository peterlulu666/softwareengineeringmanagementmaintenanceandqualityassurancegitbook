<H2><A name="Overview of COCOMO"><FONT color=#000000><FONT size=4>Overview of 
COCOMO</A><FONT color=#000000><FONT size=4> </FONT></H2>
<P><FONT size=4>The COCOMO cost estimation model is used by thousands of 
software project managers, and is based on a study of hundreds of software 
projects. Unlike other cost estimation models, COCOMO is an open model, so all 
of the details are published, including:</FONT> </P>
<UL>
  <LI><FONT size=4>The underlying cost estimation equations</FONT> 
  <LI><FONT size=4>Every assumption made in the model (e.g. "the project will 
  enjoy good management")</FONT> 
  <LI><FONT size=4>Every definition (e.g. the precise definition of the Product 
  Design phase of a project)</FONT> 
  <LI><FONT size=4>The costs included in an estimate are explicitly stated (e.g. 
  project managers are included, secretaries aren't)</FONT> </LI></UL>
<P><FONT size=4>Because COCOMO is well defined, and because it doesn't rely upon 
proprietary estimation algorithms, Costar offers these advantages to its 
users:</FONT></P>
<UL>
  <LI><FONT size=4>COCOMO estimates are more objective and repeatable than 
  estimates made by methods relying on proprietary models</FONT> 
  <LI><FONT size=4>COCOMO can be calibrated to reflect your software development 
  environment, and to produce more accurate estimates</FONT> </LI></UL>
<P><FONT size=4>Costar is a faithful implementation of the COCOMO model that is 
easy to use on small projects, and yet powerful enough to plan and control large 
projects.</FONT></P>
<P><FONT size=4>Typically, you'll start with only a rough description of the 
software system that you'll be developing, and you'll use Costar to give you 
early estimates about the proper schedule and staffing levels. As you refine 
your knowledge of the problem, and as you design more of the system, you can use 
Costar to produce more and more refined estimates.</FONT></P>
<P><FONT size=4>Costar allows you to define a software structure to meet your 
needs. Your initial estimate might be made on the basis of a system containing 
3,000 lines of code. Your second estimate might be more refined so that you now 
understand that your system will consist of two subsystems (and you'll have a 
more accurate idea about how many lines of code will be in each of the 
subsystems). Your next estimate will continue the process -- you can use Costar 
to define the components of each subsystem. Costar permits you to continue this 
process until you arrive at the level of detail that suits your 
needs.</FONT></P>
<P><FONT size=4><FONT color=#000000>One word of warning</FONT>: It is so easy to 
use Costar to make software cost estimates, that it's possible to misuse it -- 
every Costar user should spend the time to learn the underlying COCOMO 
assumptions and definitions from <EM><B>Software Engineering Economics</B></EM> 
and <EM><B>Software Cost Estimation with COCOMO II</B></EM>.</FONT></P>
<H2><FONT color=#000000><FONT size=4>Introduction to the COCOMO Model 
</FONT></FONT></H2>
<P><FONT size=4>The most fundamental calculation in the COCOMO model is the use 
of the Effort Equation to estimate the number of Person-Months required to 
develop a project. Most of the other COCOMO results, including the estimates for 
Requirements and Maintenance, are derived from this quantity.</FONT></P>
<H3><A name="Delivered Source Instructions"><FONT color=#000000 size=4>Source 
Lines of Code</FONT></A></H3>
<P><FONT size=4>The COCOMO calculations are based on your estimates of a 
project's size in Source Lines of Code (SLOC). SLOC is defined such 
that:</FONT></P>
<UL>
  <LI><FONT size=4>Only Source lines that are DELIVERED as part of the product 
  are included -- test drivers and other support software is excluded</FONT> 
  <LI><FONT size=4>SOURCE lines are created by the project staff -- code created 
  by applications generators is excluded</FONT> 
  <LI><FONT size=4>One SLOC is one logical line of code</FONT> 
  <LI><FONT size=4>Declarations are counted as SLOC</FONT> 
  <LI><FONT size=4>Comments are not counted as SLOC</FONT> </LI></UL>
<P><FONT size=4>The original COCOMO 81 model was defined in terms of Delivered 
Source Instructions, which are very similar to SLOC.&nbsp; The major difference 
between DSI and SLOC is that a single Source Line of Code may be several 
physical lines.&nbsp; For example, an "if-then-else" statement would be counted 
as one SLOC, but might be counted as several DSI.</FONT></P>
<H3><A name="The Development Mode"><FONT color=#000000><FONT size=4>The Scale 
Drivers</FONT></FONT></A></H3>
<P><FONT size=4>In the COCOMO II model, some of the most important factors 
contributing to a project's duration and cost are the Scale Drivers. You set 
each Scale Driver to describe your project; these Scale Drivers determine the 
exponent used in the Effort Equation.</FONT></P>
<P><FONT size=4>The 5 Scale Drivers are:</FONT></P>
<UL>
  <LI><FONT size=4>Precedentedness</FONT> 
  <LI><FONT size=4>Development Flexibility</FONT> 
  <LI><FONT size=4>Architecture / Risk Resolution</FONT> 
  <LI><FONT size=4>Team Cohesion</FONT> 
  <LI><FONT size=4>Process Maturity</FONT> </LI></UL>
<P><FONT size=4>Note that the Scale Drivers have replaced the Development Mode 
of COCOMO 81.&nbsp; The first two Scale Drivers, Precedentedness and Development 
Flexibility actually describe much the same influences that the original 
Development Mode did.</FONT></P>
<H3><FONT color=#000000 size=4>Cost Drivers</FONT></H3>
<P><FONT size=4>COCOMO II has 17 cost drivers  you assess your project, 
development environment, and team to set each cost driver. The cost drivers are 
multiplicative factors that determine the effort required to complete your 
software project. For example, if your project will develop software that 
controls an airplane's flight, you would set the Required Software Reliability 
(RELY) cost driver to Very High. That rating corresponds to an effort multiplier 
of 1.26, meaning that your project will require 26% more effort than a typical 
software project.</FONT></P>
<P><FONT size=4><A href="http://www.softstarsystems.com/cdtable.htm">Click 
here</A> to see which Cost Drivers are in which Costar models.</FONT></P>
<P><FONT size=4>COCOMO II defines each of the cost drivers, and the Effort 
Multiplier associated with each rating. Check the Costar help for details about 
the definitions and how to set the cost drivers.</FONT></P>
<H3><FONT color=#000000 size=4>COCOMO II Effort Equation</FONT></H3>
<P><FONT size=4>The COCOMO II model makes its estimates of required effort 
(measured in Person-Months  PM) based primarily on your estimate of the 
software project's size (as measured in thousands of SLOC, 
KSLOC)):<BR></FONT><B><BR>&nbsp;&nbsp; </B><FONT size=4>&nbsp; Effort = 2.94 * 
EAF * (KSLOC)<SUP>E</SUP><BR><BR>Where<BR>&nbsp;&nbsp;&nbsp; EAF&nbsp;&nbsp; Is 
the Effort Adjustment Factor derived from the Cost Drivers<BR>&nbsp;&nbsp;&nbsp; 
E&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Is an exponent derived from the five 
Scale Drivers<BR><BR>As an example, a project with all Nominal Cost Drivers and 
Scale Drivers would have an EAF of 1.00 and exponent, E, of 1.0997. Assuming 
that the project is projected to consist of 8,000 source lines of code, COCOMO 
II estimates that 28.9 Person-Months of effort is required to complete 
it:<BR><BR>&nbsp;&nbsp;&nbsp; Effort = 2.94 * (1.0) * (8)<SUP>1.0997</SUP> = 
28.9 Person-Months</FONT></P>
<H3><FONT color=#000000 size=4>Effort Adjustment Factor</FONT></H3>
<P align=left><FONT size=4>The Effort Adjustment Factor in the effort equation 
is simply the product of the effort multipliers corresponding to each of the 
cost drivers for your project.<BR><BR>For example, if your project is rated Very 
High for Complexity (effort multiplier of 1.34), and Low for Language &amp; 
Tools Experience (effort multiplier of 1.09), and all of the other cost drivers 
are rated to be Nominal (effort multiplier of 1.00), the EAF is the product of 
1.34 and 1.09.<BR><BR>&nbsp;&nbsp;&nbsp; Effort Adjustment Factor = EAF = 1.34 * 
1.09 = 1.46<BR><BR>&nbsp;&nbsp;&nbsp; Effort = 2.94 * (1.46) * 
(8)<SUP>1.0997</SUP> = 42.3 Person-Months</FONT></P>
<H3><FONT color=#000000 size=4>COCOMO II Schedule Equation</FONT></H3>
<P align=left><FONT size=4>The COCOMO II schedule equation predicts the number 
of months required to complete your software project. The duration of a project 
is based on the effort predicted by the effort 
equation:<BR><BR>&nbsp;&nbsp;&nbsp; Duration = 3.67 * 
(Effort)<SUP>SE</SUP><BR><BR>Where<BR>&nbsp;&nbsp;&nbsp; Effort&nbsp;&nbsp; Is 
the effort from the COCOMO II effort equation<BR>&nbsp;&nbsp;&nbsp; 
SE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Is the schedule equation exponent 
derived from the five Scale Drivers<BR><BR>Continuing the example, and 
substituting the exponent of 0.3179 that is calculated from the scale drivers, 
yields an estimate of just over a year, and an average staffing of between 3 and 
4 people:<BR><BR>&nbsp;&nbsp;&nbsp; Duration = 3.67 * (42.3)<SUP>0.3179</SUP> = 
12.1 months<BR><BR>&nbsp;&nbsp;&nbsp; Average staffing = (42.3 Person-Months) / 
(12.1 Months) = 3.5 people</FONT></P>
<H3 align=left><FONT color=#000000 size=4>The SCED Cost Driver</FONT></H3>
<P align=left><FONT size=4>The COCOMO cost driver for Required Development 
Schedule (SCED) is unique, and requires a special explanation.<BR><BR>The SCED 
cost driver is used to account for the observation that a project developed on 
an accelerated schedule will require more effort than a project developed on its 
optimum schedule. A SCED rating of Very Low corresponds to an Effort Multiplier 
of 1.43 (in the COCOMO II.2000 model) and means that you intend to finish your 
project in 75% of the optimum schedule (as determined by a previous COCOMO 
estimate). Continuing the example used earlier, but assuming that SCED has a 
rating of Very Low, COCOMO produces these estimates:<BR><BR>&nbsp;&nbsp;&nbsp; 
Duration = 75% * 12.1 Months = 9.1 Months<BR><BR>&nbsp;&nbsp;&nbsp; Effort 
Adjustment Factor = EAF = 1.34 * 1.09 * 1.43 = 2.09<BR><BR>&nbsp;&nbsp;&nbsp; 
Effort = 2.94 * (2.09) * (8)<SUP>1.0997 </SUP>= 60.4 Person-Months</FONT> 
<B><BR></B><FONT size=4><BR>&nbsp;&nbsp;&nbsp; Average staffing = (60.4 
Person-Months) / (9.1 Months) = 6.7 people<BR><BR>Notice that the calculation of 
duration isn't based directly on the effort (number of Person-Months)  instead 
it's based on the schedule that would have been required for the project 
assuming it had been developed on the nominal schedule. Remember that the SCED 
cost driver means "accelerated from the nominal schedule".</FONT></P>
<P align=left><FONT size=4>The Costar command <I><B>Constraints | Constrain 
Project</B> </I>displays a dialog box that lets you trade off duration vs. 
effort (SCED is set for you automatically).&nbsp; You can use the dialog box to 
constrain your project to have a fixed duration, or a fixed cost.</FONT></P>
