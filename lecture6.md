<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:48.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:48.0px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:27.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:27.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture6/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:77.50px;top:34.96px" class="cls_002"><span class="cls_002">Managing Software Quality</span></div>
<div style="position:absolute;left:33.70px;top:327.16px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:33.70px;top:369.16px" class="cls_004"><span class="cls_004">§</span><span class="cls_003">Quality cannot be added as an afterthought</span></div>
<div style="position:absolute;left:33.70px;top:411.16px" class="cls_004"><span class="cls_004">§</span><span class="cls_003">To measure is to know</span></div>
<div style="position:absolute;left:33.70px;top:452.20px" class="cls_004"><span class="cls_004">§</span><span class="cls_003">Product quality vs process quality</span></div>
<div style="position:absolute;left:7.20px;top:501.60px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:519.60px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Commitment to quality pays off</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">2</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Approaches to quality</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Quality of the product versus quality of the process</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">Check whether (product or process) </span><span class="cls_010">conforms to</span></div>
<div style="position:absolute;left:75.85px;top:242.16px" class="cls_009"><span class="cls_009">certain norms</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Improve quality by improving the product or</span></div>
<div style="position:absolute;left:75.85px;top:340.32px" class="cls_009"><span class="cls_009">process</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">3</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Approaches to quality</span></div>
<div style="position:absolute;left:248.29px;top:158.80px" class="cls_011"><span class="cls_011">Conformance</span></div>
<div style="position:absolute;left:504.67px;top:158.80px" class="cls_011"><span class="cls_011">Improvement</span></div>
<div style="position:absolute;left:45.42px;top:274.72px" class="cls_011"><span class="cls_011">Product</span></div>
<div style="position:absolute;left:269.29px;top:274.72px" class="cls_012"><span class="cls_012">ISO 9126</span></div>
<div style="position:absolute;left:501.79px;top:274.72px" class="cls_012"><span class="cls_012">‘best practices’</span></div>
<div style="position:absolute;left:538.29px;top:362.56px" class="cls_012"><span class="cls_012">CMM</span></div>
<div style="position:absolute;left:269.29px;top:376.96px" class="cls_012"><span class="cls_012">ISO 9001</span></div>
<div style="position:absolute;left:45.42px;top:391.36px" class="cls_011"><span class="cls_011">Process</span></div>
<div style="position:absolute;left:532.79px;top:391.60px" class="cls_012"><span class="cls_012">SPICE</span></div>
<div style="position:absolute;left:285.79px;top:406.00px" class="cls_012"><span class="cls_012">SQA</span></div>
<div style="position:absolute;left:527.29px;top:420.64px" class="cls_012"><span class="cls_012">Bootstap</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">4</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">What is quality?</span></div>
<div style="position:absolute;left:365.67px;top:228.00px" class="cls_013"><span class="cls_013">+</span></div>
<div style="position:absolute;left:164.42px;top:379.60px" class="cls_012"><span class="cls_012">software</span></div>
<div style="position:absolute;left:513.17px;top:379.60px" class="cls_012"><span class="cls_012">measures</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">5</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">How to measure “complexity”?</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> The length of the program?</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> The number of goto’s?</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> The number of if-statements?</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> The sum of these numbers?</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Yet something else?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">6</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">A measurement framework</span></div>
<div style="position:absolute;left:141.19px;top:153.60px" class="cls_014"><span class="cls_014">Formal world</span></div>
<div style="position:absolute;left:446.06px;top:153.60px" class="cls_014"><span class="cls_014">“Real” world</span></div>
<div style="position:absolute;left:153.69px;top:202.08px" class="cls_014"><span class="cls_014">scale type</span></div>
<div style="position:absolute;left:104.16px;top:241.92px" class="cls_014"><span class="cls_014">belongs to</span></div>
<div style="position:absolute;left:179.69px;top:282.00px" class="cls_014"><span class="cls_014">unit</span></div>
<div style="position:absolute;left:477.55px;top:280.08px" class="cls_014"><span class="cls_014">entity</span></div>
<div style="position:absolute;left:85.89px;top:322.08px" class="cls_014"><span class="cls_014">expressed in</span></div>
<div style="position:absolute;left:509.59px;top:321.60px" class="cls_014"><span class="cls_014">has</span></div>
<div style="position:absolute;left:335.99px;top:345.60px" class="cls_014"><span class="cls_014">measures</span></div>
<div style="position:absolute;left:172.69px;top:361.44px" class="cls_014"><span class="cls_014">value</span></div>
<div style="position:absolute;left:466.55px;top:361.44px" class="cls_014"><span class="cls_014">attribute</span></div>
<div style="position:absolute;left:108.97px;top:402.00px" class="cls_014"><span class="cls_014">computes  used in</span></div>
<div style="position:absolute;left:508.74px;top:402.48px" class="cls_014"><span class="cls_014">part of</span></div>
<div style="position:absolute;left:313.72px;top:423.60px" class="cls_014"><span class="cls_014">formalizes</span></div>
<div style="position:absolute;left:99.69px;top:442.08px" class="cls_014"><span class="cls_014">attribute-relation model</span></div>
<div style="position:absolute;left:434.55px;top:442.08px" class="cls_014"><span class="cls_014">attribute relation</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">7</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Scale types</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Nominal: just classification</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Ordinal: linear ordering (>)</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Interval: like ordinal, but interval between values</span></div>
<div style="position:absolute;left:88.20px;top:311.28px" class="cls_009"><span class="cls_009">is the same (so average has a meaning)</span></div>
<div style="position:absolute;left:61.20px;top:346.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Ratio: like interval, but there is a 0 (zero) (so A can</span></div>
<div style="position:absolute;left:88.20px;top:375.12px" class="cls_009"><span class="cls_009">be twice B)</span></div>
<div style="position:absolute;left:61.20px;top:409.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Absolute: counting number of occurrences</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">8</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Representation condition</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> A measure M is </span><span class="cls_010">valid</span><span class="cls_009"> if it satisfies the</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_009"><span class="cls_009">representation condition, i.e. if A>B in the real</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_009"><span class="cls_009">world, then M(A)>M(B)</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> E.g. if we measure complexity as the number of if-</span></div>
<div style="position:absolute;left:88.20px;top:300.24px" class="cls_009"><span class="cls_009">statements, then:</span></div>
<div style="position:absolute;left:97.20px;top:334.24px" class="cls_015"><span class="cls_015">§</span><span class="cls_012"> Two programs with the same number of if-statements are</span></div>
<div style="position:absolute;left:119.70px;top:358.24px" class="cls_012"><span class="cls_012">equally complex</span></div>
<div style="position:absolute;left:97.20px;top:386.32px" class="cls_015"><span class="cls_015">§</span><span class="cls_012"> If program A has more if-statements than program B, then A</span></div>
<div style="position:absolute;left:119.70px;top:410.32px" class="cls_012"><span class="cls_012">is more complex than B</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_007"><span class="cls_007">9</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">More on measures</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_010">Direct</span><span class="cls_009"> versus </span><span class="cls_010">indirect</span><span class="cls_009"> measures</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_010"> Internal </span><span class="cls_009">versus </span><span class="cls_010">external</span><span class="cls_009"> attributes</span></div>
<div style="position:absolute;left:90.80px;top:247.12px" class="cls_015"><span class="cls_015">§</span><span class="cls_012"> External attributes can only be measured indirectly</span></div>
<div style="position:absolute;left:90.80px;top:276.16px" class="cls_015"><span class="cls_015">§</span><span class="cls_012"> Most quality attributes are external</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Scale type of a combined measure is the ‘weakest’</span></div>
<div style="position:absolute;left:75.85px;top:363.12px" class="cls_009"><span class="cls_009">of the scale types of its constituents</span></div>
<div style="position:absolute;left:90.80px;top:397.12px" class="cls_015"><span class="cls_015">§</span><span class="cls_012"> This is often violated; see cost estimation models</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">10</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Quality attributes (McCall)</span></div>
<div style="position:absolute;left:61.20px;top:140.32px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Product operation</span></div>
<div style="position:absolute;left:90.80px;top:164.16px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Correctnessdoes it do what I want?</span></div>
<div style="position:absolute;left:90.80px;top:185.28px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Reliability</span></div>
<div style="position:absolute;left:277.20px;top:185.28px" class="cls_014"><span class="cls_014">does it do it accurately all of the time?</span></div>
<div style="position:absolute;left:90.80px;top:207.12px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Efficiency</span></div>
<div style="position:absolute;left:277.20px;top:207.12px" class="cls_014"><span class="cls_014">will it run on my hardware as well as it can?</span></div>
<div style="position:absolute;left:90.80px;top:229.20px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Integrity</span></div>
<div style="position:absolute;left:277.20px;top:229.20px" class="cls_014"><span class="cls_014">is it secure?</span></div>
<div style="position:absolute;left:90.80px;top:250.32px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Usability</span></div>
<div style="position:absolute;left:277.20px;top:250.32px" class="cls_014"><span class="cls_014">can I use it?</span></div>
<div style="position:absolute;left:61.20px;top:272.32px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Product revision</span></div>
<div style="position:absolute;left:90.80px;top:296.16px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Maintainability</span></div>
<div style="position:absolute;left:277.20px;top:296.16px" class="cls_014"><span class="cls_014">can I fix it?</span></div>
<div style="position:absolute;left:90.80px;top:317.28px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Testability</span></div>
<div style="position:absolute;left:277.20px;top:317.28px" class="cls_014"><span class="cls_014">can I test it?</span></div>
<div style="position:absolute;left:90.80px;top:339.12px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Flexibility</span></div>
<div style="position:absolute;left:277.20px;top:339.12px" class="cls_014"><span class="cls_014">can I change it?</span></div>
<div style="position:absolute;left:61.20px;top:360.16px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Product transition</span></div>
<div style="position:absolute;left:90.80px;top:385.20px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Portability</span></div>
<div style="position:absolute;left:277.20px;top:385.20px" class="cls_014"><span class="cls_014">will I be able to use it on another machine?</span></div>
<div style="position:absolute;left:90.80px;top:406.32px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Reusability</span></div>
<div style="position:absolute;left:277.20px;top:406.32px" class="cls_014"><span class="cls_014">will I be able to reuse some of the software?</span></div>
<div style="position:absolute;left:90.80px;top:428.16px" class="cls_016"><span class="cls_016">§</span><span class="cls_014"> Interoperability</span></div>
<div style="position:absolute;left:277.20px;top:428.16px" class="cls_014"><span class="cls_014">will I be able to interface it with another system?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">11</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Taxonomy of quality attributes (ISO 9126)</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Functionality</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Reliability</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Usability</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Efficiency</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Maintainability</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Portability</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">12</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">ISO 9126 (cnt’d)</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> ISO 9126 measures ‘quality in use’: the extent to</span></div>
<div style="position:absolute;left:75.85px;top:208.32px" class="cls_009"><span class="cls_009">which users can achieve their goal</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Quality in use is modeled in four characteristics:</span></div>
<div style="position:absolute;left:90.80px;top:276.16px" class="cls_015"><span class="cls_015">§</span><span class="cls_012"> Effectiveness</span></div>
<div style="position:absolute;left:90.80px;top:305.20px" class="cls_015"><span class="cls_015">§</span><span class="cls_012"> Productivity</span></div>
<div style="position:absolute;left:90.80px;top:334.24px" class="cls_015"><span class="cls_015">§</span><span class="cls_012"> Safety</span></div>
<div style="position:absolute;left:90.80px;top:362.32px" class="cls_015"><span class="cls_015">§</span><span class="cls_012"> Satisfaction</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">13</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Perspectives on quality</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Transcendent (“I really like this program”)</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> User-based (“fitness for use”)</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Product-based (based on attributes of the</span></div>
<div style="position:absolute;left:88.20px;top:311.28px" class="cls_009"><span class="cls_009">software)</span></div>
<div style="position:absolute;left:61.20px;top:381.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Manufacturing-based (conformance to specs)</span></div>
<div style="position:absolute;left:61.20px;top:450.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Value-based (balancing time and cost vs profits)</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">14</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">ISO 9001</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Model for quality assurance in design,</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_009"><span class="cls_009">development, production, installation and</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_009"><span class="cls_009">servicing</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Basic premise: confidence in product</span></div>
<div style="position:absolute;left:88.20px;top:300.24px" class="cls_009"><span class="cls_009">conformance can be obtained by adequate</span></div>
<div style="position:absolute;left:88.20px;top:329.28px" class="cls_009"><span class="cls_009">demonstration of supplier’s capabilities in</span></div>
<div style="position:absolute;left:88.20px;top:357.12px" class="cls_009"><span class="cls_009">processes (design, development, …)</span></div>
<div style="position:absolute;left:61.20px;top:427.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> ISO registration by an officially accredited body,</span></div>
<div style="position:absolute;left:88.20px;top:455.28px" class="cls_009"><span class="cls_009">re-registration every three years</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">15</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Capability Maturity Model (CMM)</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Initial level: software development is ad-hoc</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">Repeatable level: basic processes are in place</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">Defined level: there are </span><span class="cls_010">standard</span><span class="cls_009"> processes</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Quantitatively managed level: data is gatheread</span></div>
<div style="position:absolute;left:75.85px;top:311.28px" class="cls_009"><span class="cls_009">and analyzed routinely</span></div>
<div style="position:absolute;left:61.20px;top:346.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Optimizing level: stable base, data is gathered to</span></div>
<div style="position:absolute;left:75.85px;top:375.12px" class="cls_009"><span class="cls_009">improve the process</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">16</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.14px" class="cls_006"><span class="cls_006">Initial  </span><span class="cls_017">Þ</span><span class="cls_006"> repeatable level</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">Requirements management</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Project planning</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Project monitoring and control</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Supplier agreement management</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Measurement and analysis</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Process and product quality assurance</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">Configuration management</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">17</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.14px" class="cls_006"><span class="cls_006">Repeatable  </span><span class="cls_017">Þ</span><span class="cls_006"> defined level</span></div>
<div style="position:absolute;left:61.20px;top:144.16px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Requirements development</span></div>
<div style="position:absolute;left:61.20px;top:173.20px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Technical solution</span></div>
<div style="position:absolute;left:61.20px;top:202.24px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Product integration</span></div>
<div style="position:absolute;left:61.20px;top:231.28px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Verification</span></div>
<div style="position:absolute;left:61.20px;top:260.32px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Validation</span></div>
<div style="position:absolute;left:61.20px;top:288.16px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Organization process focus</span></div>
<div style="position:absolute;left:61.20px;top:317.20px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Organization process definition</span></div>
<div style="position:absolute;left:61.20px;top:346.24px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Organizational training</span></div>
<div style="position:absolute;left:61.20px;top:375.28px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Integrated project management</span></div>
<div style="position:absolute;left:61.20px;top:404.32px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Risk management</span></div>
<div style="position:absolute;left:61.20px;top:432.16px" class="cls_015"><span class="cls_015">§</span><span class="cls_011"> Decision analysis and resolution</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">18</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">CMM: critical notes</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Most appropriate for big companies</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Pure CMM approach may stifle creativity</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">Crude 5-point scale (now: CMMI)</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">19</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:22.40px" class="cls_006"><span class="cls_006">Get started on Software Process</span></div>
<div style="position:absolute;left:61.20px;top:56.48px" class="cls_006"><span class="cls_006">Improvement (SPI)</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Formulate hypotheses</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">Carefully select metrics</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">Collect data</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Interpret data</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Initiate improvement actions</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Iterate</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_008"><span class="cls_008">§</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">20</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Lessons w.r.t. data collection</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">Closed loop principle: result of data analysis must</span></div>
<div style="position:absolute;left:75.85px;top:277.20px" class="cls_009"><span class="cls_009">be useful to supplier of data</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009">Do not use data collected for other purposes</span></div>
<div style="position:absolute;left:61.20px;top:346.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Focus on continuous improvement</span></div>
<div style="position:absolute;left:61.20px;top:381.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Only collect data you really need</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">21</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture6/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_006"><span class="cls_006">Summary</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Product quality versus process quality</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Quality conformance versus quality improvement</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Quality has to be actively pursued</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> There are different notions of quality</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Quality has many aspects</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Quality is hard to measure</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_007"><span class="cls_007">22</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>

</body>
</html>
