<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:40.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:40.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:25.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:23.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:23.8px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:italic;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:47.6px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:47.6px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture18/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:88.88px;top:60.32px" class="cls_002"><span class="cls_002">Component-Based Software</span></div>
<div style="position:absolute;left:241.18px;top:108.32px" class="cls_002"><span class="cls_002">Engineering</span></div>
<div style="position:absolute;left:75.20px;top:360.44px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:75.20px;top:396.44px" class="cls_003"><span class="cls_003">• assemble systems out of (reusable) components</span></div>
<div style="position:absolute;left:75.20px;top:432.44px" class="cls_003"><span class="cls_003">• compatibility of components</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">LEGO analogy</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Set of building blocks in different shapes and</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_007"><span class="cls_007">colors</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Can be combined in different ways</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Composition through small stubs in one and</span></div>
<div style="position:absolute;left:75.85px;top:271.20px" class="cls_007"><span class="cls_007">corresponding holes in another building block</span></div>
<div style="position:absolute;left:61.20px;top:340.26px" class="cls_006"><span class="cls_006">§</span><span class="cls_008">Þ </span><span class="cls_007">LEGO blocks are generic and easily composable</span></div>
<div style="position:absolute;left:61.20px;top:409.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> LEGO can be combined with LEGO, not with</span></div>
<div style="position:absolute;left:75.85px;top:438.24px" class="cls_007"><span class="cls_007">Meccano</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:62.45px;top:58.64px" class="cls_005"><span class="cls_005">Why CBSE?</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">CBSE increases quality, especially evolvability and</span></div>
<div style="position:absolute;left:75.85px;top:208.32px" class="cls_007"><span class="cls_007">maintainability</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">CBSE increases productivity</span></div>
<div style="position:absolute;left:61.20px;top:346.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">CBSE shortens development time</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Component model</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Defines the types of building block, and the recipe</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_007"><span class="cls_007">for putting them together</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> More precisely, a component model defines</span></div>
<div style="position:absolute;left:75.85px;top:271.20px" class="cls_007"><span class="cls_007">standards for:</span></div>
<div style="position:absolute;left:90.80px;top:305.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Properties individual components must satisfy</span></div>
<div style="position:absolute;left:90.80px;top:334.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Methods and mechanisms for composing components</span></div>
<div style="position:absolute;left:61.20px;top:398.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Consequently, a component has to conform to</span></div>
<div style="position:absolute;left:75.85px;top:427.20px" class="cls_007"><span class="cls_007">some component model</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">A software component:</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Implements some functionality</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Has explicit dependencies through provides and</span></div>
<div style="position:absolute;left:75.85px;top:242.16px" class="cls_007"><span class="cls_007">required interfaces</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Communicates through its interfaces only</span></div>
<div style="position:absolute;left:61.20px;top:381.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Has structure and behavior that conforms to a</span></div>
<div style="position:absolute;left:75.85px;top:409.20px" class="cls_007"><span class="cls_007">component model</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">A component technology</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Is the implementation of a component model, by</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_007"><span class="cls_007">means of:</span></div>
<div style="position:absolute;left:90.80px;top:207.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Standards and guidelines for the implementation and</span></div>
<div style="position:absolute;left:105.95px;top:231.28px" class="cls_011"><span class="cls_011">execution of software components</span></div>
<div style="position:absolute;left:90.80px;top:260.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Executable software that supports the implementation,</span></div>
<div style="position:absolute;left:105.95px;top:284.32px" class="cls_011"><span class="cls_011">assembly, deployment, execution of components</span></div>
<div style="position:absolute;left:61.20px;top:348.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Examples: EJB, COM+, .NET, CORBA</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Component forms</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Component goes through different stages:</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_007"><span class="cls_007">development, packaging, distribution, deployment,</span></div>
<div style="position:absolute;left:75.85px;top:202.32px" class="cls_007"><span class="cls_007">execution</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Across these stages, components are represented</span></div>
<div style="position:absolute;left:75.85px;top:265.20px" class="cls_007"><span class="cls_007">in different forms:</span></div>
<div style="position:absolute;left:90.80px;top:299.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> During development: UML, e.g.</span></div>
<div style="position:absolute;left:90.80px;top:328.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> When packaging: in a .zip file, e.g.</span></div>
<div style="position:absolute;left:90.80px;top:357.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> In the execution stage: blocks of code and data</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Characterization of component forms</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:22.40px" class="cls_005"><span class="cls_005">Component specification vs component</span></div>
<div style="position:absolute;left:61.20px;top:56.48px" class="cls_005"><span class="cls_005">interface</span></div>
<div style="position:absolute;left:61.20px;top:170.64px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Interface describes how components interact:</span></div>
<div style="position:absolute;left:75.85px;top:199.68px" class="cls_012"><span class="cls_012">usage contract</span></div>
<div style="position:absolute;left:61.20px;top:268.56px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Specification is about the component as a whole,</span></div>
<div style="position:absolute;left:75.85px;top:297.60px" class="cls_007"><span class="cls_007">while an interface might be about part of a</span></div>
<div style="position:absolute;left:75.85px;top:326.64px" class="cls_007"><span class="cls_007">component only</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_009"><span class="cls_009">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Hiding of component internals</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Black box: only specification is known</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Glass box: internals may be inspected, but not</span></div>
<div style="position:absolute;left:75.85px;top:242.16px" class="cls_007"><span class="cls_007">changed</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Grey box: part of the internals may be inspected,</span></div>
<div style="position:absolute;left:75.85px;top:306.24px" class="cls_007"><span class="cls_007">limited modification is allowed</span></div>
<div style="position:absolute;left:61.20px;top:340.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> While box: component is open to inspection and</span></div>
<div style="position:absolute;left:75.85px;top:369.12px" class="cls_007"><span class="cls_007">modification</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Managing quality in CBSE</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_012"> Who</span><span class="cls_007"> manages the quality: the component, or the</span></div>
<div style="position:absolute;left:75.85px;top:208.32px" class="cls_007"><span class="cls_007">execution platform</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_012"> Scope</span><span class="cls_007"> of management: per-collaboration, or</span></div>
<div style="position:absolute;left:75.85px;top:306.24px" class="cls_007"><span class="cls_007">system-wide</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Common features of component models</span></div>
<div style="position:absolute;left:61.20px;top:142.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Infrastructure mechanisms, for binding, execution, etc</span></div>
<div style="position:absolute;left:61.20px;top:168.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Instantiation</span></div>
<div style="position:absolute;left:61.20px;top:195.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Binding (design time, compile time, …)</span></div>
<div style="position:absolute;left:61.20px;top:221.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Mechanisms for communication between components</span></div>
<div style="position:absolute;left:61.20px;top:248.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Discovery of components</span></div>
<div style="position:absolute;left:61.20px;top:274.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Announcement of component capabilities (interfaces)</span></div>
<div style="position:absolute;left:61.20px;top:300.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Development support</span></div>
<div style="position:absolute;left:61.20px;top:327.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Language independence</span></div>
<div style="position:absolute;left:61.20px;top:353.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Platform independence</span></div>
<div style="position:absolute;left:61.20px;top:380.32px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Analysis support</span></div>
<div style="position:absolute;left:61.20px;top:406.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Support for upgrading and extension</span></div>
<div style="position:absolute;left:61.20px;top:432.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_013"> Support for quality properties</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Development process in CBSE</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Two separate development processes:</span></div>
<div style="position:absolute;left:90.80px;top:213.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Development of components</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Development of systems out of components</span></div>
<div style="position:absolute;left:61.20px;top:306.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Separate process to </span><span class="cls_012">assess</span><span class="cls_007"> components</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">CBSE system development process</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Requirements: also considers </span><span class="cls_012">availability</span><span class="cls_007"> of</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_007"><span class="cls_007">components (like in COTS)</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Analysis and design: very similar to what we</span></div>
<div style="position:absolute;left:75.85px;top:237.12px" class="cls_007"><span class="cls_007">normally do</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Implementation: less coding, focus on selection of</span></div>
<div style="position:absolute;left:75.85px;top:300.24px" class="cls_007"><span class="cls_007">components, provision of glue code</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Integration: largely automated</span></div>
<div style="position:absolute;left:61.20px;top:369.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Testing: verification of components is necessary</span></div>
<div style="position:absolute;left:61.20px;top:404.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Release: as in classical approaches</span></div>
<div style="position:absolute;left:61.20px;top:438.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Maintenance: replace components</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Component assessment</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Find components</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Verify components</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Store components in repository</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Component development process</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Components are intended for reuse</span></div>
<div style="position:absolute;left:277.20px;top:219.06px" class="cls_014"><span class="cls_014">Þ</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Managing requirements is more difficult</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> More effort required to develop reusable</span></div>
<div style="position:absolute;left:75.85px;top:381.12px" class="cls_007"><span class="cls_007">components</span></div>
<div style="position:absolute;left:61.20px;top:415.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> More effort in documentation for consumers</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Component development process</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Requirements: combination of top-down (from</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_007"><span class="cls_007">system) and bottom-up (generality)</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Analysis and design: generality is an issue,</span></div>
<div style="position:absolute;left:75.85px;top:237.12px" class="cls_007"><span class="cls_007">assumptions about system (use) must be made</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Implementation: largely determined by component</span></div>
<div style="position:absolute;left:75.85px;top:300.24px" class="cls_007"><span class="cls_007">technology</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Testing: extensive (no assumptions of usage!), and</span></div>
<div style="position:absolute;left:75.85px;top:363.12px" class="cls_007"><span class="cls_007">well-documented</span></div>
<div style="position:absolute;left:61.20px;top:398.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Release: not only executables, also metadata</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Component maintenance</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Who is responsible: producer or consumer?</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Blame analysis: relation between manifestation of</span></div>
<div style="position:absolute;left:75.85px;top:208.32px" class="cls_007"><span class="cls_007">a fault and its cause, e.g.</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Component A requires more CPU time</span></div>
<div style="position:absolute;left:90.80px;top:270.16px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> As a consequence, B does not complete in time</span></div>
<div style="position:absolute;left:90.80px;top:299.20px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> As required by C, so</span></div>
<div style="position:absolute;left:90.80px;top:328.24px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> C issues a time-out error to its user</span></div>
<div style="position:absolute;left:90.80px;top:357.28px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Analysis: goes from C to B to A to input of A</span></div>
<div style="position:absolute;left:90.80px;top:385.12px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Who does the analysis, if producers of A,B,C are different?</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Architecture and CBSE</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Architecture-driven: top-down: components are</span></div>
<div style="position:absolute;left:75.85px;top:208.32px" class="cls_007"><span class="cls_007">identified as part of an architectural design</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Product line: family of similar products, with 1</span></div>
<div style="position:absolute;left:75.85px;top:306.24px" class="cls_007"><span class="cls_007">architecture</span></div>
<div style="position:absolute;left:61.20px;top:375.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">COTS-based: bottom-up, architecture is secondary</span></div>
<div style="position:absolute;left:75.85px;top:404.16px" class="cls_007"><span class="cls_007">to components found</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture18/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Summary</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> To enable composition, components must be</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_007"><span class="cls_007">compatible: achieved by component model</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Separation of development process for</span></div>
<div style="position:absolute;left:75.85px;top:271.20px" class="cls_007"><span class="cls_007">components from that of assembling systems out</span></div>
<div style="position:absolute;left:75.85px;top:300.24px" class="cls_007"><span class="cls_007">of components</span></div>
<div style="position:absolute;left:61.20px;top:369.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Architectural plan organizes how components fit</span></div>
<div style="position:absolute;left:75.85px;top:398.16px" class="cls_007"><span class="cls_007">together and meet quality requirements</span></div>
<div style="position:absolute;left:5.67px;top:498.00px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:5.67px;top:512.88px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_009"><span class="cls_009">20</span></div>
</div>

</body>
</html>
