<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:36.0px;color:rgb(0,101,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:36.0px;color:rgb(0,101,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:21.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:21.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:28.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:28.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:26.0px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:26.0px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:18.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:18.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Times,serif;font-size:26.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Times,serif;font-size:26.8px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:17.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:17.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:22.0px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:22.0px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:17.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:17.1px;color:rgb(0,51,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Times,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_012{font-family:Times,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_013{font-family:Times,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Times,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture22/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Managing people</span></div>
<div style="position:absolute;left:79.08px;top:194.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Managing people working as individuals and</span></div>
<div style="position:absolute;left:106.07px;top:228.56px" class="cls_004"><span class="cls_004">in groups</span></div>
<div style="position:absolute;left:79.08px;top:308.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> People are an organisation’s most important</span></div>
<div style="position:absolute;left:106.07px;top:342.56px" class="cls_004"><span class="cls_004">assets</span></div>
<div style="position:absolute;left:22.50px;top:502.24px" class="cls_005"><span class="cls_005">1</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Management activities</span></div>
<div style="position:absolute;left:79.20px;top:188.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Problem solving (using available people)</span></div>
<div style="position:absolute;left:79.20px;top:229.52px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Motivating (people who work on a project)</span></div>
<div style="position:absolute;left:79.20px;top:269.60px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Planning (what people are going to do)</span></div>
<div style="position:absolute;left:79.20px;top:309.68px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Estimating (how fast people will work)</span></div>
<div style="position:absolute;left:79.20px;top:350.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Controlling (people's activities)</span></div>
<div style="position:absolute;left:79.20px;top:390.56px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Organizing (the way in which people work)</span></div>
<div style="position:absolute;left:22.50px;top:502.24px" class="cls_005"><span class="cls_005">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Problem solving</span></div>
<div style="position:absolute;left:79.20px;top:188.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Requires the integration of different types of</span></div>
<div style="position:absolute;left:106.20px;top:222.56px" class="cls_004"><span class="cls_004">knowledge (computer, task, domain,</span></div>
<div style="position:absolute;left:106.20px;top:255.68px" class="cls_004"><span class="cls_004">organisation)</span></div>
<div style="position:absolute;left:79.20px;top:296.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Development of a model of the solution and</span></div>
<div style="position:absolute;left:106.20px;top:329.60px" class="cls_004"><span class="cls_004">testing of this model against the problem</span></div>
<div style="position:absolute;left:79.20px;top:370.64px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Representation of this model in an appropriate</span></div>
<div style="position:absolute;left:106.20px;top:403.52px" class="cls_004"><span class="cls_004">notation or programming language</span></div>
<div style="position:absolute;left:22.50px;top:502.24px" class="cls_005"><span class="cls_005">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Motivation</span></div>
<div style="position:absolute;left:79.20px;top:188.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> An important role of a manager is to motivate</span></div>
<div style="position:absolute;left:106.20px;top:222.56px" class="cls_004"><span class="cls_004">the people working on a project</span></div>
<div style="position:absolute;left:79.20px;top:262.64px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Motivation is a complex issue but it appears</span></div>
<div style="position:absolute;left:106.20px;top:296.48px" class="cls_004"><span class="cls_004">that their are different types of motivation</span></div>
<div style="position:absolute;left:106.20px;top:329.60px" class="cls_004"><span class="cls_004">based on</span></div>
<div style="position:absolute;left:115.20px;top:369.60px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Basic needs (e.g. food, sleep, etc.)</span></div>
<div style="position:absolute;left:115.20px;top:403.68px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Personal needs (e.g. respect, self-esteem)</span></div>
<div style="position:absolute;left:115.20px;top:438.48px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Social needs (e.g. to be accepted as part of a</span></div>
<div style="position:absolute;left:137.70px;top:467.52px" class="cls_007"><span class="cls_007">group)</span></div>
<div style="position:absolute;left:22.50px;top:502.24px" class="cls_005"><span class="cls_005">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Human needs hierarchy</span></div>
<div style="position:absolute;left:155.25px;top:207.46px" class="cls_008"><span class="cls_008">Self-</span></div>
<div style="position:absolute;left:89.92px;top:237.10px" class="cls_008"><span class="cls_008">realization needs</span></div>
<div style="position:absolute;left:286.00px;top:299.44px" class="cls_008"><span class="cls_008">Esteem needs</span></div>
<div style="position:absolute;left:286.00px;top:355.89px" class="cls_008"><span class="cls_008">Social needs</span></div>
<div style="position:absolute;left:286.00px;top:412.23px" class="cls_008"><span class="cls_008">Safety needs</span></div>
<div style="position:absolute;left:256.24px;top:474.57px" class="cls_008"><span class="cls_008">Physiological needs</span></div>
<div style="position:absolute;left:22.50px;top:502.24px" class="cls_005"><span class="cls_005">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Personality types</span></div>
<div style="position:absolute;left:79.20px;top:188.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> The needs hierarchy is almost certainly an</span></div>
<div style="position:absolute;left:106.20px;top:222.56px" class="cls_004"><span class="cls_004">over-simplification</span></div>
<div style="position:absolute;left:79.20px;top:262.64px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Motivation should also take into account</span></div>
<div style="position:absolute;left:106.20px;top:296.48px" class="cls_004"><span class="cls_004">different personality types:</span></div>
<div style="position:absolute;left:115.20px;top:335.52px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Task-oriented</span></div>
<div style="position:absolute;left:115.20px;top:370.56px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Self-oriented</span></div>
<div style="position:absolute;left:115.20px;top:404.64px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Interaction-oriented</span></div>
<div style="position:absolute;left:22.50px;top:502.24px" class="cls_005"><span class="cls_005">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Personality types</span></div>
<div style="position:absolute;left:79.20px;top:188.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Task-oriented.</span></div>
<div style="position:absolute;left:115.20px;top:228.48px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> The motivation for doing the work is the work itself</span></div>
<div style="position:absolute;left:79.20px;top:263.60px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Self-oriented.</span></div>
<div style="position:absolute;left:115.20px;top:303.60px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> The work is a means to an end which is the</span></div>
<div style="position:absolute;left:137.70px;top:331.68px" class="cls_007"><span class="cls_007">achievement of individual goals - e.g. to get rich, to</span></div>
<div style="position:absolute;left:137.70px;top:360.48px" class="cls_007"><span class="cls_007">play tennis, to travel etc.</span></div>
<div style="position:absolute;left:79.20px;top:396.56px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Interaction-oriented</span></div>
<div style="position:absolute;left:115.20px;top:435.60px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> The principal motivation is the presence and actions</span></div>
<div style="position:absolute;left:137.70px;top:464.64px" class="cls_007"><span class="cls_007">of co-workers. People go to work because they like</span></div>
<div style="position:absolute;left:137.70px;top:493.68px" class="cls_007"><span class="cls_007">to go to work</span></div>
<div style="position:absolute;left:22.50px;top:502.24px" class="cls_005"><span class="cls_005">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Group working</span></div>
<div style="position:absolute;left:79.20px;top:188.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Most software engineering is a group activity</span></div>
<div style="position:absolute;left:115.20px;top:228.48px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> The development schedule for most non-trivial</span></div>
<div style="position:absolute;left:137.70px;top:257.52px" class="cls_007"><span class="cls_007">software projects is such that they cannot be</span></div>
<div style="position:absolute;left:137.70px;top:285.60px" class="cls_007"><span class="cls_007">completed by one person working alone</span></div>
<div style="position:absolute;left:79.20px;top:321.68px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Group interaction is a key determinant of group</span></div>
<div style="position:absolute;left:106.20px;top:354.56px" class="cls_004"><span class="cls_004">performance</span></div>
<div style="position:absolute;left:79.20px;top:395.60px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Flexibility in group composition is limited</span></div>
<div style="position:absolute;left:115.20px;top:434.64px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Managers must do the best they can with available</span></div>
<div style="position:absolute;left:137.70px;top:463.68px" class="cls_007"><span class="cls_007">people</span></div>
<div style="position:absolute;left:22.50px;top:502.24px" class="cls_005"><span class="cls_005">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Group composition</span></div>
<div style="position:absolute;left:85.20px;top:194.48px" class="cls_009"><span class="cls_009">l</span><span class="cls_010">  Group composed of members who share the</span></div>
<div style="position:absolute;left:112.20px;top:221.60px" class="cls_010"><span class="cls_010">same motivation can be problematic</span></div>
<div style="position:absolute;left:121.20px;top:252.56px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Task-oriented - everyone wants to do their own thing</span></div>
<div style="position:absolute;left:121.20px;top:284.48px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Self-oriented - everyone wants to be the boss</span></div>
<div style="position:absolute;left:121.20px;top:316.64px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Interaction-oriented - too much chatting, not enough</span></div>
<div style="position:absolute;left:143.70px;top:342.56px" class="cls_010"><span class="cls_010">work</span></div>
<div style="position:absolute;left:85.20px;top:374.48px" class="cls_009"><span class="cls_009">l</span><span class="cls_010">  An effective group has a balance of all types</span></div>
<div style="position:absolute;left:85.20px;top:406.64px" class="cls_009"><span class="cls_009">l</span><span class="cls_010">  Can be difficult to achieve because most</span></div>
<div style="position:absolute;left:112.20px;top:432.56px" class="cls_010"><span class="cls_010">engineers are task-oriented</span></div>
<div style="position:absolute;left:85.20px;top:464.48px" class="cls_009"><span class="cls_009">l</span><span class="cls_010">  Need for all members to be involved in decisions which</span></div>
<div style="position:absolute;left:112.20px;top:490.64px" class="cls_010"><span class="cls_010">affect the group</span></div>
<div style="position:absolute;left:22.50px;top:502.24px" class="cls_005"><span class="cls_005">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Group leadership</span></div>
<div style="position:absolute;left:79.20px;top:195.48px" class="cls_003"><span class="cls_003">l</span></div>
<div style="position:absolute;left:106.20px;top:188.48px" class="cls_004"><span class="cls_004">Leadership depends on respect not title or</span></div>
<div style="position:absolute;left:106.20px;top:222.56px" class="cls_004"><span class="cls_004">status</span></div>
<div style="position:absolute;left:79.20px;top:262.64px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> There should be both a technical and a</span></div>
<div style="position:absolute;left:106.20px;top:296.48px" class="cls_004"><span class="cls_004">managerial leader</span></div>
<div style="position:absolute;left:79.20px;top:336.56px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> A career path based on technical competence</span></div>
<div style="position:absolute;left:106.20px;top:370.64px" class="cls_004"><span class="cls_004">should be supported</span></div>
<div style="position:absolute;left:15.25px;top:502.24px" class="cls_005"><span class="cls_005">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Group cohesiveness</span></div>
<div style="position:absolute;left:79.20px;top:188.48px" class="cls_009"><span class="cls_009">l</span><span class="cls_010">  In a cohesive group, members consider the group to be</span></div>
<div style="position:absolute;left:106.20px;top:215.60px" class="cls_010"><span class="cls_010">more important than any individual in it</span></div>
<div style="position:absolute;left:79.20px;top:246.56px" class="cls_009"><span class="cls_009">l</span><span class="cls_010">  Advantages of a cohesive group are:</span></div>
<div style="position:absolute;left:115.20px;top:278.48px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Group quality standards can be developed</span></div>
<div style="position:absolute;left:115.20px;top:310.64px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Group members work closely together so inhibitions</span></div>
<div style="position:absolute;left:137.70px;top:336.56px" class="cls_010"><span class="cls_010">caused by ignorance are reduced</span></div>
<div style="position:absolute;left:115.20px;top:368.48px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Team members  learn from each other and get to know</span></div>
<div style="position:absolute;left:137.70px;top:394.64px" class="cls_010"><span class="cls_010">each other’s work</span></div>
<div style="position:absolute;left:115.20px;top:426.56px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Egoless programming where members strive to improve</span></div>
<div style="position:absolute;left:137.70px;top:452.48px" class="cls_010"><span class="cls_010">each other’s programs can be practised</span></div>
<div style="position:absolute;left:15.97px;top:502.24px" class="cls_005"><span class="cls_005">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Group communications</span></div>
<div style="position:absolute;left:79.20px;top:188.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Good communications are essential for</span></div>
<div style="position:absolute;left:106.20px;top:222.56px" class="cls_004"><span class="cls_004">effective group working</span></div>
<div style="position:absolute;left:79.20px;top:262.64px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Information must be exchanged on the status</span></div>
<div style="position:absolute;left:106.20px;top:296.48px" class="cls_004"><span class="cls_004">of work, design decisions and changes to</span></div>
<div style="position:absolute;left:106.20px;top:329.60px" class="cls_004"><span class="cls_004">previous decisions</span></div>
<div style="position:absolute;left:79.20px;top:370.64px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Good communications also strengthens group</span></div>
<div style="position:absolute;left:106.20px;top:403.52px" class="cls_004"><span class="cls_004">cohesion as it promotes understanding</span></div>
<div style="position:absolute;left:15.25px;top:502.24px" class="cls_005"><span class="cls_005">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:65.83px;top:99.84px" class="cls_002"><span class="cls_002">Group communications</span></div>
<div style="position:absolute;left:79.20px;top:193.48px" class="cls_009"><span class="cls_009">l</span></div>
<div style="position:absolute;left:106.20px;top:188.48px" class="cls_010"><span class="cls_010">Status of group members</span></div>
<div style="position:absolute;left:115.20px;top:220.64px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Higher status members tend to dominate conversations</span></div>
<div style="position:absolute;left:79.20px;top:252.56px" class="cls_009"><span class="cls_009">l</span><span class="cls_010">  Personalities in groups</span></div>
<div style="position:absolute;left:115.20px;top:283.52px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Too many people of the same personality type can be a</span></div>
<div style="position:absolute;left:137.70px;top:310.64px" class="cls_010"><span class="cls_010">problem</span></div>
<div style="position:absolute;left:79.20px;top:342.56px" class="cls_009"><span class="cls_009">l</span><span class="cls_010">  Sexual composition of group</span></div>
<div style="position:absolute;left:115.20px;top:373.52px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Mixed-sex groups tend to communicate better</span></div>
<div style="position:absolute;left:79.20px;top:405.68px" class="cls_009"><span class="cls_009">l</span><span class="cls_010">  Communication channels</span></div>
<div style="position:absolute;left:115.20px;top:437.60px" class="cls_011"><span class="cls_011">-</span><span class="cls_010"> Communications channelled though a central</span></div>
<div style="position:absolute;left:137.70px;top:463.52px" class="cls_010"><span class="cls_010">coordinator tend to be ineffective</span></div>
<div style="position:absolute;left:15.25px;top:502.24px" class="cls_005"><span class="cls_005">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Group organisation</span></div>
<div style="position:absolute;left:79.20px;top:188.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Software engineering group sizes should be</span></div>
<div style="position:absolute;left:106.20px;top:222.56px" class="cls_004"><span class="cls_004">relatively small (&lt; 8 members)</span></div>
<div style="position:absolute;left:79.20px;top:262.64px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Break big projects down into multiple smaller</span></div>
<div style="position:absolute;left:106.20px;top:296.48px" class="cls_004"><span class="cls_004">projects</span></div>
<div style="position:absolute;left:79.20px;top:336.56px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Small teams may be organized in an informal,</span></div>
<div style="position:absolute;left:106.20px;top:370.64px" class="cls_004"><span class="cls_004">democratic way</span></div>
<div style="position:absolute;left:79.20px;top:410.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Chief programmer teams try to make the most</span></div>
<div style="position:absolute;left:106.20px;top:444.56px" class="cls_004"><span class="cls_004">effective use of skills and experience</span></div>
<div style="position:absolute;left:15.25px;top:502.24px" class="cls_005"><span class="cls_005">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Choosing and keeping people</span></div>
<div style="position:absolute;left:79.20px;top:185.60px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Choosing people to work on a project is a</span></div>
<div style="position:absolute;left:106.20px;top:215.60px" class="cls_004"><span class="cls_004">major managerial responsibility</span></div>
<div style="position:absolute;left:79.20px;top:252.56px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Appointment decisions are usually based on</span></div>
<div style="position:absolute;left:115.20px;top:289.68px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> information provided by the candidate (their resume)</span></div>
<div style="position:absolute;left:115.20px;top:320.64px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> information gained at an interview</span></div>
<div style="position:absolute;left:115.20px;top:352.56px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> recommendations from other people who know the</span></div>
<div style="position:absolute;left:137.70px;top:378.48px" class="cls_007"><span class="cls_007">candidate</span></div>
<div style="position:absolute;left:79.20px;top:410.48px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Some companies use psychological or aptitude</span></div>
<div style="position:absolute;left:106.20px;top:440.48px" class="cls_004"><span class="cls_004">tests</span></div>
<div style="position:absolute;left:115.20px;top:477.60px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> There is no agreement on whether or not these</span></div>
<div style="position:absolute;left:15.25px;top:502.24px" class="cls_005"><span class="cls_005">15</span></div>
<div style="position:absolute;left:137.70px;top:503.52px" class="cls_007"><span class="cls_007">tests are actually useful</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:97.20px;top:96.48px" class="cls_002"><span class="cls_002">Staff Selection factors</span></div>
<div style="position:absolute;left:69.10px;top:182.71px" class="cls_012"><span class="cls_012">Factor</span></div>
<div style="position:absolute;left:273.48px;top:182.71px" class="cls_012"><span class="cls_012">Explanation</span></div>
<div style="position:absolute;left:69.10px;top:195.51px" class="cls_013"><span class="cls_013">Applica tion doma in</span></div>
<div style="position:absolute;left:273.48px;top:195.51px" class="cls_013"><span class="cls_013">F or  a projec t to de velop a suc ce ssful syste m , the</span></div>
<div style="position:absolute;left:69.10px;top:207.23px" class="cls_013"><span class="cls_013">experience</span></div>
<div style="position:absolute;left:273.48px;top:207.23px" class="cls_013"><span class="cls_013">deve lopers m ust understand the application dom a in.</span></div>
<div style="position:absolute;left:69.10px;top:220.04px" class="cls_013"><span class="cls_013">P la t fo rm  e x p er ie n ce</span></div>
<div style="position:absolute;left:273.48px;top:220.04px" class="cls_013"><span class="cls_013">M ay be signific ant if</span></div>
<div style="position:absolute;left:461.37px;top:220.04px" class="cls_013"><span class="cls_013">low-level programming is</span></div>
<div style="position:absolute;left:273.48px;top:231.76px" class="cls_013"><span class="cls_013">involved. Otherwise, not usua lly a critical attribute .</span></div>
<div style="position:absolute;left:69.10px;top:244.55px" class="cls_013"><span class="cls_013">P rogra mm ing la nguage</span></div>
<div style="position:absolute;left:273.48px;top:244.55px" class="cls_013"><span class="cls_013">Norma lly only signific ant for short   dura tion projec ts</span></div>
<div style="position:absolute;left:69.10px;top:256.29px" class="cls_013"><span class="cls_013">experience</span></div>
<div style="position:absolute;left:273.48px;top:256.29px" class="cls_013"><span class="cls_013">whe re there  is insufficient tim e to</span></div>
<div style="position:absolute;left:569.02px;top:256.29px" class="cls_013"><span class="cls_013">learn a new</span></div>
<div style="position:absolute;left:273.48px;top:268.01px" class="cls_013"><span class="cls_013">language.</span></div>
<div style="position:absolute;left:69.10px;top:280.80px" class="cls_013"><span class="cls_013">Educational background</span></div>
<div style="position:absolute;left:273.48px;top:280.80px" class="cls_013"><span class="cls_013">M ay provide an indic ator of the  ba sic</span></div>
<div style="position:absolute;left:574.53px;top:280.80px" class="cls_013"><span class="cls_013">funda m entals</span></div>
<div style="position:absolute;left:273.48px;top:292.54px" class="cls_013"><span class="cls_013">which the ca ndida te  should know a nd of  their ability</span></div>
<div style="position:absolute;left:273.48px;top:304.27px" class="cls_013"><span class="cls_013">to lea rn.  This fa ctor bec ome s increa singly</span></div>
<div style="position:absolute;left:601.87px;top:304.27px" class="cls_013"><span class="cls_013">irre levant</span></div>
<div style="position:absolute;left:273.48px;top:316.01px" class="cls_013"><span class="cls_013">as engineers gain experience across a</span></div>
<div style="position:absolute;left:600.05px;top:316.01px" class="cls_013"><span class="cls_013">ra nge of</span></div>
<div style="position:absolute;left:273.48px;top:327.73px" class="cls_013"><span class="cls_013">proje cts.</span></div>
<div style="position:absolute;left:69.10px;top:340.52px" class="cls_013"><span class="cls_013">C omm unica tion</span></div>
<div style="position:absolute;left:191.08px;top:340.52px" class="cls_013"><span class="cls_013">ability</span></div>
<div style="position:absolute;left:273.48px;top:340.52px" class="cls_013"><span class="cls_013">Ve ry im porta nt bec ause of the  ne ed for proje ct staff to</span></div>
<div style="position:absolute;left:273.48px;top:352.26px" class="cls_013"><span class="cls_013">com m unica te   ora lly and in writing w ith othe r</span></div>
<div style="position:absolute;left:273.48px;top:363.98px" class="cls_013"><span class="cls_013">engine ers, m ana gers and custom ers.</span></div>
<div style="position:absolute;left:69.10px;top:376.77px" class="cls_013"><span class="cls_013">Ada pta bility</span></div>
<div style="position:absolute;left:273.48px;top:376.77px" class="cls_013"><span class="cls_013">Ada pta bility m a y be  judged by looking a t  the diffe rent</span></div>
<div style="position:absolute;left:273.48px;top:388.51px" class="cls_013"><span class="cls_013">types of e xperie nce  whic h c andidates have  ha d.T his is</span></div>
<div style="position:absolute;left:273.48px;top:400.24px" class="cls_013"><span class="cls_013">an important a ttribute   as it indic ates an ability to</span></div>
<div style="position:absolute;left:273.48px;top:411.93px" class="cls_013"><span class="cls_013">learn.</span></div>
<div style="position:absolute;left:69.10px;top:424.72px" class="cls_013"><span class="cls_013">Attitude</span></div>
<div style="position:absolute;left:273.48px;top:424.72px" class="cls_013"><span class="cls_013">P rojec t sta ff should  have  a  positive  a ttitude to their</span></div>
<div style="position:absolute;left:273.48px;top:436.45px" class="cls_013"><span class="cls_013">work a nd should be  willing to lea rn new skills. This</span></div>
<div style="position:absolute;left:273.48px;top:448.19px" class="cls_013"><span class="cls_013">is a n im portant a ttribute  but ofte n ve ry difficult</span></div>
<div style="position:absolute;left:654.76px;top:448.19px" class="cls_013"><span class="cls_013">to</span></div>
<div style="position:absolute;left:273.48px;top:459.92px" class="cls_013"><span class="cls_013">asse ss.</span></div>
<div style="position:absolute;left:69.10px;top:472.71px" class="cls_013"><span class="cls_013">P ersona lity</span></div>
<div style="position:absolute;left:273.48px;top:472.71px" class="cls_013"><span class="cls_013">Aga in, a n im portant attribute  but</span></div>
<div style="position:absolute;left:530.74px;top:472.71px" class="cls_013"><span class="cls_013">diffic ult to assess.</span></div>
<div style="position:absolute;left:273.48px;top:484.44px" class="cls_013"><span class="cls_013">C andidate s m ust be re asonably com patible with other</span></div>
<div style="position:absolute;left:273.48px;top:496.17px" class="cls_013"><span class="cls_013">tea m  m em bers. No pa rticular type of persona lity is</span></div>
<div style="position:absolute;left:273.48px;top:507.90px" class="cls_013"><span class="cls_013">m ore  or less suited to software enginee ring.</span></div>
<div style="position:absolute;left:15.25px;top:502.24px" class="cls_005"><span class="cls_005">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:102.48px" class="cls_002"><span class="cls_002">Working environments</span></div>
<div style="position:absolute;left:79.20px;top:195.48px" class="cls_003"><span class="cls_003">l</span></div>
<div style="position:absolute;left:106.20px;top:188.48px" class="cls_004"><span class="cls_004">Physical workplace provision has an important</span></div>
<div style="position:absolute;left:106.20px;top:222.56px" class="cls_004"><span class="cls_004">effect on individual productivity and satisfaction</span></div>
<div style="position:absolute;left:115.20px;top:261.60px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Comfort</span></div>
<div style="position:absolute;left:115.20px;top:296.64px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Privacy</span></div>
<div style="position:absolute;left:115.20px;top:330.48px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Facilities</span></div>
<div style="position:absolute;left:79.20px;top:366.56px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Health and safety considerations must be</span></div>
<div style="position:absolute;left:106.20px;top:399.68px" class="cls_004"><span class="cls_004">taken into account</span></div>
<div style="position:absolute;left:115.20px;top:439.68px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Lighting</span></div>
<div style="position:absolute;left:115.20px;top:473.52px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Heating</span></div>
<div style="position:absolute;left:15.25px;top:502.24px" class="cls_005"><span class="cls_005">17</span></div>
<div style="position:absolute;left:115.20px;top:508.56px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Furniture</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture22/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.20px;top:63.60px" class="cls_002"><span class="cls_002">The People Capability Maturity</span></div>
<div style="position:absolute;left:79.20px;top:102.72px" class="cls_002"><span class="cls_002">Model</span></div>
<div style="position:absolute;left:79.20px;top:185.60px" class="cls_003"><span class="cls_003">l</span><span class="cls_004"> Five stage model</span></div>
<div style="position:absolute;left:115.20px;top:221.52px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Initial. Ad-hoc people management</span></div>
<div style="position:absolute;left:115.20px;top:253.68px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Repeatable. Policies developed for capability</span></div>
<div style="position:absolute;left:137.70px;top:279.60px" class="cls_007"><span class="cls_007">improvement</span></div>
<div style="position:absolute;left:115.20px;top:311.52px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Defined. Standardized people management across</span></div>
<div style="position:absolute;left:137.70px;top:337.68px" class="cls_007"><span class="cls_007">the organization</span></div>
<div style="position:absolute;left:115.20px;top:368.64px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Managed. Quantitative goals for people</span></div>
<div style="position:absolute;left:137.70px;top:394.56px" class="cls_007"><span class="cls_007">management in place</span></div>
<div style="position:absolute;left:115.20px;top:426.48px" class="cls_006"><span class="cls_006">-</span><span class="cls_007"> Optimising. Continuous focus on improving</span></div>
<div style="position:absolute;left:137.70px;top:452.64px" class="cls_007"><span class="cls_007">individual competence and workforce motivation</span></div>
<div style="position:absolute;left:15.25px;top:502.24px" class="cls_005"><span class="cls_005">18</span></div>
</div>

</body>
</html>
