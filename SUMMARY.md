# Summary

* [Introduction](README.md)
* [Lecture 1](lecture1.md)
* [Lecture 1 Note](lecture1note.md)
* [Lecture 2](lecture2.md)
* [Lecture 2 Note](lecture2note.md)
* [Lecture 3](lecture3.md)
* [Lecture 3 Note](lecture3note.md)
* [Lecture 4](lecture4.md)
* [Lecture 4 Lecture](lecture4lecture.md)
* [Lecture 4 Note](lecture4note.md)
* [Lecture 5](lecture5.md)
* [Lecture 5 Note](lecture5note.md)
* [Lecture 6](lecture6.md)
* [Lecture 6 Note](lecture6note.md)
* [Lecture 7](lecture7.md)
* [Lecture 7 Note](lecture7note.md)
* [COCOMO](COCOMO.md)
* [COCOMO Model Note](COCOMOModel.md)
* [Lecture 8](lecture8.md)
* [Lecture 8 Note](lecture8note.md)
* [Lecture 9](lecture9.md)
* [Lecture 9 Note](lecture9note.md)
* [Lecture 10](lecture10.md)
* [Lecture 10 Note](lecture10note.md)
* [Lecture 18](lecture18.md)
* [Lecture 16](lecture16.md)
* [Lecture 16 Note](lecture16note.md)
* [Lecture 17](lecture17.md)
* [Lecture 20](lecture20.md)
* [Lecture 22](lecture22.md)
* [Lecture 19](lecture19.md)
* [Lecture 19 Note](lecture19note.md) 
* [test](test/lecture1.md) 











