<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:"Calibri",serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:"Calibri",serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:"Calibri",serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Calibri",serif;font-size:30.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:"Calibri",serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:12.1px;color:rgb(137,136,137);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Calibri",serif;font-size:44.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:32.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:"Calibri",serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:"Calibri",serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:"Calibri",serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture20/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:116.21px;top:35.36px" class="cls_002"><span class="cls_002">Global Software Development</span></div>
<div style="position:absolute;left:45.20px;top:306.16px" class="cls_003"><span class="cls_003">Main issue:</span></div>
<div style="position:absolute;left:45.20px;top:352.00px" class="cls_004"><span class="cls_004">§</span><span class="cls_003"> distance matters</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:164.23px;top:17.12px" class="cls_002"><span class="cls_002">Collocated versus</span></div>
<div style="position:absolute;left:177.78px;top:65.12px" class="cls_002"><span class="cls_002">global/multisite</span></div>
<div style="position:absolute;left:109.32px;top:173.28px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Collocated: housed within walking distance</span></div>
<div style="position:absolute;left:145.32px;top:209.20px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> People reinvent the wheel if they have to walk</span></div>
<div style="position:absolute;left:167.82px;top:234.16px" class="cls_009"><span class="cls_009">more than 30 meters, or climb the stairs</span></div>
<div style="position:absolute;left:109.32px;top:301.20px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Main question: how to overcome distance in</span></div>
<div style="position:absolute;left:136.32px;top:330.24px" class="cls_007"><span class="cls_007">global projects:</span></div>
<div style="position:absolute;left:145.32px;top:366.16px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> Communication</span></div>
<div style="position:absolute;left:145.32px;top:397.12px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> Coordination</span></div>
<div style="position:absolute;left:145.32px;top:428.32px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> Control</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_010"><span class="cls_010">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:113.55px;top:15.68px" class="cls_002"><span class="cls_002">Arguments for global software</span></div>
<div style="position:absolute;left:252.29px;top:63.68px" class="cls_002"><span class="cls_002">development</span></div>
<div style="position:absolute;left:43.20px;top:121.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Cost savings</span></div>
<div style="position:absolute;left:43.20px;top:193.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Faster delivery (“follow the sun”)</span></div>
<div style="position:absolute;left:43.20px;top:265.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Larger pool of developers</span></div>
<div style="position:absolute;left:43.20px;top:337.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Better modularization</span></div>
<div style="position:absolute;left:43.20px;top:409.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Little proof that these advantages materialize</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_010"><span class="cls_010">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:263.74px;top:37.12px" class="cls_011"><span class="cls_011">Challenges</span></div>
<div style="position:absolute;left:382.67px;top:121.84px" class="cls_013"><span class="cls_013">distance</span></div>
<div style="position:absolute;left:242.29px;top:178.24px" class="cls_012"><span class="cls_012">temporal</span></div>
<div style="position:absolute;left:379.79px;top:178.24px" class="cls_012"><span class="cls_012">geographical</span></div>
<div style="position:absolute;left:554.17px;top:178.24px" class="cls_012"><span class="cls_012">sociocultural</span></div>
<div style="position:absolute;left:38.29px;top:240.64px" class="cls_012"><span class="cls_012">communication</span></div>
<div style="position:absolute;left:269.29px;top:240.64px" class="cls_012"><span class="cls_012">X</span></div>
<div style="position:absolute;left:428.04px;top:240.64px" class="cls_012"><span class="cls_012">X</span></div>
<div style="position:absolute;left:600.92px;top:240.64px" class="cls_012"><span class="cls_012">X</span></div>
<div style="position:absolute;left:51.04px;top:328.48px" class="cls_012"><span class="cls_012">coordination</span></div>
<div style="position:absolute;left:269.29px;top:328.48px" class="cls_012"><span class="cls_012">X</span></div>
<div style="position:absolute;left:428.04px;top:328.48px" class="cls_012"><span class="cls_012">X</span></div>
<div style="position:absolute;left:75.54px;top:423.76px" class="cls_012"><span class="cls_012">control</span></div>
<div style="position:absolute;left:269.29px;top:423.76px" class="cls_012"><span class="cls_012">X</span></div>
<div style="position:absolute;left:600.92px;top:423.76px" class="cls_012"><span class="cls_012">X</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_010"><span class="cls_010">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:98.35px;top:37.12px" class="cls_011"><span class="cls_011">Temporal distance challenges</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Communication:</span></div>
<div style="position:absolute;left:79.20px;top:172.64px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Being effective (asynchronous is less effective,</span></div>
<div style="position:absolute;left:101.70px;top:206.48px" class="cls_016"><span class="cls_016">misunderstandings, …)</span></div>
<div style="position:absolute;left:43.20px;top:247.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Coordination:</span></div>
<div style="position:absolute;left:79.20px;top:292.64px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Cost is larger (travels, infrastructure cost, …)</span></div>
<div style="position:absolute;left:43.20px;top:333.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Control:</span></div>
<div style="position:absolute;left:79.20px;top:379.52px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Delays (wait for next teleconference meeting,</span></div>
<div style="position:absolute;left:101.70px;top:412.64px" class="cls_016"><span class="cls_016">send email and wait, search for contact, …)</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_010"><span class="cls_010">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:113.65px;top:10.72px" class="cls_011"><span class="cls_011">Geographical distance</span></div>
<div style="position:absolute;left:216.87px;top:63.52px" class="cls_011"><span class="cls_011">challenges</span></div>
<div style="position:absolute;left:109.32px;top:137.16px" class="cls_017"><span class="cls_017">•</span><span class="cls_018"> Communication:</span></div>
<div style="position:absolute;left:145.32px;top:170.16px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> Effective information exchange (less informal exchange,</span></div>
<div style="position:absolute;left:167.82px;top:193.20px" class="cls_020"><span class="cls_020">different languages, different domain knowledge, …)</span></div>
<div style="position:absolute;left:145.32px;top:222.24px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> Build a team (cohesiveness, “them and us” feelings,</span></div>
<div style="position:absolute;left:167.82px;top:245.28px" class="cls_020"><span class="cls_020">trust, …)</span></div>
<div style="position:absolute;left:109.32px;top:274.20px" class="cls_017"><span class="cls_017">•</span><span class="cls_018"> Coordination:</span></div>
<div style="position:absolute;left:145.32px;top:306.24px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> Task awareness (shared mental model, …)</span></div>
<div style="position:absolute;left:145.32px;top:335.28px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> Sense of urgency (perception, …)</span></div>
<div style="position:absolute;left:109.32px;top:364.20px" class="cls_017"><span class="cls_017">•</span><span class="cls_018"> Control:</span></div>
<div style="position:absolute;left:145.32px;top:396.24px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> Accurate status information (tracking, blaming, …)</span></div>
<div style="position:absolute;left:145.32px;top:425.28px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> Uniform process (different tools and techniques, …)</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_010"><span class="cls_010">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:57.52px;top:37.12px" class="cls_011"><span class="cls_011">Geographical distance: awareness</span></div>
<div style="position:absolute;left:43.20px;top:118.56px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Activity awareness:</span></div>
<div style="position:absolute;left:79.20px;top:151.60px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> What are the others doing?</span></div>
<div style="position:absolute;left:43.20px;top:178.56px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Availability awareness:</span></div>
<div style="position:absolute;left:79.20px;top:211.60px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> When can I reach them?</span></div>
<div style="position:absolute;left:43.20px;top:239.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Process awareness:</span></div>
<div style="position:absolute;left:79.20px;top:272.56px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> What are they doing?</span></div>
<div style="position:absolute;left:43.20px;top:299.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Perspective awareness:</span></div>
<div style="position:absolute;left:79.20px;top:332.56px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> What are the others thinking, and why?</span></div>
<div style="position:absolute;left:43.20px;top:388.56px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Improving awareness and familiarity with other</span></div>
<div style="position:absolute;left:70.20px;top:413.52px" class="cls_007"><span class="cls_007">members helps!</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_010"><span class="cls_010">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:66.21px;top:37.12px" class="cls_011"><span class="cls_011">Sociocultural distance challenges</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Communication:</span></div>
<div style="position:absolute;left:79.20px;top:172.64px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Cultural misunderstandings (corporate, technical,</span></div>
<div style="position:absolute;left:101.70px;top:206.48px" class="cls_016"><span class="cls_016">national, …)</span></div>
<div style="position:absolute;left:43.20px;top:247.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Coordination:</span></div>
<div style="position:absolute;left:79.20px;top:292.64px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Effectiveness (vocabulary, communication style, …)</span></div>
<div style="position:absolute;left:43.20px;top:333.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Control:</span></div>
<div style="position:absolute;left:79.20px;top:379.52px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Quality and expertise (CMM level 5 does not</span></div>
<div style="position:absolute;left:101.70px;top:412.64px" class="cls_016"><span class="cls_016">guarantee quality)</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_010"><span class="cls_010">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:215.99px;top:37.12px" class="cls_011"><span class="cls_011">National culture</span></div>
<div style="position:absolute;left:109.32px;top:137.28px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> American managers have a hamburger style of</span></div>
<div style="position:absolute;left:136.32px;top:165.12px" class="cls_007"><span class="cls_007">management. They start with sweet talk - the</span></div>
<div style="position:absolute;left:136.32px;top:194.16px" class="cls_007"><span class="cls_007">top of the bun. Then the criticism is slipped in</span></div>
<div style="position:absolute;left:136.32px;top:223.20px" class="cls_007"><span class="cls_007">- the meat. Finally, some encouraging words -</span></div>
<div style="position:absolute;left:136.32px;top:252.24px" class="cls_007"><span class="cls_007">the bottom bun.</span></div>
<div style="position:absolute;left:109.32px;top:324.24px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> With the Germans, all one gets is the meat.</span></div>
<div style="position:absolute;left:109.32px;top:396.24px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> With the Japanese, all one gets is the bun; one</span></div>
<div style="position:absolute;left:136.32px;top:425.28px" class="cls_007"><span class="cls_007">has to smell the meat.</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:670.08px;top:507.60px" class="cls_010"><span class="cls_010">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:159.37px;top:37.12px" class="cls_011"><span class="cls_011">Hofstede’s dimensions</span></div>
<div style="position:absolute;left:43.20px;top:119.64px" class="cls_017"><span class="cls_017">•</span><span class="cls_018"> Power distance</span></div>
<div style="position:absolute;left:22.67px;top:144.24px" class="cls_021"><span class="cls_021">#</span></div>
<div style="position:absolute;left:79.20px;top:149.52px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> status is important versus individuals are equal</span></div>
<div style="position:absolute;left:43.20px;top:174.60px" class="cls_017"><span class="cls_017">•</span><span class="cls_018"> Collectivism versus individualism</span></div>
<div style="position:absolute;left:22.67px;top:198.24px" class="cls_021"><span class="cls_021">#</span></div>
<div style="position:absolute;left:79.20px;top:204.48px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> Individuals are part of a group, or everyone looks after</span></div>
<div style="position:absolute;left:101.70px;top:224.64px" class="cls_020"><span class="cls_020">himself</span></div>
<div style="position:absolute;left:43.20px;top:249.48px" class="cls_017"><span class="cls_017">•</span><span class="cls_018"> Femininity versus masculinity</span></div>
<div style="position:absolute;left:79.20px;top:279.60px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> Earnings, challenges, recognition (masculine) versus good</span></div>
<div style="position:absolute;left:101.70px;top:299.52px" class="cls_020"><span class="cls_020">relationships, cooperation, security (feminine)</span></div>
<div style="position:absolute;left:43.20px;top:325.56px" class="cls_017"><span class="cls_017">•</span><span class="cls_018"> Uncertainty avoidance</span></div>
<div style="position:absolute;left:22.67px;top:336.96px" class="cls_021"><span class="cls_021">#</span></div>
<div style="position:absolute;left:79.20px;top:354.48px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> Strict rules that mitigate uncertainty versus more flexible</span></div>
<div style="position:absolute;left:43.20px;top:380.52px" class="cls_017"><span class="cls_017">•</span><span class="cls_018"> Long-term versus short-term orientation</span></div>
<div style="position:absolute;left:79.20px;top:409.68px" class="cls_019"><span class="cls_019">-</span><span class="cls_020"> Persistence in pursuing goals, order (LT) versus protecting</span></div>
<div style="position:absolute;left:101.70px;top:429.60px" class="cls_020"><span class="cls_020">one’s face, tradition (ST)</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_010"><span class="cls_010">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:187.86px;top:61.36px" class="cls_011"><span class="cls_011">Power distance</span></div>
<div style="position:absolute;left:109.32px;top:235.12px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> North America, Europe: managers have to</span></div>
<div style="position:absolute;left:136.32px;top:274.24px" class="cls_003"><span class="cls_003">convince their team members</span></div>
<div style="position:absolute;left:109.32px;top:366.16px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Asia: people respect authority</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:664.34px;top:507.60px" class="cls_010"><span class="cls_010">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:67.59px;top:37.12px" class="cls_011"><span class="cls_011">Collectivism versus individualism</span></div>
<div style="position:absolute;left:43.20px;top:165.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Asia: personal relationships are more</span></div>
<div style="position:absolute;left:70.20px;top:200.56px" class="cls_003"><span class="cls_003">important than the task at hand</span></div>
<div style="position:absolute;left:43.20px;top:284.56px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> North-America, Europe: very task-oriented</span></div>
<div style="position:absolute;left:43.20px;top:411.52px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> IDV (Individualism Index) differs</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_010"><span class="cls_010">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:159.42px;top:37.12px" class="cls_011"><span class="cls_011">Uncertainty avoidance</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Low uncertainty avoidance (UAI): can better</span></div>
<div style="position:absolute;left:70.20px;top:165.52px" class="cls_003"><span class="cls_003">cope with uncertainty: they can deal with</span></div>
<div style="position:absolute;left:70.20px;top:204.64px" class="cls_003"><span class="cls_003">agile approaches, il-defined requirements, etc.</span></div>
<div style="position:absolute;left:43.20px;top:250.48px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> High uncertainty avoidance: favor waterfall,</span></div>
<div style="position:absolute;left:70.20px;top:288.64px" class="cls_003"><span class="cls_003">contracts, etc.</span></div>
<div style="position:absolute;left:43.20px;top:380.56px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Latin America, Japan: high UAI</span></div>
<div style="position:absolute;left:43.20px;top:426.64px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> North America, India: low UAI</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_010"><span class="cls_010">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:109.87px;top:9.52px" class="cls_011"><span class="cls_011">How to overcome</span></div>
<div style="position:absolute;left:183.77px;top:62.56px" class="cls_011"><span class="cls_011">distance?</span></div>
<div style="position:absolute;left:109.32px;top:143.20px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Common ground</span></div>
<div style="position:absolute;left:109.32px;top:235.12px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Coupling of work</span></div>
<div style="position:absolute;left:109.32px;top:327.28px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Collaboration readiness</span></div>
<div style="position:absolute;left:109.32px;top:419.20px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Technology readiness</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_010"><span class="cls_010">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:209.29px;top:37.12px" class="cls_011"><span class="cls_011">Common ground</span></div>
<div style="position:absolute;left:109.32px;top:179.28px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> How much common knowledge members</span></div>
<div style="position:absolute;left:136.32px;top:211.20px" class="cls_007"><span class="cls_007">have, and are aware of</span></div>
<div style="position:absolute;left:109.32px;top:251.28px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Common ground has to be established:</span></div>
<div style="position:absolute;left:145.32px;top:290.32px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> Traveling, especially at start of project</span></div>
<div style="position:absolute;left:145.32px;top:325.12px" class="cls_008"><span class="cls_008">-</span><span class="cls_009"> Socialization (kick-off meetings)</span></div>
<div style="position:absolute;left:109.32px;top:399.12px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Intense interaction is more important for</span></div>
<div style="position:absolute;left:136.32px;top:431.28px" class="cls_007"><span class="cls_007">success than CMM level</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_010"><span class="cls_010">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:208.27px;top:37.12px" class="cls_011"><span class="cls_011">Coupling of work</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Tasks that require much collaboration: at</span></div>
<div style="position:absolute;left:70.20px;top:165.52px" class="cls_003"><span class="cls_003">same site</span></div>
<div style="position:absolute;left:43.20px;top:258.64px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Little interaction required: different sites</span></div>
<div style="position:absolute;left:79.20px;top:303.68px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> E.g., testing or implementing relatively</span></div>
<div style="position:absolute;left:101.70px;top:336.56px" class="cls_016"><span class="cls_016">independent subsystems</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_010"><span class="cls_010">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:149.26px;top:37.12px" class="cls_011"><span class="cls_011">Collaboration readiness</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Transition to global development organization:</span></div>
<div style="position:absolute;left:79.20px;top:172.64px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Requires changing work habits</span></div>
<div style="position:absolute;left:79.20px;top:213.68px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Learning new tools</span></div>
<div style="position:absolute;left:79.20px;top:253.52px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Needs incentives for individuals to cooperate</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_010"><span class="cls_010">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:168.93px;top:37.12px" class="cls_011"><span class="cls_011">Technology readiness</span></div>
<div style="position:absolute;left:43.20px;top:157.68px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Project management tools (workflow</span></div>
<div style="position:absolute;left:70.20px;top:185.52px" class="cls_007"><span class="cls_007">management)</span></div>
<div style="position:absolute;left:43.20px;top:221.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Web-enabled versions of tools</span></div>
<div style="position:absolute;left:43.20px;top:257.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Remote control of builds and tests</span></div>
<div style="position:absolute;left:43.20px;top:293.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Web-based project repositories</span></div>
<div style="position:absolute;left:43.20px;top:329.52px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Real-time collaboration tools (simple media for</span></div>
<div style="position:absolute;left:70.20px;top:358.56px" class="cls_007"><span class="cls_007">simple messages, rich media for complex ones)</span></div>
<div style="position:absolute;left:43.20px;top:394.56px" class="cls_006"><span class="cls_006">•</span><span class="cls_007"> Knowledge management technology (codification</span></div>
<div style="position:absolute;left:70.20px;top:423.60px" class="cls_007"><span class="cls_007">AND personalization)</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_010"><span class="cls_010">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.44px;top:15.68px" class="cls_002"><span class="cls_002">Organizing work in global software</span></div>
<div style="position:absolute;left:252.29px;top:63.68px" class="cls_002"><span class="cls_002">development</span></div>
<div style="position:absolute;left:43.20px;top:127.60px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Reduce the need for informal communication</span></div>
<div style="position:absolute;left:79.20px;top:172.64px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Usually through organizational means, e.g.:</span></div>
<div style="position:absolute;left:115.20px;top:212.64px" class="cls_019"><span class="cls_019">•</span><span class="cls_020"> Put user interface people together</span></div>
<div style="position:absolute;left:115.20px;top:247.68px" class="cls_019"><span class="cls_019">•</span><span class="cls_020"> Use gross structure (architecture) to divide work</span></div>
<div style="position:absolute;left:133.20px;top:275.52px" class="cls_020"><span class="cls_020">(Conway’s Law)</span></div>
<div style="position:absolute;left:115.20px;top:310.56px" class="cls_019"><span class="cls_019">•</span><span class="cls_020"> Split according to life cycle phases</span></div>
<div style="position:absolute;left:43.20px;top:392.56px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Provide technologies that ease informal</span></div>
<div style="position:absolute;left:70.20px;top:430.48px" class="cls_003"><span class="cls_003">communication</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_010"><span class="cls_010">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture20/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:274.89px;top:37.12px" class="cls_011"><span class="cls_011">Summary</span></div>
<div style="position:absolute;left:109.32px;top:235.12px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Distance matters</span></div>
<div style="position:absolute;left:109.32px;top:327.28px" class="cls_014"><span class="cls_014">•</span><span class="cls_003"> Main challenges:</span></div>
<div style="position:absolute;left:145.32px;top:373.28px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Deal with lack of informal communication</span></div>
<div style="position:absolute;left:145.32px;top:413.12px" class="cls_015"><span class="cls_015">-</span><span class="cls_016"> Handle cultural differences</span></div>
<div style="position:absolute;left:7.20px;top:492.00px" class="cls_005"><span class="cls_005">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:509.04px" class="cls_005"><span class="cls_005"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
<div style="position:absolute;left:663.45px;top:507.60px" class="cls_010"><span class="cls_010">20</span></div>
</div>

</body>
</html>
