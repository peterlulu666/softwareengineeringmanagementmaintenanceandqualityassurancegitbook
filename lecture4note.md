## Quiz        

1. The configuration items
    - source **code** components 
    - the **requirements** specification 
    - the **design** documentation 
    - the **test** plan 
    - **test** cases 
    - **test** results 
    - the user **manual**

2. A baseline 
    - It is **official** documents
    - It is a specification or product that has been **formally** reviewed and agreed upon, that thereafter serves as the basis for further development, and that can be changed only through formal change control procedures
    - It is the shared library or database that contains all **approved** item

3. The Configuration (or Change) Control Board (CCB)
    - It is responsible for formal approval of changing item
    - The CCB ensures that any change to the baseline is properly **authorized** and **executed** 
    - The CCB is staffed with people from the various parties **involved** in the project, such as **development**, **testing**, and **quality assurance**

4. A change request
    - Any proposed change to the baseline
    - an **error** found in some **component**
    - a **discrepancy** found between a **design document** and its **implementation**
    - an **enhancement** caused by changed user requirement

5. If an item has to be changed, the person responsible for implementing the change gets a **copy** of that item and the item is temporarily **locked**, so that others can not simultaneously update the same item

6. When an item is changed, the **old** version is **kept** as well

7. we may even create different **branches** of revisions

8. The functionalities of these software configuration management (SCM) tools
    - Components
        - SCM tools support **storing**, **retrieving**, and **accessing** components
    - Structure
        - SCM tools support support the **representation** and use of the **structure** of a **system** made up of components and their interfaces
    - Construction
        - SCM tools support the construction of an **executable** version of the system
    - Auditing
        - SCM tools allow one to follow **trails**: which **changes** have been made to this component, who did those changes, and why 
        - This way, a searchable archive of the system is **maintained**
    - Accounting
        - The **searchable** archive allows one to **gather statistics** about the system and the development process
    - Controlling
        - SCM tools may be used for **traceability** purposes 
        - If sufficient information is stored, we may **trace** defects to requirements, **analyze** the impact of changes, and the like 
    - Process 
        - SCM tools may support users in selecting **tasks** and performing those **tasks** in the appropriate context
    - Team 
        - SCM tools may support **collaboration**

9. **Early SCM tools** emphasized the **product-oriented tasks** of configuration management
    - emphasized the logging of physical file changes 

10. **Present-day SCM tools** increasingly provide the **other functionalities**
    - process aspects 
    - many have adopted a **change-oriented** next to or **instead of a version-oriented** view of configuration 

11. **Agile** project has a **running system**. This process is known as the **daily build**

12. The Configuration Management Plan
    - Management
        - This section describes **how** the project is being **organized**. Particular attention is paid to **responsibilities** which directly affect configuration management
    - Activities
        - This section describes how a **configuration** will be **identified and controlled** and how its **status** will be **accounted and reported**

13. The Configuration Management
    - Configuration management is concerned with the **management** of all **artifacts** produced in the course of a **software development project**
    - For larger and multi-site project 
    - The history and development of configuration management is closely tied to the history and development of configuration-management **tool** 
        - Early SCM tools  
            - emphasized the logging of physical file changes  
        - Present-day SCM tool 
            - process aspects 
            - many have adopted a **change-oriented** next to or **instead of a version-oriented** view of configuration 
        - Agile






