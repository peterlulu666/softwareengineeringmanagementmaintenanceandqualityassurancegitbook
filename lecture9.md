<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:44.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:29.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:"Lucida Sans Unicode",serif;font-size:12.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:"Lucida Sans Unicode",serif;font-size:11.9px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:"Calibri Bold",serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_013{font-family:"Calibri Bold",serif;font-size:24.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,144);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:22.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,102);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:24.1px;color:rgb(0,0,144);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:20.1px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,102);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture9/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:79.94px;top:34.96px" class="cls_002"><span class="cls_002">Requirements Engineering</span></div>
<div style="position:absolute;left:103.20px;top:309.88px" class="cls_003"><span class="cls_003">Main issues:</span></div>
<div style="position:absolute;left:103.20px;top:351.88px" class="cls_003"><span class="cls_003">• What do we want to build</span></div>
<div style="position:absolute;left:103.20px;top:393.88px" class="cls_003"><span class="cls_003">• How do we write this down</span></div>
<div style="position:absolute;left:7.20px;top:501.60px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:7.20px;top:519.60px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Requirements Engineering</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> the first step in finding a solution for a data</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_007"><span class="cls_007">processing problem</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> the results of requirements engineering is a</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_007"><span class="cls_007">requirements specification</span></div>
<div style="position:absolute;left:61.20px;top:306.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> requirements specification</span></div>
<div style="position:absolute;left:97.20px;top:339.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> contract for the customer</span></div>
<div style="position:absolute;left:97.20px;top:368.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> starting point for design</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">2</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Natural language specs are dangerous</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">“All users have the same control field”</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_011"><span class="cls_011">§</span><span class="cls_007"> the same value in the control field?</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_011"><span class="cls_011">§</span><span class="cls_007"> the same format of the control field?</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_011"><span class="cls_011">§</span><span class="cls_007"> there is one (1) control field for all users?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">3</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Requirements engineering, main steps</span></div>
<div style="position:absolute;left:61.20px;top:173.28px" class="cls_007"><span class="cls_007">1. </span><span class="cls_012">understanding</span><span class="cls_007"> the problem: elicitation</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_007"><span class="cls_007">2. </span><span class="cls_012">describing</span><span class="cls_007"> the problem: specification</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_007"><span class="cls_007">3. </span><span class="cls_012">agreeing upon</span><span class="cls_007"> the nature of the problem: validation</span></div>
<div style="position:absolute;left:61.20px;top:363.12px" class="cls_007"><span class="cls_007">4. </span><span class="cls_012">agreeing upon</span><span class="cls_007"> the boundaries of the problem:</span></div>
<div style="position:absolute;left:88.20px;top:389.28px" class="cls_007"><span class="cls_007">negotiation</span></div>
<div style="position:absolute;left:61.20px;top:453.12px" class="cls_007"><span class="cls_007">This is an iterative process</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">4</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Framework for RE process</span></div>
<div style="position:absolute;left:314.88px;top:165.04px" class="cls_009"><span class="cls_009">specification</span></div>
<div style="position:absolute;left:124.31px;top:298.24px" class="cls_009"><span class="cls_009">elicitation</span></div>
<div style="position:absolute;left:325.06px;top:298.24px" class="cls_009"><span class="cls_009">doc & mgt</span></div>
<div style="position:absolute;left:543.25px;top:298.24px" class="cls_009"><span class="cls_009">validation</span></div>
<div style="position:absolute;left:321.00px;top:431.68px" class="cls_009"><span class="cls_009">negotiation</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">5</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Conceptual modeling</span></div>
<div style="position:absolute;left:59.58px;top:108.72px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> you model part of reality: the Universe of</span></div>
<div style="position:absolute;left:86.58px;top:137.76px" class="cls_007"><span class="cls_007">Discourse (UoD)</span></div>
<div style="position:absolute;left:59.58px;top:206.64px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> this model is an </span><span class="cls_012">explicit</span><span class="cls_007"> conceptual model</span></div>
<div style="position:absolute;left:59.58px;top:275.76px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> people in the UoD have an </span><span class="cls_012">implicit</span><span class="cls_007"> conceptual</span></div>
<div style="position:absolute;left:86.58px;top:304.80px" class="cls_007"><span class="cls_007">model of that UoD</span></div>
<div style="position:absolute;left:59.58px;top:373.68px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> making this implicit model explicit poses</span></div>
<div style="position:absolute;left:86.58px;top:402.72px" class="cls_007"><span class="cls_007">problems:</span></div>
<div style="position:absolute;left:95.58px;top:436.72px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> analysis problems</span></div>
<div style="position:absolute;left:95.58px;top:465.76px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> negotiation problems</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">6</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Requirements engineering is difficult</span></div>
<div style="position:absolute;left:88.20px;top:144.24px" class="cls_007"><span class="cls_007">Success depends on the degree with which we</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_007"><span class="cls_007">manage to properly describe the system desired</span></div>
<div style="position:absolute;left:88.20px;top:277.20px" class="cls_012"><span class="cls_012">Software is not continuous!</span></div>
<div style="position:absolute;left:88.20px;top:380.16px" class="cls_007"><span class="cls_007">Tsjechow vs Chekhov vs </span><span class="cls_013">ЦЕХОВ</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">7</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Beware of subtle mismatches</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> a library employee may also be a client</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> there is a difference between `a book` and `a copy</span></div>
<div style="position:absolute;left:88.20px;top:242.16px" class="cls_007"><span class="cls_007">of a book`</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> status info `present` / `not present` is not</span></div>
<div style="position:absolute;left:88.20px;top:340.32px" class="cls_007"><span class="cls_007">sufficient; a (copy of a) book may be lost, stolen,</span></div>
<div style="position:absolute;left:88.20px;top:369.12px" class="cls_007"><span class="cls_007">in repair, ...</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">8</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Humans as information sources</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> different backgrounds</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> short-term vs long-term memory</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> human prejudices</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> limited capability for rational thinking</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:685.13px;top:514.50px" class="cls_010"><span class="cls_010">9</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Negotiation problems</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> existing methods are “Taylorian”</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> they may work in a “technical” environment, but</span></div>
<div style="position:absolute;left:88.20px;top:242.16px" class="cls_007"><span class="cls_007">many UoDs contain people as well, and their</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_007"><span class="cls_007">models may be irrational, incomplete,</span></div>
<div style="position:absolute;left:88.20px;top:300.24px" class="cls_007"><span class="cls_007">inconsistent, contradictory</span></div>
<div style="position:absolute;left:61.20px;top:369.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> as an analyst, you cannot neglect these aspects;</span></div>
<div style="position:absolute;left:88.20px;top:398.16px" class="cls_007"><span class="cls_007">you participate in shaping the UoD</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">10</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Point to ponder #1</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> how do you handle conflicts during requirements</span></div>
<div style="position:absolute;left:88.20px;top:277.20px" class="cls_007"><span class="cls_007">engineering?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">11</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">How we study the world around us</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> people have a set of assumptions about a topic</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_007"><span class="cls_007">they study (paradigm)</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> this set of assumptions concerns:</span></div>
<div style="position:absolute;left:97.20px;top:241.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> how knowledge is gathered</span></div>
<div style="position:absolute;left:97.20px;top:270.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> how the world is organized</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> this in turn results in two dimensions:</span></div>
<div style="position:absolute;left:97.20px;top:334.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> subjective-objective (wrt knowledge)</span></div>
<div style="position:absolute;left:97.20px;top:362.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> conflict-order (wrt the world)</span></div>
<div style="position:absolute;left:61.20px;top:392.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> which results in 4 archetypical approaches to</span></div>
<div style="position:absolute;left:88.20px;top:421.20px" class="cls_007"><span class="cls_007">requirements engineering</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">12</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:64.70px;top:59.12px" class="cls_005"><span class="cls_005">Four approaches to RE</span></div>
<div style="position:absolute;left:49.20px;top:131.76px" class="cls_006"><span class="cls_006">§</span><span class="cls_012"> functional</span><span class="cls_007"> (objective+order): the analyst is the</span></div>
<div style="position:absolute;left:76.20px;top:160.80px" class="cls_007"><span class="cls_007">expert who empirically seeks the truth</span></div>
<div style="position:absolute;left:49.20px;top:195.84px" class="cls_006"><span class="cls_006">§</span><span class="cls_012"> social-relativism</span><span class="cls_007"> (subjective+order): the analyst is</span></div>
<div style="position:absolute;left:76.20px;top:224.64px" class="cls_007"><span class="cls_007">a `change agent’. RE is a learning process guided</span></div>
<div style="position:absolute;left:76.20px;top:252.72px" class="cls_007"><span class="cls_007">by the analyst</span></div>
<div style="position:absolute;left:49.20px;top:287.76px" class="cls_006"><span class="cls_006">§</span><span class="cls_012"> radical-structuralism</span><span class="cls_007"> (objective+ conflict): there is a</span></div>
<div style="position:absolute;left:76.20px;top:316.80px" class="cls_007"><span class="cls_007">struggle between classes; the analyst chooses</span></div>
<div style="position:absolute;left:76.20px;top:344.64px" class="cls_007"><span class="cls_007">for either party</span></div>
<div style="position:absolute;left:49.20px;top:379.68px" class="cls_006"><span class="cls_006">§</span><span class="cls_012"> neohumanism</span><span class="cls_007"> (subjective+conflict): the analyst is</span></div>
<div style="position:absolute;left:76.20px;top:408.72px" class="cls_007"><span class="cls_007">kind of a social therapist, bringing parties</span></div>
<div style="position:absolute;left:76.20px;top:437.76px" class="cls_007"><span class="cls_007">together</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">13</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Point to ponder #2</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> how does the London Ambulance System</span></div>
<div style="position:absolute;left:88.20px;top:242.16px" class="cls_007"><span class="cls_007">example from chapter 1 relate to the different</span></div>
<div style="position:absolute;left:88.20px;top:271.20px" class="cls_007"><span class="cls_007">possible approaches to requirements</span></div>
<div style="position:absolute;left:88.20px;top:300.24px" class="cls_007"><span class="cls_007">engineering?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">14</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Elicitation techniques</span></div>
<div style="position:absolute;left:61.20px;top:144.32px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> interview</span></div>
<div style="position:absolute;left:375.70px;top:144.32px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> synthesis from existing</span></div>
<div style="position:absolute;left:402.70px;top:171.20px" class="cls_015"><span class="cls_015">system</span></div>
<div style="position:absolute;left:61.20px;top:176.24px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> Delphi technique</span></div>
<div style="position:absolute;left:375.70px;top:202.16px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> domain analysis</span></div>
<div style="position:absolute;left:61.20px;top:208.16px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> brainstorming session</span></div>
<div style="position:absolute;left:375.70px;top:234.32px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> Business Process</span></div>
<div style="position:absolute;left:61.20px;top:239.12px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> task analysis</span></div>
<div style="position:absolute;left:402.70px;top:261.20px" class="cls_015"><span class="cls_015">Redesign (BPR)</span></div>
<div style="position:absolute;left:61.20px;top:271.28px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> scenario analysis</span></div>
<div style="position:absolute;left:375.70px;top:292.16px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> prototyping</span></div>
<div style="position:absolute;left:61.20px;top:303.20px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> ethnography</span></div>
<div style="position:absolute;left:61.20px;top:334.16px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> form analysis</span></div>
<div style="position:absolute;left:61.20px;top:366.32px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> analysis of natural</span></div>
<div style="position:absolute;left:88.20px;top:393.20px" class="cls_015"><span class="cls_015">language descriptions</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">15</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Task Analysis</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Task analysis is the process of analyzing the way</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_007"><span class="cls_007">people perform their jobs: the things they do, the</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_007"><span class="cls_007">things they act on and the things they need to</span></div>
<div style="position:absolute;left:88.20px;top:231.12px" class="cls_007"><span class="cls_007">know.</span></div>
<div style="position:absolute;left:61.20px;top:265.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> The relation between tasks and goals: a task is</span></div>
<div style="position:absolute;left:88.20px;top:294.24px" class="cls_007"><span class="cls_007">performed in order to achieve a goal.</span></div>
<div style="position:absolute;left:61.20px;top:329.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Task analysis has a broad scope.</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">16</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Task Analysis (cntd)</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Task analysis concentrates on the current</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_007"><span class="cls_007">situation. However, it can be used as a starting</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_007"><span class="cls_007">point for a new system:</span></div>
<div style="position:absolute;left:97.20px;top:236.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> users will refer to new elements of a system and its</span></div>
<div style="position:absolute;left:119.70px;top:260.32px" class="cls_009"><span class="cls_009">functionality</span></div>
<div style="position:absolute;left:97.20px;top:288.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> scenario-based analysis can be used to exploit new</span></div>
<div style="position:absolute;left:119.70px;top:312.16px" class="cls_009"><span class="cls_009">possibilities</span></div>
<div style="position:absolute;left:61.20px;top:342.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> See also the role of task analysis as discussed in</span></div>
<div style="position:absolute;left:88.20px;top:371.28px" class="cls_007"><span class="cls_007">the context of user interface design (chapter 16)</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">17</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Scenario-Based Analysis</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Provides a more user-oriented view perspective</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_007"><span class="cls_007">on the design and development of an interactive</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_007"><span class="cls_007">system.</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> The defining property of a scenario is that it</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_007"><span class="cls_007">projects a concrete description of an activity that</span></div>
<div style="position:absolute;left:88.20px;top:294.24px" class="cls_007"><span class="cls_007">the user engages in when performing a specific</span></div>
<div style="position:absolute;left:88.20px;top:323.28px" class="cls_007"><span class="cls_007">task, a description sufficiently detailed so that the</span></div>
<div style="position:absolute;left:88.20px;top:352.32px" class="cls_007"><span class="cls_007">design implications can be inferred and reasoned</span></div>
<div style="position:absolute;left:88.20px;top:381.12px" class="cls_007"><span class="cls_007">about.</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">18</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Scenario-Based Analysis (example)</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> first shot:</span></div>
<div style="position:absolute;left:97.20px;top:178.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> check due back date</span></div>
<div style="position:absolute;left:97.20px;top:207.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> if overdue, collect fine</span></div>
<div style="position:absolute;left:97.20px;top:236.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> record book as being available again</span></div>
<div style="position:absolute;left:97.20px;top:264.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> put book back</span></div>
<div style="position:absolute;left:61.20px;top:329.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> as a result of discussion with library employee:</span></div>
<div style="position:absolute;left:97.20px;top:362.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> what if person returning the book is not registered as a</span></div>
<div style="position:absolute;left:119.70px;top:386.32px" class="cls_009"><span class="cls_009">client?</span></div>
<div style="position:absolute;left:97.20px;top:415.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> what if the book is damaged?</span></div>
<div style="position:absolute;left:97.20px;top:444.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> how to handle in case the client has other books that are</span></div>
<div style="position:absolute;left:119.70px;top:468.16px" class="cls_009"><span class="cls_009">overdue, and/or an outstanding reservation?</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">19</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Scenario-Based Analysis (cntd)</span></div>
<div style="position:absolute;left:88.20px;top:144.32px" class="cls_015"><span class="cls_015">The scenario view</span></div>
<div style="position:absolute;left:402.70px;top:144.32px" class="cls_015"><span class="cls_015">The standard view</span></div>
<div style="position:absolute;left:61.20px;top:208.16px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> concrete descriptions</span></div>
<div style="position:absolute;left:375.70px;top:208.16px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> abstract descriptions</span></div>
<div style="position:absolute;left:61.20px;top:239.12px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> focus on particular</span></div>
<div style="position:absolute;left:375.70px;top:239.12px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> focus on generic types</span></div>
<div style="position:absolute;left:88.20px;top:266.24px" class="cls_015"><span class="cls_015">instances</span></div>
<div style="position:absolute;left:375.70px;top:271.28px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> technology-driven</span></div>
<div style="position:absolute;left:61.20px;top:298.16px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> work-driven</span></div>
<div style="position:absolute;left:375.70px;top:303.20px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> complete, exhaustive</span></div>
<div style="position:absolute;left:61.20px;top:329.12px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> open-ended,</span></div>
<div style="position:absolute;left:375.70px;top:334.16px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> formal, rigorous</span></div>
<div style="position:absolute;left:88.20px;top:356.24px" class="cls_015"><span class="cls_015">fragmentary</span></div>
<div style="position:absolute;left:375.70px;top:366.32px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> specified outcomes</span></div>
<div style="position:absolute;left:61.20px;top:387.20px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> informal, rough,</span></div>
<div style="position:absolute;left:88.20px;top:414.32px" class="cls_015"><span class="cls_015">colloquial</span></div>
<div style="position:absolute;left:61.20px;top:445.28px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> envisioned outcomes</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">20</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Scenario-Based Analysis (cntd)</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Application areas:</span></div>
<div style="position:absolute;left:97.20px;top:178.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> requirements analysis</span></div>
<div style="position:absolute;left:97.20px;top:207.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> user-designer communication</span></div>
<div style="position:absolute;left:97.20px;top:236.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> design rationale</span></div>
<div style="position:absolute;left:97.20px;top:264.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> sofware architecture (& its analysis)</span></div>
<div style="position:absolute;left:97.20px;top:293.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> software design</span></div>
<div style="position:absolute;left:97.20px;top:322.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> implementation</span></div>
<div style="position:absolute;left:97.20px;top:351.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> verification & validation</span></div>
<div style="position:absolute;left:97.20px;top:380.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> documentation and training</span></div>
<div style="position:absolute;left:97.20px;top:408.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> evaluation</span></div>
<div style="position:absolute;left:97.20px;top:437.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> team building</span></div>
<div style="position:absolute;left:61.20px;top:467.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Scenarios must be structured and managed</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">21</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Form analysis (example</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_007"><span class="cls_007">Proceedings request form:</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_007"><span class="cls_007">Client name</span></div>
<div style="position:absolute;left:349.20px;top:179.28px" class="cls_007"><span class="cls_007">……………</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_007"><span class="cls_007">Title</span></div>
<div style="position:absolute;left:349.20px;top:213.12px" class="cls_007"><span class="cls_007">……………</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_007"><span class="cls_007">Editor</span></div>
<div style="position:absolute;left:349.20px;top:248.16px" class="cls_007"><span class="cls_007">……………</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_007"><span class="cls_007">Place</span></div>
<div style="position:absolute;left:349.20px;top:283.20px" class="cls_007"><span class="cls_007">……………</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_007"><span class="cls_007">Publisher</span></div>
<div style="position:absolute;left:349.20px;top:317.28px" class="cls_007"><span class="cls_007">……………</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_007"><span class="cls_007">Year</span></div>
<div style="position:absolute;left:349.20px;top:352.32px" class="cls_007"><span class="cls_007">……………</span></div>
<div style="position:absolute;left:61.20px;top:421.20px" class="cls_007"><span class="cls_007">Certainty vs uncertainty</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">22</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:22.40px" class="cls_005"><span class="cls_005">Types of links between customer and</span></div>
<div style="position:absolute;left:61.20px;top:56.48px" class="cls_005"><span class="cls_005">developer</span></div>
<div style="position:absolute;left:61.20px;top:176.24px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> facilitated teams</span></div>
<div style="position:absolute;left:375.70px;top:176.24px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> observational study</span></div>
<div style="position:absolute;left:61.20px;top:208.16px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> intermediary</span></div>
<div style="position:absolute;left:375.70px;top:208.16px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> user group</span></div>
<div style="position:absolute;left:61.20px;top:239.12px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> support line/help desk</span></div>
<div style="position:absolute;left:375.70px;top:239.12px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> trade show</span></div>
<div style="position:absolute;left:61.20px;top:271.28px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> survey</span></div>
<div style="position:absolute;left:375.70px;top:271.28px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> marketing & sales</span></div>
<div style="position:absolute;left:61.20px;top:303.20px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> user interface</span></div>
<div style="position:absolute;left:88.20px;top:329.12px" class="cls_015"><span class="cls_015">prototyping</span></div>
<div style="position:absolute;left:61.20px;top:361.28px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> requirements</span></div>
<div style="position:absolute;left:88.20px;top:387.20px" class="cls_015"><span class="cls_015">prototyping</span></div>
<div style="position:absolute;left:61.20px;top:419.12px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> interview</span></div>
<div style="position:absolute;left:61.20px;top:451.28px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> usability lab</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">23</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Direct versus indirect links</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> lesson 1: don’t rely too much on indirect links</span></div>
<div style="position:absolute;left:88.20px;top:208.32px" class="cls_007"><span class="cls_007">(intermediaries, surrogate users)</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> lesson 2: the more links, the better - up to a point</span></div>
<div style="position:absolute;left:164.70px;top:450.96px" class="cls_016"><span class="cls_016"># of links</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">24</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Structuring a set of requirements</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_017"><span class="cls_017">1.</span><span class="cls_007">  Hierarchical structure: higher-level reqs are</span></div>
<div style="position:absolute;left:97.20px;top:173.28px" class="cls_007"><span class="cls_007">decomposed into lower-level reqs</span></div>
<div style="position:absolute;left:61.20px;top:242.16px" class="cls_017"><span class="cls_017">2.</span><span class="cls_007">  Link requirements to specific stakeholders (e.g.</span></div>
<div style="position:absolute;left:97.20px;top:271.20px" class="cls_007"><span class="cls_007">management and end users each have their own</span></div>
<div style="position:absolute;left:97.20px;top:300.24px" class="cls_007"><span class="cls_007">set)</span></div>
<div style="position:absolute;left:61.20px;top:369.12px" class="cls_007"><span class="cls_007">In both cases, elicitation and structuring go hand in</span></div>
<div style="position:absolute;left:61.20px;top:404.16px" class="cls_007"><span class="cls_007">hand</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">25</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Goal-driven requirements engineering</span></div>
<div style="position:absolute;left:397.13px;top:135.04px" class="cls_009"><span class="cls_009">serving</span></div>
<div style="position:absolute;left:408.31px;top:164.08px" class="cls_009"><span class="cls_009">cust.</span></div>
<div style="position:absolute;left:286.25px;top:189.76px" class="cls_009"><span class="cls_009">why?</span></div>
<div style="position:absolute;left:513.00px;top:189.76px" class="cls_009"><span class="cls_009">how?</span></div>
<div style="position:absolute;left:576.63px;top:254.08px" class="cls_009"><span class="cls_009">search</span></div>
<div style="position:absolute;left:222.25px;top:266.32px" class="cls_009"><span class="cls_009">search</span></div>
<div style="position:absolute;left:584.93px;top:282.88px" class="cls_009"><span class="cls_009">book</span></div>
<div style="position:absolute;left:127.00px;top:306.40px" class="cls_009"><span class="cls_009">why?</span></div>
<div style="position:absolute;left:106.00px;top:367.36px" class="cls_009"><span class="cls_009">search</span></div>
<div style="position:absolute;left:338.50px;top:367.36px" class="cls_009"><span class="cls_009">search</span></div>
<div style="position:absolute;left:114.31px;top:396.40px" class="cls_009"><span class="cls_009">book</span></div>
<div style="position:absolute;left:345.13px;top:396.40px" class="cls_009"><span class="cls_009">news</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">26</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Conflicting viewpoints</span></div>
<div style="position:absolute;left:515.25px;top:98.80px" class="cls_009"><span class="cls_009">cash fines asap</span></div>
<div style="position:absolute;left:335.50px;top:135.04px" class="cls_009"><span class="cls_009">John</span></div>
<div style="position:absolute;left:439.38px;top:223.60px" class="cls_009"><span class="cls_009">supports</span></div>
<div style="position:absolute;left:331.05px;top:238.96px" class="cls_009"><span class="cls_009">Pos A</span></div>
<div style="position:absolute;left:562.36px;top:238.96px" class="cls_009"><span class="cls_009">Arg A</span></div>
<div style="position:absolute;left:115.56px;top:276.64px" class="cls_009"><span class="cls_009">issue:</span></div>
<div style="position:absolute;left:125.56px;top:305.68px" class="cls_009"><span class="cls_009">fine</span></div>
<div style="position:absolute;left:330.50px;top:342.88px" class="cls_009"><span class="cls_009">Pos B</span></div>
<div style="position:absolute;left:561.94px;top:342.88px" class="cls_009"><span class="cls_009">Arg B</span></div>
<div style="position:absolute;left:257.88px;top:399.52px" class="cls_009"><span class="cls_009">taken-by</span></div>
<div style="position:absolute;left:335.00px;top:446.80px" class="cls_009"><span class="cls_009">Mary</span></div>
<div style="position:absolute;left:517.50px;top:467.44px" class="cls_009"><span class="cls_009">cash fines later</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">27</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Prioritizing requirements (MoSCoW)</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Must haves: </span><span class="cls_012">top priority requirements</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Should haves: </span><span class="cls_012">highly desirable</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Could haves: </span><span class="cls_012">if time allows</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Won’t haves: </span><span class="cls_012">not today</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">28</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Prioritizing requirements (Kano model)</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Attractive: more satisfied if +, not less satisfied if -</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Must-be: dissatisfied when -, at most neutral</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> One-dimensional: satisfaction proportional to</span></div>
<div style="position:absolute;left:75.85px;top:242.16px" class="cls_007"><span class="cls_007">number</span></div>
<div style="position:absolute;left:61.20px;top:277.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Indifferent: don’t care</span></div>
<div style="position:absolute;left:61.20px;top:311.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Reverse: opposite of what analist thought</span></div>
<div style="position:absolute;left:61.20px;top:346.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Questionable: preferences not clear</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">29</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Kano diagram</span></div>
<div style="position:absolute;left:320.25px;top:101.68px" class="cls_009"><span class="cls_009">satisfied</span></div>
<div style="position:absolute;left:493.25px;top:166.96px" class="cls_009"><span class="cls_009">one-dimensional</span></div>
<div style="position:absolute;left:294.75px;top:195.28px" class="cls_009"><span class="cls_009">attractive</span></div>
<div style="position:absolute;left:603.75px;top:317.20px" class="cls_009"><span class="cls_009">functional</span></div>
<div style="position:absolute;left:11.25px;top:322.96px" class="cls_009"><span class="cls_009">dysfunctional</span></div>
<div style="position:absolute;left:413.88px;top:337.12px" class="cls_009"><span class="cls_009">must-be</span></div>
<div style="position:absolute;left:306.13px;top:478.72px" class="cls_009"><span class="cls_009">dissatisfied</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">30</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">COTS selection</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">COTS: Commercial-Off-The-Shelf</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Iterative process:</span></div>
<div style="position:absolute;left:90.80px;top:213.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Define requirements</span></div>
<div style="position:absolute;left:90.80px;top:241.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Select components</span></div>
<div style="position:absolute;left:90.80px;top:270.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Rank components</span></div>
<div style="position:absolute;left:90.80px;top:299.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Select most appropriate component, or iterate</span></div>
<div style="position:absolute;left:61.20px;top:329.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> Simple ranking: weight * score (WSM - Weighted</span></div>
<div style="position:absolute;left:75.85px;top:357.12px" class="cls_007"><span class="cls_007">Scoring Method)</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">31</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Crowdsourcing</span></div>
<div style="position:absolute;left:61.20px;top:141.12px" class="cls_017"><span class="cls_017">1.</span><span class="cls_007">  Go to LEGO site</span></div>
<div style="position:absolute;left:61.20px;top:173.28px" class="cls_017"><span class="cls_017">2.</span><span class="cls_007">  Use CAD tool to design your favorite castle</span></div>
<div style="position:absolute;left:61.20px;top:205.20px" class="cls_017"><span class="cls_017">3.</span><span class="cls_007">  Generate bill of materials</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_017"><span class="cls_017">4.</span><span class="cls_007">  Pieces are collected, packaged, and sent to you</span></div>
<div style="position:absolute;left:61.20px;top:300.24px" class="cls_017"><span class="cls_017">5.</span><span class="cls_007">  Leave your model in LEGO’s gallery</span></div>
<div style="position:absolute;left:61.20px;top:332.16px" class="cls_017"><span class="cls_017">6.</span><span class="cls_007">  Most downloaded designs are prepackaged</span></div>
<div style="position:absolute;left:61.20px;top:395.28px" class="cls_006"><span class="cls_006">q</span><span class="cls_007"> No requirements engineers needed!</span></div>
<div style="position:absolute;left:61.20px;top:427.20px" class="cls_006"><span class="cls_006">q</span><span class="cls_007"> Gives rise to new business model</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">32</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Requirements specification</span></div>
<div style="position:absolute;left:61.20px;top:144.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_018">  readable</span></div>
<div style="position:absolute;left:61.20px;top:173.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_018">  understandable</span></div>
<div style="position:absolute;left:61.20px;top:202.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_018">  non-ambiguous</span></div>
<div style="position:absolute;left:61.20px;top:231.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_018">  complete</span></div>
<div style="position:absolute;left:61.20px;top:260.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_018">  verifiable</span></div>
<div style="position:absolute;left:61.20px;top:288.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_018">  consistent</span></div>
<div style="position:absolute;left:61.20px;top:317.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_018">  modifiable</span></div>
<div style="position:absolute;left:61.20px;top:346.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_018">  traceable</span></div>
<div style="position:absolute;left:61.20px;top:375.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_018">  usable</span></div>
<div style="position:absolute;left:61.20px;top:404.32px" class="cls_008"><span class="cls_008">§</span></div>
<div style="position:absolute;left:88.20px;top:404.32px" class="cls_018"><span class="cls_018">…</span></div>
<div style="position:absolute;left:61.20px;top:432.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_018">  ...</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">33</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">IEEE Standard 830</span></div>
<div style="position:absolute;left:61.20px;top:144.16px" class="cls_018"><span class="cls_018">1. Introduction</span></div>
<div style="position:absolute;left:375.70px;top:144.16px" class="cls_018"><span class="cls_018">2. General description</span></div>
<div style="position:absolute;left:88.20px;top:173.20px" class="cls_018"><span class="cls_018">1.1. Purpose</span></div>
<div style="position:absolute;left:402.70px;top:173.20px" class="cls_018"><span class="cls_018">2.1. Product perspective</span></div>
<div style="position:absolute;left:88.20px;top:202.24px" class="cls_018"><span class="cls_018">1.2. Scope</span></div>
<div style="position:absolute;left:402.70px;top:202.24px" class="cls_018"><span class="cls_018">2.2. Product functions</span></div>
<div style="position:absolute;left:88.20px;top:231.28px" class="cls_018"><span class="cls_018">1.3. Definitions, acronyms</span></div>
<div style="position:absolute;left:402.70px;top:231.28px" class="cls_018"><span class="cls_018">2.3. User characteristics</span></div>
<div style="position:absolute;left:88.20px;top:255.28px" class="cls_018"><span class="cls_018">and abbreviations</span></div>
<div style="position:absolute;left:402.70px;top:260.32px" class="cls_018"><span class="cls_018">2.4. Constraints</span></div>
<div style="position:absolute;left:88.20px;top:284.32px" class="cls_018"><span class="cls_018">1.4. References</span></div>
<div style="position:absolute;left:402.70px;top:288.16px" class="cls_018"><span class="cls_018">2.5. Assumptions and</span></div>
<div style="position:absolute;left:88.20px;top:312.16px" class="cls_018"><span class="cls_018">1.5. Overview</span></div>
<div style="position:absolute;left:402.70px;top:312.16px" class="cls_018"><span class="cls_018">dependencies</span></div>
<div style="position:absolute;left:375.70px;top:341.20px" class="cls_018"><span class="cls_018">3. Specific requirements</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">34</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">IEEE Standard 830 (cntd)</span></div>
<div style="position:absolute;left:61.20px;top:145.28px" class="cls_019"><span class="cls_019">3. Specific requirements</span></div>
<div style="position:absolute;left:375.70px;top:145.28px" class="cls_019"><span class="cls_019">3.2. Functional requirements</span></div>
<div style="position:absolute;left:88.20px;top:168.32px" class="cls_019"><span class="cls_019">3.1. External interface</span></div>
<div style="position:absolute;left:402.70px;top:168.32px" class="cls_019"><span class="cls_019">3.2.1. User class 1</span></div>
<div style="position:absolute;left:133.20px;top:187.28px" class="cls_019"><span class="cls_019">requirements</span></div>
<div style="position:absolute;left:447.70px;top:191.12px" class="cls_019"><span class="cls_019">3.2.1.1. Functional req. 1.1</span></div>
<div style="position:absolute;left:133.20px;top:210.32px" class="cls_019"><span class="cls_019">3.1.1. User interfaces</span></div>
<div style="position:absolute;left:447.70px;top:214.16px" class="cls_019"><span class="cls_019">3.2.1.2. Functional req. 1.2</span></div>
<div style="position:absolute;left:133.20px;top:233.12px" class="cls_019"><span class="cls_019">3.1.2. Hardware interfaces</span></div>
<div style="position:absolute;left:133.20px;top:256.16px" class="cls_019"><span class="cls_019">3.1.3. Software interfaces</span></div>
<div style="position:absolute;left:402.70px;top:260.24px" class="cls_019"><span class="cls_019">3.2.2. User class 2</span></div>
<div style="position:absolute;left:133.20px;top:279.20px" class="cls_019"><span class="cls_019">3.1.4. Comm. interfaces</span></div>
<div style="position:absolute;left:447.70px;top:283.28px" class="cls_019"><span class="cls_019">…</span></div>
<div style="position:absolute;left:375.70px;top:306.32px" class="cls_019"><span class="cls_019">3.3. Performance requirements</span></div>
<div style="position:absolute;left:375.70px;top:329.12px" class="cls_019"><span class="cls_019">3.4. Design constraints</span></div>
<div style="position:absolute;left:375.70px;top:352.16px" class="cls_019"><span class="cls_019">3.5. Software system attributes</span></div>
<div style="position:absolute;left:375.70px;top:375.20px" class="cls_019"><span class="cls_019">3.6. Other requirements</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">35</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Requirements management</span></div>
<div style="position:absolute;left:219.69px;top:148.96px" class="cls_009"><span class="cls_009">too early</span></div>
<div style="position:absolute;left:230.25px;top:178.00px" class="cls_009"><span class="cls_009">freeze</span></div>
<div style="position:absolute;left:497.31px;top:344.56px" class="cls_009"><span class="cls_009">requirements</span></div>
<div style="position:absolute;left:530.62px;top:373.60px" class="cls_009"><span class="cls_009">creep</span></div>
<div style="position:absolute;left:230.63px;top:459.28px" class="cls_009"><span class="cls_009">time</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">36</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Requirements management</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Requirements identification (number, goal-</span></div>
<div style="position:absolute;left:75.85px;top:173.28px" class="cls_007"><span class="cls_007">hierarchy numbering, version information,</span></div>
<div style="position:absolute;left:75.85px;top:202.32px" class="cls_007"><span class="cls_007">attributes)</span></div>
<div style="position:absolute;left:61.20px;top:237.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Requirements change management (CM)</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Requirements traceability:</span></div>
<div style="position:absolute;left:90.80px;top:305.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Where is requirement implemented?</span></div>
<div style="position:absolute;left:90.80px;top:334.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Do we need this requirement?</span></div>
<div style="position:absolute;left:90.80px;top:362.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Are all requirements linked to solution elements?</span></div>
<div style="position:absolute;left:90.80px;top:391.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> What is the impact of this requirement?</span></div>
<div style="position:absolute;left:90.80px;top:420.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Which requirement does this test case cover?</span></div>
<div style="position:absolute;left:61.20px;top:450.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007">Related to Design Space Analysis</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">37</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">The 7 sins of the analyst</span></div>
<div style="position:absolute;left:61.20px;top:179.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> noise</span></div>
<div style="position:absolute;left:61.20px;top:213.12px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> silence</span></div>
<div style="position:absolute;left:61.20px;top:248.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> overspecification</span></div>
<div style="position:absolute;left:61.20px;top:283.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> contradictions</span></div>
<div style="position:absolute;left:61.20px;top:317.28px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> ambiguity</span></div>
<div style="position:absolute;left:61.20px;top:352.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> forward references</span></div>
<div style="position:absolute;left:61.20px;top:386.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> wishful thinking</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">38</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background39.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Functional vs. Non-Functional Requirements</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> functional requirements: the system services</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_007"><span class="cls_007">which are expected by the users of the system.</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> non-functional (quality) requirements: the set of</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_007"><span class="cls_007">constraints the system must satisfy and the</span></div>
<div style="position:absolute;left:88.20px;top:265.20px" class="cls_007"><span class="cls_007">standards which must be met by the delivered</span></div>
<div style="position:absolute;left:88.20px;top:294.24px" class="cls_007"><span class="cls_007">system.</span></div>
<div style="position:absolute;left:97.20px;top:328.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> speed</span></div>
<div style="position:absolute;left:97.20px;top:357.28px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> size</span></div>
<div style="position:absolute;left:97.20px;top:385.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> ease-of-use</span></div>
<div style="position:absolute;left:97.20px;top:414.16px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> reliability</span></div>
<div style="position:absolute;left:97.20px;top:443.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> robustness</span></div>
<div style="position:absolute;left:97.20px;top:472.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> portability</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">39</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:21450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background40.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Validation of requirements</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> inspection of the requirement specification w.r.t.</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_007"><span class="cls_007">correctness, completeness, consistency,</span></div>
<div style="position:absolute;left:88.20px;top:202.32px" class="cls_007"><span class="cls_007">accuracy, readability, and testability.</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> some aids:</span></div>
<div style="position:absolute;left:97.20px;top:305.20px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> structured walkthroughs</span></div>
<div style="position:absolute;left:97.20px;top:334.24px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> prototypes</span></div>
<div style="position:absolute;left:97.20px;top:362.32px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> develop a test plan</span></div>
<div style="position:absolute;left:97.20px;top:391.12px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> tool support for formal specifications</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">40</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background41.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">Summary</span></div>
<div style="position:absolute;left:61.20px;top:144.24px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> goal: a maximally clear, and maximally complete,</span></div>
<div style="position:absolute;left:88.20px;top:173.28px" class="cls_007"><span class="cls_007">description of WHAT is wanted</span></div>
<div style="position:absolute;left:61.20px;top:208.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> RE involves  </span><span class="cls_012">elicitation</span><span class="cls_007">, </span><span class="cls_012">specification</span><span class="cls_007">, </span><span class="cls_012">validation </span><span class="cls_007">and</span></div>
<div style="position:absolute;left:88.20px;top:237.12px" class="cls_012"><span class="cls_012">negotiation</span></div>
<div style="position:absolute;left:61.20px;top:271.20px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> modeling the UoD poses both analysis and</span></div>
<div style="position:absolute;left:88.20px;top:300.24px" class="cls_007"><span class="cls_007">negotiation problems</span></div>
<div style="position:absolute;left:61.20px;top:334.32px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> you must realize that, as an analyst, you are more</span></div>
<div style="position:absolute;left:88.20px;top:363.12px" class="cls_007"><span class="cls_007">than an outside observer</span></div>
<div style="position:absolute;left:61.20px;top:398.16px" class="cls_006"><span class="cls_006">§</span><span class="cls_007"> a lot is still done in natural language, with all its</span></div>
<div style="position:absolute;left:88.20px;top:427.20px" class="cls_007"><span class="cls_007">inherent problems</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">41</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture9/background42.jpg" width=720 height=540></div>
<div style="position:absolute;left:61.20px;top:39.20px" class="cls_005"><span class="cls_005">One final lesson</span></div>
<div style="position:absolute;left:284.64px;top:152.56px" class="cls_009"><span class="cls_009">Walking on water</span></div>
<div style="position:absolute;left:344.64px;top:210.64px" class="cls_009"><span class="cls_009">and</span></div>
<div style="position:absolute;left:183.57px;top:268.48px" class="cls_009"><span class="cls_009">developing software from a specification</span></div>
<div style="position:absolute;left:323.01px;top:325.60px" class="cls_009"><span class="cls_009">are easy</span></div>
<div style="position:absolute;left:287.01px;top:383.68px" class="cls_009"><span class="cls_009">if they are frozen</span></div>
<div style="position:absolute;left:67.64px;top:446.56px" class="cls_018"><span class="cls_018">(E.V. Berard, Essays on object-oriented software engineering)</span></div>
<div style="position:absolute;left:5.67px;top:503.76px" class="cls_004"><span class="cls_004">©2008 John Wiley & Sons Ltd.</span></div>
<div style="position:absolute;left:681.31px;top:514.50px" class="cls_010"><span class="cls_010">42</span></div>
<div style="position:absolute;left:5.67px;top:518.64px" class="cls_004"><span class="cls_004"> </span><A HREF="http://www.wileyeurope.com/college/van/">www.wileyeurope.com/college/van</A> vliet</div>
</div>

</body>
</html>
