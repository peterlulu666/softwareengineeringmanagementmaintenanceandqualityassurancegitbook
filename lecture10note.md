## Quiz        

1. The Classic Modeling Technique 
    - **Entity--Relationship Modeling (ERM)** 
        - is a data modeling technique, pioneered by Chen in the seventies. UML class diagrams are based on ERM 
        - used to model the structure of data 
    - **Finite State Machines (FSM)** 
        - are used to model states and state transitions. In the early days, certain types of formal languages, as for instance used in compilers, were modeled as finite state machines. UML state machine diagrams are based on finite state machines 
        - to model states and state transitions of a system 
    - **Data Flow Diagrams (DFD)** model 
        - a system as a set of processes and data flows that connect these processes. It is the notation used in data flow design. DFDs resemble the UML sequence diagram 
        - to model functional decomposition with respect to data
    - **CRC cards** 
        - are a simple requirements elicitation tool. Much of the information collected on CRC cards can also be represented in UML communication diagrams 
        - a simple notation to document collaborative design decisions
    - **UML** evolved from some of the mainstream **object-oriented** analysis and design methods 

2. The **Entity--Relationship Modeling** 
    - Entity--relationship modeling (ERM), as pioneered by Chen, is directed at modeling the logical, semantic structure of the UoD, rather than its realization in some database system 
    - entity distinguishable object of some type 
    - entity type type of a set of entities 
    - attribute value piece of information (partially) describing an entity 
    - attribute type of a set of attribute values 
    - relationship association between two or more entities 

3. The **Finite State Machine** 
    - A simple yet powerful formalism for specifying states and state transitions is the Finite State Machine (FSM) 
    - In a **state transition diagram**, **states** are represented as **bubbles** with a label identifying the state, and **transitions** are indicated as labeled **arcs** from one state to another, where the label denotes the stimulus which triggers the transition 
    - A possible way out is to allow for a **hierarchical** decomposition of FSMs. This is the essence of a notation known as **statechart**  

4. The Data Flow Diagrams 
    - **External entities** are the source or destination of a transaction. These entities are located outside the domain considered in the data flow diagram. External entities are indicated as squares 
    - **Processes** transform data. Processes are denoted by circles 
    - **Data flows** between processes, external entities and data stores. A data flow is indicated by an arrow. Data flows are paths along which data structures travel 
    - **Data stores** lie between two processes. This is indicated by the name of the data store between two parallel lines. Data stores are places where data structures are stored until needed 

5. The CRC Cards 
    - Class 
    - Responsibility 
    - Collaborators 
    - CRC cards are especially helpful in the **early** phases of software development, to help identify components, discuss design issues in multi-disciplinary teams, and specify components informally 

6. On Objects and Related Stuff 
    - objects may be regarded as **implementations of abstract data types (ADTs)** 
    - An object then consists of a **mutable state** 
    - The **modeling viewpoint** 
        - an **object** is a conceptual **model of** some **part of** a real or imaginary **world** 
        - the three aspects of pbjects 
            - identity + variables + operations 
            - identity + state + behavior 
    - The **philosophical viewpoint** 
        - objects are **existential abstractions**, as opposed to universal abstraction 
        - Some kinds of entity have a natural beginning and end. They are created at some point in time, exist for a while, and are ultimately destroyed 
        - Other kinds of entities, such as numbers, dates and colors, have ‘eternal’ existence. They are not instantiated; they cannot be changed; they ‘live’ forever. These entities are usually referred to as values 
    - The **software engineering viewpoint** 
        - objects are **data abstractions**, encapsulating data as well as operations on those data 
        - A programming language that merely allows us to encapsulate abstract data types in modules is often termed **object-based** 
        - The adjective object-oriented then is reserved for languages that also support **inheritance** 
    - The implementation viewpoint 
        - an object is a contiguous **structure in memory** 
        - There is no concept of sharing. This scheme is known as **value semantics**. Value semantics is inadequate for object-oriented systems, since such systems require sharable objects 
        - The opposite scheme is **reference semantics**: data is represented as either an atomic object or as an aggregate of references to other objects 
    - The formal viewpoint 
        - an object is a **state machine** with a finite set of states and a finite set of state functions 
    
7. The Objects and attributes 
    - As noted above, **objects** are characterized by a set of **attributes** (properties). A table has legs, a table top, size, color, etc 
    - In ERM(Entity-relationship modeling), attributes represent **intrinsic** properties of entities, properties whose value does **not depend** on other entities 
    - Attributes denote **identifying** and **descriptive** properties, such as name or weight 
    - Relationships on the other hand denote **mutual** properties, such as an employee being assigned to a project or a book being borrowed by a member 
    - In UML, these **relationships** are called **associations** 
    - In UML, the **distinction** between **attributes** and **relationships** formally does **not exist**. Both are properties of a class 
    - It is considered good practice in UML though to model **simple** properties as **attributes**, and more **complex** properties as **associations** 

8. At the programming-language level, **single inheritance** corresponds to a tree structure, while **multiple inheritance** corresponds to a DAG 

9. The 13 diagrams of UML 2 
    - The **static** diagrams shows the **static structure** of a system 
    - Other diagrams give a **dynamic**, or behavioral view, i.e. they show **what happens when the system is executed** 
        - For instance, a **sequence diagram** shows which messages are exchanged between instances of classes 

10. The Class Diagram 
    - A class diagram is a graph in which the nodes are objects (classes) and the edges are relationships between objects 
    - These relationships fall into two classes 
        - **generalizations** 
        - **associations** 
    - The most common example of a generalization-type class diagram is a diagram depicting the **subclass**--**superclass** hierarchy 
    - The attributes of a class denote properties of that class. E.g., publisher is a property of Publication. Next to attributes, UML has another way of denoting properties of a class, viz. **associations**. A UML association is depicted as a solid line connecting two classes 

11. The **time ordering** in which this sequence of messages has to occur may be depicted in a **sequence diagram** 

12. **Communication diagrams** emphasize the objects and their **relationships** relevant to a particular interaction 

13. The sequence numbers 
    - In a **sequence diagram**, sequence numbers are **optional** 
    - in a **communication diagram**, they are **mandatory** since the ordering does not show itself graphically 

14. The Component Diagram 
    - When designing **larger systems**, it may be useful to be able to identify entities larger than a single class. Such can be done in a **component diagram** 
    - a component diagram is a **class diagram** with the **stereotype <<component>>** 
    - Components are connected by interfaces 
    - it uses the so-called ball-and-socket notation to depict interfaces 

15. The use case diagram provides an overview of a set of use cases 


























        
    



















