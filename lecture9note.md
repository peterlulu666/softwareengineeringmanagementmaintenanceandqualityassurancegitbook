## Quiz        

1. The requirements engineering 
    - the **first major step** towards the **solution of a data processing problem** 
    - we do **not** yet address the question of **how to achieve** these user **requirements** in terms of system components and their interaction 
    - The **result of the requirements engineering** phase is documented in the **requirements specification** 
        - It is the basis for a **contract**, be it formal or informal, between the **client** of the system and the development organization 
        - The requirements specification serves as **a starting point** for the next phase, the **design** phase 

2. We distinguish four processes in requirements engineering 
    - Requirements **elicitation** 
        - requirements **elicitation** is about **understanding** the problem 
    - Requirements **specification** 
        - Once the problem is understood, it has to be **described** 
        - This document **describes** the product to be **delivered**, **not** the process of how it is **developed** 
    - Requirements **validation** and **verification** 
        - Once the problem is described, the different parties involved have to **agree** upon its **nature** 
    - Requirements **negotiation** 
        - agreeing **boundaries** 
        - Usually, requirements have to be **negotiated**. Because of time **constraints** or other factors, a selection may have to be made from the list of requirements put forth 
    - Obviously, these processes involve **iteration** and **feedback** 
    - In document-driven approaches, these iterations preceed **design** and **implementation**  
    - In agile processes, **design** and **implementation** is part of the iteration and feedback loop 

3. The Requirements Elicitation 
    - The primary goal of the requirements engineering phase is to **elicit** the **contours** and **constituents** of this fuzzy problem. This process is also known as **implicit** conceptual modeling 
    - During requirements engineering we are **modeling** part of **reality**. The part of reality in which we are interested is referred to as the **universe of discourse (UoD)** 
        - Example UoDs are a **library system**, **a factory automation system**, **an assembly line**, **an elevator system** 
    - The model constructed during the requirements engineering phase is an **explicit** conceptual model of the UoD 
        - During conceptual modeling, an **implicit** conceptual model is turned into an **explicit** one 
            - **analysis** problems 
                - the implicit conceptual model is **not verbalized** 
                - the user and analyst talk a **different language** 
                - the implicit conceptual model **cannot** be completely **codified** 
            - **negotiation** problems 
                - the implicit **conceptual models** of people in the UoD may **differ** 
                - because of opposing **interests** of people involved 

4. Subtle mismatches between the analyst’s notion of terms and concepts and their proper meaning within the domain being modeled can have profound effects 
    - An implicit conceptual model consists of the **background knowledge** shared by people in the UoD 
    - an implicit conceptual model contains habits, customs, **prejudices** and even inconsistencies 
    - In research on **human information** processing one often uses a model in which human memory consists of two components: a **short-term memory** in which information is being processed, and a **long-term memory** in which the permanent knowledge is stored 
        - **Short-term memory** has a **limited capacity**: one often says that it has about seven slots 
        - **Long-term memory** on the other hand has a very **large capacity** 
    - **Humans** are also inclined to be **prejudiced** about selecting and using information 
    - **Humans** are **not** very capable of **rational thinking** 

5. The Requirements Engineering Paradigms 
    - The Negotiation 
        - Most requirements engineering methods, and software development methods in general, are **Taylorian** in nature. Around the turn of this century, **Taylor** introduced the notion of ‘scientific management’, in which tasks are recursively decomposed into **simpler tasks** and each task has one ‘best way’ to accomplish it 
        - Though this view has its merits in drawing up requirements in purely **technical** realms, many UoDs of interest involve **people** as well -- people whose model of the world is **incomplete**, **subjective**, **irrational**, and may **conflict** with the world view of others 
        - In such cases, the **analyst** is **not** a passive outside **observer** of the UoD. Rather, he actively **participates** in the **shaping** of the UoD 

6. The assumptions 
    - Analysts have a set of **assumptions** about the nature of the subject of **study**. Such a set of **assumptions** is commonly called a ‘**paradigm**’ 
        - In our field, these assumptions concern the way in which analysts acquire **knowledge** (epistemological assumptions) and their view of the social and technical **world** (ontological assumptions) 
    - The two dimensions 
        - The assumptions about **knowledge** result in an **objectivist--subjectivist** dimension 
            - If the analyst takes the **objectivist** point of view, he applies models and methods derived from the **natural sciences** to arrive at the one and only truth 
            - In the subjectivist position, his principal concern is to **understand how the individual creates, modifies and interprets the world** he or she is in 
        - The assumptions about the **world** result in an **order--conflict** dimension 
            - The **order** point of view emphasizes **order, stability, integration, and consensus** 
            - On the other hand, the **conflict** view stresses **change, conflict, and disintegration** 
    - The four paradigms for requirements engineering
        - Functionalism (objective--order) 
            - In the functionalist paradigm, the developer is the system expert who searches for measurable **cause--effect** relationships. An **empirical** organizational reality is believed to exist, independent of the observer 
        - Social-relativism (subjective--order) 
            - The analyst is a **change agent**. He seeks to facilitate the **learning** of all people involved 
        - Radical-structuralism (objective--conflict) 
            - In the radical paradigm the key assumption is that system development intervenes in the **conflict** between two or more social **classes** for power, prestige, and resources 
        - Neohumanism (subjective--conflict) 
            - The system developer acts as a social **therapist** in an attempt to draw **together**, in an open discussion, a diverse group of individuals, including customers, labor, and various levels of management 

7. The Requirements Elicitation Technique 
    - The two main sources of information for the requirements elicitation process are the **users** and the (application) **domain** 
    - The elicitation techniques
        - Interview 
        - Delphi technique 
        - Brainstorming session 
        - Task analysis 
        - Scenario(Use-case)analysis 
        - Ethnograpl 
        - Form analysis Analysis of natural language descriptions 
        - Synthesis of reqs from an existing system 
        - Domain analysis 
        - Use of reference models 
        - Business Process Redesign(BPR) 
        - Prototyping 
    - **Asking** 
        - **Asking** may take the form of an **interview**, a **brainstorm**, or a **questionnaire** 
        - a **Delphi** technique may be employed. The **Delphi** technique is an **iterative** technique in which information is exchanged in a written form until a **consensus** is reached 
    - Task analysis 
        - Task analysis is a technique to obtain a hierarchy of tasks and subtasks to be carried out by people working in the domain 
        - Task analysis is often applied at the stage when (details about) the human--computer interaction component are being **decided** upon 
        - things they act on 
        - achieve a goal 
        - broad scope 
        - **asking** is the **least certain** strategy
    - Scenario-based analysis 
        - Instead of looking for generic plans as in interviews or task analysis, the analyst may study **instances** of tasks. A scenario is a **story** which tells us how a specific task instance is executed 
        - **scenario-based** analysis is often called **use-case analysis** 
        - The focus then is on the **process** aspect, showing how the system proceeds through successive states 
        - Alternatively, the same scenario may be looked at from a **user** perspective 
    - Ethnographic 
        - In ethnography, groups of people are studied in their **natural settings** 
        - Likewise, user requirements can be studied by participating in their **daily work** for a period of time 
    - Form analysis 
        - A lot of information about the domain being modeled can often be found in various forms being used 
        - Forms provide us with information about the **data** objects of the domain, their **properties**, and their **interrelations** 
    - Natural language descriptions 
        - natural language descriptions (and forms) provide the analyst with **background** information to be used in conjunction with other elicitation techniques such as interviews 
        - A practical problem with natural language descriptions is that they are often **not** kept **up-to-date** 
        - Natural language descriptions are often taken as a starting point in **object-oriented** analysis techniques 
    - Derivation from an existing system 
        - Starting from an **existing system** we may formulate the requirements of the **new system** 
    - The domain analysis 
        - The meta-requirements analysis process 
    - Business Process Redesign 
        - In many software development projects, the people involved jump to **conclusions** rather quickly: **automation** is the answer 
    - Prototyping
        - Given the fact that it is **difficult**, if not impossible, to build the right system from the **start**, we may decide to use **prototypes** 
        - **prototyping** is the **least uncertain** strategy 

8. The Goals and Viewpoint 
    - The hierarchical structure 
        - higher-level requirements are decomposed into lower-level one 
    - The other structuring method links requirements to specific stakeholder 
        - management may have a set of requirements, and the end-users may have (another) set of requirements 
    - These different sets of **requirements** are called **viewpoints** 
    - In both cases, elicitation and structuring go hand in hand 

9. The Prioritizing Requirement 
    - The MoSCoW 
        - **Must haves**: these are the **top priority requirements**, the ones that definitely have to be realized in order to make the system acceptable to the customer 
        - **Should haves**: these requirements are not strictly mandatory, but they are **highly desirable** 
        - **Could haves**: **if time allows**, these requirements will be realized as well. In practice, they usually won’t 
        - **Won’t haves**: these requirements will **not** be realized in the **present** version. They are recorded though. They will be considered again for a next version of the system 
    - The Kano model 
        - Attractive
        - Must-be 
        - One-dimensional 
        - Indifferent 
        - Reverse 
        - Questionable 
    - Kano diagram order 
        - One-dimensional 
        - Must-be 
        - Attractive 

10. The COTS selection 
    - The Commercial Off The Shelf software 
    - the customer has to choose from what is available 
    - **COTS selection** is an **iterative process** comprising the following steps 
        - **Define requirements**. As in ordinary requirements elicitation processes, a list of requirements for the product is derived. Any of the elicitation techniques discussed above may be used in this process 
        - **Select components**. A set of components that can possibly handle the require- ments posed is determined. This selection process may involve market research, internet browsing, and a variety of other techniques 
        - **Rank the components**. The components are ranked in the order in which they satisfy the requirements 
        - **Select the most appropriate component, or iterate** 

11. The Requirements specification 
    - readable 
    - understandable 
    - non-ambiguous 
        - A requirements specification should be unambiguous, both to those who create it and to those who use it 
    - complete 
    - verifiable 
        - A requirements specification should be **verifiable**. This means that there must be a finite process to determine whether or not the **requirements** have been **met** 
    - consistent 
        - A requirements specification should be (internally) **consistent** i.e. different parts of it should **not** be in **conflict** with each other 
    - modifiable 
        - A requirements specification should be modifiable. Software models part of reality. Therefore it changes. The corresponding requirements specification has to **evolve** together with the reality being modeled 
    - traceable 
        - A requirements specification should be traceable. The origin and rationale of each and every requirement must be traceable 
    - usable 
    - A requirements specification should be **correct** 
    - A requirements specification should be **complete** 
    - Requirements should be **ranked** for **importance** or **stability**. Typically, some requirements are more important than others 

12. The IEEE Standard 830 
    - Introduction 
        - Purpose 
        - Scope 
        - Definitions, acronyms and abbreviations 
        - References 
        - Overview 
    - Overall description 
        - Product perspective 
        - Product functions 
        - User characteristics 
        - Constraints 
        - Assumptions and dependencies 
        - Requirements subsets 
    - Specific requirement 
        - External interface requirements 
            - User interfaces 
            - Hardware interfaces 
            - Software interfaces 
            - Communications interfaces 
        - Functional requirements 
            - User class 1 
                - Functional requirement 1.1 
                - Functional requirement 1.2 . . . 
            - User class 2 . . . 
        - Performance requirements 
        - Design constraints 
        - Software system attributes 
        - Other requirements 

13. The Requirements Management 
    - And even after the requirements phase is ended, requirements will change and new requirements will be put forth. The latter phenomenon is known as **requirements creep**, and is the cause for many run-away projects 
    - Requirements management involves three activities 
        - requirements identification 
        - requirements change management 
        - requirements traceability 
        - Related to Design Space Analysis 

14. The Requirements Specification Technique
    - **Meyer** lists **seven sins** which may beset the analyst when using **natural language** 





































