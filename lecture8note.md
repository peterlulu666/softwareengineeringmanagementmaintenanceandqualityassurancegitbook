## Quiz        

1. A Systems View of Project Control 
    - The variables that play a role in controlling a system may be categorized into three classes 
        - irregular variables 
            - cannot be controlled 
            - Irregular variables are those variables that are **input** to the system being controlled. Irregular variables **cannot** be **varied** by the entity that controls the system. Their values are **determined** by the **system’s environment** 
            - Examples of irregular variables are the computer **experience of the user** or the **project staffing level** 
        - goal variables 
            - minimize development time 
            - maximize efficiency 
            - lowest cost 
            - as cheaply as possible 
            - maximize quality 
        - control variable 
            - Control variables are entities which **can be manipulated** by the project manager in order to achieve the goals set forth 
            - Examples of possible control variables are the **tools to be used**, **project organization**, **efficiency of the resulting software** 
    - It is **not** possible to make a **rigid** separation between the various sets of variable 
        - If the **requirements** are stable and fixed, one may for instance try to control the project by employing adequate personnel and using a proper set of tools 
        - For another project, **manpower** may be fixed and one may try to control the project by extending the delivery date, relaxing quality constraints, etc 
    - in order to be able to control a project, the different sets of **variables must be known** 
    - The **conditions** for effective **control** of a system are used 
        - the controlling entity must know the **goals of the system** 
        - the controlling entity must have **sufficient control variety** 
        - the controlling entity must have **information on the state, input and output of the system** 
        - the controlling entity must have a **conceptual control model**. It must know **how and to what extent the different variables depend on and influence each other** 

2. We will group project **characteristics** into three **classes** 
    - product characteristics 
    - process characteristics 
    - resource characteristics 

3. The **certainty** 
    - Product certainty 
        - **requirements** are **clearly** specified 
            - high 
        - User **requirements change** frequently 
            - low 
    - Process certainty 
        - the possibility of redirecting the development process, the degree to which the process can be measured and the knowledge we have about the effect of **control actions** 
            - high 
        - the degree to which new, **unknown tools** are being used 
            - low 
    - Resource certainty 
        - The major determinant here is the availability of the **appropriate qualified personnel** 

4. The archetypal control situations 
    - Realization problem 
        - an ideal situation 
    - Allocation problem 
        - Controlling a project of this kind tends to become one of **controlling capacity** 
    - Design problem 
        - Note that the adjective **design** refers to the design of the **project**, **not** the design of the **software** 
        - which **milestones** are to be identified, which **documents** must be delivered and when, what **personnel** must be allocated, how will **responsibilities** be assigned 
    - Exploration problem 
        - A critical success factor in these cases is **the commitment of all people involved** 

5. The Risk Management 
    - Top ten risk factor 
    - A risk management strategy involves 
        - Identify the risk factors 
        - Determine the risk exposure 
            - probability * effect 
        - Develop strategies to **mitigate** the **risks** 
            - There are three general strategies to **mitigate** risks: **avoidance, transfer, and acceptance** 
        - Handle risks 
    - C1: Risks related to customers and users 
    - C2: Risks that have to do with the **scope** of the project and its **requirements** 
    - C3: Risks that concern the **execution** of the project: staffing, methodology, planning, control 
    - C4: Risks that result from **changes** in the **environment**, such as changes in the organization in which the system is to be embedded, or dependencies on outsourcing partners 

6. Techniques for Project Planning and Control
    - work breakdown structure 
        - We may graphically depict the project and its constituent activities by a **work breakdown structure** (WBS). The WBS reflects the decomposition of a project into **subtasks** down to a level needed for effective **planning** and **control** 
    - PERT charts 
        - These **network** diagrams are often termed PERT charts 
        - PERT is an acronym for **Program Evaluation and Review Technique** 
        - 1950s 
    - Gantt chart 
        - In a Gantt chart, the **time** span of each activity is depicted by the length of a **segment** drawn on an adjacent calendar 
        - **graphical visualization** of a **schedule** 
    - activity-on-node 
        - In an ‘activity-on-node’ network, the activities are depicted as nodes, while the arrows denote precedence relations between activities 




    



































