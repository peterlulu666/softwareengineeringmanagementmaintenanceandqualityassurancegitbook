## Quiz        

1. The coordination
    - external influences on the coordination mechanism
        - originate from **characteristics** of the projec
    - internal influences on the coordination mechanism
        - originate from the project’s organizational **environment**
        - conflicting coordination mechanisms 
            - conflicts between the project and the environment
            - The characteristics of the project may ask for a **flexible**, informal type of coordination mechanism, where the commitment of specialized individuals, rather than a strict adherence to **formal procedures**, is a critical success factor

2. People Management
    - A team is made up of **individuals**, each of whom has personal **goals**
        - the **individual** goals are **reconciled** into one goal for the **project** as a whole
        - it is important to identify project goals at an **early stage**
    - the functionality delivered and define **productivity** as the amount of functionality delivered per unit of time
        - Productivity is mostly defined as the **number of lines of code delivered per man-month**
        - One of the big **dangers** of using this measure is that people tend to produce as **much code** as possible
        - cost driver is the amount of **code** to be **delivered**
        - Writing less code and **reuse** of existing code is one way to **save** time and money
            - Using the amount of **code delivered per man-month** as a **productivity indicator** offers **no incentive** for software reuse
    - Another aspect of people assessment occurs in **group processes** like **peer reviews**, **inspections** and **walkthroughs**
        - used during **verification** and **validation** activities 
        - discover **errors** or assess the **quality** 
    - coordination
        - **informal** and **interpersonal communication** is known to be a primary way in which information flows into and through a development organization
        - blame-free culture 
        - the solicitation of commitment and partnership 

3. Mintzberg’s coordination mechanism 
    - Mintzberg
        - work in McGill University
        - graduat from MIT
    - People process product
        - Simple structure
            - **direct supervision** 
            - **new**, relatively **small** organization 
        - Machine bureaucracy 
            - **standardization of work processes**
        - Divisionalized form 
            - **standardization of work outputs**
            - division is project
            - goals are to be reached
            - This coordination mechanism is possible only when **the end result is specified precisely** 
        - Professional bureaucracy 
            - **standardization of worker skills** 
        - Adhocracy
            - **mutual adjustment**
            - work is divided amongst many specialists 
            - The project’s success depends on the ability of the group as a whole to reach a non-specified goal in a non-specified way

4. Management Styles
    - Reddin’s management styles 
    - Relation directedness This concerns attention to an **individual** and his **relationship** to other individuals within the organization 
    - Task directedness This concerns attention to the **results** to be **achieved** and the way in which these results must be achieved 
    - Separation style
        - This management style is usually most effective for **routine** work 
        - **Efficiency** is the central theme 
    - Relation style
        - This style is usually most effective in situations where people have to be **motivated**, **coordinated** and **trained** 
        - not of a routine 
        - innovative 
        - complex 
        - specialized 
        - weak spot is it may result in endless chaotic meeting 
        - This style best fits Mintzberg’s **mutual adjustment** coordination mechanism 
    - Commitment style 
        - This is most effective if work is done under **pressure** 
        - weak spot is once this vision has been agreed upon, the team is **not** responsive to **changes** in its environment, but blindly stumbles on along the road mapped out 
        - This style best fits Mintzberg’s **professional bureaucracy** 
    - Integration style 
        - This fits situations where the result is **uncertain** 
        - Decision-making is informal, bottom-up 
        - weak spot is goals disconnected 
        - Mintzberg’s coordination through **mutual adjustment** fits this situation well 
    - For an **experienced team** asked to develop a **well-specified application** in a familiar domain, coordination may be **achieved** through **standardization of work processes** 
    - For a **complex and innovative application**, this mechanism is **not** likely to **work** 

5. The relation and task maturity of individual team members 
    - Relation maturity concerns the **attitude** of employees towards their **job** and **management**
    - Task maturity is concerned with **technical** competence 
    - It is important that the manager **aligns** his dealings with team members with their respective **relation** and **task** maturity 

6. The team organizational form
    - Hierarchical Organization
        - dedicated to the **production** of software
        - **sub-team** 
        - **manager** 
        - the top and the bottom of the hierarchical **pyramid** 
            - The ‘real’ **work** is generally **done** at the **lower** levels of this pyramid 
            - The **decisions** are taken at a fairly **high** level 
            - The **higher** levels tends towards coordination through **standardization** 
        - The problematic aspect
            - The **judged** both socially and financially
        - reward them accordingly
    - Matrix Organization 
        - dedicated to software **development** 
        - The software is a mere **byproduct** 
        - **part-time** 
        - **multiple bosses** 
        - The problematic aspect 
            - difficult to **control progress** 
    - Chief Programmer Team 
        - consists of a **small group** 
        - no designers and programmers 
        - the code share in organization 
    - SWAT Team 
        - **Skilled With Advanced Tool** 
        - both **task** and **relation** directedness are **high** 
        - team is **small** 
        - builds **incremental** versions of a software system 
        - motivation 
            - catchy name, motto or logo 
    - Agile Team 
        - iterative development approaches 
        - collocated, short communication channels, a people-oriented attitude rather than a formalistic one 
        - small group 
        - In a planning-driven approach, the plan works like a life-jacket that people can fall back upon. In an agile team, **no** such **life-jacket** is available, and people must have swimming skills 
        - Level 3 fluent level 
        - Level 2 detaching level 
        - Level 1 following level 
    - Open Source Software Development 
        - **bazaar**: hordes of anarchist developers casually organized in a virtual networked organization 
        - **onion-like** structure 
            - The **Core Team** consists of a small team of **experienced developers** that also acts as management team 
            - The **Co-Developers** are a **larger group** of people **surrounding** the **Core Team** 
            - The **Active Users** are users of the most **recent release** of the system. They submit **bug reports** and feature requests, but do **not** themselves **program** the system 
            - Passive Users is merely using **stable releases** of the software. They do **not** interact with the **developer** 
            - **outer** layers contain **more** people than **inner** layers 
        - Developers participating in open source projects **rarely** do so **selflessly**. They expect something in return, such as the ability to **learn new things**, a **higher status** within their normal job, **attention** because they are part of a successful project, and the like 
        - The problematic aspect 
            - Communication 
            - the lack of formal documentation 
        - It is not always free 

7. General Principles for Organizing a Team 
    - Use **fewer**, and **better**, people 
        - small group of people 
        - large groups require more communication, which has a negative effect on productivity and leads to more error 
    - Try to **fit tasks to** the capabilities and motivation of the **people** available 
    - In the long run, an organization is better off if it **helps people to get the most out of themselves** 
    - Someone who does **not fit the team** should be **removed** 



        





















 












