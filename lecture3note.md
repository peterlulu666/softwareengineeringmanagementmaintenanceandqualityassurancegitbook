## Quiz        

1. These **document-driven** methods are also known as **planning-driven** or **heavyweight** methods
    - Planning-driven
        - the emphasis on an upfront plan for the whole process
    - Heavyweight
        - the emphasis placed on the process

2. Agile methods have **evolved** from approaches such as **prototyping** and **Rapid Application Development** that try to dispose of some or all of the drawbacks of the document-driven approach mentioned above
    - prototyping
        - emphasize customer collaboration and the role of people in the process
    - Rapid Application Development
        - emphasize customer collaboration and the role of people in the process
    - XP
        - ‘pure’ agile

3. Evolutionary models take into account that much of what is called **maintenance** is really **evolution**

4. In order to be able to properly assess costs and benefits, total **life cycle cost** rather than just **development cost** should be our primary focus

5. The waterfall model
    - Testing software is **not** an activity which strictly **follows** the **implementation** phase. 
    - In each phase of the software development process, we have to **compare** the results obtained against those that are **required**. 
    - In all phases, **quality** has to be **assessed** and **controlled**
    - Validation after each step
        - Validation asks if the system meets the **user’s** requirements
        - are we building the **right system**
    - Verification after each step
        - Verification asks if the system meets **its** requirements
        - are we building the **system right**
    - We therefore try to identify and tie down the **user’s** requirements as **early** as possible

6. The key values of the agile movement are
    - Individuals and interactions over processes and tools 
    - Working software over comprehensive documentation 
    - Customer collaboration over contract negotiation 
    - Responding to change over following a plan

7. Agile methods 
    - involve the **users** in **every** step taken 
    - The development **cycles** are **small** and **incremental** 
    - The series of development **cycles** is **not** extensively **planned** in advance, but the **new** situation is reviewed at the **end** of each **cycle** 
    - This includes some, but **not** too **much**, **planning** for the **next** cycle
    - do not have an extensive architectural or design phase up front
    - people-oriented, rather than process-oriented
    - Team spirit
    - Team close
    - short communication cycles
    - do not spend much energy on documentation

8. The refactoring
    - Agile methods often have a separate activity to improve the design after each increment, known as refactoring

9. Prototyping
    - One of the main **difficulties** for users is to **express** their **requirements** precisely
    - The throwaway prototyping
        - It is even feasible not to carry over the software product from the prototyping phases to the actual production phase, but to explicitly throw it away after the prototyping phases have come to an end
    - The evolutionary prototyping
        - The user starts to work with this system, which leads to new, or changed, requirements. The next version is then developed. After a number of such iterations, the user is satisfied and the last version developed is the product to be delivered
    - The **evolutionary** prototyping is used much **more** often than **throwaway** prototyping
    - In **throwaway** prototyping, the **quality** of the final design is often **higher**

10. Incremental Development
    - The functionality of the system is produced and **delivered** to the customer in small **increments**
    - Developing software this way avoids the ‘Big Bang’ effect
        - With this incremental approach, the user is closely involved in **planning** the **next** step
        - Redirecting the project becomes easier since we may **incorporate** **changed** circumstances more **quickly**
    - In each of these steps, the phased approach that we know from the **waterfall** model, is **employed**
    - Incremental development can also be used to fight the **overfunctionality**
        - Since users find it difficult to formulate their real needs, they tend to demand too much. Lacking the necessary knowledge of the malleability of software and its development process, they may be inclined to think that everything can be achieved

11. Rapid Application Development (RAD)
    - it employs the notion of a **time box**
        - a fixed time frame within which activities are done
    - the time frame is decided upon first and then the project tries to realize the requested functionality within that time frame
        - If it turns out that **not all** of the **functionality** can be **realized** within the time frame, some of the functionality is **sacrificed** 
        - The agreed **deadline** however is **immovable**
    - RAD life cycle
        - requirements planning 
        - user design 
        - construction
        - cutover
    - Joint Requirements Planning (JRP)
        - The goal of the JRP workshop is to get the requirements right the first time
        - requirements are prioritized
        - This requirement **prioritization** is known as **triage**
            - the **triage** process is used to make sure that the most important requirements are addressed first
                - The **Must have** category would include the ability to borrow and return an item, and to enroll as a member 
                - The **Should have** category might include facilities to make a reservation for an item 
                - The ability to handle fines for items returned late might be considered a **Could have** 
                - functions to profile users and notify them of newly arrived items might be classified as **Won’t haves**
    - Joint Application Design (JAD)
        - the end users play an essential role in these workshops
    - SWAT stands for Skilled With Advanced Tools
        - If necessary, some of the functionality is sacrificed instead
        - the SWAT team itself **estimates** the **time** 
        - the SWAT team **decides** upon the number and **length** of the **time boxes** 
        - the SWAT team **decides** which **functionality** to implement in each iteration

12. The DSDM
    - A well-known framework that **builds on RAD** is DSDM
    - DSDM stands for **Dynamic Systems Development Method**
    - DSDM is a **non-profit** framework
    - The DSDM process has five phases
        - feasibility study
            - identify the users of the system, the system should not be too large, and not all requirements are known upfront
        - business study
            - high-level architecture is determined
        - functional model iteration
            - Iteration is done in **timeboxes**
            - (1) identify what you will do, (2) agree on how you will do it, (3) do it, and (4) check that you have done it
        - design and build iteration
            - the distinction between those two types of iteration is not always clearcut
        - Implementation
            - customer environment
            - user training

13. Extreme Programming
    - whole team
    - XP for short, is a pure agile method
        - 2 weeks
    - pair programming  
        - code reading by your pals
        - two programmers work together
        - One of them does the coding
        - the other one looks over her shoulder, gives advice, notices small slips, asks questions, and the like
        - pilot and co-pilot
        - the roles may shift
    - test first
    - refactoring
        - improvement
        - After a task is accomplished, the system is checked to see how it can be improved
            - remove duplicate code, make it simpler, make it more flexible
    - Collective code ownership
        - the code belong to the developer and client
    - Continuous integration
        - After the code has been checked in, the full test suite is run, and again all tests have to run successfully. If not, the new code is removed again to fix it. This way, **there always is a running system**
        - add functionality incrementally
    - user story
    - sustainable pace
        - no overtime
        - not to do a lot of work at once

14. XP is based on five principles that drive its practices
    - Rapid feedback
        - obtained quickly
        - developers learn what works and what doesn’t
        - customer learns what value the system offers, and what next features are needed
    - Assume simplicity
        - Don’t build in extra complexity
    - Incremental change
        - change in small increments
        - changes a little
    - Embracing change
        - Only the most pressing problem is tackled today. The rest is left for tomorrow
    - Quality work
        - excellent quality

15. The Rational Unified Process
    - iterative development process
    - geared towards the development of object-oriented systems
    - It complements UML, the Unified Modeling Language, visual tool, visualization tools

16. RUP distinguishes four phase
    - inception phase
        - objectives
        - cost 
        - schedule 
        - risk 
        - candidate architecture 
        - business case 
    - elaboration phase
        - analyzing the problem 
        - identify use cases 
        - risk 
    - construction phase
        - building
        - manuals
        - the beta release ready 
    - transition phase
        - the system is released

17. Maintenance or Evolution
    - Maintenance
        - adjust
    - Evolution
        - add feature
    - laws of software evolution
        - Law of continuing change
            - cost-effective new version
        - Law of increasing complexity
            - avoid increasing complexity
        - Law of self regulation
            - growth
        - Law of conservation of organisational stability (invariant work rate)
        - Law of conservation of familiarity
            - quality and usage
        - Law of continuing growth
            - functionality increase in order to maintain user satisfaction
        - Law of incremental growth limit
        - Law of declining quality
        - Law of system feedback

18. The domain engineering and application engineering
    - The domain engineering 
        - domain 
        - product family 
        - reusable 
    - The application engineering
        - individual product 
        - a single version of a product 
        - waterfall-like 

19. Software product lines are particularly suitable in domains where there is a lot of variation in quite similar products, such as mobile phones, television sets, cameras

20. The process modeling
    - The process program for the review process
    - The state transition diagram of the review process
    - The petri net view of the review process
    - The purposes
        - It facilitates understanding and communication
        - It supports process management and improvement
        - It may serve as a basis for automated support












































